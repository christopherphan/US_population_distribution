// SPDX-License-Identifier: Apache-2.0

/*
 * main.js
 *
 * This file is part of US population distribution treemap
 * https://codeberg.org/christopherphan/US_population_distribution
 *
 * Christopher Phan
 * https://chrisphan.com/
 * 2023-W26
 *
 ***********************************************
 * The following applies to THIS FILE ONLY:
 *
 * Copyright 2023 Christopher Phan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
function addAllHandlers () {
  const treeMap = document.getElementById('main_svg').contentDocument
  Object.keys(data).forEach(k => addHandler(k, treeMap))
  addSvgHandler()

  const dataSourceButton = document.getElementById('data_source_button')
  empowerButton(dataSourceButton, event => toggleDiv('data_source_info'))
  const keyButton = document.getElementById('key_button')
  empowerButton(keyButton, event => {
    toggleDiv('key_info')
  })
  const dataCloseButton = document.getElementById('data_close_span')
  empowerButton(dataCloseButton, event => toggleDiv('data_source_info'))
  const keyCloseButton = document.getElementById('key_close_span')
  keyEmpowerButton(keyCloseButton, event => {
    toggleDiv('key_info')
  })
}

function addSvgHandler () {
  const mainSvg = document.getElementById('main_svg')
  mainSvg.addEventListener(
    'mouseover',
    (event) => clearInfoBox()
  )
  mainSvg.addEventListener(
    'mouseleave',
    (event) => {
      explainInfoBox()
    }
  )
}

function toggleDiv (eltID) {
  const elt = document.getElementById(eltID)
  if (elt.getAttribute('class') === 'hideable_hidden') {
    elt.setAttribute('class', 'hideable_visible')
  } else {
    elt.setAttribute('class', 'hideable_hidden')
  }
}

function countyMouseover (rect, countyData) {
  const currentClass = rect.getAttribute('class')
  rect.setAttribute('class', currentClass + ' highlight')
  displayInfo(countyData)
}

function countyMouseleave (rect) {
  let styleClass = rect.getAttribute('class')
  styleClass = styleClass.replace('highlight', '').trim()
  rect.setAttribute('class', styleClass)
  clearInfoBox()
}

function empowerButton (elt, clickAction) {
  elt.addEventListener('click', clickAction)
  elt.addEventListener('mouseover', event => buttonHighlight(elt))
  elt.addEventListener('mouseleave', event => buttonUnhighlight(elt))
}

function keyEmpowerButton (elt, clickAction) {
  elt.addEventListener('click', clickAction)
  elt.addEventListener('mouseover', event => keyButtonHighlight(elt))
  elt.addEventListener('mouseleave', event => keyButtonUnhighlight(elt))
}

function buttonHighlight (elt) {
  elt.setAttribute('class', 'button-active')
}

function buttonUnhighlight (elt) {
  elt.setAttribute('class', 'button')
}

function keyButtonHighlight (elt) {
  elt.setAttribute('class', 'key_close_button-active')
}

function keyButtonUnhighlight (elt) {
  elt.setAttribute('class', 'key_close_button')
}
function explainInfoBox () {
  clearInfoBox()

  const infoBox = document.getElementById('info_box')

  const newPara = document.createElement('p')
  const newText = document.createTextNode(
    'Each rectangle represents a county or county equivalent, and its area is ' +
    'proportional to its population. Mouse over the tree map for information about' +
    ' each county.'
  )
  newPara.append(newText)
  infoBox.append(newPara)
}

function clearInfoBox () {
  const infoBox = document.getElementById('info_box')

  while (infoBox.firstChild) {
    infoBox.removeChild(infoBox.firstChild)
  }
}

function displayInfo (countyData) {
  clearInfoBox()

  const infoBox = document.getElementById('info_box')

  // add text to info box
  countyData.forEach(item => {
    const newPara = document.createElement('p')
    const newText = document.createTextNode(
      `${item.name}, population: ${item.population.toLocaleString()}`
    )
    newPara.append(newText)
    infoBox.append(newPara)
  })
}

function addHandler (k, treeMap) {
  const currentRect = treeMap.getElementById(`rect_${k}`)
  currentRect.addEventListener(
    'mouseover',
    (event) => countyMouseover(event.target, data[k])
  )
  currentRect.addEventListener(
    'mouseleave',
    (event) => countyMouseleave(event.target)
  )
}

window.onload = function () {
  console.log('loaded!')
  explainInfoBox()
  addAllHandlers()
}
