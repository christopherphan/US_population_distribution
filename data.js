// SPDX-License-Identifier: CC0-1.0

/* data.js
 *
 * This file is part of US population distribution treemap
 * https://codeberg.org/christopherphan/US_population_distribution
 *
 * Christopher Phan
 * https://chrisphan.com/
 * 2023-W26
 *
 *
 * This file was created from U.S. Census data:
 *
 * - Annual Estimates of the Resident Population for Counties in the United States:
 *   April 1, 2020 to July 1, 2022 (CO-EST2022-POP)
 *
 *   Source: U.S. Census Bureau, Population Division
 *
 *   Release Date: March 2023
 *
 *   https://www.census.gov/data/datasets/time-series/demo/popest/2020s-counties-total.html
 *
 * - Statistical area data file
 *
 *   Note: The 2010 OMB Standards for Delineating Metropolitan and Micropolitan
 *   Statistical Areas are at
 *   https://www.gpo.gov/fdsys/pkg/FR-2010-06-28/pdf/2010-15605.pdf and
 *   https://www.gpo.gov/fdsys/pkg/FR-2010-07-07/pdf/2010-16368.pdf.
 *
 *   Source:
 *   File prepared by U.S. Census Bureau, Population Division, based on Office
 *   of Management and Budget, March 2020 delineations
 *   https://www.whitehouse.gov/wp-content/uploads/2020/03/Bulletin-20-01.pdf.
 *   Internet Release Date: April 2020
 *
 *   https://www2.census.gov/programs-surveys/metro-micro/geographies/reference-files/2020/delineation-files/list1_2020.xls
 *
 * - Additional info from:
 *
 *   https://www2.census.gov/programs-surveys/popest/datasets/2020-2022/metro/totals/cbsa-est2022.csv
 *
 ****************************************************************************************
 * THIS FILE ONLY released under: https://creativecommons.org/publicdomain/zero/1.0/
 *
 * To the extent possible under law, Christopher Phan has waived all copyright
 * and related or neighboring rights to data.js. This work is published from:
 * United States.
 * ***************************************************************************
 * THE FOLLOWING APPLIES TO THIS FILE ONLY:
 *
 * Creative Commons Legal Code
 *
 *  CC0 1.0 Universal
 *
 *  CREATIVE COMMONS CORPORATION IS NOT A LAW FIRM AND DOES NOT PROVIDE LEGAL
 *  SERVICES. DISTRIBUTION OF THIS DOCUMENT DOES NOT CREATE AN ATTORNEY-CLIENT
 *  RELATIONSHIP. CREATIVE COMMONS PROVIDES THIS INFORMATION ON AN "AS-IS"
 *  BASIS. CREATIVE COMMONS MAKES NO WARRANTIES REGARDING THE USE OF THIS
 *  DOCUMENT OR THE INFORMATION OR WORKS PROVIDED HEREUNDER, AND DISCLAIMS
 *  LIABILITY FOR DAMAGES RESULTING FROM THE USE OF THIS DOCUMENT OR THE
 *  INFORMATION OR WORKS PROVIDED HEREUNDER.
 *
 *  Statement of Purpose
 *
 *  The laws of most jurisdictions throughout the world automatically confer
 *  exclusive Copyright and Related Rights (defined below) upon the creator and
 *  subsequent owner(s) (each and all, an "owner") of an original work of
 *  authorship and/or a database (each, a "Work").
 *
 *  Certain owners wish to permanently relinquish those rights to a Work for
 *  the purpose of contributing to a commons of creative, cultural and
 *  scientific works ("Commons") that the public can reliably and without fear
 *  of later claims of infringement build upon, modify, incorporate in other
 *  works, reuse and redistribute as freely as possible in any form whatsoever
 *  and for any purposes, including without limitation commercial purposes.
 *  These owners may contribute to the Commons to promote the ideal of a free
 *  culture and the further production of creative, cultural and scientific
 *  works, or to gain reputation or greater distribution for their Work in part
 *  through the use and efforts of others.
 *
 *  For these and/or other purposes and motivations, and without any
 *  expectation of additional consideration or compensation, the person
 *  associating CC0 with a Work (the "Affirmer"), to the extent that he or she
 *  is an owner of Copyright and Related Rights in the Work, voluntarily elects
 *  to apply CC0 to the Work and publicly distribute the Work under its terms,
 *  with knowledge of his or her Copyright and Related Rights in the Work and
 *  the meaning and intended legal effect of CC0 on those rights.
 *
 *  1. Copyright and Related Rights. A Work made available under CC0 may be
 *  protected by copyright and related or neighboring rights ("Copyright and
 *  Related Rights"). Copyright and Related Rights include, but are not
 *  limited to, the following:
 *
 *    i. the right to reproduce, adapt, distribute, perform, display,
 *       communicate, and translate a Work;
 *
 *    ii. moral rights retained by the original author(s) and/or performer(s);
 *
 *    iii. publicity and privacy rights pertaining to a person's image or
 *         likeness depicted in a Work;
 *
 *    iv. rights protecting against unfair competition in regards to a Work,
 *        subject to the limitations in paragraph 4(a), below;
 *
 *    v. rights protecting the extraction, dissemination, use and reuse of data
 *       in a Work;
 *
 *    vi. database rights (such as those arising under Directive 96/9/EC of the
 *        European Parliament and of the Council of 11 March 1996 on the legal
 *        protection of databases, and under any national implementation thereof,
 *        including any amended or successor version of such directive); and
 *
 *    vii.  other similar, equivalent or corresponding rights throughout the
 *          world based on applicable law or treaty, and any national implementations
 *          thereof.
 *
 *  2. Waiver. To the greatest extent permitted by, but not in contravention
 *  of, applicable law, Affirmer hereby overtly, fully, permanently,
 *  irrevocably and unconditionally waives, abandons, and surrenders all of
 *  Affirmer's Copyright and Related Rights and associated claims and causes of
 *  action, whether now known or unknown (including existing as well as future
 *  claims and causes of action), in the Work (i) in all territories
 *  worldwide, (ii) for the maximum duration provided by applicable law or
 *  treaty (including future time extensions), (iii) in any current or future
 *  medium and for any number of copies, and (iv) for any purpose whatsoever,
 *  including without limitation commercial, advertising or promotional
 *  purposes (the "Waiver"). Affirmer makes the Waiver for the benefit of each
 *  member of the public at large and to the detriment of Affirmer's heirs and
 *  successors, fully intending that such Waiver shall not be subject to
 *  revocation, rescission, cancellation, termination, or any other legal or
 *  equitable action to disrupt the quiet enjoyment of the Work by the public
 *  as contemplated by Affirmer's express Statement of Purpose.
 *
 *  3. Public License Fallback. Should any part of the Waiver for any reason be
 *  judged legally invalid or ineffective under applicable law, then the Waiver
 *  shall be preserved to the maximum extent permitted taking into account
 *  Affirmer's express Statement of Purpose. In addition, to the extent the
 *  Waiver is so judged Affirmer hereby grants to each affected person a
 *  royalty-free, non transferable, non sublicensable, non exclusive,
 *  irrevocable and unconditional license to exercise Affirmer's Copyright and
 *  Related Rights in the Work (i) in all territories worldwide, (ii) for the
 *  maximum duration provided by applicable law or treaty (including future
 *  time extensions), (iii) in any current or future medium and for any
 *  number of copies, and (iv) for any purpose whatsoever, including without
 *  limitation commercial, advertising or promotional purposes (the "License").
 *  The License shall be deemed effective as of the date CC0 was applied by
 *  Affirmer to the Work. Should any part of the License for any reason be
 *  judged legally invalid or ineffective under applicable law, such partial
 *  invalidity or ineffectiveness shall not invalidate the remainder of the
 *  License, and in such case Affirmer hereby affirms that he or she will not
 *  (i) exercise any of his or her remaining Copyright and Related Rights in
 *  the Work or (ii) assert any associated claims and causes of action with
 *  respect to the Work, in either case contrary to Affirmer's express
 *  Statement of Purpose.
 *
 *  4. Limitations and Disclaimers.
 *
 *  a. No trademark or patent rights held by Affirmer are waived, abandoned,
 *     surrendered, licensed or otherwise affected by this document.
 *
 *  b. Affirmer offers the Work as-is and makes no representations or
 *     warranties of any kind concerning the Work, express, implied, statutory or
 *     otherwise, including without limitation warranties of title,
 *     merchantability, fitness for a particular purpose, non infringement, or the
 *     absence of latent or other defects, accuracy, or the present or absence of
 *     errors, whether or not discoverable, all to the greatest extent permissible
 *     under applicable law.
 *
 *  c. Affirmer disclaims responsibility for clearing rights of other persons
 *     that may apply to the Work or any use thereof, including without limitation
 *     any person's Copyright and Related Rights in the Work. Further, Affirmer
 *     disclaims responsibility for obtaining any necessary consents, permissions
 *     or other rights required for any use of the Work.
 *
 *  d. Affirmer understands and acknowledges that Creative Commons is not a
 *     party to this document and has no duty or obligation with respect to this
 *     CC0 or use of the Work.
 ******************************************************************
 */
const data = {
  3: [
    {
      index: 1,
      name: 'Montgomery-Selma-Alexander City, AL CSA',
      population: 473370,
      area_type: 'CSA',
    },
    {
      index: 2,
      name: 'Montgomery, AL MSA',
      population: 385460,
      area_type: 'MSA',
    },
    {
      index: 3,
      name: 'Autauga County, Alabama',
      population: 59759,
      area_type: 'County',
    },
  ],
  5: [
    {
      index: 4,
      name: 'Mobile-Daphne-Fairhope, AL CSA',
      population: 672968,
      area_type: 'CSA',
    },
    {
      index: 5,
      name: 'Baldwin County, Alabama (Daphne-Fairhope-Foley, AL MSA)',
      population: 246435,
      area_type: 'MSA',
    },
  ],
  8: [
    {
      index: 7,
      name: 'Eufaula, AL-GA μSA',
      population: 26955,
      area_type: 'μSA',
    },
    {
      index: 8,
      name: 'Barbour County, Alabama',
      population: 24706,
      area_type: 'County',
    },
  ],
  11: [
    {
      index: 9,
      name: 'Birmingham-Hoover-Talladega, AL CSA',
      population: 1352565,
      area_type: 'CSA',
    },
    {
      index: 10,
      name: 'Birmingham-Hoover, AL MSA',
      population: 1116857,
      area_type: 'MSA',
    },
    {
      index: 11,
      name: 'Bibb County, Alabama',
      population: 22005,
      area_type: 'County',
    },
  ],
  12: [
    {
      index: 9,
      name: 'Birmingham-Hoover-Talladega, AL CSA',
      population: 1352565,
      area_type: 'CSA',
    },
    {
      index: 10,
      name: 'Birmingham-Hoover, AL MSA',
      population: 1116857,
      area_type: 'MSA',
    },
    {
      index: 12,
      name: 'Blount County, Alabama',
      population: 59512,
      area_type: 'County',
    },
  ],
  13: [
    {
      index: 13,
      name: 'Calhoun County, Alabama (Anniston-Oxford, AL MSA)',
      population: 115788,
      area_type: 'MSA',
    },
  ],
  17: [
    {
      index: 15,
      name: 'Atlanta--Athens-Clarke County--Sandy Springs, GA-AL CSA',
      population: 7088898,
      area_type: 'CSA',
    },
    {
      index: 16,
      name: 'LaGrange, GA-AL μSA',
      population: 104279,
      area_type: 'μSA',
    },
    {
      index: 17,
      name: 'Chambers County, Alabama',
      population: 34088,
      area_type: 'County',
    },
  ],
  18: [
    {
      index: 9,
      name: 'Birmingham-Hoover-Talladega, AL CSA',
      population: 1352565,
      area_type: 'CSA',
    },
    {
      index: 10,
      name: 'Birmingham-Hoover, AL MSA',
      population: 1116857,
      area_type: 'MSA',
    },
    {
      index: 18,
      name: 'Chilton County, Alabama',
      population: 45884,
      area_type: 'County',
    },
  ],
  19: [
    {
      index: 19,
      name: 'Coffee County, Alabama (Enterprise, AL μSA)',
      population: 54805,
      area_type: 'μSA',
    },
  ],
  22: [
    {
      index: 21,
      name: 'Florence-Muscle Shoals, AL MSA',
      population: 153911,
      area_type: 'MSA',
    },
    {
      index: 22,
      name: 'Colbert County, Alabama',
      population: 58033,
      area_type: 'County',
    },
  ],
  24: [
    {
      index: 1,
      name: 'Montgomery-Selma-Alexander City, AL CSA',
      population: 473370,
      area_type: 'CSA',
    },
    {
      index: 23,
      name: 'Alexander City, AL μSA',
      population: 51143,
      area_type: 'μSA',
    },
    {
      index: 24,
      name: 'Coosa County, Alabama',
      population: 10166,
      area_type: 'County',
    },
  ],
  25: [
    {
      index: 9,
      name: 'Birmingham-Hoover-Talladega, AL CSA',
      population: 1352565,
      area_type: 'CSA',
    },
    {
      index: 25,
      name: 'Cullman County, Alabama (Cullman, AL μSA)',
      population: 90665,
      area_type: 'μSA',
    },
  ],
  28: [
    {
      index: 27,
      name: 'Dothan-Ozark, AL CSA',
      population: 202061,
      area_type: 'CSA',
    },
    {
      index: 28,
      name: 'Dale County, Alabama (Ozark, AL μSA)',
      population: 49544,
      area_type: 'μSA',
    },
  ],
  30: [
    {
      index: 1,
      name: 'Montgomery-Selma-Alexander City, AL CSA',
      population: 473370,
      area_type: 'CSA',
    },
    {
      index: 30,
      name: 'Dallas County, Alabama (Selma, AL μSA)',
      population: 36767,
      area_type: 'μSA',
    },
  ],
  33: [
    {
      index: 32,
      name: 'Scottsboro-Fort Payne, AL CSA',
      population: 124889,
      area_type: 'CSA',
    },
    {
      index: 33,
      name: 'DeKalb County, Alabama (Fort Payne, AL μSA)',
      population: 71998,
      area_type: 'μSA',
    },
  ],
  35: [
    {
      index: 1,
      name: 'Montgomery-Selma-Alexander City, AL CSA',
      population: 473370,
      area_type: 'CSA',
    },
    {
      index: 2,
      name: 'Montgomery, AL MSA',
      population: 385460,
      area_type: 'MSA',
    },
    {
      index: 35,
      name: 'Elmore County, Alabama',
      population: 89563,
      area_type: 'County',
    },
  ],
  37: [
    {
      index: 36,
      name: 'Pensacola-Ferry Pass, FL-AL CSA',
      population: 559812,
      area_type: 'CSA',
    },
    {
      index: 37,
      name: 'Escambia County, Alabama (Atmore, AL μSA)',
      population: 36666,
      area_type: 'μSA',
    },
  ],
  39: [
    {
      index: 39,
      name: 'Etowah County, Alabama (Gadsden, AL MSA)',
      population: 103088,
      area_type: 'MSA',
    },
  ],
  42: [
    {
      index: 27,
      name: 'Dothan-Ozark, AL CSA',
      population: 202061,
      area_type: 'CSA',
    },
    {
      index: 41,
      name: 'Dothan, AL MSA',
      population: 152517,
      area_type: 'MSA',
    },
    {
      index: 42,
      name: 'Geneva County, Alabama',
      population: 26783,
      area_type: 'County',
    },
  ],
  44: [
    {
      index: 43,
      name: 'Tuscaloosa, AL MSA',
      population: 277494,
      area_type: 'MSA',
    },
    {
      index: 44,
      name: 'Greene County, Alabama',
      population: 7422,
      area_type: 'County',
    },
  ],
  45: [
    {
      index: 43,
      name: 'Tuscaloosa, AL MSA',
      population: 277494,
      area_type: 'MSA',
    },
    {
      index: 45,
      name: 'Hale County, Alabama',
      population: 14595,
      area_type: 'County',
    },
  ],
  46: [
    {
      index: 27,
      name: 'Dothan-Ozark, AL CSA',
      population: 202061,
      area_type: 'CSA',
    },
    {
      index: 41,
      name: 'Dothan, AL MSA',
      population: 152517,
      area_type: 'MSA',
    },
    {
      index: 46,
      name: 'Henry County, Alabama',
      population: 17655,
      area_type: 'County',
    },
  ],
  47: [
    {
      index: 27,
      name: 'Dothan-Ozark, AL CSA',
      population: 202061,
      area_type: 'CSA',
    },
    {
      index: 41,
      name: 'Dothan, AL MSA',
      population: 152517,
      area_type: 'MSA',
    },
    {
      index: 47,
      name: 'Houston County, Alabama',
      population: 108079,
      area_type: 'County',
    },
  ],
  48: [
    {
      index: 32,
      name: 'Scottsboro-Fort Payne, AL CSA',
      population: 124889,
      area_type: 'CSA',
    },
    {
      index: 48,
      name: 'Jackson County, Alabama (Scottsboro, AL μSA)',
      population: 52891,
      area_type: 'μSA',
    },
  ],
  50: [
    {
      index: 9,
      name: 'Birmingham-Hoover-Talladega, AL CSA',
      population: 1352565,
      area_type: 'CSA',
    },
    {
      index: 10,
      name: 'Birmingham-Hoover, AL MSA',
      population: 1116857,
      area_type: 'MSA',
    },
    {
      index: 50,
      name: 'Jefferson County, Alabama',
      population: 665409,
      area_type: 'County',
    },
  ],
  51: [
    {
      index: 21,
      name: 'Florence-Muscle Shoals, AL MSA',
      population: 153911,
      area_type: 'MSA',
    },
    {
      index: 51,
      name: 'Lauderdale County, Alabama',
      population: 95878,
      area_type: 'County',
    },
  ],
  54: [
    {
      index: 52,
      name: 'Huntsville-Decatur, AL CSA',
      population: 671890,
      area_type: 'CSA',
    },
    {
      index: 53,
      name: 'Decatur, AL MSA',
      population: 157425,
      area_type: 'MSA',
    },
    {
      index: 54,
      name: 'Lawrence County, Alabama',
      population: 33214,
      area_type: 'County',
    },
  ],
  56: [
    {
      index: 55,
      name: 'Columbus-Auburn-Opelika, GA-AL CSA',
      population: 504883,
      area_type: 'CSA',
    },
    {
      index: 56,
      name: 'Lee County, Alabama (Auburn-Opelika, AL MSA)',
      population: 180773,
      area_type: 'MSA',
    },
  ],
  59: [
    {
      index: 52,
      name: 'Huntsville-Decatur, AL CSA',
      population: 671890,
      area_type: 'CSA',
    },
    {
      index: 58,
      name: 'Huntsville, AL MSA',
      population: 514465,
      area_type: 'MSA',
    },
    {
      index: 59,
      name: 'Limestone County, Alabama',
      population: 110900,
      area_type: 'County',
    },
  ],
  60: [
    {
      index: 1,
      name: 'Montgomery-Selma-Alexander City, AL CSA',
      population: 473370,
      area_type: 'CSA',
    },
    {
      index: 2,
      name: 'Montgomery, AL MSA',
      population: 385460,
      area_type: 'MSA',
    },
    {
      index: 60,
      name: 'Lowndes County, Alabama',
      population: 9777,
      area_type: 'County',
    },
  ],
  61: [
    {
      index: 52,
      name: 'Huntsville-Decatur, AL CSA',
      population: 671890,
      area_type: 'CSA',
    },
    {
      index: 58,
      name: 'Huntsville, AL MSA',
      population: 514465,
      area_type: 'MSA',
    },
    {
      index: 61,
      name: 'Madison County, Alabama',
      population: 403565,
      area_type: 'County',
    },
  ],
  62: [
    {
      index: 62,
      name: 'Marshall County, Alabama (Albertville, AL μSA)',
      population: 99423,
      area_type: 'μSA',
    },
  ],
  65: [
    {
      index: 4,
      name: 'Mobile-Daphne-Fairhope, AL CSA',
      population: 672968,
      area_type: 'CSA',
    },
    {
      index: 64,
      name: 'Mobile, AL MSA',
      population: 426533,
      area_type: 'MSA',
    },
    {
      index: 65,
      name: 'Mobile County, Alabama',
      population: 411411,
      area_type: 'County',
    },
  ],
  66: [
    {
      index: 1,
      name: 'Montgomery-Selma-Alexander City, AL CSA',
      population: 473370,
      area_type: 'CSA',
    },
    {
      index: 2,
      name: 'Montgomery, AL MSA',
      population: 385460,
      area_type: 'MSA',
    },
    {
      index: 66,
      name: 'Montgomery County, Alabama',
      population: 226361,
      area_type: 'County',
    },
  ],
  67: [
    {
      index: 52,
      name: 'Huntsville-Decatur, AL CSA',
      population: 671890,
      area_type: 'CSA',
    },
    {
      index: 53,
      name: 'Decatur, AL MSA',
      population: 157425,
      area_type: 'MSA',
    },
    {
      index: 67,
      name: 'Morgan County, Alabama',
      population: 124211,
      area_type: 'County',
    },
  ],
  68: [
    {
      index: 43,
      name: 'Tuscaloosa, AL MSA',
      population: 277494,
      area_type: 'MSA',
    },
    {
      index: 68,
      name: 'Pickens County, Alabama',
      population: 18697,
      area_type: 'County',
    },
  ],
  69: [
    {
      index: 69,
      name: 'Pike County, Alabama (Troy, AL μSA)',
      population: 33014,
      area_type: 'μSA',
    },
  ],
  72: [
    {
      index: 55,
      name: 'Columbus-Auburn-Opelika, GA-AL CSA',
      population: 504883,
      area_type: 'CSA',
    },
    {
      index: 71,
      name: 'Columbus, GA-AL MSA',
      population: 324110,
      area_type: 'MSA',
    },
    {
      index: 72,
      name: 'Russell County, Alabama',
      population: 58555,
      area_type: 'County',
    },
  ],
  73: [
    {
      index: 9,
      name: 'Birmingham-Hoover-Talladega, AL CSA',
      population: 1352565,
      area_type: 'CSA',
    },
    {
      index: 10,
      name: 'Birmingham-Hoover, AL MSA',
      population: 1116857,
      area_type: 'MSA',
    },
    {
      index: 73,
      name: 'St. Clair County, Alabama',
      population: 93932,
      area_type: 'County',
    },
  ],
  74: [
    {
      index: 9,
      name: 'Birmingham-Hoover-Talladega, AL CSA',
      population: 1352565,
      area_type: 'CSA',
    },
    {
      index: 10,
      name: 'Birmingham-Hoover, AL MSA',
      population: 1116857,
      area_type: 'MSA',
    },
    {
      index: 74,
      name: 'Shelby County, Alabama',
      population: 230115,
      area_type: 'County',
    },
  ],
  75: [
    {
      index: 9,
      name: 'Birmingham-Hoover-Talladega, AL CSA',
      population: 1352565,
      area_type: 'CSA',
    },
    {
      index: 75,
      name: 'Talladega County, Alabama (Talladega-Sylacauga, AL μSA)',
      population: 80704,
      area_type: 'μSA',
    },
  ],
  77: [
    {
      index: 1,
      name: 'Montgomery-Selma-Alexander City, AL CSA',
      population: 473370,
      area_type: 'CSA',
    },
    {
      index: 23,
      name: 'Alexander City, AL μSA',
      population: 51143,
      area_type: 'μSA',
    },
    {
      index: 77,
      name: 'Tallapoosa County, Alabama',
      population: 40977,
      area_type: 'County',
    },
  ],
  78: [
    {
      index: 43,
      name: 'Tuscaloosa, AL MSA',
      population: 277494,
      area_type: 'MSA',
    },
    {
      index: 78,
      name: 'Tuscaloosa County, Alabama',
      population: 236780,
      area_type: 'County',
    },
  ],
  79: [
    {
      index: 9,
      name: 'Birmingham-Hoover-Talladega, AL CSA',
      population: 1352565,
      area_type: 'CSA',
    },
    {
      index: 79,
      name: 'Walker County, Alabama (Jasper, AL μSA)',
      population: 64339,
      area_type: 'μSA',
    },
  ],
  81: [
    {
      index: 4,
      name: 'Mobile-Daphne-Fairhope, AL CSA',
      population: 672968,
      area_type: 'CSA',
    },
    {
      index: 64,
      name: 'Mobile, AL MSA',
      population: 426533,
      area_type: 'MSA',
    },
    {
      index: 81,
      name: 'Washington County, Alabama',
      population: 15122,
      area_type: 'County',
    },
  ],
  83: [
    {
      index: 82,
      name: 'Anchorage, AK MSA',
      population: 400470,
      area_type: 'MSA',
    },
    {
      index: 83,
      name: 'Anchorage Municipality, Alaska',
      population: 287145,
      area_type: 'County',
    },
  ],
  84: [
    {
      index: 84,
      name: 'Fairbanks North Star Borough, Alaska (Fairbanks, AK MSA)',
      population: 95356,
      area_type: 'MSA',
    },
  ],
  86: [
    {
      index: 86,
      name: 'Juneau City and Borough, Alaska (Juneau, AK μSA)',
      population: 31685,
      area_type: 'μSA',
    },
  ],
  88: [
    {
      index: 88,
      name: 'Ketchikan Gateway Borough, Alaska (Ketchikan, AK μSA)',
      population: 13741,
      area_type: 'μSA',
    },
  ],
  90: [
    {
      index: 82,
      name: 'Anchorage, AK MSA',
      population: 400470,
      area_type: 'MSA',
    },
    {
      index: 90,
      name: 'Matanuska-Susitna Borough, Alaska',
      population: 113325,
      area_type: 'County',
    },
  ],
  91: [
    {
      index: 91,
      name: 'Cochise County, Arizona (Sierra Vista-Douglas, AZ MSA)',
      population: 125663,
      area_type: 'MSA',
    },
  ],
  93: [
    {
      index: 93,
      name: 'Coconino County, Arizona (Flagstaff, AZ MSA)',
      population: 144060,
      area_type: 'MSA',
    },
  ],
  96: [
    {
      index: 95,
      name: 'Phoenix-Mesa, AZ CSA',
      population: 5069600,
      area_type: 'CSA',
    },
    {
      index: 96,
      name: 'Gila County, Arizona (Payson, AZ μSA)',
      population: 53922,
      area_type: 'μSA',
    },
  ],
  98: [
    {
      index: 98,
      name: 'Graham County, Arizona (Safford, AZ μSA)',
      population: 38779,
      area_type: 'μSA',
    },
  ],
  101: [
    {
      index: 95,
      name: 'Phoenix-Mesa, AZ CSA',
      population: 5069600,
      area_type: 'CSA',
    },
    {
      index: 100,
      name: 'Phoenix-Mesa-Chandler, AZ MSA',
      population: 5015678,
      area_type: 'MSA',
    },
    {
      index: 101,
      name: 'Maricopa County, Arizona',
      population: 4551524,
      area_type: 'County',
    },
  ],
  102: [
    {
      index: 102,
      name: 'Mohave County, Arizona (Lake Havasu City-Kingman, AZ MSA)',
      population: 220816,
      area_type: 'MSA',
    },
  ],
  104: [
    {
      index: 104,
      name: 'Navajo County, Arizona (Show Low, AZ μSA)',
      population: 108650,
      area_type: 'μSA',
    },
  ],
  107: [
    {
      index: 106,
      name: 'Tucson-Nogales, AZ CSA',
      population: 1106356,
      area_type: 'CSA',
    },
    {
      index: 107,
      name: 'Pima County, Arizona (Tucson, AZ MSA)',
      population: 1057597,
      area_type: 'MSA',
    },
  ],
  109: [
    {
      index: 95,
      name: 'Phoenix-Mesa, AZ CSA',
      population: 5069600,
      area_type: 'CSA',
    },
    {
      index: 100,
      name: 'Phoenix-Mesa-Chandler, AZ MSA',
      population: 5015678,
      area_type: 'MSA',
    },
    {
      index: 109,
      name: 'Pinal County, Arizona',
      population: 464154,
      area_type: 'County',
    },
  ],
  110: [
    {
      index: 106,
      name: 'Tucson-Nogales, AZ CSA',
      population: 1106356,
      area_type: 'CSA',
    },
    {
      index: 110,
      name: 'Santa Cruz County, Arizona (Nogales, AZ μSA)',
      population: 48759,
      area_type: 'μSA',
    },
  ],
  112: [
    {
      index: 112,
      name: 'Yavapai County, Arizona (Prescott Valley-Prescott, AZ MSA)',
      population: 246191,
      area_type: 'MSA',
    },
  ],
  114: [
    {
      index: 114,
      name: 'Yuma County, Arizona (Yuma, AZ MSA)',
      population: 207842,
      area_type: 'MSA',
    },
  ],
  116: [
    {
      index: 116,
      name: 'Baxter County, Arkansas (Mountain Home, AR μSA)',
      population: 42435,
      area_type: 'μSA',
    },
  ],
  119: [
    {
      index: 118,
      name: 'Fayetteville-Springdale-Rogers, AR MSA',
      population: 576403,
      area_type: 'MSA',
    },
    {
      index: 119,
      name: 'Benton County, Arkansas',
      population: 302863,
      area_type: 'County',
    },
  ],
  121: [
    {
      index: 120,
      name: 'Harrison, AR μSA',
      population: 45362,
      area_type: 'μSA',
    },
    {
      index: 121,
      name: 'Boone County, Arkansas',
      population: 38284,
      area_type: 'County',
    },
  ],
  123: [
    {
      index: 122,
      name: 'Camden, AR μSA',
      population: 26744,
      area_type: 'μSA',
    },
    {
      index: 123,
      name: 'Calhoun County, Arkansas',
      population: 4695,
      area_type: 'County',
    },
  ],
  124: [
    {
      index: 124,
      name: 'Clark County, Arkansas (Arkadelphia, AR μSA)',
      population: 21250,
      area_type: 'μSA',
    },
  ],
  128: [
    {
      index: 126,
      name: 'Little Rock-North Little Rock, AR CSA',
      population: 919999,
      area_type: 'CSA',
    },
    {
      index: 127,
      name: 'Pine Bluff, AR MSA',
      population: 84629,
      area_type: 'MSA',
    },
    {
      index: 128,
      name: 'Cleveland County, Arkansas',
      population: 7467,
      area_type: 'County',
    },
  ],
  129: [
    {
      index: 129,
      name: 'Columbia County, Arkansas (Magnolia, AR μSA)',
      population: 22216,
      area_type: 'μSA',
    },
  ],
  133: [
    {
      index: 131,
      name: 'Jonesboro-Paragould, AR CSA',
      population: 181960,
      area_type: 'CSA',
    },
    {
      index: 132,
      name: 'Jonesboro, AR MSA',
      population: 135512,
      area_type: 'MSA',
    },
    {
      index: 133,
      name: 'Craighead County, Arkansas',
      population: 113017,
      area_type: 'County',
    },
  ],
  135: [
    {
      index: 134,
      name: 'Fort Smith, AR-OK MSA',
      population: 247072,
      area_type: 'MSA',
    },
    {
      index: 135,
      name: 'Crawford County, Arkansas',
      population: 61075,
      area_type: 'County',
    },
  ],
  138: [
    {
      index: 136,
      name: 'Memphis-Forrest City, TN-MS-AR CSA',
      population: 1354756,
      area_type: 'CSA',
    },
    {
      index: 137,
      name: 'Memphis, TN-MS-AR MSA',
      population: 1332305,
      area_type: 'MSA',
    },
    {
      index: 138,
      name: 'Crittenden County, Arkansas',
      population: 47061,
      area_type: 'County',
    },
  ],
  140: [
    {
      index: 126,
      name: 'Little Rock-North Little Rock, AR CSA',
      population: 919999,
      area_type: 'CSA',
    },
    {
      index: 139,
      name: 'Little Rock-North Little Rock-Conway, AR MSA',
      population: 757615,
      area_type: 'MSA',
    },
    {
      index: 140,
      name: 'Faulkner County, Arkansas',
      population: 127665,
      area_type: 'County',
    },
  ],
  141: [
    {
      index: 134,
      name: 'Fort Smith, AR-OK MSA',
      population: 247072,
      area_type: 'MSA',
    },
    {
      index: 141,
      name: 'Franklin County, Arkansas',
      population: 17271,
      area_type: 'County',
    },
  ],
  143: [
    {
      index: 142,
      name: 'Hot Springs-Malvern, AR CSA',
      population: 133292,
      area_type: 'CSA',
    },
    {
      index: 143,
      name: 'Garland County, Arkansas (Hot Springs, AR MSA)',
      population: 100089,
      area_type: 'MSA',
    },
  ],
  145: [
    {
      index: 126,
      name: 'Little Rock-North Little Rock, AR CSA',
      population: 919999,
      area_type: 'CSA',
    },
    {
      index: 139,
      name: 'Little Rock-North Little Rock-Conway, AR MSA',
      population: 757615,
      area_type: 'MSA',
    },
    {
      index: 145,
      name: 'Grant County, Arkansas',
      population: 18160,
      area_type: 'County',
    },
  ],
  146: [
    {
      index: 131,
      name: 'Jonesboro-Paragould, AR CSA',
      population: 181960,
      area_type: 'CSA',
    },
    {
      index: 146,
      name: 'Greene County, Arkansas (Paragould, AR μSA)',
      population: 46448,
      area_type: 'μSA',
    },
  ],
  149: [
    {
      index: 148,
      name: 'Hope, AR μSA',
      population: 27634,
      area_type: 'μSA',
    },
    {
      index: 149,
      name: 'Hempstead County, Arkansas',
      population: 19453,
      area_type: 'County',
    },
  ],
  150: [
    {
      index: 142,
      name: 'Hot Springs-Malvern, AR CSA',
      population: 133292,
      area_type: 'CSA',
    },
    {
      index: 150,
      name: 'Hot Spring County, Arkansas (Malvern, AR μSA)',
      population: 33203,
      area_type: 'μSA',
    },
  ],
  153: [
    {
      index: 152,
      name: 'Batesville, AR μSA',
      population: 55755,
      area_type: 'μSA',
    },
    {
      index: 153,
      name: 'Independence County, Arkansas',
      population: 37945,
      area_type: 'County',
    },
  ],
  154: [
    {
      index: 126,
      name: 'Little Rock-North Little Rock, AR CSA',
      population: 919999,
      area_type: 'CSA',
    },
    {
      index: 127,
      name: 'Pine Bluff, AR MSA',
      population: 84629,
      area_type: 'MSA',
    },
    {
      index: 154,
      name: 'Jefferson County, Arkansas',
      population: 64246,
      area_type: 'County',
    },
  ],
  155: [
    {
      index: 126,
      name: 'Little Rock-North Little Rock, AR CSA',
      population: 919999,
      area_type: 'CSA',
    },
    {
      index: 127,
      name: 'Pine Bluff, AR MSA',
      population: 84629,
      area_type: 'MSA',
    },
    {
      index: 155,
      name: 'Lincoln County, Arkansas',
      population: 12916,
      area_type: 'County',
    },
  ],
  157: [
    {
      index: 156,
      name: 'Texarkana, TX-AR MSA',
      population: 146408,
      area_type: 'MSA',
    },
    {
      index: 157,
      name: 'Little River County, Arkansas',
      population: 11821,
      area_type: 'County',
    },
  ],
  158: [
    {
      index: 126,
      name: 'Little Rock-North Little Rock, AR CSA',
      population: 919999,
      area_type: 'CSA',
    },
    {
      index: 139,
      name: 'Little Rock-North Little Rock-Conway, AR MSA',
      population: 757615,
      area_type: 'MSA',
    },
    {
      index: 158,
      name: 'Lonoke County, Arkansas',
      population: 75225,
      area_type: 'County',
    },
  ],
  159: [
    {
      index: 118,
      name: 'Fayetteville-Springdale-Rogers, AR MSA',
      population: 576403,
      area_type: 'MSA',
    },
    {
      index: 159,
      name: 'Madison County, Arkansas',
      population: 17486,
      area_type: 'County',
    },
  ],
  160: [
    {
      index: 156,
      name: 'Texarkana, TX-AR MSA',
      population: 146408,
      area_type: 'MSA',
    },
    {
      index: 160,
      name: 'Miller County, Arkansas',
      population: 42552,
      area_type: 'County',
    },
  ],
  161: [
    {
      index: 161,
      name: 'Mississippi County, Arkansas (Blytheville, AR μSA)',
      population: 38896,
      area_type: 'μSA',
    },
  ],
  163: [
    {
      index: 148,
      name: 'Hope, AR μSA',
      population: 27634,
      area_type: 'μSA',
    },
    {
      index: 163,
      name: 'Nevada County, Arkansas',
      population: 8181,
      area_type: 'County',
    },
  ],
  164: [
    {
      index: 120,
      name: 'Harrison, AR μSA',
      population: 45362,
      area_type: 'μSA',
    },
    {
      index: 164,
      name: 'Newton County, Arkansas',
      population: 7078,
      area_type: 'County',
    },
  ],
  165: [
    {
      index: 122,
      name: 'Camden, AR μSA',
      population: 26744,
      area_type: 'μSA',
    },
    {
      index: 165,
      name: 'Ouachita County, Arkansas',
      population: 22049,
      area_type: 'County',
    },
  ],
  166: [
    {
      index: 126,
      name: 'Little Rock-North Little Rock, AR CSA',
      population: 919999,
      area_type: 'CSA',
    },
    {
      index: 139,
      name: 'Little Rock-North Little Rock-Conway, AR MSA',
      population: 757615,
      area_type: 'MSA',
    },
    {
      index: 166,
      name: 'Perry County, Arkansas',
      population: 10063,
      area_type: 'County',
    },
  ],
  167: [
    {
      index: 167,
      name: 'Phillips County, Arkansas (Helena-West Helena, AR μSA)',
      population: 15304,
      area_type: 'μSA',
    },
  ],
  169: [
    {
      index: 131,
      name: 'Jonesboro-Paragould, AR CSA',
      population: 181960,
      area_type: 'CSA',
    },
    {
      index: 132,
      name: 'Jonesboro, AR MSA',
      population: 135512,
      area_type: 'MSA',
    },
    {
      index: 169,
      name: 'Poinsett County, Arkansas',
      population: 22495,
      area_type: 'County',
    },
  ],
  171: [
    {
      index: 170,
      name: 'Russellville, AR μSA',
      population: 84194,
      area_type: 'μSA',
    },
    {
      index: 171,
      name: 'Pope County, Arkansas',
      population: 64065,
      area_type: 'County',
    },
  ],
  172: [
    {
      index: 126,
      name: 'Little Rock-North Little Rock, AR CSA',
      population: 919999,
      area_type: 'CSA',
    },
    {
      index: 139,
      name: 'Little Rock-North Little Rock-Conway, AR MSA',
      population: 757615,
      area_type: 'MSA',
    },
    {
      index: 172,
      name: 'Pulaski County, Arkansas',
      population: 399145,
      area_type: 'County',
    },
  ],
  173: [
    {
      index: 136,
      name: 'Memphis-Forrest City, TN-MS-AR CSA',
      population: 1354756,
      area_type: 'CSA',
    },
    {
      index: 173,
      name: 'St. Francis County, Arkansas (Forrest City, AR μSA)',
      population: 22451,
      area_type: 'μSA',
    },
  ],
  175: [
    {
      index: 126,
      name: 'Little Rock-North Little Rock, AR CSA',
      population: 919999,
      area_type: 'CSA',
    },
    {
      index: 139,
      name: 'Little Rock-North Little Rock-Conway, AR MSA',
      population: 757615,
      area_type: 'MSA',
    },
    {
      index: 175,
      name: 'Saline County, Arkansas',
      population: 127357,
      area_type: 'County',
    },
  ],
  176: [
    {
      index: 134,
      name: 'Fort Smith, AR-OK MSA',
      population: 247072,
      area_type: 'MSA',
    },
    {
      index: 176,
      name: 'Sebastian County, Arkansas',
      population: 129059,
      area_type: 'County',
    },
  ],
  177: [
    {
      index: 152,
      name: 'Batesville, AR μSA',
      population: 55755,
      area_type: 'μSA',
    },
    {
      index: 177,
      name: 'Sharp County, Arkansas',
      population: 17810,
      area_type: 'County',
    },
  ],
  178: [
    {
      index: 178,
      name: 'Union County, Arkansas (El Dorado, AR μSA)',
      population: 37752,
      area_type: 'μSA',
    },
  ],
  180: [
    {
      index: 118,
      name: 'Fayetteville-Springdale-Rogers, AR MSA',
      population: 576403,
      area_type: 'MSA',
    },
    {
      index: 180,
      name: 'Washington County, Arkansas',
      population: 256054,
      area_type: 'County',
    },
  ],
  181: [
    {
      index: 126,
      name: 'Little Rock-North Little Rock, AR CSA',
      population: 919999,
      area_type: 'CSA',
    },
    {
      index: 181,
      name: 'White County, Arkansas (Searcy, AR μSA)',
      population: 77755,
      area_type: 'μSA',
    },
  ],
  183: [
    {
      index: 170,
      name: 'Russellville, AR μSA',
      population: 84194,
      area_type: 'μSA',
    },
    {
      index: 183,
      name: 'Yell County, Arkansas',
      population: 20129,
      area_type: 'County',
    },
  ],
  186: [
    {
      index: 184,
      name: 'San Jose-San Francisco-Oakland, CA CSA',
      population: 9482708,
      area_type: 'CSA',
    },
    {
      index: 185,
      name: 'San Francisco-Oakland-Berkeley, CA MSA',
      population: 4579599,
      area_type: 'MSA',
    },
    {
      index: 186,
      name: 'Alameda County, California',
      population: 1628997,
      area_type: 'County',
    },
  ],
  187: [
    {
      index: 187,
      name: 'Butte County, California (Chico, CA MSA)',
      population: 207303,
      area_type: 'MSA',
    },
  ],
  189: [
    {
      index: 184,
      name: 'San Jose-San Francisco-Oakland, CA CSA',
      population: 9482708,
      area_type: 'CSA',
    },
    {
      index: 185,
      name: 'San Francisco-Oakland-Berkeley, CA MSA',
      population: 4579599,
      area_type: 'MSA',
    },
    {
      index: 189,
      name: 'Contra Costa County, California',
      population: 1156966,
      area_type: 'County',
    },
  ],
  190: [
    {
      index: 190,
      name: 'Del Norte County, California (Crescent City, CA μSA)',
      population: 27082,
      area_type: 'μSA',
    },
  ],
  194: [
    {
      index: 192,
      name: 'Sacramento-Roseville, CA CSA',
      population: 2701808,
      area_type: 'CSA',
    },
    {
      index: 193,
      name: 'Sacramento-Roseville-Folsom, CA MSA',
      population: 2416702,
      area_type: 'MSA',
    },
    {
      index: 194,
      name: 'El Dorado County, California',
      population: 192646,
      area_type: 'County',
    },
  ],
  196: [
    {
      index: 195,
      name: 'Fresno-Madera-Hanford, CA CSA',
      population: 1328427,
      area_type: 'CSA',
    },
    {
      index: 196,
      name: 'Fresno County, California (Fresno, CA MSA)',
      population: 1015190,
      area_type: 'MSA',
    },
  ],
  198: [
    {
      index: 198,
      name: 'Humboldt County, California (Eureka-Arcata, CA μSA)',
      population: 135010,
      area_type: 'μSA',
    },
  ],
  200: [
    {
      index: 200,
      name: 'Imperial County, California (El Centro, CA MSA)',
      population: 178713,
      area_type: 'MSA',
    },
  ],
  202: [
    {
      index: 202,
      name: 'Kern County, California (Bakersfield, CA MSA)',
      population: 916108,
      area_type: 'MSA',
    },
  ],
  204: [
    {
      index: 195,
      name: 'Fresno-Madera-Hanford, CA CSA',
      population: 1328427,
      area_type: 'CSA',
    },
    {
      index: 204,
      name: 'Kings County, California (Hanford-Corcoran, CA MSA)',
      population: 152981,
      area_type: 'MSA',
    },
  ],
  206: [
    {
      index: 206,
      name: 'Lake County, California (Clearlake, CA μSA)',
      population: 68191,
      area_type: 'μSA',
    },
  ],
  208: [
    {
      index: 208,
      name: 'Lassen County, California (Susanville, CA μSA)',
      population: 29904,
      area_type: 'μSA',
    },
  ],
  212: [
    {
      index: 210,
      name: 'Los Angeles-Long Beach, CA CSA',
      population: 18372485,
      area_type: 'CSA',
    },
    {
      index: 211,
      name: 'Los Angeles-Long Beach-Anaheim, CA MSA',
      population: 12872322,
      area_type: 'MSA',
    },
    {
      index: 212,
      name: 'Los Angeles County, California',
      population: 9721138,
      area_type: 'County',
    },
  ],
  213: [
    {
      index: 195,
      name: 'Fresno-Madera-Hanford, CA CSA',
      population: 1328427,
      area_type: 'CSA',
    },
    {
      index: 213,
      name: 'Madera County, California (Madera, CA MSA)',
      population: 160256,
      area_type: 'MSA',
    },
  ],
  215: [
    {
      index: 184,
      name: 'San Jose-San Francisco-Oakland, CA CSA',
      population: 9482708,
      area_type: 'CSA',
    },
    {
      index: 185,
      name: 'San Francisco-Oakland-Berkeley, CA MSA',
      population: 4579599,
      area_type: 'MSA',
    },
    {
      index: 215,
      name: 'Marin County, California',
      population: 256018,
      area_type: 'County',
    },
  ],
  216: [
    {
      index: 216,
      name: 'Mendocino County, California (Ukiah, CA μSA)',
      population: 89783,
      area_type: 'μSA',
    },
  ],
  218: [
    {
      index: 184,
      name: 'San Jose-San Francisco-Oakland, CA CSA',
      population: 9482708,
      area_type: 'CSA',
    },
    {
      index: 218,
      name: 'Merced County, California (Merced, CA MSA)',
      population: 290014,
      area_type: 'MSA',
    },
  ],
  220: [
    {
      index: 220,
      name: 'Monterey County, California (Salinas, CA MSA)',
      population: 432858,
      area_type: 'MSA',
    },
  ],
  222: [
    {
      index: 184,
      name: 'San Jose-San Francisco-Oakland, CA CSA',
      population: 9482708,
      area_type: 'CSA',
    },
    {
      index: 222,
      name: 'Napa County, California (Napa, CA MSA)',
      population: 134300,
      area_type: 'MSA',
    },
  ],
  224: [
    {
      index: 192,
      name: 'Sacramento-Roseville, CA CSA',
      population: 2701808,
      area_type: 'CSA',
    },
    {
      index: 224,
      name: 'Nevada County, California (Truckee-Grass Valley, CA μSA)',
      population: 102293,
      area_type: 'μSA',
    },
  ],
  226: [
    {
      index: 210,
      name: 'Los Angeles-Long Beach, CA CSA',
      population: 18372485,
      area_type: 'CSA',
    },
    {
      index: 211,
      name: 'Los Angeles-Long Beach-Anaheim, CA MSA',
      population: 12872322,
      area_type: 'MSA',
    },
    {
      index: 226,
      name: 'Orange County, California',
      population: 3151184,
      area_type: 'County',
    },
  ],
  227: [
    {
      index: 192,
      name: 'Sacramento-Roseville, CA CSA',
      population: 2701808,
      area_type: 'CSA',
    },
    {
      index: 193,
      name: 'Sacramento-Roseville-Folsom, CA MSA',
      population: 2416702,
      area_type: 'MSA',
    },
    {
      index: 227,
      name: 'Placer County, California',
      population: 417772,
      area_type: 'County',
    },
  ],
  229: [
    {
      index: 210,
      name: 'Los Angeles-Long Beach, CA CSA',
      population: 18372485,
      area_type: 'CSA',
    },
    {
      index: 228,
      name: 'Riverside-San Bernardino-Ontario, CA MSA',
      population: 4667558,
      area_type: 'MSA',
    },
    {
      index: 229,
      name: 'Riverside County, California',
      population: 2473902,
      area_type: 'County',
    },
  ],
  230: [
    {
      index: 192,
      name: 'Sacramento-Roseville, CA CSA',
      population: 2701808,
      area_type: 'CSA',
    },
    {
      index: 193,
      name: 'Sacramento-Roseville-Folsom, CA MSA',
      population: 2416702,
      area_type: 'MSA',
    },
    {
      index: 230,
      name: 'Sacramento County, California',
      population: 1584169,
      area_type: 'County',
    },
  ],
  232: [
    {
      index: 184,
      name: 'San Jose-San Francisco-Oakland, CA CSA',
      population: 9482708,
      area_type: 'CSA',
    },
    {
      index: 231,
      name: 'San Jose-Sunnyvale-Santa Clara, CA MSA',
      population: 1938524,
      area_type: 'MSA',
    },
    {
      index: 232,
      name: 'San Benito County, California',
      population: 67579,
      area_type: 'County',
    },
  ],
  233: [
    {
      index: 210,
      name: 'Los Angeles-Long Beach, CA CSA',
      population: 18372485,
      area_type: 'CSA',
    },
    {
      index: 228,
      name: 'Riverside-San Bernardino-Ontario, CA MSA',
      population: 4667558,
      area_type: 'MSA',
    },
    {
      index: 233,
      name: 'San Bernardino County, California',
      population: 2193656,
      area_type: 'County',
    },
  ],
  234: [
    {
      index: 234,
      name: 'San Diego County, California (San Diego-Chula Vista-Carlsbad, CA MSA)',
      population: 3276208,
      area_type: 'MSA',
    },
  ],
  236: [
    {
      index: 184,
      name: 'San Jose-San Francisco-Oakland, CA CSA',
      population: 9482708,
      area_type: 'CSA',
    },
    {
      index: 185,
      name: 'San Francisco-Oakland-Berkeley, CA MSA',
      population: 4579599,
      area_type: 'MSA',
    },
    {
      index: 236,
      name: 'San Francisco County, California',
      population: 808437,
      area_type: 'County',
    },
  ],
  237: [
    {
      index: 184,
      name: 'San Jose-San Francisco-Oakland, CA CSA',
      population: 9482708,
      area_type: 'CSA',
    },
    {
      index: 237,
      name: 'San Joaquin County, California (Stockton, CA MSA)',
      population: 793229,
      area_type: 'MSA',
    },
  ],
  239: [
    {
      index: 239,
      name: 'San Luis Obispo County, California (San Luis Obispo-Paso Robles, CA MSA)',
      population: 282013,
      area_type: 'MSA',
    },
  ],
  241: [
    {
      index: 184,
      name: 'San Jose-San Francisco-Oakland, CA CSA',
      population: 9482708,
      area_type: 'CSA',
    },
    {
      index: 185,
      name: 'San Francisco-Oakland-Berkeley, CA MSA',
      population: 4579599,
      area_type: 'MSA',
    },
    {
      index: 241,
      name: 'San Mateo County, California',
      population: 729181,
      area_type: 'County',
    },
  ],
  242: [
    {
      index: 242,
      name: 'Santa Barbara County, California (Santa Maria-Santa Barbara, CA MSA)',
      population: 443837,
      area_type: 'MSA',
    },
  ],
  244: [
    {
      index: 184,
      name: 'San Jose-San Francisco-Oakland, CA CSA',
      population: 9482708,
      area_type: 'CSA',
    },
    {
      index: 231,
      name: 'San Jose-Sunnyvale-Santa Clara, CA MSA',
      population: 1938524,
      area_type: 'MSA',
    },
    {
      index: 244,
      name: 'Santa Clara County, California',
      population: 1870945,
      area_type: 'County',
    },
  ],
  245: [
    {
      index: 184,
      name: 'San Jose-San Francisco-Oakland, CA CSA',
      population: 9482708,
      area_type: 'CSA',
    },
    {
      index: 245,
      name: 'Santa Cruz County, California (Santa Cruz-Watsonville, CA MSA)',
      population: 264370,
      area_type: 'MSA',
    },
  ],
  248: [
    {
      index: 247,
      name: 'Redding-Red Bluff, CA CSA',
      population: 246175,
      area_type: 'CSA',
    },
    {
      index: 248,
      name: 'Shasta County, California (Redding, CA MSA)',
      population: 180930,
      area_type: 'MSA',
    },
  ],
  250: [
    {
      index: 184,
      name: 'San Jose-San Francisco-Oakland, CA CSA',
      population: 9482708,
      area_type: 'CSA',
    },
    {
      index: 250,
      name: 'Solano County, California (Vallejo, CA MSA)',
      population: 448747,
      area_type: 'MSA',
    },
  ],
  252: [
    {
      index: 184,
      name: 'San Jose-San Francisco-Oakland, CA CSA',
      population: 9482708,
      area_type: 'CSA',
    },
    {
      index: 252,
      name: 'Sonoma County, California (Santa Rosa-Petaluma, CA MSA)',
      population: 482650,
      area_type: 'MSA',
    },
  ],
  254: [
    {
      index: 184,
      name: 'San Jose-San Francisco-Oakland, CA CSA',
      population: 9482708,
      area_type: 'CSA',
    },
    {
      index: 254,
      name: 'Stanislaus County, California (Modesto, CA MSA)',
      population: 551275,
      area_type: 'MSA',
    },
  ],
  257: [
    {
      index: 192,
      name: 'Sacramento-Roseville, CA CSA',
      population: 2701808,
      area_type: 'CSA',
    },
    {
      index: 256,
      name: 'Yuba City, CA MSA',
      population: 182813,
      area_type: 'MSA',
    },
    {
      index: 257,
      name: 'Sutter County, California',
      population: 98503,
      area_type: 'County',
    },
  ],
  258: [
    {
      index: 247,
      name: 'Redding-Red Bluff, CA CSA',
      population: 246175,
      area_type: 'CSA',
    },
    {
      index: 258,
      name: 'Tehama County, California (Red Bluff, CA μSA)',
      population: 65245,
      area_type: 'μSA',
    },
  ],
  260: [
    {
      index: 260,
      name: 'Tulare County, California (Visalia, CA MSA)',
      population: 477544,
      area_type: 'MSA',
    },
  ],
  262: [
    {
      index: 262,
      name: 'Tuolumne County, California (Sonora, CA μSA)',
      population: 54531,
      area_type: 'μSA',
    },
  ],
  264: [
    {
      index: 210,
      name: 'Los Angeles-Long Beach, CA CSA',
      population: 18372485,
      area_type: 'CSA',
    },
    {
      index: 264,
      name: 'Ventura County, California (Oxnard-Thousand Oaks-Ventura, CA MSA)',
      population: 832605,
      area_type: 'MSA',
    },
  ],
  266: [
    {
      index: 192,
      name: 'Sacramento-Roseville, CA CSA',
      population: 2701808,
      area_type: 'CSA',
    },
    {
      index: 193,
      name: 'Sacramento-Roseville-Folsom, CA MSA',
      population: 2416702,
      area_type: 'MSA',
    },
    {
      index: 266,
      name: 'Yolo County, California',
      population: 222115,
      area_type: 'County',
    },
  ],
  267: [
    {
      index: 192,
      name: 'Sacramento-Roseville, CA CSA',
      population: 2701808,
      area_type: 'CSA',
    },
    {
      index: 256,
      name: 'Yuba City, CA MSA',
      population: 182813,
      area_type: 'MSA',
    },
    {
      index: 267,
      name: 'Yuba County, California',
      population: 84310,
      area_type: 'County',
    },
  ],
  270: [
    {
      index: 268,
      name: 'Denver-Aurora, CO CSA',
      population: 3663515,
      area_type: 'CSA',
    },
    {
      index: 269,
      name: 'Denver-Aurora-Lakewood, CO MSA',
      population: 2985871,
      area_type: 'MSA',
    },
    {
      index: 270,
      name: 'Adams County, Colorado',
      population: 527575,
      area_type: 'County',
    },
  ],
  271: [
    {
      index: 268,
      name: 'Denver-Aurora, CO CSA',
      population: 3663515,
      area_type: 'CSA',
    },
    {
      index: 269,
      name: 'Denver-Aurora-Lakewood, CO MSA',
      population: 2985871,
      area_type: 'MSA',
    },
    {
      index: 271,
      name: 'Arapahoe County, Colorado',
      population: 655808,
      area_type: 'County',
    },
  ],
  272: [
    {
      index: 268,
      name: 'Denver-Aurora, CO CSA',
      population: 3663515,
      area_type: 'CSA',
    },
    {
      index: 272,
      name: 'Boulder County, Colorado (Boulder, CO MSA)',
      population: 327468,
      area_type: 'MSA',
    },
  ],
  274: [
    {
      index: 268,
      name: 'Denver-Aurora, CO CSA',
      population: 3663515,
      area_type: 'CSA',
    },
    {
      index: 269,
      name: 'Denver-Aurora-Lakewood, CO MSA',
      population: 2985871,
      area_type: 'MSA',
    },
    {
      index: 274,
      name: 'Broomfield County, Colorado',
      population: 76121,
      area_type: 'County',
    },
  ],
  275: [
    {
      index: 268,
      name: 'Denver-Aurora, CO CSA',
      population: 3663515,
      area_type: 'CSA',
    },
    {
      index: 269,
      name: 'Denver-Aurora-Lakewood, CO MSA',
      population: 2985871,
      area_type: 'MSA',
    },
    {
      index: 275,
      name: 'Clear Creek County, Colorado',
      population: 9355,
      area_type: 'County',
    },
  ],
  276: [
    {
      index: 268,
      name: 'Denver-Aurora, CO CSA',
      population: 3663515,
      area_type: 'CSA',
    },
    {
      index: 269,
      name: 'Denver-Aurora-Lakewood, CO MSA',
      population: 2985871,
      area_type: 'MSA',
    },
    {
      index: 276,
      name: 'Denver County, Colorado',
      population: 713252,
      area_type: 'County',
    },
  ],
  277: [
    {
      index: 268,
      name: 'Denver-Aurora, CO CSA',
      population: 3663515,
      area_type: 'CSA',
    },
    {
      index: 269,
      name: 'Denver-Aurora-Lakewood, CO MSA',
      population: 2985871,
      area_type: 'MSA',
    },
    {
      index: 277,
      name: 'Douglas County, Colorado',
      population: 375988,
      area_type: 'County',
    },
  ],
  279: [
    {
      index: 278,
      name: 'Edwards-Glenwood Springs, CO CSA',
      population: 134432,
      area_type: 'CSA',
    },
    {
      index: 279,
      name: 'Eagle County, Colorado (Edwards, CO μSA)',
      population: 55285,
      area_type: 'μSA',
    },
  ],
  281: [
    {
      index: 268,
      name: 'Denver-Aurora, CO CSA',
      population: 3663515,
      area_type: 'CSA',
    },
    {
      index: 269,
      name: 'Denver-Aurora-Lakewood, CO MSA',
      population: 2985871,
      area_type: 'MSA',
    },
    {
      index: 281,
      name: 'Elbert County, Colorado',
      population: 27799,
      area_type: 'County',
    },
  ],
  283: [
    {
      index: 282,
      name: 'Colorado Springs, CO MSA',
      population: 765424,
      area_type: 'MSA',
    },
    {
      index: 283,
      name: 'El Paso County, Colorado',
      population: 740567,
      area_type: 'County',
    },
  ],
  285: [
    {
      index: 284,
      name: 'Pueblo-Cañon City, CO CSA',
      population: 219165,
      area_type: 'CSA',
    },
    {
      index: 285,
      name: 'Fremont County, Colorado (Cañon City, CO μSA)',
      population: 49621,
      area_type: 'μSA',
    },
  ],
  288: [
    {
      index: 278,
      name: 'Edwards-Glenwood Springs, CO CSA',
      population: 134432,
      area_type: 'CSA',
    },
    {
      index: 287,
      name: 'Glenwood Springs, CO μSA',
      population: 79147,
      area_type: 'μSA',
    },
    {
      index: 288,
      name: 'Garfield County, Colorado',
      population: 62271,
      area_type: 'County',
    },
  ],
  289: [
    {
      index: 268,
      name: 'Denver-Aurora, CO CSA',
      population: 3663515,
      area_type: 'CSA',
    },
    {
      index: 269,
      name: 'Denver-Aurora-Lakewood, CO MSA',
      population: 2985871,
      area_type: 'MSA',
    },
    {
      index: 289,
      name: 'Gilpin County, Colorado',
      population: 5891,
      area_type: 'County',
    },
  ],
  290: [
    {
      index: 268,
      name: 'Denver-Aurora, CO CSA',
      population: 3663515,
      area_type: 'CSA',
    },
    {
      index: 269,
      name: 'Denver-Aurora-Lakewood, CO MSA',
      population: 2985871,
      area_type: 'MSA',
    },
    {
      index: 290,
      name: 'Jefferson County, Colorado',
      population: 576143,
      area_type: 'County',
    },
  ],
  291: [
    {
      index: 291,
      name: 'La Plata County, Colorado (Durango, CO μSA)',
      population: 56607,
      area_type: 'μSA',
    },
  ],
  293: [
    {
      index: 293,
      name: 'Larimer County, Colorado (Fort Collins, CO MSA)',
      population: 366778,
      area_type: 'MSA',
    },
  ],
  295: [
    {
      index: 295,
      name: 'Logan County, Colorado (Sterling, CO μSA)',
      population: 20823,
      area_type: 'μSA',
    },
  ],
  297: [
    {
      index: 297,
      name: 'Mesa County, Colorado (Grand Junction, CO MSA)',
      population: 158636,
      area_type: 'MSA',
    },
  ],
  300: [
    {
      index: 299,
      name: 'Steamboat Springs-Craig, CO CSA',
      population: 38184,
      area_type: 'CSA',
    },
    {
      index: 300,
      name: 'Moffat County, Colorado (Craig, CO μSA)',
      population: 13177,
      area_type: 'μSA',
    },
  ],
  303: [
    {
      index: 302,
      name: 'Montrose, CO μSA',
      population: 48911,
      area_type: 'μSA',
    },
    {
      index: 303,
      name: 'Montrose County, Colorado',
      population: 43811,
      area_type: 'County',
    },
  ],
  304: [
    {
      index: 304,
      name: 'Morgan County, Colorado (Fort Morgan, CO μSA)',
      population: 29239,
      area_type: 'μSA',
    },
  ],
  306: [
    {
      index: 302,
      name: 'Montrose, CO μSA',
      population: 48911,
      area_type: 'μSA',
    },
    {
      index: 306,
      name: 'Ouray County, Colorado',
      population: 5100,
      area_type: 'County',
    },
  ],
  307: [
    {
      index: 268,
      name: 'Denver-Aurora, CO CSA',
      population: 3663515,
      area_type: 'CSA',
    },
    {
      index: 269,
      name: 'Denver-Aurora-Lakewood, CO MSA',
      population: 2985871,
      area_type: 'MSA',
    },
    {
      index: 307,
      name: 'Park County, Colorado',
      population: 17939,
      area_type: 'County',
    },
  ],
  308: [
    {
      index: 278,
      name: 'Edwards-Glenwood Springs, CO CSA',
      population: 134432,
      area_type: 'CSA',
    },
    {
      index: 287,
      name: 'Glenwood Springs, CO μSA',
      population: 79147,
      area_type: 'μSA',
    },
    {
      index: 308,
      name: 'Pitkin County, Colorado',
      population: 16876,
      area_type: 'County',
    },
  ],
  309: [
    {
      index: 284,
      name: 'Pueblo-Cañon City, CO CSA',
      population: 219165,
      area_type: 'CSA',
    },
    {
      index: 309,
      name: 'Pueblo County, Colorado (Pueblo, CO MSA)',
      population: 169544,
      area_type: 'MSA',
    },
  ],
  311: [
    {
      index: 299,
      name: 'Steamboat Springs-Craig, CO CSA',
      population: 38184,
      area_type: 'CSA',
    },
    {
      index: 311,
      name: 'Routt County, Colorado (Steamboat Springs, CO μSA)',
      population: 25007,
      area_type: 'μSA',
    },
  ],
  313: [
    {
      index: 313,
      name: 'Summit County, Colorado (Breckenridge, CO μSA)',
      population: 30565,
      area_type: 'μSA',
    },
  ],
  315: [
    {
      index: 282,
      name: 'Colorado Springs, CO MSA',
      population: 765424,
      area_type: 'MSA',
    },
    {
      index: 315,
      name: 'Teller County, Colorado',
      population: 24857,
      area_type: 'County',
    },
  ],
  316: [
    {
      index: 268,
      name: 'Denver-Aurora, CO CSA',
      population: 3663515,
      area_type: 'CSA',
    },
    {
      index: 316,
      name: 'Weld County, Colorado (Greeley, CO MSA)',
      population: 350176,
      area_type: 'MSA',
    },
  ],
  319: [
    {
      index: 318,
      name: 'Philadelphia-Reading-Camden, PA-NJ-DE-MD CSA',
      population: 7381187,
      area_type: 'CSA',
    },
    {
      index: 319,
      name: 'Kent County, Delaware (Dover, DE MSA)',
      population: 186946,
      area_type: 'MSA',
    },
  ],
  322: [
    {
      index: 318,
      name: 'Philadelphia-Reading-Camden, PA-NJ-DE-MD CSA',
      population: 7381187,
      area_type: 'CSA',
    },
    {
      index: 321,
      name: 'Philadelphia-Camden-Wilmington, PA-NJ-DE-MD MSA',
      population: 6241164,
      area_type: 'MSA',
    },
    {
      index: 322,
      name: 'New Castle County, Delaware',
      population: 575494,
      area_type: 'County',
    },
  ],
  325: [
    {
      index: 323,
      name: 'Salisbury-Cambridge, MD-DE CSA',
      population: 471758,
      area_type: 'CSA',
    },
    {
      index: 324,
      name: 'Salisbury, MD-DE MSA',
      population: 439032,
      area_type: 'MSA',
    },
    {
      index: 325,
      name: 'Sussex County, Delaware',
      population: 255956,
      area_type: 'County',
    },
  ],
  328: [
    {
      index: 326,
      name: 'Washington-Baltimore-Arlington, DC-MD-VA-WV-PA CSA',
      population: 9968104,
      area_type: 'CSA',
    },
    {
      index: 327,
      name: 'Washington-Arlington-Alexandria, DC-VA-MD-WV MSA',
      population: 6373756,
      area_type: 'MSA',
    },
    {
      index: 328,
      name: 'District of Columbia, District of Columbia',
      population: 671803,
      area_type: 'County',
    },
  ],
  331: [
    {
      index: 329,
      name: 'Gainesville-Lake City, FL CSA',
      population: 420190,
      area_type: 'CSA',
    },
    {
      index: 330,
      name: 'Gainesville, FL MSA',
      population: 348282,
      area_type: 'MSA',
    },
    {
      index: 331,
      name: 'Alachua County, Florida',
      population: 284030,
      area_type: 'County',
    },
  ],
  334: [
    {
      index: 332,
      name: 'Jacksonville-St. Marys-Palatka, FL-GA CSA',
      population: 1807412,
      area_type: 'CSA',
    },
    {
      index: 333,
      name: 'Jacksonville, FL MSA',
      population: 1675668,
      area_type: 'MSA',
    },
    {
      index: 334,
      name: 'Baker County, Florida',
      population: 27803,
      area_type: 'County',
    },
  ],
  335: [
    {
      index: 335,
      name: 'Bay County, Florida (Panama City, FL MSA)',
      population: 185134,
      area_type: 'MSA',
    },
  ],
  337: [
    {
      index: 337,
      name: 'Brevard County, Florida (Palm Bay-Melbourne-Titusville, FL MSA)',
      population: 630693,
      area_type: 'MSA',
    },
  ],
  341: [
    {
      index: 339,
      name: 'Miami-Port St. Lucie-Fort Lauderdale, FL CSA',
      population: 6909110,
      area_type: 'CSA',
    },
    {
      index: 340,
      name: 'Miami-Fort Lauderdale-Pompano Beach, FL MSA',
      population: 6139340,
      area_type: 'MSA',
    },
    {
      index: 341,
      name: 'Broward County, Florida',
      population: 1947026,
      area_type: 'County',
    },
  ],
  343: [
    {
      index: 342,
      name: 'North Port-Sarasota, FL CSA',
      population: 1129384,
      area_type: 'CSA',
    },
    {
      index: 343,
      name: 'Charlotte County, Florida (Punta Gorda, FL MSA)',
      population: 202661,
      area_type: 'MSA',
    },
  ],
  345: [
    {
      index: 345,
      name: 'Citrus County, Florida (Homosassa Springs, FL MSA)',
      population: 162529,
      area_type: 'MSA',
    },
  ],
  347: [
    {
      index: 332,
      name: 'Jacksonville-St. Marys-Palatka, FL-GA CSA',
      population: 1807412,
      area_type: 'CSA',
    },
    {
      index: 333,
      name: 'Jacksonville, FL MSA',
      population: 1675668,
      area_type: 'MSA',
    },
    {
      index: 347,
      name: 'Clay County, Florida',
      population: 226589,
      area_type: 'County',
    },
  ],
  349: [
    {
      index: 348,
      name: 'Cape Coral-Fort Myers-Naples, FL CSA',
      population: 1261786,
      area_type: 'CSA',
    },
    {
      index: 349,
      name: 'Collier County, Florida (Naples-Marco Island, FL MSA)',
      population: 397994,
      area_type: 'MSA',
    },
  ],
  351: [
    {
      index: 329,
      name: 'Gainesville-Lake City, FL CSA',
      population: 420190,
      area_type: 'CSA',
    },
    {
      index: 351,
      name: 'Columbia County, Florida (Lake City, FL μSA)',
      population: 71908,
      area_type: 'μSA',
    },
  ],
  353: [
    {
      index: 342,
      name: 'North Port-Sarasota, FL CSA',
      population: 1129384,
      area_type: 'CSA',
    },
    {
      index: 353,
      name: 'DeSoto County, Florida (Arcadia, FL μSA)',
      population: 35312,
      area_type: 'μSA',
    },
  ],
  355: [
    {
      index: 332,
      name: 'Jacksonville-St. Marys-Palatka, FL-GA CSA',
      population: 1807412,
      area_type: 'CSA',
    },
    {
      index: 333,
      name: 'Jacksonville, FL MSA',
      population: 1675668,
      area_type: 'MSA',
    },
    {
      index: 355,
      name: 'Duval County, Florida',
      population: 1016536,
      area_type: 'County',
    },
  ],
  357: [
    {
      index: 36,
      name: 'Pensacola-Ferry Pass, FL-AL CSA',
      population: 559812,
      area_type: 'CSA',
    },
    {
      index: 356,
      name: 'Pensacola-Ferry Pass-Brent, FL MSA',
      population: 523146,
      area_type: 'MSA',
    },
    {
      index: 357,
      name: 'Escambia County, Florida',
      population: 324878,
      area_type: 'County',
    },
  ],
  360: [
    {
      index: 358,
      name: 'Orlando-Lakeland-Deltona, FL CSA',
      population: 4428098,
      area_type: 'CSA',
    },
    {
      index: 359,
      name: 'Deltona-Daytona Beach-Ormond Beach, FL MSA',
      population: 705897,
      area_type: 'MSA',
    },
    {
      index: 360,
      name: 'Flagler County, Florida',
      population: 126705,
      area_type: 'County',
    },
  ],
  362: [
    {
      index: 361,
      name: 'Tallahassee, FL MSA',
      population: 390992,
      area_type: 'MSA',
    },
    {
      index: 362,
      name: 'Gadsden County, Florida',
      population: 43403,
      area_type: 'County',
    },
  ],
  363: [
    {
      index: 329,
      name: 'Gainesville-Lake City, FL CSA',
      population: 420190,
      area_type: 'CSA',
    },
    {
      index: 330,
      name: 'Gainesville, FL MSA',
      population: 348282,
      area_type: 'MSA',
    },
    {
      index: 363,
      name: 'Gilchrist County, Florida',
      population: 18992,
      area_type: 'County',
    },
  ],
  364: [
    {
      index: 358,
      name: 'Orlando-Lakeland-Deltona, FL CSA',
      population: 4428098,
      area_type: 'CSA',
    },
    {
      index: 364,
      name: 'Hardee County, Florida (Wauchula, FL μSA)',
      population: 25645,
      area_type: 'μSA',
    },
  ],
  366: [
    {
      index: 348,
      name: 'Cape Coral-Fort Myers-Naples, FL CSA',
      population: 1261786,
      area_type: 'CSA',
    },
    {
      index: 366,
      name: 'Hendry County, Florida (Clewiston, FL μSA)',
      population: 41339,
      area_type: 'μSA',
    },
  ],
  369: [
    {
      index: 368,
      name: 'Tampa-St. Petersburg-Clearwater, FL MSA',
      population: 3290730,
      area_type: 'MSA',
    },
    {
      index: 369,
      name: 'Hernando County, Florida',
      population: 206896,
      area_type: 'County',
    },
  ],
  370: [
    {
      index: 370,
      name: 'Highlands County, Florida (Sebring-Avon Park, FL MSA)',
      population: 105618,
      area_type: 'MSA',
    },
  ],
  372: [
    {
      index: 368,
      name: 'Tampa-St. Petersburg-Clearwater, FL MSA',
      population: 3290730,
      area_type: 'MSA',
    },
    {
      index: 372,
      name: 'Hillsborough County, Florida',
      population: 1513301,
      area_type: 'County',
    },
  ],
  373: [
    {
      index: 339,
      name: 'Miami-Port St. Lucie-Fort Lauderdale, FL CSA',
      population: 6909110,
      area_type: 'CSA',
    },
    {
      index: 373,
      name: 'Indian River County, Florida (Sebastian-Vero Beach, FL MSA)',
      population: 167352,
      area_type: 'MSA',
    },
  ],
  375: [
    {
      index: 361,
      name: 'Tallahassee, FL MSA',
      population: 390992,
      area_type: 'MSA',
    },
    {
      index: 375,
      name: 'Jefferson County, Florida',
      population: 15042,
      area_type: 'County',
    },
  ],
  377: [
    {
      index: 358,
      name: 'Orlando-Lakeland-Deltona, FL CSA',
      population: 4428098,
      area_type: 'CSA',
    },
    {
      index: 376,
      name: 'Orlando-Kissimmee-Sanford, FL MSA',
      population: 2764182,
      area_type: 'MSA',
    },
    {
      index: 377,
      name: 'Lake County, Florida',
      population: 410139,
      area_type: 'County',
    },
  ],
  378: [
    {
      index: 348,
      name: 'Cape Coral-Fort Myers-Naples, FL CSA',
      population: 1261786,
      area_type: 'CSA',
    },
    {
      index: 378,
      name: 'Lee County, Florida (Cape Coral-Fort Myers, FL MSA)',
      population: 822453,
      area_type: 'MSA',
    },
  ],
  380: [
    {
      index: 361,
      name: 'Tallahassee, FL MSA',
      population: 390992,
      area_type: 'MSA',
    },
    {
      index: 380,
      name: 'Leon County, Florida',
      population: 297369,
      area_type: 'County',
    },
  ],
  381: [
    {
      index: 329,
      name: 'Gainesville-Lake City, FL CSA',
      population: 420190,
      area_type: 'CSA',
    },
    {
      index: 330,
      name: 'Gainesville, FL MSA',
      population: 348282,
      area_type: 'MSA',
    },
    {
      index: 381,
      name: 'Levy County, Florida',
      population: 45260,
      area_type: 'County',
    },
  ],
  383: [
    {
      index: 342,
      name: 'North Port-Sarasota, FL CSA',
      population: 1129384,
      area_type: 'CSA',
    },
    {
      index: 382,
      name: 'North Port-Sarasota-Bradenton, FL MSA',
      population: 891411,
      area_type: 'MSA',
    },
    {
      index: 383,
      name: 'Manatee County, Florida',
      population: 429125,
      area_type: 'County',
    },
  ],
  384: [
    {
      index: 384,
      name: 'Marion County, Florida (Ocala, FL MSA)',
      population: 396415,
      area_type: 'MSA',
    },
  ],
  387: [
    {
      index: 339,
      name: 'Miami-Port St. Lucie-Fort Lauderdale, FL CSA',
      population: 6909110,
      area_type: 'CSA',
    },
    {
      index: 386,
      name: 'Port St. Lucie, FL MSA',
      population: 520710,
      area_type: 'MSA',
    },
    {
      index: 387,
      name: 'Martin County, Florida',
      population: 162006,
      area_type: 'County',
    },
  ],
  388: [
    {
      index: 339,
      name: 'Miami-Port St. Lucie-Fort Lauderdale, FL CSA',
      population: 6909110,
      area_type: 'CSA',
    },
    {
      index: 340,
      name: 'Miami-Fort Lauderdale-Pompano Beach, FL MSA',
      population: 6139340,
      area_type: 'MSA',
    },
    {
      index: 388,
      name: 'Miami-Dade County, Florida',
      population: 2673837,
      area_type: 'County',
    },
  ],
  389: [
    {
      index: 339,
      name: 'Miami-Port St. Lucie-Fort Lauderdale, FL CSA',
      population: 6909110,
      area_type: 'CSA',
    },
    {
      index: 389,
      name: 'Monroe County, Florida (Key West, FL μSA)',
      population: 81708,
      area_type: 'μSA',
    },
  ],
  391: [
    {
      index: 332,
      name: 'Jacksonville-St. Marys-Palatka, FL-GA CSA',
      population: 1807412,
      area_type: 'CSA',
    },
    {
      index: 333,
      name: 'Jacksonville, FL MSA',
      population: 1675668,
      area_type: 'MSA',
    },
    {
      index: 391,
      name: 'Nassau County, Florida',
      population: 97899,
      area_type: 'County',
    },
  ],
  393: [
    {
      index: 392,
      name: 'Crestview-Fort Walton Beach-Destin, FL MSA',
      population: 299786,
      area_type: 'MSA',
    },
    {
      index: 393,
      name: 'Okaloosa County, Florida',
      population: 216482,
      area_type: 'County',
    },
  ],
  394: [
    {
      index: 394,
      name: 'Okeechobee County, Florida (Okeechobee, FL μSA)',
      population: 40412,
      area_type: 'μSA',
    },
  ],
  396: [
    {
      index: 358,
      name: 'Orlando-Lakeland-Deltona, FL CSA',
      population: 4428098,
      area_type: 'CSA',
    },
    {
      index: 376,
      name: 'Orlando-Kissimmee-Sanford, FL MSA',
      population: 2764182,
      area_type: 'MSA',
    },
    {
      index: 396,
      name: 'Orange County, Florida',
      population: 1452726,
      area_type: 'County',
    },
  ],
  397: [
    {
      index: 358,
      name: 'Orlando-Lakeland-Deltona, FL CSA',
      population: 4428098,
      area_type: 'CSA',
    },
    {
      index: 376,
      name: 'Orlando-Kissimmee-Sanford, FL MSA',
      population: 2764182,
      area_type: 'MSA',
    },
    {
      index: 397,
      name: 'Osceola County, Florida',
      population: 422545,
      area_type: 'County',
    },
  ],
  398: [
    {
      index: 339,
      name: 'Miami-Port St. Lucie-Fort Lauderdale, FL CSA',
      population: 6909110,
      area_type: 'CSA',
    },
    {
      index: 340,
      name: 'Miami-Fort Lauderdale-Pompano Beach, FL MSA',
      population: 6139340,
      area_type: 'MSA',
    },
    {
      index: 398,
      name: 'Palm Beach County, Florida',
      population: 1518477,
      area_type: 'County',
    },
  ],
  399: [
    {
      index: 368,
      name: 'Tampa-St. Petersburg-Clearwater, FL MSA',
      population: 3290730,
      area_type: 'MSA',
    },
    {
      index: 399,
      name: 'Pasco County, Florida',
      population: 608794,
      area_type: 'County',
    },
  ],
  400: [
    {
      index: 368,
      name: 'Tampa-St. Petersburg-Clearwater, FL MSA',
      population: 3290730,
      area_type: 'MSA',
    },
    {
      index: 400,
      name: 'Pinellas County, Florida',
      population: 961739,
      area_type: 'County',
    },
  ],
  401: [
    {
      index: 358,
      name: 'Orlando-Lakeland-Deltona, FL CSA',
      population: 4428098,
      area_type: 'CSA',
    },
    {
      index: 401,
      name: 'Polk County, Florida (Lakeland-Winter Haven, FL MSA)',
      population: 787404,
      area_type: 'MSA',
    },
  ],
  403: [
    {
      index: 332,
      name: 'Jacksonville-St. Marys-Palatka, FL-GA CSA',
      population: 1807412,
      area_type: 'CSA',
    },
    {
      index: 403,
      name: 'Putnam County, Florida (Palatka, FL μSA)',
      population: 74731,
      area_type: 'μSA',
    },
  ],
  405: [
    {
      index: 332,
      name: 'Jacksonville-St. Marys-Palatka, FL-GA CSA',
      population: 1807412,
      area_type: 'CSA',
    },
    {
      index: 333,
      name: 'Jacksonville, FL MSA',
      population: 1675668,
      area_type: 'MSA',
    },
    {
      index: 405,
      name: 'St. Johns County, Florida',
      population: 306841,
      area_type: 'County',
    },
  ],
  406: [
    {
      index: 339,
      name: 'Miami-Port St. Lucie-Fort Lauderdale, FL CSA',
      population: 6909110,
      area_type: 'CSA',
    },
    {
      index: 386,
      name: 'Port St. Lucie, FL MSA',
      population: 520710,
      area_type: 'MSA',
    },
    {
      index: 406,
      name: 'St. Lucie County, Florida',
      population: 358704,
      area_type: 'County',
    },
  ],
  407: [
    {
      index: 36,
      name: 'Pensacola-Ferry Pass, FL-AL CSA',
      population: 559812,
      area_type: 'CSA',
    },
    {
      index: 356,
      name: 'Pensacola-Ferry Pass-Brent, FL MSA',
      population: 523146,
      area_type: 'MSA',
    },
    {
      index: 407,
      name: 'Santa Rosa County, Florida',
      population: 198268,
      area_type: 'County',
    },
  ],
  408: [
    {
      index: 342,
      name: 'North Port-Sarasota, FL CSA',
      population: 1129384,
      area_type: 'CSA',
    },
    {
      index: 382,
      name: 'North Port-Sarasota-Bradenton, FL MSA',
      population: 891411,
      area_type: 'MSA',
    },
    {
      index: 408,
      name: 'Sarasota County, Florida',
      population: 462286,
      area_type: 'County',
    },
  ],
  409: [
    {
      index: 358,
      name: 'Orlando-Lakeland-Deltona, FL CSA',
      population: 4428098,
      area_type: 'CSA',
    },
    {
      index: 376,
      name: 'Orlando-Kissimmee-Sanford, FL MSA',
      population: 2764182,
      area_type: 'MSA',
    },
    {
      index: 409,
      name: 'Seminole County, Florida',
      population: 478772,
      area_type: 'County',
    },
  ],
  410: [
    {
      index: 358,
      name: 'Orlando-Lakeland-Deltona, FL CSA',
      population: 4428098,
      area_type: 'CSA',
    },
    {
      index: 410,
      name: 'Sumter County, Florida (The Villages, FL MSA)',
      population: 144970,
      area_type: 'MSA',
    },
  ],
  412: [
    {
      index: 358,
      name: 'Orlando-Lakeland-Deltona, FL CSA',
      population: 4428098,
      area_type: 'CSA',
    },
    {
      index: 359,
      name: 'Deltona-Daytona Beach-Ormond Beach, FL MSA',
      population: 705897,
      area_type: 'MSA',
    },
    {
      index: 412,
      name: 'Volusia County, Florida',
      population: 579192,
      area_type: 'County',
    },
  ],
  413: [
    {
      index: 361,
      name: 'Tallahassee, FL MSA',
      population: 390992,
      area_type: 'MSA',
    },
    {
      index: 413,
      name: 'Wakulla County, Florida',
      population: 35178,
      area_type: 'County',
    },
  ],
  414: [
    {
      index: 392,
      name: 'Crestview-Fort Walton Beach-Destin, FL MSA',
      population: 299786,
      area_type: 'MSA',
    },
    {
      index: 414,
      name: 'Walton County, Florida',
      population: 83304,
      area_type: 'County',
    },
  ],
  416: [
    {
      index: 415,
      name: 'Douglas, GA μSA',
      population: 51355,
      area_type: 'μSA',
    },
    {
      index: 416,
      name: 'Atkinson County, Georgia',
      population: 8183,
      area_type: 'County',
    },
  ],
  418: [
    {
      index: 417,
      name: 'Milledgeville, GA μSA',
      population: 52022,
      area_type: 'μSA',
    },
    {
      index: 418,
      name: 'Baldwin County, Georgia',
      population: 43635,
      area_type: 'County',
    },
  ],
  420: [
    {
      index: 15,
      name: 'Atlanta--Athens-Clarke County--Sandy Springs, GA-AL CSA',
      population: 7088898,
      area_type: 'CSA',
    },
    {
      index: 419,
      name: 'Atlanta-Sandy Springs-Alpharetta, GA MSA',
      population: 6222106,
      area_type: 'MSA',
    },
    {
      index: 420,
      name: 'Barrow County, Georgia',
      population: 89299,
      area_type: 'County',
    },
  ],
  421: [
    {
      index: 15,
      name: 'Atlanta--Athens-Clarke County--Sandy Springs, GA-AL CSA',
      population: 7088898,
      area_type: 'CSA',
    },
    {
      index: 419,
      name: 'Atlanta-Sandy Springs-Alpharetta, GA MSA',
      population: 6222106,
      area_type: 'MSA',
    },
    {
      index: 421,
      name: 'Bartow County, Georgia',
      population: 112816,
      area_type: 'County',
    },
  ],
  422: [
    {
      index: 422,
      name: 'Ben Hill County, Georgia (Fitzgerald, GA μSA)',
      population: 17069,
      area_type: 'μSA',
    },
  ],
  426: [
    {
      index: 424,
      name: 'Macon-Bibb County--Warner Robins, GA CSA',
      population: 432109,
      area_type: 'CSA',
    },
    {
      index: 425,
      name: 'Macon-Bibb County, GA MSA',
      population: 233916,
      area_type: 'MSA',
    },
    {
      index: 426,
      name: 'Bibb County, Georgia',
      population: 156197,
      area_type: 'County',
    },
  ],
  428: [
    {
      index: 427,
      name: 'Brunswick, GA MSA',
      population: 114442,
      area_type: 'MSA',
    },
    {
      index: 428,
      name: 'Brantley County, Georgia',
      population: 18183,
      area_type: 'County',
    },
  ],
  430: [
    {
      index: 429,
      name: 'Valdosta, GA MSA',
      population: 149849,
      area_type: 'MSA',
    },
    {
      index: 430,
      name: 'Brooks County, Georgia',
      population: 16253,
      area_type: 'County',
    },
  ],
  433: [
    {
      index: 431,
      name: 'Savannah-Hinesville-Statesboro, GA CSA',
      population: 618706,
      area_type: 'CSA',
    },
    {
      index: 432,
      name: 'Savannah, GA MSA',
      population: 418373,
      area_type: 'MSA',
    },
    {
      index: 433,
      name: 'Bryan County, Georgia',
      population: 48225,
      area_type: 'County',
    },
  ],
  434: [
    {
      index: 431,
      name: 'Savannah-Hinesville-Statesboro, GA CSA',
      population: 618706,
      area_type: 'CSA',
    },
    {
      index: 434,
      name: 'Bulloch County, Georgia (Statesboro, GA μSA)',
      population: 83059,
      area_type: 'μSA',
    },
  ],
  437: [
    {
      index: 436,
      name: 'Augusta-Richmond County, GA-SC MSA',
      population: 624083,
      area_type: 'MSA',
    },
    {
      index: 437,
      name: 'Burke County, Georgia',
      population: 24388,
      area_type: 'County',
    },
  ],
  438: [
    {
      index: 15,
      name: 'Atlanta--Athens-Clarke County--Sandy Springs, GA-AL CSA',
      population: 7088898,
      area_type: 'CSA',
    },
    {
      index: 419,
      name: 'Atlanta-Sandy Springs-Alpharetta, GA MSA',
      population: 6222106,
      area_type: 'MSA',
    },
    {
      index: 438,
      name: 'Butts County, Georgia',
      population: 26649,
      area_type: 'County',
    },
  ],
  439: [
    {
      index: 332,
      name: 'Jacksonville-St. Marys-Palatka, FL-GA CSA',
      population: 1807412,
      area_type: 'CSA',
    },
    {
      index: 439,
      name: 'Camden County, Georgia (St. Marys, GA μSA)',
      population: 57013,
      area_type: 'μSA',
    },
  ],
  441: [
    {
      index: 15,
      name: 'Atlanta--Athens-Clarke County--Sandy Springs, GA-AL CSA',
      population: 7088898,
      area_type: 'CSA',
    },
    {
      index: 419,
      name: 'Atlanta-Sandy Springs-Alpharetta, GA MSA',
      population: 6222106,
      area_type: 'MSA',
    },
    {
      index: 441,
      name: 'Carroll County, Georgia',
      population: 124592,
      area_type: 'County',
    },
  ],
  444: [
    {
      index: 442,
      name: 'Chattanooga-Cleveland-Dalton, TN-GA CSA',
      population: 1018929,
      area_type: 'CSA',
    },
    {
      index: 443,
      name: 'Chattanooga, TN-GA MSA',
      population: 574507,
      area_type: 'MSA',
    },
    {
      index: 444,
      name: 'Catoosa County, Georgia',
      population: 68826,
      area_type: 'County',
    },
  ],
  445: [
    {
      index: 431,
      name: 'Savannah-Hinesville-Statesboro, GA CSA',
      population: 618706,
      area_type: 'CSA',
    },
    {
      index: 432,
      name: 'Savannah, GA MSA',
      population: 418373,
      area_type: 'MSA',
    },
    {
      index: 445,
      name: 'Chatham County, Georgia',
      population: 301107,
      area_type: 'County',
    },
  ],
  446: [
    {
      index: 55,
      name: 'Columbus-Auburn-Opelika, GA-AL CSA',
      population: 504883,
      area_type: 'CSA',
    },
    {
      index: 71,
      name: 'Columbus, GA-AL MSA',
      population: 324110,
      area_type: 'MSA',
    },
    {
      index: 446,
      name: 'Chattahoochee County, Georgia',
      population: 8819,
      area_type: 'County',
    },
  ],
  447: [
    {
      index: 442,
      name: 'Chattanooga-Cleveland-Dalton, TN-GA CSA',
      population: 1018929,
      area_type: 'CSA',
    },
    {
      index: 447,
      name: 'Chattooga County, Georgia (Summerville, GA μSA)',
      population: 24936,
      area_type: 'μSA',
    },
  ],
  449: [
    {
      index: 15,
      name: 'Atlanta--Athens-Clarke County--Sandy Springs, GA-AL CSA',
      population: 7088898,
      area_type: 'CSA',
    },
    {
      index: 419,
      name: 'Atlanta-Sandy Springs-Alpharetta, GA MSA',
      population: 6222106,
      area_type: 'MSA',
    },
    {
      index: 449,
      name: 'Cherokee County, Georgia',
      population: 281278,
      area_type: 'County',
    },
  ],
  451: [
    {
      index: 15,
      name: 'Atlanta--Athens-Clarke County--Sandy Springs, GA-AL CSA',
      population: 7088898,
      area_type: 'CSA',
    },
    {
      index: 450,
      name: 'Athens-Clarke County, GA MSA',
      population: 220405,
      area_type: 'MSA',
    },
    {
      index: 451,
      name: 'Clarke County, Georgia',
      population: 129875,
      area_type: 'County',
    },
  ],
  452: [
    {
      index: 15,
      name: 'Atlanta--Athens-Clarke County--Sandy Springs, GA-AL CSA',
      population: 7088898,
      area_type: 'CSA',
    },
    {
      index: 419,
      name: 'Atlanta-Sandy Springs-Alpharetta, GA MSA',
      population: 6222106,
      area_type: 'MSA',
    },
    {
      index: 452,
      name: 'Clayton County, Georgia',
      population: 296564,
      area_type: 'County',
    },
  ],
  453: [
    {
      index: 15,
      name: 'Atlanta--Athens-Clarke County--Sandy Springs, GA-AL CSA',
      population: 7088898,
      area_type: 'CSA',
    },
    {
      index: 419,
      name: 'Atlanta-Sandy Springs-Alpharetta, GA MSA',
      population: 6222106,
      area_type: 'MSA',
    },
    {
      index: 453,
      name: 'Cobb County, Georgia',
      population: 771952,
      area_type: 'County',
    },
  ],
  454: [
    {
      index: 415,
      name: 'Douglas, GA μSA',
      population: 51355,
      area_type: 'μSA',
    },
    {
      index: 454,
      name: 'Coffee County, Georgia',
      population: 43172,
      area_type: 'County',
    },
  ],
  455: [
    {
      index: 455,
      name: 'Colquitt County, Georgia (Moultrie, GA μSA)',
      population: 45762,
      area_type: 'μSA',
    },
  ],
  457: [
    {
      index: 436,
      name: 'Augusta-Richmond County, GA-SC MSA',
      population: 624083,
      area_type: 'MSA',
    },
    {
      index: 457,
      name: 'Columbia County, Georgia',
      population: 162419,
      area_type: 'County',
    },
  ],
  458: [
    {
      index: 15,
      name: 'Atlanta--Athens-Clarke County--Sandy Springs, GA-AL CSA',
      population: 7088898,
      area_type: 'CSA',
    },
    {
      index: 419,
      name: 'Atlanta-Sandy Springs-Alpharetta, GA MSA',
      population: 6222106,
      area_type: 'MSA',
    },
    {
      index: 458,
      name: 'Coweta County, Georgia',
      population: 152882,
      area_type: 'County',
    },
  ],
  459: [
    {
      index: 424,
      name: 'Macon-Bibb County--Warner Robins, GA CSA',
      population: 432109,
      area_type: 'CSA',
    },
    {
      index: 425,
      name: 'Macon-Bibb County, GA MSA',
      population: 233916,
      area_type: 'MSA',
    },
    {
      index: 459,
      name: 'Crawford County, Georgia',
      population: 12140,
      area_type: 'County',
    },
  ],
  460: [
    {
      index: 460,
      name: 'Crisp County, Georgia (Cordele, GA μSA)',
      population: 19708,
      area_type: 'μSA',
    },
  ],
  462: [
    {
      index: 442,
      name: 'Chattanooga-Cleveland-Dalton, TN-GA CSA',
      population: 1018929,
      area_type: 'CSA',
    },
    {
      index: 443,
      name: 'Chattanooga, TN-GA MSA',
      population: 574507,
      area_type: 'MSA',
    },
    {
      index: 462,
      name: 'Dade County, Georgia',
      population: 16081,
      area_type: 'County',
    },
  ],
  463: [
    {
      index: 15,
      name: 'Atlanta--Athens-Clarke County--Sandy Springs, GA-AL CSA',
      population: 7088898,
      area_type: 'CSA',
    },
    {
      index: 419,
      name: 'Atlanta-Sandy Springs-Alpharetta, GA MSA',
      population: 6222106,
      area_type: 'MSA',
    },
    {
      index: 463,
      name: 'Dawson County, Georgia',
      population: 30138,
      area_type: 'County',
    },
  ],
  464: [
    {
      index: 464,
      name: 'Decatur County, Georgia (Bainbridge, GA μSA)',
      population: 28982,
      area_type: 'μSA',
    },
  ],
  466: [
    {
      index: 15,
      name: 'Atlanta--Athens-Clarke County--Sandy Springs, GA-AL CSA',
      population: 7088898,
      area_type: 'CSA',
    },
    {
      index: 419,
      name: 'Atlanta-Sandy Springs-Alpharetta, GA MSA',
      population: 6222106,
      area_type: 'MSA',
    },
    {
      index: 466,
      name: 'DeKalb County, Georgia',
      population: 762820,
      area_type: 'County',
    },
  ],
  468: [
    {
      index: 467,
      name: 'Albany, GA MSA',
      population: 145786,
      area_type: 'MSA',
    },
    {
      index: 468,
      name: 'Dougherty County, Georgia',
      population: 82966,
      area_type: 'County',
    },
  ],
  469: [
    {
      index: 15,
      name: 'Atlanta--Athens-Clarke County--Sandy Springs, GA-AL CSA',
      population: 7088898,
      area_type: 'CSA',
    },
    {
      index: 419,
      name: 'Atlanta-Sandy Springs-Alpharetta, GA MSA',
      population: 6222106,
      area_type: 'MSA',
    },
    {
      index: 469,
      name: 'Douglas County, Georgia',
      population: 147316,
      area_type: 'County',
    },
  ],
  470: [
    {
      index: 429,
      name: 'Valdosta, GA MSA',
      population: 149849,
      area_type: 'MSA',
    },
    {
      index: 470,
      name: 'Echols County, Georgia',
      population: 3686,
      area_type: 'County',
    },
  ],
  471: [
    {
      index: 431,
      name: 'Savannah-Hinesville-Statesboro, GA CSA',
      population: 618706,
      area_type: 'CSA',
    },
    {
      index: 432,
      name: 'Savannah, GA MSA',
      population: 418373,
      area_type: 'MSA',
    },
    {
      index: 471,
      name: 'Effingham County, Georgia',
      population: 69041,
      area_type: 'County',
    },
  ],
  472: [
    {
      index: 15,
      name: 'Atlanta--Athens-Clarke County--Sandy Springs, GA-AL CSA',
      population: 7088898,
      area_type: 'CSA',
    },
    {
      index: 419,
      name: 'Atlanta-Sandy Springs-Alpharetta, GA MSA',
      population: 6222106,
      area_type: 'MSA',
    },
    {
      index: 472,
      name: 'Fayette County, Georgia',
      population: 122030,
      area_type: 'County',
    },
  ],
  473: [
    {
      index: 15,
      name: 'Atlanta--Athens-Clarke County--Sandy Springs, GA-AL CSA',
      population: 7088898,
      area_type: 'CSA',
    },
    {
      index: 473,
      name: 'Floyd County, Georgia (Rome, GA MSA)',
      population: 99443,
      area_type: 'MSA',
    },
  ],
  475: [
    {
      index: 15,
      name: 'Atlanta--Athens-Clarke County--Sandy Springs, GA-AL CSA',
      population: 7088898,
      area_type: 'CSA',
    },
    {
      index: 419,
      name: 'Atlanta-Sandy Springs-Alpharetta, GA MSA',
      population: 6222106,
      area_type: 'MSA',
    },
    {
      index: 475,
      name: 'Forsyth County, Georgia',
      population: 267237,
      area_type: 'County',
    },
  ],
  476: [
    {
      index: 15,
      name: 'Atlanta--Athens-Clarke County--Sandy Springs, GA-AL CSA',
      population: 7088898,
      area_type: 'CSA',
    },
    {
      index: 419,
      name: 'Atlanta-Sandy Springs-Alpharetta, GA MSA',
      population: 6222106,
      area_type: 'MSA',
    },
    {
      index: 476,
      name: 'Fulton County, Georgia',
      population: 1074634,
      area_type: 'County',
    },
  ],
  477: [
    {
      index: 427,
      name: 'Brunswick, GA MSA',
      population: 114442,
      area_type: 'MSA',
    },
    {
      index: 477,
      name: 'Glynn County, Georgia',
      population: 85079,
      area_type: 'County',
    },
  ],
  478: [
    {
      index: 442,
      name: 'Chattanooga-Cleveland-Dalton, TN-GA CSA',
      population: 1018929,
      area_type: 'CSA',
    },
    {
      index: 478,
      name: 'Gordon County, Georgia (Calhoun, GA μSA)',
      population: 58954,
      area_type: 'μSA',
    },
  ],
  480: [
    {
      index: 15,
      name: 'Atlanta--Athens-Clarke County--Sandy Springs, GA-AL CSA',
      population: 7088898,
      area_type: 'CSA',
    },
    {
      index: 419,
      name: 'Atlanta-Sandy Springs-Alpharetta, GA MSA',
      population: 6222106,
      area_type: 'MSA',
    },
    {
      index: 480,
      name: 'Gwinnett County, Georgia',
      population: 975353,
      area_type: 'County',
    },
  ],
  481: [
    {
      index: 15,
      name: 'Atlanta--Athens-Clarke County--Sandy Springs, GA-AL CSA',
      population: 7088898,
      area_type: 'CSA',
    },
    {
      index: 481,
      name: 'Habersham County, Georgia (Cornelia, GA μSA)',
      population: 47475,
      area_type: 'μSA',
    },
  ],
  483: [
    {
      index: 15,
      name: 'Atlanta--Athens-Clarke County--Sandy Springs, GA-AL CSA',
      population: 7088898,
      area_type: 'CSA',
    },
    {
      index: 483,
      name: 'Hall County, Georgia (Gainesville, GA MSA)',
      population: 212692,
      area_type: 'MSA',
    },
  ],
  485: [
    {
      index: 417,
      name: 'Milledgeville, GA μSA',
      population: 52022,
      area_type: 'μSA',
    },
    {
      index: 485,
      name: 'Hancock County, Georgia',
      population: 8387,
      area_type: 'County',
    },
  ],
  486: [
    {
      index: 15,
      name: 'Atlanta--Athens-Clarke County--Sandy Springs, GA-AL CSA',
      population: 7088898,
      area_type: 'CSA',
    },
    {
      index: 419,
      name: 'Atlanta-Sandy Springs-Alpharetta, GA MSA',
      population: 6222106,
      area_type: 'MSA',
    },
    {
      index: 486,
      name: 'Haralson County, Georgia',
      population: 31337,
      area_type: 'County',
    },
  ],
  487: [
    {
      index: 55,
      name: 'Columbus-Auburn-Opelika, GA-AL CSA',
      population: 504883,
      area_type: 'CSA',
    },
    {
      index: 71,
      name: 'Columbus, GA-AL MSA',
      population: 324110,
      area_type: 'MSA',
    },
    {
      index: 487,
      name: 'Harris County, Georgia',
      population: 36276,
      area_type: 'County',
    },
  ],
  488: [
    {
      index: 15,
      name: 'Atlanta--Athens-Clarke County--Sandy Springs, GA-AL CSA',
      population: 7088898,
      area_type: 'CSA',
    },
    {
      index: 419,
      name: 'Atlanta-Sandy Springs-Alpharetta, GA MSA',
      population: 6222106,
      area_type: 'MSA',
    },
    {
      index: 488,
      name: 'Heard County, Georgia',
      population: 11725,
      area_type: 'County',
    },
  ],
  489: [
    {
      index: 15,
      name: 'Atlanta--Athens-Clarke County--Sandy Springs, GA-AL CSA',
      population: 7088898,
      area_type: 'CSA',
    },
    {
      index: 419,
      name: 'Atlanta-Sandy Springs-Alpharetta, GA MSA',
      population: 6222106,
      area_type: 'MSA',
    },
    {
      index: 489,
      name: 'Henry County, Georgia',
      population: 248364,
      area_type: 'County',
    },
  ],
  491: [
    {
      index: 424,
      name: 'Macon-Bibb County--Warner Robins, GA CSA',
      population: 432109,
      area_type: 'CSA',
    },
    {
      index: 490,
      name: 'Warner Robins, GA MSA',
      population: 198193,
      area_type: 'MSA',
    },
    {
      index: 491,
      name: 'Houston County, Georgia',
      population: 169631,
      area_type: 'County',
    },
  ],
  492: [
    {
      index: 15,
      name: 'Atlanta--Athens-Clarke County--Sandy Springs, GA-AL CSA',
      population: 7088898,
      area_type: 'CSA',
    },
    {
      index: 492,
      name: 'Jackson County, Georgia (Jefferson, GA μSA)',
      population: 83936,
      area_type: 'μSA',
    },
  ],
  494: [
    {
      index: 15,
      name: 'Atlanta--Athens-Clarke County--Sandy Springs, GA-AL CSA',
      population: 7088898,
      area_type: 'CSA',
    },
    {
      index: 419,
      name: 'Atlanta-Sandy Springs-Alpharetta, GA MSA',
      population: 6222106,
      area_type: 'MSA',
    },
    {
      index: 494,
      name: 'Jasper County, Georgia',
      population: 15951,
      area_type: 'County',
    },
  ],
  496: [
    {
      index: 495,
      name: 'Dublin, GA μSA',
      population: 65267,
      area_type: 'μSA',
    },
    {
      index: 496,
      name: 'Johnson County, Georgia',
      population: 9242,
      area_type: 'County',
    },
  ],
  497: [
    {
      index: 424,
      name: 'Macon-Bibb County--Warner Robins, GA CSA',
      population: 432109,
      area_type: 'CSA',
    },
    {
      index: 425,
      name: 'Macon-Bibb County, GA MSA',
      population: 233916,
      area_type: 'MSA',
    },
    {
      index: 497,
      name: 'Jones County, Georgia',
      population: 28472,
      area_type: 'County',
    },
  ],
  498: [
    {
      index: 15,
      name: 'Atlanta--Athens-Clarke County--Sandy Springs, GA-AL CSA',
      population: 7088898,
      area_type: 'CSA',
    },
    {
      index: 419,
      name: 'Atlanta-Sandy Springs-Alpharetta, GA MSA',
      population: 6222106,
      area_type: 'MSA',
    },
    {
      index: 498,
      name: 'Lamar County, Georgia',
      population: 19467,
      area_type: 'County',
    },
  ],
  499: [
    {
      index: 429,
      name: 'Valdosta, GA MSA',
      population: 149849,
      area_type: 'MSA',
    },
    {
      index: 499,
      name: 'Lanier County, Georgia',
      population: 10171,
      area_type: 'County',
    },
  ],
  500: [
    {
      index: 495,
      name: 'Dublin, GA μSA',
      population: 65267,
      area_type: 'μSA',
    },
    {
      index: 500,
      name: 'Laurens County, Georgia',
      population: 49660,
      area_type: 'County',
    },
  ],
  501: [
    {
      index: 467,
      name: 'Albany, GA MSA',
      population: 145786,
      area_type: 'MSA',
    },
    {
      index: 501,
      name: 'Lee County, Georgia',
      population: 33642,
      area_type: 'County',
    },
  ],
  503: [
    {
      index: 431,
      name: 'Savannah-Hinesville-Statesboro, GA CSA',
      population: 618706,
      area_type: 'CSA',
    },
    {
      index: 502,
      name: 'Hinesville, GA MSA',
      population: 86378,
      area_type: 'MSA',
    },
    {
      index: 503,
      name: 'Liberty County, Georgia',
      population: 68030,
      area_type: 'County',
    },
  ],
  504: [
    {
      index: 436,
      name: 'Augusta-Richmond County, GA-SC MSA',
      population: 624083,
      area_type: 'MSA',
    },
    {
      index: 504,
      name: 'Lincoln County, Georgia',
      population: 7841,
      area_type: 'County',
    },
  ],
  505: [
    {
      index: 431,
      name: 'Savannah-Hinesville-Statesboro, GA CSA',
      population: 618706,
      area_type: 'CSA',
    },
    {
      index: 502,
      name: 'Hinesville, GA MSA',
      population: 86378,
      area_type: 'MSA',
    },
    {
      index: 505,
      name: 'Long County, Georgia',
      population: 18348,
      area_type: 'County',
    },
  ],
  506: [
    {
      index: 429,
      name: 'Valdosta, GA MSA',
      population: 149849,
      area_type: 'MSA',
    },
    {
      index: 506,
      name: 'Lowndes County, Georgia',
      population: 119739,
      area_type: 'County',
    },
  ],
  507: [
    {
      index: 436,
      name: 'Augusta-Richmond County, GA-SC MSA',
      population: 624083,
      area_type: 'MSA',
    },
    {
      index: 507,
      name: 'McDuffie County, Georgia',
      population: 21713,
      area_type: 'County',
    },
  ],
  508: [
    {
      index: 427,
      name: 'Brunswick, GA MSA',
      population: 114442,
      area_type: 'MSA',
    },
    {
      index: 508,
      name: 'McIntosh County, Georgia',
      population: 11180,
      area_type: 'County',
    },
  ],
  509: [
    {
      index: 15,
      name: 'Atlanta--Athens-Clarke County--Sandy Springs, GA-AL CSA',
      population: 7088898,
      area_type: 'CSA',
    },
    {
      index: 450,
      name: 'Athens-Clarke County, GA MSA',
      population: 220405,
      area_type: 'MSA',
    },
    {
      index: 509,
      name: 'Madison County, Georgia',
      population: 31473,
      area_type: 'County',
    },
  ],
  510: [
    {
      index: 55,
      name: 'Columbus-Auburn-Opelika, GA-AL CSA',
      population: 504883,
      area_type: 'CSA',
    },
    {
      index: 71,
      name: 'Columbus, GA-AL MSA',
      population: 324110,
      area_type: 'MSA',
    },
    {
      index: 510,
      name: 'Marion County, Georgia',
      population: 7449,
      area_type: 'County',
    },
  ],
  511: [
    {
      index: 15,
      name: 'Atlanta--Athens-Clarke County--Sandy Springs, GA-AL CSA',
      population: 7088898,
      area_type: 'CSA',
    },
    {
      index: 419,
      name: 'Atlanta-Sandy Springs-Alpharetta, GA MSA',
      population: 6222106,
      area_type: 'MSA',
    },
    {
      index: 511,
      name: 'Meriwether County, Georgia',
      population: 20845,
      area_type: 'County',
    },
  ],
  512: [
    {
      index: 424,
      name: 'Macon-Bibb County--Warner Robins, GA CSA',
      population: 432109,
      area_type: 'CSA',
    },
    {
      index: 425,
      name: 'Macon-Bibb County, GA MSA',
      population: 233916,
      area_type: 'MSA',
    },
    {
      index: 512,
      name: 'Monroe County, Georgia',
      population: 29427,
      area_type: 'County',
    },
  ],
  514: [
    {
      index: 513,
      name: 'Vidalia, GA μSA',
      population: 35492,
      area_type: 'μSA',
    },
    {
      index: 514,
      name: 'Montgomery County, Georgia',
      population: 8655,
      area_type: 'County',
    },
  ],
  515: [
    {
      index: 15,
      name: 'Atlanta--Athens-Clarke County--Sandy Springs, GA-AL CSA',
      population: 7088898,
      area_type: 'CSA',
    },
    {
      index: 419,
      name: 'Atlanta-Sandy Springs-Alpharetta, GA MSA',
      population: 6222106,
      area_type: 'MSA',
    },
    {
      index: 515,
      name: 'Morgan County, Georgia',
      population: 21031,
      area_type: 'County',
    },
  ],
  517: [
    {
      index: 442,
      name: 'Chattanooga-Cleveland-Dalton, TN-GA CSA',
      population: 1018929,
      area_type: 'CSA',
    },
    {
      index: 516,
      name: 'Dalton, GA MSA',
      population: 143604,
      area_type: 'MSA',
    },
    {
      index: 517,
      name: 'Murray County, Georgia',
      population: 40472,
      area_type: 'County',
    },
  ],
  518: [
    {
      index: 55,
      name: 'Columbus-Auburn-Opelika, GA-AL CSA',
      population: 504883,
      area_type: 'CSA',
    },
    {
      index: 71,
      name: 'Columbus, GA-AL MSA',
      population: 324110,
      area_type: 'MSA',
    },
    {
      index: 518,
      name: 'Muscogee County, Georgia',
      population: 202616,
      area_type: 'County',
    },
  ],
  519: [
    {
      index: 15,
      name: 'Atlanta--Athens-Clarke County--Sandy Springs, GA-AL CSA',
      population: 7088898,
      area_type: 'CSA',
    },
    {
      index: 419,
      name: 'Atlanta-Sandy Springs-Alpharetta, GA MSA',
      population: 6222106,
      area_type: 'MSA',
    },
    {
      index: 519,
      name: 'Newton County, Georgia',
      population: 117621,
      area_type: 'County',
    },
  ],
  520: [
    {
      index: 15,
      name: 'Atlanta--Athens-Clarke County--Sandy Springs, GA-AL CSA',
      population: 7088898,
      area_type: 'CSA',
    },
    {
      index: 450,
      name: 'Athens-Clarke County, GA MSA',
      population: 220405,
      area_type: 'MSA',
    },
    {
      index: 520,
      name: 'Oconee County, Georgia',
      population: 43588,
      area_type: 'County',
    },
  ],
  521: [
    {
      index: 15,
      name: 'Atlanta--Athens-Clarke County--Sandy Springs, GA-AL CSA',
      population: 7088898,
      area_type: 'CSA',
    },
    {
      index: 450,
      name: 'Athens-Clarke County, GA MSA',
      population: 220405,
      area_type: 'MSA',
    },
    {
      index: 521,
      name: 'Oglethorpe County, Georgia',
      population: 15469,
      area_type: 'County',
    },
  ],
  522: [
    {
      index: 15,
      name: 'Atlanta--Athens-Clarke County--Sandy Springs, GA-AL CSA',
      population: 7088898,
      area_type: 'CSA',
    },
    {
      index: 419,
      name: 'Atlanta-Sandy Springs-Alpharetta, GA MSA',
      population: 6222106,
      area_type: 'MSA',
    },
    {
      index: 522,
      name: 'Paulding County, Georgia',
      population: 178421,
      area_type: 'County',
    },
  ],
  523: [
    {
      index: 424,
      name: 'Macon-Bibb County--Warner Robins, GA CSA',
      population: 432109,
      area_type: 'CSA',
    },
    {
      index: 490,
      name: 'Warner Robins, GA MSA',
      population: 198193,
      area_type: 'MSA',
    },
    {
      index: 523,
      name: 'Peach County, Georgia',
      population: 28562,
      area_type: 'County',
    },
  ],
  524: [
    {
      index: 15,
      name: 'Atlanta--Athens-Clarke County--Sandy Springs, GA-AL CSA',
      population: 7088898,
      area_type: 'CSA',
    },
    {
      index: 419,
      name: 'Atlanta-Sandy Springs-Alpharetta, GA MSA',
      population: 6222106,
      area_type: 'MSA',
    },
    {
      index: 524,
      name: 'Pickens County, Georgia',
      population: 34826,
      area_type: 'County',
    },
  ],
  526: [
    {
      index: 525,
      name: 'Waycross, GA μSA',
      population: 55782,
      area_type: 'μSA',
    },
    {
      index: 526,
      name: 'Pierce County, Georgia',
      population: 20168,
      area_type: 'County',
    },
  ],
  527: [
    {
      index: 15,
      name: 'Atlanta--Athens-Clarke County--Sandy Springs, GA-AL CSA',
      population: 7088898,
      area_type: 'CSA',
    },
    {
      index: 419,
      name: 'Atlanta-Sandy Springs-Alpharetta, GA MSA',
      population: 6222106,
      area_type: 'MSA',
    },
    {
      index: 527,
      name: 'Pike County, Georgia',
      population: 19990,
      area_type: 'County',
    },
  ],
  528: [
    {
      index: 15,
      name: 'Atlanta--Athens-Clarke County--Sandy Springs, GA-AL CSA',
      population: 7088898,
      area_type: 'CSA',
    },
    {
      index: 528,
      name: 'Polk County, Georgia (Cedartown, GA μSA)',
      population: 43709,
      area_type: 'μSA',
    },
  ],
  530: [
    {
      index: 7,
      name: 'Eufaula, AL-GA μSA',
      population: 26955,
      area_type: 'μSA',
    },
    {
      index: 530,
      name: 'Quitman County, Georgia',
      population: 2249,
      area_type: 'County',
    },
  ],
  531: [
    {
      index: 436,
      name: 'Augusta-Richmond County, GA-SC MSA',
      population: 624083,
      area_type: 'MSA',
    },
    {
      index: 531,
      name: 'Richmond County, Georgia',
      population: 206640,
      area_type: 'County',
    },
  ],
  532: [
    {
      index: 15,
      name: 'Atlanta--Athens-Clarke County--Sandy Springs, GA-AL CSA',
      population: 7088898,
      area_type: 'CSA',
    },
    {
      index: 419,
      name: 'Atlanta-Sandy Springs-Alpharetta, GA MSA',
      population: 6222106,
      area_type: 'MSA',
    },
    {
      index: 532,
      name: 'Rockdale County, Georgia',
      population: 94984,
      area_type: 'County',
    },
  ],
  534: [
    {
      index: 533,
      name: 'Americus, GA μSA',
      population: 33373,
      area_type: 'μSA',
    },
    {
      index: 534,
      name: 'Schley County, Georgia',
      population: 4496,
      area_type: 'County',
    },
  ],
  535: [
    {
      index: 15,
      name: 'Atlanta--Athens-Clarke County--Sandy Springs, GA-AL CSA',
      population: 7088898,
      area_type: 'CSA',
    },
    {
      index: 419,
      name: 'Atlanta-Sandy Springs-Alpharetta, GA MSA',
      population: 6222106,
      area_type: 'MSA',
    },
    {
      index: 535,
      name: 'Spalding County, Georgia',
      population: 68919,
      area_type: 'County',
    },
  ],
  536: [
    {
      index: 15,
      name: 'Atlanta--Athens-Clarke County--Sandy Springs, GA-AL CSA',
      population: 7088898,
      area_type: 'CSA',
    },
    {
      index: 536,
      name: 'Stephens County, Georgia (Toccoa, GA μSA)',
      population: 26767,
      area_type: 'μSA',
    },
  ],
  538: [
    {
      index: 55,
      name: 'Columbus-Auburn-Opelika, GA-AL CSA',
      population: 504883,
      area_type: 'CSA',
    },
    {
      index: 71,
      name: 'Columbus, GA-AL MSA',
      population: 324110,
      area_type: 'MSA',
    },
    {
      index: 538,
      name: 'Stewart County, Georgia',
      population: 4648,
      area_type: 'County',
    },
  ],
  539: [
    {
      index: 533,
      name: 'Americus, GA μSA',
      population: 33373,
      area_type: 'μSA',
    },
    {
      index: 539,
      name: 'Sumter County, Georgia',
      population: 28877,
      area_type: 'County',
    },
  ],
  540: [
    {
      index: 55,
      name: 'Columbus-Auburn-Opelika, GA-AL CSA',
      population: 504883,
      area_type: 'CSA',
    },
    {
      index: 71,
      name: 'Columbus, GA-AL MSA',
      population: 324110,
      area_type: 'MSA',
    },
    {
      index: 540,
      name: 'Talbot County, Georgia',
      population: 5747,
      area_type: 'County',
    },
  ],
  541: [
    {
      index: 467,
      name: 'Albany, GA MSA',
      population: 145786,
      area_type: 'MSA',
    },
    {
      index: 541,
      name: 'Terrell County, Georgia',
      population: 8754,
      area_type: 'County',
    },
  ],
  542: [
    {
      index: 542,
      name: 'Thomas County, Georgia (Thomasville, GA μSA)',
      population: 45561,
      area_type: 'μSA',
    },
  ],
  544: [
    {
      index: 544,
      name: 'Tift County, Georgia (Tifton, GA μSA)',
      population: 41412,
      area_type: 'μSA',
    },
  ],
  546: [
    {
      index: 513,
      name: 'Vidalia, GA μSA',
      population: 35492,
      area_type: 'μSA',
    },
    {
      index: 546,
      name: 'Toombs County, Georgia',
      population: 26837,
      area_type: 'County',
    },
  ],
  547: [
    {
      index: 495,
      name: 'Dublin, GA μSA',
      population: 65267,
      area_type: 'μSA',
    },
    {
      index: 547,
      name: 'Treutlen County, Georgia',
      population: 6365,
      area_type: 'County',
    },
  ],
  548: [
    {
      index: 15,
      name: 'Atlanta--Athens-Clarke County--Sandy Springs, GA-AL CSA',
      population: 7088898,
      area_type: 'CSA',
    },
    {
      index: 16,
      name: 'LaGrange, GA-AL μSA',
      population: 104279,
      area_type: 'μSA',
    },
    {
      index: 548,
      name: 'Troup County, Georgia',
      population: 70191,
      area_type: 'County',
    },
  ],
  549: [
    {
      index: 424,
      name: 'Macon-Bibb County--Warner Robins, GA CSA',
      population: 432109,
      area_type: 'CSA',
    },
    {
      index: 425,
      name: 'Macon-Bibb County, GA MSA',
      population: 233916,
      area_type: 'MSA',
    },
    {
      index: 549,
      name: 'Twiggs County, Georgia',
      population: 7680,
      area_type: 'County',
    },
  ],
  550: [
    {
      index: 15,
      name: 'Atlanta--Athens-Clarke County--Sandy Springs, GA-AL CSA',
      population: 7088898,
      area_type: 'CSA',
    },
    {
      index: 550,
      name: 'Upson County, Georgia (Thomaston, GA μSA)',
      population: 28086,
      area_type: 'μSA',
    },
  ],
  552: [
    {
      index: 442,
      name: 'Chattanooga-Cleveland-Dalton, TN-GA CSA',
      population: 1018929,
      area_type: 'CSA',
    },
    {
      index: 443,
      name: 'Chattanooga, TN-GA MSA',
      population: 574507,
      area_type: 'MSA',
    },
    {
      index: 552,
      name: 'Walker County, Georgia',
      population: 68915,
      area_type: 'County',
    },
  ],
  553: [
    {
      index: 15,
      name: 'Atlanta--Athens-Clarke County--Sandy Springs, GA-AL CSA',
      population: 7088898,
      area_type: 'CSA',
    },
    {
      index: 419,
      name: 'Atlanta-Sandy Springs-Alpharetta, GA MSA',
      population: 6222106,
      area_type: 'MSA',
    },
    {
      index: 553,
      name: 'Walton County, Georgia',
      population: 103065,
      area_type: 'County',
    },
  ],
  554: [
    {
      index: 525,
      name: 'Waycross, GA μSA',
      population: 55782,
      area_type: 'μSA',
    },
    {
      index: 554,
      name: 'Ware County, Georgia',
      population: 35614,
      area_type: 'County',
    },
  ],
  555: [
    {
      index: 431,
      name: 'Savannah-Hinesville-Statesboro, GA CSA',
      population: 618706,
      area_type: 'CSA',
    },
    {
      index: 555,
      name: 'Wayne County, Georgia (Jesup, GA μSA)',
      population: 30896,
      area_type: 'μSA',
    },
  ],
  557: [
    {
      index: 442,
      name: 'Chattanooga-Cleveland-Dalton, TN-GA CSA',
      population: 1018929,
      area_type: 'CSA',
    },
    {
      index: 516,
      name: 'Dalton, GA MSA',
      population: 143604,
      area_type: 'MSA',
    },
    {
      index: 557,
      name: 'Whitfield County, Georgia',
      population: 103132,
      area_type: 'County',
    },
  ],
  558: [
    {
      index: 467,
      name: 'Albany, GA MSA',
      population: 145786,
      area_type: 'MSA',
    },
    {
      index: 558,
      name: 'Worth County, Georgia',
      population: 20424,
      area_type: 'County',
    },
  ],
  559: [
    {
      index: 559,
      name: 'Hawaii County, Hawaii (Hilo, HI μSA)',
      population: 206315,
      area_type: 'μSA',
    },
  ],
  561: [
    {
      index: 561,
      name: 'Honolulu County, Hawaii (Urban Honolulu, HI MSA)',
      population: 995638,
      area_type: 'MSA',
    },
  ],
  563: [
    {
      index: 563,
      name: 'Kauai County, Hawaii (Kapaa, HI μSA)',
      population: 73810,
      area_type: 'μSA',
    },
  ],
  565: [
    {
      index: 565,
      name: 'Maui County, Hawaii (Kahului-Wailuku-Lahaina, HI MSA)',
      population: 164351,
      area_type: 'MSA',
    },
  ],
  569: [
    {
      index: 567,
      name: 'Boise City-Mountain Home-Ontario, ID-OR CSA',
      population: 899574,
      area_type: 'CSA',
    },
    {
      index: 568,
      name: 'Boise City, ID MSA',
      population: 811336,
      area_type: 'MSA',
    },
    {
      index: 569,
      name: 'Ada County, Idaho',
      population: 518907,
      area_type: 'County',
    },
  ],
  571: [
    {
      index: 570,
      name: 'Pocatello, ID MSA',
      population: 97585,
      area_type: 'MSA',
    },
    {
      index: 571,
      name: 'Bannock County, Idaho',
      population: 89517,
      area_type: 'County',
    },
  ],
  573: [
    {
      index: 572,
      name: 'Idaho Falls-Rexburg-Blackfoot, ID CSA',
      population: 284485,
      area_type: 'CSA',
    },
    {
      index: 573,
      name: 'Bingham County, Idaho (Blackfoot, ID μSA)',
      population: 49923,
      area_type: 'μSA',
    },
  ],
  576: [
    {
      index: 575,
      name: 'Hailey, ID μSA',
      population: 26019,
      area_type: 'μSA',
    },
    {
      index: 576,
      name: 'Blaine County, Idaho',
      population: 24866,
      area_type: 'County',
    },
  ],
  577: [
    {
      index: 567,
      name: 'Boise City-Mountain Home-Ontario, ID-OR CSA',
      population: 899574,
      area_type: 'CSA',
    },
    {
      index: 568,
      name: 'Boise City, ID MSA',
      population: 811336,
      area_type: 'MSA',
    },
    {
      index: 577,
      name: 'Boise County, Idaho',
      population: 8333,
      area_type: 'County',
    },
  ],
  578: [
    {
      index: 578,
      name: 'Bonner County, Idaho (Sandpoint, ID μSA)',
      population: 51414,
      area_type: 'μSA',
    },
  ],
  581: [
    {
      index: 572,
      name: 'Idaho Falls-Rexburg-Blackfoot, ID CSA',
      population: 284485,
      area_type: 'CSA',
    },
    {
      index: 580,
      name: 'Idaho Falls, ID MSA',
      population: 165608,
      area_type: 'MSA',
    },
    {
      index: 581,
      name: 'Bonneville County, Idaho',
      population: 129496,
      area_type: 'County',
    },
  ],
  582: [
    {
      index: 572,
      name: 'Idaho Falls-Rexburg-Blackfoot, ID CSA',
      population: 284485,
      area_type: 'CSA',
    },
    {
      index: 580,
      name: 'Idaho Falls, ID MSA',
      population: 165608,
      area_type: 'MSA',
    },
    {
      index: 582,
      name: 'Butte County, Idaho',
      population: 2684,
      area_type: 'County',
    },
  ],
  583: [
    {
      index: 575,
      name: 'Hailey, ID μSA',
      population: 26019,
      area_type: 'μSA',
    },
    {
      index: 583,
      name: 'Camas County, Idaho',
      population: 1153,
      area_type: 'County',
    },
  ],
  584: [
    {
      index: 567,
      name: 'Boise City-Mountain Home-Ontario, ID-OR CSA',
      population: 899574,
      area_type: 'CSA',
    },
    {
      index: 568,
      name: 'Boise City, ID MSA',
      population: 811336,
      area_type: 'MSA',
    },
    {
      index: 584,
      name: 'Canyon County, Idaho',
      population: 251065,
      area_type: 'County',
    },
  ],
  586: [
    {
      index: 585,
      name: 'Burley, ID μSA',
      population: 47849,
      area_type: 'μSA',
    },
    {
      index: 586,
      name: 'Cassia County, Idaho',
      population: 25655,
      area_type: 'County',
    },
  ],
  587: [
    {
      index: 567,
      name: 'Boise City-Mountain Home-Ontario, ID-OR CSA',
      population: 899574,
      area_type: 'CSA',
    },
    {
      index: 587,
      name: 'Elmore County, Idaho (Mountain Home, ID μSA)',
      population: 29403,
      area_type: 'μSA',
    },
  ],
  590: [
    {
      index: 589,
      name: 'Logan, UT-ID MSA',
      population: 155362,
      area_type: 'MSA',
    },
    {
      index: 590,
      name: 'Franklin County, Idaho',
      population: 15189,
      area_type: 'County',
    },
  ],
  592: [
    {
      index: 572,
      name: 'Idaho Falls-Rexburg-Blackfoot, ID CSA',
      population: 284485,
      area_type: 'CSA',
    },
    {
      index: 591,
      name: 'Rexburg, ID μSA',
      population: 68954,
      area_type: 'μSA',
    },
    {
      index: 592,
      name: 'Fremont County, Idaho',
      population: 13978,
      area_type: 'County',
    },
  ],
  593: [
    {
      index: 567,
      name: 'Boise City-Mountain Home-Ontario, ID-OR CSA',
      population: 899574,
      area_type: 'CSA',
    },
    {
      index: 568,
      name: 'Boise City, ID MSA',
      population: 811336,
      area_type: 'MSA',
    },
    {
      index: 593,
      name: 'Gem County, Idaho',
      population: 20418,
      area_type: 'County',
    },
  ],
  594: [
    {
      index: 572,
      name: 'Idaho Falls-Rexburg-Blackfoot, ID CSA',
      population: 284485,
      area_type: 'CSA',
    },
    {
      index: 580,
      name: 'Idaho Falls, ID MSA',
      population: 165608,
      area_type: 'MSA',
    },
    {
      index: 594,
      name: 'Jefferson County, Idaho',
      population: 33428,
      area_type: 'County',
    },
  ],
  596: [
    {
      index: 595,
      name: 'Twin Falls, ID MSA',
      population: 119007,
      area_type: 'MSA',
    },
    {
      index: 596,
      name: 'Jerome County, Idaho',
      population: 25311,
      area_type: 'County',
    },
  ],
  598: [
    {
      index: 597,
      name: "Spokane-Spokane Valley-Coeur d'Alene, WA-ID CSA",
      population: 781497,
      area_type: 'CSA',
    },
    {
      index: 598,
      name: "Kootenai County, Idaho (Coeur d'Alene, ID MSA)",
      population: 183578,
      area_type: 'MSA',
    },
  ],
  601: [
    {
      index: 600,
      name: 'Pullman-Moscow, WA-ID CSA',
      population: 88597,
      area_type: 'CSA',
    },
    {
      index: 601,
      name: 'Latah County, Idaho (Moscow, ID μSA)',
      population: 40978,
      area_type: 'μSA',
    },
  ],
  603: [
    {
      index: 572,
      name: 'Idaho Falls-Rexburg-Blackfoot, ID CSA',
      population: 284485,
      area_type: 'CSA',
    },
    {
      index: 591,
      name: 'Rexburg, ID μSA',
      population: 68954,
      area_type: 'μSA',
    },
    {
      index: 603,
      name: 'Madison County, Idaho',
      population: 54976,
      area_type: 'County',
    },
  ],
  604: [
    {
      index: 585,
      name: 'Burley, ID μSA',
      population: 47849,
      area_type: 'μSA',
    },
    {
      index: 604,
      name: 'Minidoka County, Idaho',
      population: 22194,
      area_type: 'County',
    },
  ],
  606: [
    {
      index: 605,
      name: 'Lewiston, ID-WA MSA',
      population: 65512,
      area_type: 'MSA',
    },
    {
      index: 606,
      name: 'Nez Perce County, Idaho',
      population: 43004,
      area_type: 'County',
    },
  ],
  607: [
    {
      index: 567,
      name: 'Boise City-Mountain Home-Ontario, ID-OR CSA',
      population: 899574,
      area_type: 'CSA',
    },
    {
      index: 568,
      name: 'Boise City, ID MSA',
      population: 811336,
      area_type: 'MSA',
    },
    {
      index: 607,
      name: 'Owyhee County, Idaho',
      population: 12613,
      area_type: 'County',
    },
  ],
  609: [
    {
      index: 567,
      name: 'Boise City-Mountain Home-Ontario, ID-OR CSA',
      population: 899574,
      area_type: 'CSA',
    },
    {
      index: 608,
      name: 'Ontario, OR-ID μSA',
      population: 58835,
      area_type: 'μSA',
    },
    {
      index: 609,
      name: 'Payette County, Idaho',
      population: 26956,
      area_type: 'County',
    },
  ],
  610: [
    {
      index: 570,
      name: 'Pocatello, ID MSA',
      population: 97585,
      area_type: 'MSA',
    },
    {
      index: 610,
      name: 'Power County, Idaho',
      population: 8068,
      area_type: 'County',
    },
  ],
  612: [
    {
      index: 611,
      name: 'Jackson, WY-ID μSA',
      population: 35831,
      area_type: 'μSA',
    },
    {
      index: 612,
      name: 'Teton County, Idaho',
      population: 12544,
      area_type: 'County',
    },
  ],
  613: [
    {
      index: 595,
      name: 'Twin Falls, ID MSA',
      population: 119007,
      area_type: 'MSA',
    },
    {
      index: 613,
      name: 'Twin Falls County, Idaho',
      population: 93696,
      area_type: 'County',
    },
  ],
  616: [
    {
      index: 614,
      name: 'Quincy-Hannibal, IL-MO CSA',
      population: 113474,
      area_type: 'CSA',
    },
    {
      index: 615,
      name: 'Quincy, IL-MO μSA',
      population: 74616,
      area_type: 'μSA',
    },
    {
      index: 616,
      name: 'Adams County, Illinois',
      population: 64725,
      area_type: 'County',
    },
  ],
  619: [
    {
      index: 617,
      name: 'Cape Girardeau-Sikeston, MO-IL CSA',
      population: 136115,
      area_type: 'CSA',
    },
    {
      index: 618,
      name: 'Cape Girardeau, MO-IL MSA',
      population: 98275,
      area_type: 'MSA',
    },
    {
      index: 619,
      name: 'Alexander County, Illinois',
      population: 4858,
      area_type: 'County',
    },
  ],
  622: [
    {
      index: 620,
      name: 'St. Louis-St. Charles-Farmington, MO-IL CSA',
      population: 2905202,
      area_type: 'CSA',
    },
    {
      index: 621,
      name: 'St. Louis, MO-IL MSA',
      population: 2801319,
      area_type: 'MSA',
    },
    {
      index: 622,
      name: 'Bond County, Illinois',
      population: 16566,
      area_type: 'County',
    },
  ],
  625: [
    {
      index: 623,
      name: 'Rockford-Freeport-Rochelle, IL CSA',
      population: 430320,
      area_type: 'CSA',
    },
    {
      index: 624,
      name: 'Rockford, IL MSA',
      population: 335342,
      area_type: 'MSA',
    },
    {
      index: 625,
      name: 'Boone County, Illinois',
      population: 53154,
      area_type: 'County',
    },
  ],
  628: [
    {
      index: 626,
      name: 'Chicago-Naperville, IL-IN-WI CSA',
      population: 9806184,
      area_type: 'CSA',
    },
    {
      index: 627,
      name: 'Ottawa, IL μSA',
      population: 146478,
      area_type: 'μSA',
    },
    {
      index: 628,
      name: 'Bureau County, Illinois',
      population: 32828,
      area_type: 'County',
    },
  ],
  629: [
    {
      index: 620,
      name: 'St. Louis-St. Charles-Farmington, MO-IL CSA',
      population: 2905202,
      area_type: 'CSA',
    },
    {
      index: 621,
      name: 'St. Louis, MO-IL MSA',
      population: 2801319,
      area_type: 'MSA',
    },
    {
      index: 629,
      name: 'Calhoun County, Illinois',
      population: 4360,
      area_type: 'County',
    },
  ],
  631: [
    {
      index: 630,
      name: 'Champaign-Urbana, IL MSA',
      population: 223265,
      area_type: 'MSA',
    },
    {
      index: 631,
      name: 'Champaign County, Illinois',
      population: 206542,
      area_type: 'County',
    },
  ],
  633: [
    {
      index: 632,
      name: 'Springfield-Jacksonville-Lincoln, IL CSA',
      population: 304681,
      area_type: 'CSA',
    },
    {
      index: 633,
      name: 'Christian County, Illinois (Taylorville, IL μSA)',
      population: 33436,
      area_type: 'μSA',
    },
  ],
  635: [
    {
      index: 620,
      name: 'St. Louis-St. Charles-Farmington, MO-IL CSA',
      population: 2905202,
      area_type: 'CSA',
    },
    {
      index: 621,
      name: 'St. Louis, MO-IL MSA',
      population: 2801319,
      area_type: 'MSA',
    },
    {
      index: 635,
      name: 'Clinton County, Illinois',
      population: 36909,
      area_type: 'County',
    },
  ],
  637: [
    {
      index: 636,
      name: 'Charleston-Mattoon, IL μSA',
      population: 56658,
      area_type: 'μSA',
    },
    {
      index: 637,
      name: 'Coles County, Illinois',
      population: 46334,
      area_type: 'County',
    },
  ],
  639: [
    {
      index: 626,
      name: 'Chicago-Naperville, IL-IN-WI CSA',
      population: 9806184,
      area_type: 'CSA',
    },
    {
      index: 638,
      name: 'Chicago-Naperville-Elgin, IL-IN-WI MSA',
      population: 9441957,
      area_type: 'MSA',
    },
    {
      index: 639,
      name: 'Cook County, Illinois',
      population: 5109292,
      area_type: 'County',
    },
  ],
  640: [
    {
      index: 636,
      name: 'Charleston-Mattoon, IL μSA',
      population: 56658,
      area_type: 'μSA',
    },
    {
      index: 640,
      name: 'Cumberland County, Illinois',
      population: 10324,
      area_type: 'County',
    },
  ],
  641: [
    {
      index: 626,
      name: 'Chicago-Naperville, IL-IN-WI CSA',
      population: 9806184,
      area_type: 'CSA',
    },
    {
      index: 638,
      name: 'Chicago-Naperville-Elgin, IL-IN-WI MSA',
      population: 9441957,
      area_type: 'MSA',
    },
    {
      index: 641,
      name: 'DeKalb County, Illinois',
      population: 100232,
      area_type: 'County',
    },
  ],
  642: [
    {
      index: 626,
      name: 'Chicago-Naperville, IL-IN-WI CSA',
      population: 9806184,
      area_type: 'CSA',
    },
    {
      index: 638,
      name: 'Chicago-Naperville-Elgin, IL-IN-WI MSA',
      population: 9441957,
      area_type: 'MSA',
    },
    {
      index: 642,
      name: 'DuPage County, Illinois',
      population: 920901,
      area_type: 'County',
    },
  ],
  643: [
    {
      index: 643,
      name: 'Effingham County, Illinois (Effingham, IL μSA)',
      population: 34325,
      area_type: 'μSA',
    },
  ],
  646: [
    {
      index: 645,
      name: 'Peoria, IL MSA',
      population: 396466,
      area_type: 'MSA',
    },
    {
      index: 646,
      name: 'Fulton County, Illinois',
      population: 33021,
      area_type: 'County',
    },
  ],
  647: [
    {
      index: 626,
      name: 'Chicago-Naperville, IL-IN-WI CSA',
      population: 9806184,
      area_type: 'CSA',
    },
    {
      index: 638,
      name: 'Chicago-Naperville-Elgin, IL-IN-WI MSA',
      population: 9441957,
      area_type: 'MSA',
    },
    {
      index: 647,
      name: 'Grundy County, Illinois',
      population: 53041,
      area_type: 'County',
    },
  ],
  650: [
    {
      index: 648,
      name: 'Burlington-Fort Madison-Keokuk, IA-IL-MO CSA',
      population: 101251,
      area_type: 'CSA',
    },
    {
      index: 649,
      name: 'Fort Madison-Keokuk, IA-IL-MO μSA',
      population: 56807,
      area_type: 'μSA',
    },
    {
      index: 650,
      name: 'Hancock County, Illinois',
      population: 17244,
      area_type: 'County',
    },
  ],
  652: [
    {
      index: 648,
      name: 'Burlington-Fort Madison-Keokuk, IA-IL-MO CSA',
      population: 101251,
      area_type: 'CSA',
    },
    {
      index: 651,
      name: 'Burlington, IA-IL μSA',
      population: 44444,
      area_type: 'μSA',
    },
    {
      index: 652,
      name: 'Henderson County, Illinois',
      population: 6151,
      area_type: 'County',
    },
  ],
  655: [
    {
      index: 653,
      name: 'Davenport-Moline, IA-IL CSA',
      population: 468095,
      area_type: 'CSA',
    },
    {
      index: 654,
      name: 'Davenport-Moline-Rock Island, IA-IL MSA',
      population: 379374,
      area_type: 'MSA',
    },
    {
      index: 655,
      name: 'Henry County, Illinois',
      population: 48419,
      area_type: 'County',
    },
  ],
  657: [
    {
      index: 656,
      name: 'Carbondale-Marion, IL MSA',
      population: 132693,
      area_type: 'MSA',
    },
    {
      index: 657,
      name: 'Jackson County, Illinois',
      population: 52617,
      area_type: 'County',
    },
  ],
  658: [
    {
      index: 658,
      name: 'Jefferson County, Illinois (Mount Vernon, IL μSA)',
      population: 36400,
      area_type: 'μSA',
    },
  ],
  660: [
    {
      index: 620,
      name: 'St. Louis-St. Charles-Farmington, MO-IL CSA',
      population: 2905202,
      area_type: 'CSA',
    },
    {
      index: 621,
      name: 'St. Louis, MO-IL MSA',
      population: 2801319,
      area_type: 'MSA',
    },
    {
      index: 660,
      name: 'Jersey County, Illinois',
      population: 21246,
      area_type: 'County',
    },
  ],
  661: [
    {
      index: 656,
      name: 'Carbondale-Marion, IL MSA',
      population: 132693,
      area_type: 'MSA',
    },
    {
      index: 661,
      name: 'Johnson County, Illinois',
      population: 13381,
      area_type: 'County',
    },
  ],
  662: [
    {
      index: 626,
      name: 'Chicago-Naperville, IL-IN-WI CSA',
      population: 9806184,
      area_type: 'CSA',
    },
    {
      index: 638,
      name: 'Chicago-Naperville-Elgin, IL-IN-WI MSA',
      population: 9441957,
      area_type: 'MSA',
    },
    {
      index: 662,
      name: 'Kane County, Illinois',
      population: 514182,
      area_type: 'County',
    },
  ],
  663: [
    {
      index: 626,
      name: 'Chicago-Naperville, IL-IN-WI CSA',
      population: 9806184,
      area_type: 'CSA',
    },
    {
      index: 663,
      name: 'Kankakee County, Illinois (Kankakee, IL MSA)',
      population: 106074,
      area_type: 'MSA',
    },
  ],
  665: [
    {
      index: 626,
      name: 'Chicago-Naperville, IL-IN-WI CSA',
      population: 9806184,
      area_type: 'CSA',
    },
    {
      index: 638,
      name: 'Chicago-Naperville-Elgin, IL-IN-WI MSA',
      population: 9441957,
      area_type: 'MSA',
    },
    {
      index: 665,
      name: 'Kendall County, Illinois',
      population: 137254,
      area_type: 'County',
    },
  ],
  666: [
    {
      index: 666,
      name: 'Knox County, Illinois (Galesburg, IL μSA)',
      population: 48640,
      area_type: 'μSA',
    },
  ],
  668: [
    {
      index: 626,
      name: 'Chicago-Naperville, IL-IN-WI CSA',
      population: 9806184,
      area_type: 'CSA',
    },
    {
      index: 638,
      name: 'Chicago-Naperville-Elgin, IL-IN-WI MSA',
      population: 9441957,
      area_type: 'MSA',
    },
    {
      index: 668,
      name: 'Lake County, Illinois',
      population: 709150,
      area_type: 'County',
    },
  ],
  669: [
    {
      index: 626,
      name: 'Chicago-Naperville, IL-IN-WI CSA',
      population: 9806184,
      area_type: 'CSA',
    },
    {
      index: 627,
      name: 'Ottawa, IL μSA',
      population: 146478,
      area_type: 'μSA',
    },
    {
      index: 669,
      name: 'LaSalle County, Illinois',
      population: 108078,
      area_type: 'County',
    },
  ],
  671: [
    {
      index: 670,
      name: 'Dixon-Sterling, IL CSA',
      population: 88506,
      area_type: 'CSA',
    },
    {
      index: 671,
      name: 'Lee County, Illinois (Dixon, IL μSA)',
      population: 33848,
      area_type: 'μSA',
    },
  ],
  674: [
    {
      index: 673,
      name: 'Bloomington-Pontiac, IL CSA',
      population: 206662,
      area_type: 'CSA',
    },
    {
      index: 674,
      name: 'Livingston County, Illinois (Pontiac, IL μSA)',
      population: 35521,
      area_type: 'μSA',
    },
  ],
  676: [
    {
      index: 632,
      name: 'Springfield-Jacksonville-Lincoln, IL CSA',
      population: 304681,
      area_type: 'CSA',
    },
    {
      index: 676,
      name: 'Logan County, Illinois (Lincoln, IL μSA)',
      population: 27591,
      area_type: 'μSA',
    },
  ],
  678: [
    {
      index: 678,
      name: 'McDonough County, Illinois (Macomb, IL μSA)',
      population: 26861,
      area_type: 'μSA',
    },
  ],
  680: [
    {
      index: 626,
      name: 'Chicago-Naperville, IL-IN-WI CSA',
      population: 9806184,
      area_type: 'CSA',
    },
    {
      index: 638,
      name: 'Chicago-Naperville-Elgin, IL-IN-WI MSA',
      population: 9441957,
      area_type: 'MSA',
    },
    {
      index: 680,
      name: 'McHenry County, Illinois',
      population: 311747,
      area_type: 'County',
    },
  ],
  681: [
    {
      index: 673,
      name: 'Bloomington-Pontiac, IL CSA',
      population: 206662,
      area_type: 'CSA',
    },
    {
      index: 681,
      name: 'McLean County, Illinois (Bloomington, IL MSA)',
      population: 171141,
      area_type: 'MSA',
    },
  ],
  683: [
    {
      index: 683,
      name: 'Macon County, Illinois (Decatur, IL MSA)',
      population: 101483,
      area_type: 'MSA',
    },
  ],
  685: [
    {
      index: 620,
      name: 'St. Louis-St. Charles-Farmington, MO-IL CSA',
      population: 2905202,
      area_type: 'CSA',
    },
    {
      index: 621,
      name: 'St. Louis, MO-IL MSA',
      population: 2801319,
      area_type: 'MSA',
    },
    {
      index: 685,
      name: 'Macoupin County, Illinois',
      population: 44245,
      area_type: 'County',
    },
  ],
  686: [
    {
      index: 620,
      name: 'St. Louis-St. Charles-Farmington, MO-IL CSA',
      population: 2905202,
      area_type: 'CSA',
    },
    {
      index: 621,
      name: 'St. Louis, MO-IL MSA',
      population: 2801319,
      area_type: 'MSA',
    },
    {
      index: 686,
      name: 'Madison County, Illinois',
      population: 263864,
      area_type: 'County',
    },
  ],
  687: [
    {
      index: 620,
      name: 'St. Louis-St. Charles-Farmington, MO-IL CSA',
      population: 2905202,
      area_type: 'CSA',
    },
    {
      index: 687,
      name: 'Marion County, Illinois (Centralia, IL μSA)',
      population: 36914,
      area_type: 'μSA',
    },
  ],
  689: [
    {
      index: 645,
      name: 'Peoria, IL MSA',
      population: 396466,
      area_type: 'MSA',
    },
    {
      index: 689,
      name: 'Marshall County, Illinois',
      population: 11678,
      area_type: 'County',
    },
  ],
  692: [
    {
      index: 690,
      name: 'Paducah-Mayfield, KY-IL CSA',
      population: 134411,
      area_type: 'CSA',
    },
    {
      index: 691,
      name: 'Paducah, KY-IL μSA',
      population: 97999,
      area_type: 'μSA',
    },
    {
      index: 692,
      name: 'Massac County, Illinois',
      population: 13896,
      area_type: 'County',
    },
  ],
  694: [
    {
      index: 632,
      name: 'Springfield-Jacksonville-Lincoln, IL CSA',
      population: 304681,
      area_type: 'CSA',
    },
    {
      index: 693,
      name: 'Springfield, IL MSA',
      population: 206655,
      area_type: 'MSA',
    },
    {
      index: 694,
      name: 'Menard County, Illinois',
      population: 12121,
      area_type: 'County',
    },
  ],
  695: [
    {
      index: 653,
      name: 'Davenport-Moline, IA-IL CSA',
      population: 468095,
      area_type: 'CSA',
    },
    {
      index: 654,
      name: 'Davenport-Moline-Rock Island, IA-IL MSA',
      population: 379374,
      area_type: 'MSA',
    },
    {
      index: 695,
      name: 'Mercer County, Illinois',
      population: 15504,
      area_type: 'County',
    },
  ],
  696: [
    {
      index: 620,
      name: 'St. Louis-St. Charles-Farmington, MO-IL CSA',
      population: 2905202,
      area_type: 'CSA',
    },
    {
      index: 621,
      name: 'St. Louis, MO-IL MSA',
      population: 2801319,
      area_type: 'MSA',
    },
    {
      index: 696,
      name: 'Monroe County, Illinois',
      population: 35033,
      area_type: 'County',
    },
  ],
  698: [
    {
      index: 632,
      name: 'Springfield-Jacksonville-Lincoln, IL CSA',
      population: 304681,
      area_type: 'CSA',
    },
    {
      index: 697,
      name: 'Jacksonville, IL μSA',
      population: 36999,
      area_type: 'μSA',
    },
    {
      index: 698,
      name: 'Morgan County, Illinois',
      population: 32209,
      area_type: 'County',
    },
  ],
  699: [
    {
      index: 623,
      name: 'Rockford-Freeport-Rochelle, IL CSA',
      population: 430320,
      area_type: 'CSA',
    },
    {
      index: 699,
      name: 'Ogle County, Illinois (Rochelle, IL μSA)',
      population: 51351,
      area_type: 'μSA',
    },
  ],
  701: [
    {
      index: 645,
      name: 'Peoria, IL MSA',
      population: 396466,
      area_type: 'MSA',
    },
    {
      index: 701,
      name: 'Peoria County, Illinois',
      population: 178383,
      area_type: 'County',
    },
  ],
  702: [
    {
      index: 630,
      name: 'Champaign-Urbana, IL MSA',
      population: 223265,
      area_type: 'MSA',
    },
    {
      index: 702,
      name: 'Piatt County, Illinois',
      population: 16723,
      area_type: 'County',
    },
  ],
  703: [
    {
      index: 626,
      name: 'Chicago-Naperville, IL-IN-WI CSA',
      population: 9806184,
      area_type: 'CSA',
    },
    {
      index: 627,
      name: 'Ottawa, IL μSA',
      population: 146478,
      area_type: 'μSA',
    },
    {
      index: 703,
      name: 'Putnam County, Illinois',
      population: 5572,
      area_type: 'County',
    },
  ],
  704: [
    {
      index: 653,
      name: 'Davenport-Moline, IA-IL CSA',
      population: 468095,
      area_type: 'CSA',
    },
    {
      index: 654,
      name: 'Davenport-Moline-Rock Island, IA-IL MSA',
      population: 379374,
      area_type: 'MSA',
    },
    {
      index: 704,
      name: 'Rock Island County, Illinois',
      population: 141527,
      area_type: 'County',
    },
  ],
  705: [
    {
      index: 620,
      name: 'St. Louis-St. Charles-Farmington, MO-IL CSA',
      population: 2905202,
      area_type: 'CSA',
    },
    {
      index: 621,
      name: 'St. Louis, MO-IL MSA',
      population: 2801319,
      area_type: 'MSA',
    },
    {
      index: 705,
      name: 'St. Clair County, Illinois',
      population: 252671,
      area_type: 'County',
    },
  ],
  706: [
    {
      index: 632,
      name: 'Springfield-Jacksonville-Lincoln, IL CSA',
      population: 304681,
      area_type: 'CSA',
    },
    {
      index: 693,
      name: 'Springfield, IL MSA',
      population: 206655,
      area_type: 'MSA',
    },
    {
      index: 706,
      name: 'Sangamon County, Illinois',
      population: 194534,
      area_type: 'County',
    },
  ],
  707: [
    {
      index: 632,
      name: 'Springfield-Jacksonville-Lincoln, IL CSA',
      population: 304681,
      area_type: 'CSA',
    },
    {
      index: 697,
      name: 'Jacksonville, IL μSA',
      population: 36999,
      area_type: 'μSA',
    },
    {
      index: 707,
      name: 'Scott County, Illinois',
      population: 4790,
      area_type: 'County',
    },
  ],
  708: [
    {
      index: 645,
      name: 'Peoria, IL MSA',
      population: 396466,
      area_type: 'MSA',
    },
    {
      index: 708,
      name: 'Stark County, Illinois',
      population: 5345,
      area_type: 'County',
    },
  ],
  709: [
    {
      index: 623,
      name: 'Rockford-Freeport-Rochelle, IL CSA',
      population: 430320,
      area_type: 'CSA',
    },
    {
      index: 709,
      name: 'Stephenson County, Illinois (Freeport, IL μSA)',
      population: 43627,
      area_type: 'μSA',
    },
  ],
  711: [
    {
      index: 645,
      name: 'Peoria, IL MSA',
      population: 396466,
      area_type: 'MSA',
    },
    {
      index: 711,
      name: 'Tazewell County, Illinois',
      population: 129911,
      area_type: 'County',
    },
  ],
  712: [
    {
      index: 712,
      name: 'Vermilion County, Illinois (Danville, IL MSA)',
      population: 72337,
      area_type: 'MSA',
    },
  ],
  714: [
    {
      index: 670,
      name: 'Dixon-Sterling, IL CSA',
      population: 88506,
      area_type: 'CSA',
    },
    {
      index: 714,
      name: 'Whiteside County, Illinois (Sterling, IL μSA)',
      population: 54658,
      area_type: 'μSA',
    },
  ],
  716: [
    {
      index: 626,
      name: 'Chicago-Naperville, IL-IN-WI CSA',
      population: 9806184,
      area_type: 'CSA',
    },
    {
      index: 638,
      name: 'Chicago-Naperville-Elgin, IL-IN-WI MSA',
      population: 9441957,
      area_type: 'MSA',
    },
    {
      index: 716,
      name: 'Will County, Illinois',
      population: 696757,
      area_type: 'County',
    },
  ],
  717: [
    {
      index: 656,
      name: 'Carbondale-Marion, IL MSA',
      population: 132693,
      area_type: 'MSA',
    },
    {
      index: 717,
      name: 'Williamson County, Illinois',
      population: 66695,
      area_type: 'County',
    },
  ],
  718: [
    {
      index: 623,
      name: 'Rockford-Freeport-Rochelle, IL CSA',
      population: 430320,
      area_type: 'CSA',
    },
    {
      index: 624,
      name: 'Rockford, IL MSA',
      population: 335342,
      area_type: 'MSA',
    },
    {
      index: 718,
      name: 'Winnebago County, Illinois',
      population: 282188,
      area_type: 'County',
    },
  ],
  719: [
    {
      index: 645,
      name: 'Peoria, IL MSA',
      population: 396466,
      area_type: 'MSA',
    },
    {
      index: 719,
      name: 'Woodford County, Illinois',
      population: 38128,
      area_type: 'County',
    },
  ],
  721: [
    {
      index: 720,
      name: 'Fort Wayne-Huntington-Auburn, IN CSA',
      population: 653136,
      area_type: 'CSA',
    },
    {
      index: 721,
      name: 'Adams County, Indiana (Decatur, IN μSA)',
      population: 36068,
      area_type: 'μSA',
    },
  ],
  724: [
    {
      index: 720,
      name: 'Fort Wayne-Huntington-Auburn, IN CSA',
      population: 653136,
      area_type: 'CSA',
    },
    {
      index: 723,
      name: 'Fort Wayne, IN MSA',
      population: 426076,
      area_type: 'MSA',
    },
    {
      index: 724,
      name: 'Allen County, Indiana',
      population: 391449,
      area_type: 'County',
    },
  ],
  726: [
    {
      index: 725,
      name: 'Indianapolis-Carmel-Muncie, IN CSA',
      population: 2524790,
      area_type: 'CSA',
    },
    {
      index: 726,
      name: 'Bartholomew County, Indiana (Columbus, IN MSA)',
      population: 83540,
      area_type: 'MSA',
    },
  ],
  730: [
    {
      index: 728,
      name: 'Lafayette-West Lafayette-Frankfort, IN CSA',
      population: 259295,
      area_type: 'CSA',
    },
    {
      index: 729,
      name: 'Lafayette-West Lafayette, IN MSA',
      population: 226452,
      area_type: 'MSA',
    },
    {
      index: 730,
      name: 'Benton County, Indiana',
      population: 8719,
      area_type: 'County',
    },
  ],
  732: [
    {
      index: 725,
      name: 'Indianapolis-Carmel-Muncie, IN CSA',
      population: 2524790,
      area_type: 'CSA',
    },
    {
      index: 731,
      name: 'Indianapolis-Carmel-Anderson, IN MSA',
      population: 2141779,
      area_type: 'MSA',
    },
    {
      index: 732,
      name: 'Boone County, Indiana',
      population: 74164,
      area_type: 'County',
    },
  ],
  733: [
    {
      index: 725,
      name: 'Indianapolis-Carmel-Muncie, IN CSA',
      population: 2524790,
      area_type: 'CSA',
    },
    {
      index: 731,
      name: 'Indianapolis-Carmel-Anderson, IN MSA',
      population: 2141779,
      area_type: 'MSA',
    },
    {
      index: 733,
      name: 'Brown County, Indiana',
      population: 15570,
      area_type: 'County',
    },
  ],
  734: [
    {
      index: 728,
      name: 'Lafayette-West Lafayette-Frankfort, IN CSA',
      population: 259295,
      area_type: 'CSA',
    },
    {
      index: 729,
      name: 'Lafayette-West Lafayette, IN MSA',
      population: 226452,
      area_type: 'MSA',
    },
    {
      index: 734,
      name: 'Carroll County, Indiana',
      population: 20555,
      area_type: 'County',
    },
  ],
  735: [
    {
      index: 735,
      name: 'Cass County, Indiana (Logansport, IN μSA)',
      population: 37540,
      area_type: 'μSA',
    },
  ],
  739: [
    {
      index: 737,
      name: 'Louisville/Jefferson County--Elizabethtown--Bardstown, KY-IN CSA',
      population: 1513559,
      area_type: 'CSA',
    },
    {
      index: 738,
      name: 'Louisville/Jefferson County, KY-IN MSA',
      population: 1284553,
      area_type: 'MSA',
    },
    {
      index: 739,
      name: 'Clark County, Indiana',
      population: 124237,
      area_type: 'County',
    },
  ],
  741: [
    {
      index: 740,
      name: 'Terre Haute, IN MSA',
      population: 184875,
      area_type: 'MSA',
    },
    {
      index: 741,
      name: 'Clay County, Indiana',
      population: 26379,
      area_type: 'County',
    },
  ],
  742: [
    {
      index: 728,
      name: 'Lafayette-West Lafayette-Frankfort, IN CSA',
      population: 259295,
      area_type: 'CSA',
    },
    {
      index: 742,
      name: 'Clinton County, Indiana (Frankfort, IN μSA)',
      population: 32843,
      area_type: 'μSA',
    },
  ],
  744: [
    {
      index: 744,
      name: 'Daviess County, Indiana (Washington, IN μSA)',
      population: 33418,
      area_type: 'μSA',
    },
  ],
  748: [
    {
      index: 746,
      name: 'Cincinnati-Wilmington-Maysville, OH-KY-IN CSA',
      population: 2323945,
      area_type: 'CSA',
    },
    {
      index: 747,
      name: 'Cincinnati, OH-KY-IN MSA',
      population: 2265051,
      area_type: 'MSA',
    },
    {
      index: 748,
      name: 'Dearborn County, Indiana',
      population: 51138,
      area_type: 'County',
    },
  ],
  749: [
    {
      index: 725,
      name: 'Indianapolis-Carmel-Muncie, IN CSA',
      population: 2524790,
      area_type: 'CSA',
    },
    {
      index: 749,
      name: 'Decatur County, Indiana (Greensburg, IN μSA)',
      population: 26416,
      area_type: 'μSA',
    },
  ],
  751: [
    {
      index: 720,
      name: 'Fort Wayne-Huntington-Auburn, IN CSA',
      population: 653136,
      area_type: 'CSA',
    },
    {
      index: 751,
      name: 'DeKalb County, Indiana (Auburn, IN μSA)',
      population: 43731,
      area_type: 'μSA',
    },
  ],
  753: [
    {
      index: 725,
      name: 'Indianapolis-Carmel-Muncie, IN CSA',
      population: 2524790,
      area_type: 'CSA',
    },
    {
      index: 753,
      name: 'Delaware County, Indiana (Muncie, IN MSA)',
      population: 112031,
      area_type: 'MSA',
    },
  ],
  756: [
    {
      index: 755,
      name: 'Jasper, IN μSA',
      population: 55800,
      area_type: 'μSA',
    },
    {
      index: 756,
      name: 'Dubois County, Indiana',
      population: 43632,
      area_type: 'County',
    },
  ],
  758: [
    {
      index: 757,
      name: 'South Bend-Elkhart-Mishawaka, IN-MI CSA',
      population: 810585,
      area_type: 'CSA',
    },
    {
      index: 758,
      name: 'Elkhart County, Indiana (Elkhart-Goshen, IN MSA)',
      population: 206890,
      area_type: 'MSA',
    },
  ],
  761: [
    {
      index: 760,
      name: 'Richmond-Connersville, IN CSA',
      population: 89622,
      area_type: 'CSA',
    },
    {
      index: 761,
      name: 'Fayette County, Indiana (Connersville, IN μSA)',
      population: 23349,
      area_type: 'μSA',
    },
  ],
  763: [
    {
      index: 737,
      name: 'Louisville/Jefferson County--Elizabethtown--Bardstown, KY-IN CSA',
      population: 1513559,
      area_type: 'CSA',
    },
    {
      index: 738,
      name: 'Louisville/Jefferson County, KY-IN MSA',
      population: 1284553,
      area_type: 'MSA',
    },
    {
      index: 763,
      name: 'Floyd County, Indiana',
      population: 80714,
      area_type: 'County',
    },
  ],
  764: [
    {
      index: 746,
      name: 'Cincinnati-Wilmington-Maysville, OH-KY-IN CSA',
      population: 2323945,
      area_type: 'CSA',
    },
    {
      index: 747,
      name: 'Cincinnati, OH-KY-IN MSA',
      population: 2265051,
      area_type: 'MSA',
    },
    {
      index: 764,
      name: 'Franklin County, Indiana',
      population: 23028,
      area_type: 'County',
    },
  ],
  765: [
    {
      index: 765,
      name: 'Grant County, Indiana (Marion, IN μSA)',
      population: 66022,
      area_type: 'μSA',
    },
  ],
  767: [
    {
      index: 725,
      name: 'Indianapolis-Carmel-Muncie, IN CSA',
      population: 2524790,
      area_type: 'CSA',
    },
    {
      index: 731,
      name: 'Indianapolis-Carmel-Anderson, IN MSA',
      population: 2141779,
      area_type: 'MSA',
    },
    {
      index: 767,
      name: 'Hamilton County, Indiana',
      population: 364921,
      area_type: 'County',
    },
  ],
  768: [
    {
      index: 725,
      name: 'Indianapolis-Carmel-Muncie, IN CSA',
      population: 2524790,
      area_type: 'CSA',
    },
    {
      index: 731,
      name: 'Indianapolis-Carmel-Anderson, IN MSA',
      population: 2141779,
      area_type: 'MSA',
    },
    {
      index: 768,
      name: 'Hancock County, Indiana',
      population: 83070,
      area_type: 'County',
    },
  ],
  769: [
    {
      index: 737,
      name: 'Louisville/Jefferson County--Elizabethtown--Bardstown, KY-IN CSA',
      population: 1513559,
      area_type: 'CSA',
    },
    {
      index: 738,
      name: 'Louisville/Jefferson County, KY-IN MSA',
      population: 1284553,
      area_type: 'MSA',
    },
    {
      index: 769,
      name: 'Harrison County, Indiana',
      population: 39851,
      area_type: 'County',
    },
  ],
  770: [
    {
      index: 725,
      name: 'Indianapolis-Carmel-Muncie, IN CSA',
      population: 2524790,
      area_type: 'CSA',
    },
    {
      index: 731,
      name: 'Indianapolis-Carmel-Anderson, IN MSA',
      population: 2141779,
      area_type: 'MSA',
    },
    {
      index: 770,
      name: 'Hendricks County, Indiana',
      population: 182534,
      area_type: 'County',
    },
  ],
  771: [
    {
      index: 725,
      name: 'Indianapolis-Carmel-Muncie, IN CSA',
      population: 2524790,
      area_type: 'CSA',
    },
    {
      index: 771,
      name: 'Henry County, Indiana (New Castle, IN μSA)',
      population: 48915,
      area_type: 'μSA',
    },
  ],
  774: [
    {
      index: 773,
      name: 'Kokomo-Peru, IN CSA',
      population: 119248,
      area_type: 'CSA',
    },
    {
      index: 774,
      name: 'Howard County, Indiana (Kokomo, IN MSA)',
      population: 83574,
      area_type: 'MSA',
    },
  ],
  776: [
    {
      index: 720,
      name: 'Fort Wayne-Huntington-Auburn, IN CSA',
      population: 653136,
      area_type: 'CSA',
    },
    {
      index: 776,
      name: 'Huntington County, Indiana (Huntington, IN μSA)',
      population: 36834,
      area_type: 'μSA',
    },
  ],
  778: [
    {
      index: 725,
      name: 'Indianapolis-Carmel-Muncie, IN CSA',
      population: 2524790,
      area_type: 'CSA',
    },
    {
      index: 778,
      name: 'Jackson County, Indiana (Seymour, IN μSA)',
      population: 46300,
      area_type: 'μSA',
    },
  ],
  780: [
    {
      index: 626,
      name: 'Chicago-Naperville, IL-IN-WI CSA',
      population: 9806184,
      area_type: 'CSA',
    },
    {
      index: 638,
      name: 'Chicago-Naperville-Elgin, IL-IN-WI MSA',
      population: 9441957,
      area_type: 'MSA',
    },
    {
      index: 780,
      name: 'Jasper County, Indiana',
      population: 33281,
      area_type: 'County',
    },
  ],
  781: [
    {
      index: 781,
      name: 'Jefferson County, Indiana (Madison, IN μSA)',
      population: 32946,
      area_type: 'μSA',
    },
  ],
  783: [
    {
      index: 725,
      name: 'Indianapolis-Carmel-Muncie, IN CSA',
      population: 2524790,
      area_type: 'CSA',
    },
    {
      index: 783,
      name: 'Jennings County, Indiana (North Vernon, IN μSA)',
      population: 27536,
      area_type: 'μSA',
    },
  ],
  785: [
    {
      index: 725,
      name: 'Indianapolis-Carmel-Muncie, IN CSA',
      population: 2524790,
      area_type: 'CSA',
    },
    {
      index: 731,
      name: 'Indianapolis-Carmel-Anderson, IN MSA',
      population: 2141779,
      area_type: 'MSA',
    },
    {
      index: 785,
      name: 'Johnson County, Indiana',
      population: 165782,
      area_type: 'County',
    },
  ],
  786: [
    {
      index: 786,
      name: 'Knox County, Indiana (Vincennes, IN μSA)',
      population: 35789,
      area_type: 'μSA',
    },
  ],
  788: [
    {
      index: 757,
      name: 'South Bend-Elkhart-Mishawaka, IN-MI CSA',
      population: 810585,
      area_type: 'CSA',
    },
    {
      index: 788,
      name: 'Kosciusko County, Indiana (Warsaw, IN μSA)',
      population: 80826,
      area_type: 'μSA',
    },
  ],
  790: [
    {
      index: 626,
      name: 'Chicago-Naperville, IL-IN-WI CSA',
      population: 9806184,
      area_type: 'CSA',
    },
    {
      index: 638,
      name: 'Chicago-Naperville-Elgin, IL-IN-WI MSA',
      population: 9441957,
      area_type: 'MSA',
    },
    {
      index: 790,
      name: 'Lake County, Indiana',
      population: 499689,
      area_type: 'County',
    },
  ],
  791: [
    {
      index: 626,
      name: 'Chicago-Naperville, IL-IN-WI CSA',
      population: 9806184,
      area_type: 'CSA',
    },
    {
      index: 791,
      name: 'LaPorte County, Indiana (Michigan City-La Porte, IN MSA)',
      population: 111675,
      area_type: 'MSA',
    },
  ],
  794: [
    {
      index: 793,
      name: 'Bloomington-Bedford, IN CSA',
      population: 206449,
      area_type: 'CSA',
    },
    {
      index: 794,
      name: 'Lawrence County, Indiana (Bedford, IN μSA)',
      population: 45222,
      area_type: 'μSA',
    },
  ],
  796: [
    {
      index: 725,
      name: 'Indianapolis-Carmel-Muncie, IN CSA',
      population: 2524790,
      area_type: 'CSA',
    },
    {
      index: 731,
      name: 'Indianapolis-Carmel-Anderson, IN MSA',
      population: 2141779,
      area_type: 'MSA',
    },
    {
      index: 796,
      name: 'Madison County, Indiana',
      population: 131744,
      area_type: 'County',
    },
  ],
  797: [
    {
      index: 725,
      name: 'Indianapolis-Carmel-Muncie, IN CSA',
      population: 2524790,
      area_type: 'CSA',
    },
    {
      index: 731,
      name: 'Indianapolis-Carmel-Anderson, IN MSA',
      population: 2141779,
      area_type: 'MSA',
    },
    {
      index: 797,
      name: 'Marion County, Indiana',
      population: 969466,
      area_type: 'County',
    },
  ],
  798: [
    {
      index: 757,
      name: 'South Bend-Elkhart-Mishawaka, IN-MI CSA',
      population: 810585,
      area_type: 'CSA',
    },
    {
      index: 798,
      name: 'Marshall County, Indiana (Plymouth, IN μSA)',
      population: 46332,
      area_type: 'μSA',
    },
  ],
  800: [
    {
      index: 773,
      name: 'Kokomo-Peru, IN CSA',
      population: 119248,
      area_type: 'CSA',
    },
    {
      index: 800,
      name: 'Miami County, Indiana (Peru, IN μSA)',
      population: 35674,
      area_type: 'μSA',
    },
  ],
  803: [
    {
      index: 793,
      name: 'Bloomington-Bedford, IN CSA',
      population: 206449,
      area_type: 'CSA',
    },
    {
      index: 802,
      name: 'Bloomington, IN MSA',
      population: 161227,
      area_type: 'MSA',
    },
    {
      index: 803,
      name: 'Monroe County, Indiana',
      population: 139745,
      area_type: 'County',
    },
  ],
  804: [
    {
      index: 725,
      name: 'Indianapolis-Carmel-Muncie, IN CSA',
      population: 2524790,
      area_type: 'CSA',
    },
    {
      index: 804,
      name: 'Montgomery County, Indiana (Crawfordsville, IN μSA)',
      population: 38273,
      area_type: 'μSA',
    },
  ],
  806: [
    {
      index: 725,
      name: 'Indianapolis-Carmel-Muncie, IN CSA',
      population: 2524790,
      area_type: 'CSA',
    },
    {
      index: 731,
      name: 'Indianapolis-Carmel-Anderson, IN MSA',
      population: 2141779,
      area_type: 'MSA',
    },
    {
      index: 806,
      name: 'Morgan County, Indiana',
      population: 72236,
      area_type: 'County',
    },
  ],
  807: [
    {
      index: 626,
      name: 'Chicago-Naperville, IL-IN-WI CSA',
      population: 9806184,
      area_type: 'CSA',
    },
    {
      index: 638,
      name: 'Chicago-Naperville-Elgin, IL-IN-WI MSA',
      population: 9441957,
      area_type: 'MSA',
    },
    {
      index: 807,
      name: 'Newton County, Indiana',
      population: 13823,
      area_type: 'County',
    },
  ],
  808: [
    {
      index: 720,
      name: 'Fort Wayne-Huntington-Auburn, IN CSA',
      population: 653136,
      area_type: 'CSA',
    },
    {
      index: 808,
      name: 'Noble County, Indiana (Kendallville, IN μSA)',
      population: 47367,
      area_type: 'μSA',
    },
  ],
  810: [
    {
      index: 746,
      name: 'Cincinnati-Wilmington-Maysville, OH-KY-IN CSA',
      population: 2323945,
      area_type: 'CSA',
    },
    {
      index: 747,
      name: 'Cincinnati, OH-KY-IN MSA',
      population: 2265051,
      area_type: 'MSA',
    },
    {
      index: 810,
      name: 'Ohio County, Indiana',
      population: 6114,
      area_type: 'County',
    },
  ],
  811: [
    {
      index: 793,
      name: 'Bloomington-Bedford, IN CSA',
      population: 206449,
      area_type: 'CSA',
    },
    {
      index: 802,
      name: 'Bloomington, IN MSA',
      population: 161227,
      area_type: 'MSA',
    },
    {
      index: 811,
      name: 'Owen County, Indiana',
      population: 21482,
      area_type: 'County',
    },
  ],
  812: [
    {
      index: 740,
      name: 'Terre Haute, IN MSA',
      population: 184875,
      area_type: 'MSA',
    },
    {
      index: 812,
      name: 'Parke County, Indiana',
      population: 16369,
      area_type: 'County',
    },
  ],
  813: [
    {
      index: 755,
      name: 'Jasper, IN μSA',
      population: 55800,
      area_type: 'μSA',
    },
    {
      index: 813,
      name: 'Pike County, Indiana',
      population: 12168,
      area_type: 'County',
    },
  ],
  814: [
    {
      index: 626,
      name: 'Chicago-Naperville, IL-IN-WI CSA',
      population: 9806184,
      area_type: 'CSA',
    },
    {
      index: 638,
      name: 'Chicago-Naperville-Elgin, IL-IN-WI MSA',
      population: 9441957,
      area_type: 'MSA',
    },
    {
      index: 814,
      name: 'Porter County, Indiana',
      population: 174791,
      area_type: 'County',
    },
  ],
  816: [
    {
      index: 815,
      name: 'Evansville, IN-KY MSA',
      population: 314038,
      area_type: 'MSA',
    },
    {
      index: 816,
      name: 'Posey County, Indiana',
      population: 25063,
      area_type: 'County',
    },
  ],
  817: [
    {
      index: 725,
      name: 'Indianapolis-Carmel-Muncie, IN CSA',
      population: 2524790,
      area_type: 'CSA',
    },
    {
      index: 731,
      name: 'Indianapolis-Carmel-Anderson, IN MSA',
      population: 2141779,
      area_type: 'MSA',
    },
    {
      index: 817,
      name: 'Putnam County, Indiana',
      population: 37301,
      area_type: 'County',
    },
  ],
  819: [
    {
      index: 757,
      name: 'South Bend-Elkhart-Mishawaka, IN-MI CSA',
      population: 810585,
      area_type: 'CSA',
    },
    {
      index: 818,
      name: 'South Bend-Mishawaka, IN-MI MSA',
      population: 323637,
      area_type: 'MSA',
    },
    {
      index: 819,
      name: 'St. Joseph County, Indiana',
      population: 272234,
      area_type: 'County',
    },
  ],
  820: [
    {
      index: 737,
      name: 'Louisville/Jefferson County--Elizabethtown--Bardstown, KY-IN CSA',
      population: 1513559,
      area_type: 'CSA',
    },
    {
      index: 820,
      name: 'Scott County, Indiana (Scottsburg, IN μSA)',
      population: 24588,
      area_type: 'μSA',
    },
  ],
  822: [
    {
      index: 725,
      name: 'Indianapolis-Carmel-Muncie, IN CSA',
      population: 2524790,
      area_type: 'CSA',
    },
    {
      index: 731,
      name: 'Indianapolis-Carmel-Anderson, IN MSA',
      population: 2141779,
      area_type: 'MSA',
    },
    {
      index: 822,
      name: 'Shelby County, Indiana',
      population: 44991,
      area_type: 'County',
    },
  ],
  823: [
    {
      index: 720,
      name: 'Fort Wayne-Huntington-Auburn, IN CSA',
      population: 653136,
      area_type: 'CSA',
    },
    {
      index: 823,
      name: 'Steuben County, Indiana (Angola, IN μSA)',
      population: 34725,
      area_type: 'μSA',
    },
  ],
  825: [
    {
      index: 740,
      name: 'Terre Haute, IN MSA',
      population: 184875,
      area_type: 'MSA',
    },
    {
      index: 825,
      name: 'Sullivan County, Indiana',
      population: 20670,
      area_type: 'County',
    },
  ],
  826: [
    {
      index: 728,
      name: 'Lafayette-West Lafayette-Frankfort, IN CSA',
      population: 259295,
      area_type: 'CSA',
    },
    {
      index: 729,
      name: 'Lafayette-West Lafayette, IN MSA',
      population: 226452,
      area_type: 'MSA',
    },
    {
      index: 826,
      name: 'Tippecanoe County, Indiana',
      population: 188717,
      area_type: 'County',
    },
  ],
  827: [
    {
      index: 746,
      name: 'Cincinnati-Wilmington-Maysville, OH-KY-IN CSA',
      population: 2323945,
      area_type: 'CSA',
    },
    {
      index: 747,
      name: 'Cincinnati, OH-KY-IN MSA',
      population: 2265051,
      area_type: 'MSA',
    },
    {
      index: 827,
      name: 'Union County, Indiana',
      population: 6952,
      area_type: 'County',
    },
  ],
  828: [
    {
      index: 815,
      name: 'Evansville, IN-KY MSA',
      population: 314038,
      area_type: 'MSA',
    },
    {
      index: 828,
      name: 'Vanderburgh County, Indiana',
      population: 179744,
      area_type: 'County',
    },
  ],
  829: [
    {
      index: 740,
      name: 'Terre Haute, IN MSA',
      population: 184875,
      area_type: 'MSA',
    },
    {
      index: 829,
      name: 'Vermillion County, Indiana',
      population: 15451,
      area_type: 'County',
    },
  ],
  830: [
    {
      index: 740,
      name: 'Terre Haute, IN MSA',
      population: 184875,
      area_type: 'MSA',
    },
    {
      index: 830,
      name: 'Vigo County, Indiana',
      population: 106006,
      area_type: 'County',
    },
  ],
  831: [
    {
      index: 831,
      name: 'Wabash County, Indiana (Wabash, IN μSA)',
      population: 30828,
      area_type: 'μSA',
    },
  ],
  833: [
    {
      index: 728,
      name: 'Lafayette-West Lafayette-Frankfort, IN CSA',
      population: 259295,
      area_type: 'CSA',
    },
    {
      index: 729,
      name: 'Lafayette-West Lafayette, IN MSA',
      population: 226452,
      area_type: 'MSA',
    },
    {
      index: 833,
      name: 'Warren County, Indiana',
      population: 8461,
      area_type: 'County',
    },
  ],
  834: [
    {
      index: 815,
      name: 'Evansville, IN-KY MSA',
      population: 314038,
      area_type: 'MSA',
    },
    {
      index: 834,
      name: 'Warrick County, Indiana',
      population: 65185,
      area_type: 'County',
    },
  ],
  835: [
    {
      index: 737,
      name: 'Louisville/Jefferson County--Elizabethtown--Bardstown, KY-IN CSA',
      population: 1513559,
      area_type: 'CSA',
    },
    {
      index: 738,
      name: 'Louisville/Jefferson County, KY-IN MSA',
      population: 1284553,
      area_type: 'MSA',
    },
    {
      index: 835,
      name: 'Washington County, Indiana',
      population: 28224,
      area_type: 'County',
    },
  ],
  836: [
    {
      index: 760,
      name: 'Richmond-Connersville, IN CSA',
      population: 89622,
      area_type: 'CSA',
    },
    {
      index: 836,
      name: 'Wayne County, Indiana (Richmond, IN μSA)',
      population: 66273,
      area_type: 'μSA',
    },
  ],
  838: [
    {
      index: 720,
      name: 'Fort Wayne-Huntington-Auburn, IN CSA',
      population: 653136,
      area_type: 'CSA',
    },
    {
      index: 838,
      name: 'Wells County, Indiana (Bluffton, IN μSA)',
      population: 28335,
      area_type: 'μSA',
    },
  ],
  840: [
    {
      index: 720,
      name: 'Fort Wayne-Huntington-Auburn, IN CSA',
      population: 653136,
      area_type: 'CSA',
    },
    {
      index: 723,
      name: 'Fort Wayne, IN MSA',
      population: 426076,
      area_type: 'MSA',
    },
    {
      index: 840,
      name: 'Whitley County, Indiana',
      population: 34627,
      area_type: 'County',
    },
  ],
  843: [
    {
      index: 841,
      name: 'Cedar Rapids-Iowa City, IA CSA',
      population: 454583,
      area_type: 'CSA',
    },
    {
      index: 842,
      name: 'Cedar Rapids, IA MSA',
      population: 275592,
      area_type: 'MSA',
    },
    {
      index: 843,
      name: 'Benton County, Iowa',
      population: 25711,
      area_type: 'County',
    },
  ],
  845: [
    {
      index: 844,
      name: 'Waterloo-Cedar Falls, IA MSA',
      population: 167889,
      area_type: 'MSA',
    },
    {
      index: 845,
      name: 'Black Hawk County, Iowa',
      population: 130274,
      area_type: 'County',
    },
  ],
  848: [
    {
      index: 846,
      name: 'Des Moines-Ames-West Des Moines, IA CSA',
      population: 910923,
      area_type: 'CSA',
    },
    {
      index: 847,
      name: 'Ames, IA MSA',
      population: 126282,
      area_type: 'MSA',
    },
    {
      index: 848,
      name: 'Boone County, Iowa',
      population: 26609,
      area_type: 'County',
    },
  ],
  849: [
    {
      index: 844,
      name: 'Waterloo-Cedar Falls, IA MSA',
      population: 167889,
      area_type: 'MSA',
    },
    {
      index: 849,
      name: 'Bremer County, Iowa',
      population: 25259,
      area_type: 'County',
    },
  ],
  850: [
    {
      index: 850,
      name: 'Buena Vista County, Iowa (Storm Lake, IA μSA)',
      population: 20600,
      area_type: 'μSA',
    },
  ],
  852: [
    {
      index: 852,
      name: 'Carroll County, Iowa (Carroll, IA μSA)',
      population: 20567,
      area_type: 'μSA',
    },
  ],
  855: [
    {
      index: 854,
      name: 'Mason City, IA μSA',
      population: 49728,
      area_type: 'μSA',
    },
    {
      index: 855,
      name: 'Cerro Gordo County, Iowa',
      population: 42409,
      area_type: 'County',
    },
  ],
  857: [
    {
      index: 856,
      name: 'Spencer-Spirit Lake, IA CSA',
      population: 34503,
      area_type: 'CSA',
    },
    {
      index: 857,
      name: 'Clay County, Iowa (Spencer, IA μSA)',
      population: 16475,
      area_type: 'μSA',
    },
  ],
  859: [
    {
      index: 653,
      name: 'Davenport-Moline, IA-IL CSA',
      population: 468095,
      area_type: 'CSA',
    },
    {
      index: 859,
      name: 'Clinton County, Iowa (Clinton, IA μSA)',
      population: 46344,
      area_type: 'μSA',
    },
  ],
  862: [
    {
      index: 846,
      name: 'Des Moines-Ames-West Des Moines, IA CSA',
      population: 910923,
      area_type: 'CSA',
    },
    {
      index: 861,
      name: 'Des Moines-West Des Moines, IA MSA',
      population: 729053,
      area_type: 'MSA',
    },
    {
      index: 862,
      name: 'Dallas County, Iowa',
      population: 108016,
      area_type: 'County',
    },
  ],
  863: [
    {
      index: 648,
      name: 'Burlington-Fort Madison-Keokuk, IA-IL-MO CSA',
      population: 101251,
      area_type: 'CSA',
    },
    {
      index: 651,
      name: 'Burlington, IA-IL μSA',
      population: 44444,
      area_type: 'μSA',
    },
    {
      index: 863,
      name: 'Des Moines County, Iowa',
      population: 38293,
      area_type: 'County',
    },
  ],
  864: [
    {
      index: 856,
      name: 'Spencer-Spirit Lake, IA CSA',
      population: 34503,
      area_type: 'CSA',
    },
    {
      index: 864,
      name: 'Dickinson County, Iowa (Spirit Lake, IA μSA)',
      population: 18028,
      area_type: 'μSA',
    },
  ],
  866: [
    {
      index: 866,
      name: 'Dubuque County, Iowa (Dubuque, IA MSA)',
      population: 98677,
      area_type: 'MSA',
    },
  ],
  868: [
    {
      index: 844,
      name: 'Waterloo-Cedar Falls, IA MSA',
      population: 167889,
      area_type: 'MSA',
    },
    {
      index: 868,
      name: 'Grundy County, Iowa',
      population: 12356,
      area_type: 'County',
    },
  ],
  869: [
    {
      index: 846,
      name: 'Des Moines-Ames-West Des Moines, IA CSA',
      population: 910923,
      area_type: 'CSA',
    },
    {
      index: 861,
      name: 'Des Moines-West Des Moines, IA MSA',
      population: 729053,
      area_type: 'MSA',
    },
    {
      index: 869,
      name: 'Guthrie County, Iowa',
      population: 10647,
      area_type: 'County',
    },
  ],
  872: [
    {
      index: 870,
      name: 'Omaha-Council Bluffs-Fremont, NE-IA CSA',
      population: 1013668,
      area_type: 'CSA',
    },
    {
      index: 871,
      name: 'Omaha-Council Bluffs, NE-IA MSA',
      population: 976671,
      area_type: 'MSA',
    },
    {
      index: 872,
      name: 'Harrison County, Iowa',
      population: 14658,
      area_type: 'County',
    },
  ],
  873: [
    {
      index: 846,
      name: 'Des Moines-Ames-West Des Moines, IA CSA',
      population: 910923,
      area_type: 'CSA',
    },
    {
      index: 861,
      name: 'Des Moines-West Des Moines, IA MSA',
      population: 729053,
      area_type: 'MSA',
    },
    {
      index: 873,
      name: 'Jasper County, Iowa',
      population: 37938,
      area_type: 'County',
    },
  ],
  874: [
    {
      index: 874,
      name: 'Jefferson County, Iowa (Fairfield, IA μSA)',
      population: 15698,
      area_type: 'μSA',
    },
  ],
  877: [
    {
      index: 841,
      name: 'Cedar Rapids-Iowa City, IA CSA',
      population: 454583,
      area_type: 'CSA',
    },
    {
      index: 876,
      name: 'Iowa City, IA MSA',
      population: 178991,
      area_type: 'MSA',
    },
    {
      index: 877,
      name: 'Johnson County, Iowa',
      population: 156420,
      area_type: 'County',
    },
  ],
  878: [
    {
      index: 841,
      name: 'Cedar Rapids-Iowa City, IA CSA',
      population: 454583,
      area_type: 'CSA',
    },
    {
      index: 842,
      name: 'Cedar Rapids, IA MSA',
      population: 275592,
      area_type: 'MSA',
    },
    {
      index: 878,
      name: 'Jones County, Iowa',
      population: 20848,
      area_type: 'County',
    },
  ],
  879: [
    {
      index: 648,
      name: 'Burlington-Fort Madison-Keokuk, IA-IL-MO CSA',
      population: 101251,
      area_type: 'CSA',
    },
    {
      index: 649,
      name: 'Fort Madison-Keokuk, IA-IL-MO μSA',
      population: 56807,
      area_type: 'μSA',
    },
    {
      index: 879,
      name: 'Lee County, Iowa',
      population: 32840,
      area_type: 'County',
    },
  ],
  880: [
    {
      index: 841,
      name: 'Cedar Rapids-Iowa City, IA CSA',
      population: 454583,
      area_type: 'CSA',
    },
    {
      index: 842,
      name: 'Cedar Rapids, IA MSA',
      population: 275592,
      area_type: 'MSA',
    },
    {
      index: 880,
      name: 'Linn County, Iowa',
      population: 229033,
      area_type: 'County',
    },
  ],
  881: [
    {
      index: 846,
      name: 'Des Moines-Ames-West Des Moines, IA CSA',
      population: 910923,
      area_type: 'CSA',
    },
    {
      index: 861,
      name: 'Des Moines-West Des Moines, IA MSA',
      population: 729053,
      area_type: 'MSA',
    },
    {
      index: 881,
      name: 'Madison County, Iowa',
      population: 17036,
      area_type: 'County',
    },
  ],
  882: [
    {
      index: 846,
      name: 'Des Moines-Ames-West Des Moines, IA CSA',
      population: 910923,
      area_type: 'CSA',
    },
    {
      index: 882,
      name: 'Mahaska County, Iowa (Oskaloosa, IA μSA)',
      population: 21946,
      area_type: 'μSA',
    },
  ],
  884: [
    {
      index: 846,
      name: 'Des Moines-Ames-West Des Moines, IA CSA',
      population: 910923,
      area_type: 'CSA',
    },
    {
      index: 884,
      name: 'Marion County, Iowa (Pella, IA μSA)',
      population: 33642,
      area_type: 'μSA',
    },
  ],
  886: [
    {
      index: 886,
      name: 'Marshall County, Iowa (Marshalltown, IA μSA)',
      population: 39879,
      area_type: 'μSA',
    },
  ],
  888: [
    {
      index: 870,
      name: 'Omaha-Council Bluffs-Fremont, NE-IA CSA',
      population: 1013668,
      area_type: 'CSA',
    },
    {
      index: 871,
      name: 'Omaha-Council Bluffs, NE-IA MSA',
      population: 976671,
      area_type: 'MSA',
    },
    {
      index: 888,
      name: 'Mills County, Iowa',
      population: 14553,
      area_type: 'County',
    },
  ],
  889: [
    {
      index: 653,
      name: 'Davenport-Moline, IA-IL CSA',
      population: 468095,
      area_type: 'CSA',
    },
    {
      index: 889,
      name: 'Muscatine County, Iowa (Muscatine, IA μSA)',
      population: 42377,
      area_type: 'μSA',
    },
  ],
  891: [
    {
      index: 846,
      name: 'Des Moines-Ames-West Des Moines, IA CSA',
      population: 910923,
      area_type: 'CSA',
    },
    {
      index: 861,
      name: 'Des Moines-West Des Moines, IA MSA',
      population: 729053,
      area_type: 'MSA',
    },
    {
      index: 891,
      name: 'Polk County, Iowa',
      population: 501089,
      area_type: 'County',
    },
  ],
  892: [
    {
      index: 870,
      name: 'Omaha-Council Bluffs-Fremont, NE-IA CSA',
      population: 1013668,
      area_type: 'CSA',
    },
    {
      index: 871,
      name: 'Omaha-Council Bluffs, NE-IA MSA',
      population: 976671,
      area_type: 'MSA',
    },
    {
      index: 892,
      name: 'Pottawattamie County, Iowa',
      population: 93173,
      area_type: 'County',
    },
  ],
  893: [
    {
      index: 653,
      name: 'Davenport-Moline, IA-IL CSA',
      population: 468095,
      area_type: 'CSA',
    },
    {
      index: 654,
      name: 'Davenport-Moline-Rock Island, IA-IL MSA',
      population: 379374,
      area_type: 'MSA',
    },
    {
      index: 893,
      name: 'Scott County, Iowa',
      population: 173924,
      area_type: 'County',
    },
  ],
  894: [
    {
      index: 846,
      name: 'Des Moines-Ames-West Des Moines, IA CSA',
      population: 910923,
      area_type: 'CSA',
    },
    {
      index: 847,
      name: 'Ames, IA MSA',
      population: 126282,
      area_type: 'MSA',
    },
    {
      index: 894,
      name: 'Story County, Iowa',
      population: 99673,
      area_type: 'County',
    },
  ],
  895: [
    {
      index: 895,
      name: 'Wapello County, Iowa (Ottumwa, IA μSA)',
      population: 35043,
      area_type: 'μSA',
    },
  ],
  897: [
    {
      index: 846,
      name: 'Des Moines-Ames-West Des Moines, IA CSA',
      population: 910923,
      area_type: 'CSA',
    },
    {
      index: 861,
      name: 'Des Moines-West Des Moines, IA MSA',
      population: 729053,
      area_type: 'MSA',
    },
    {
      index: 897,
      name: 'Warren County, Iowa',
      population: 54327,
      area_type: 'County',
    },
  ],
  898: [
    {
      index: 841,
      name: 'Cedar Rapids-Iowa City, IA CSA',
      population: 454583,
      area_type: 'CSA',
    },
    {
      index: 876,
      name: 'Iowa City, IA MSA',
      population: 178991,
      area_type: 'MSA',
    },
    {
      index: 898,
      name: 'Washington County, Iowa',
      population: 22571,
      area_type: 'County',
    },
  ],
  899: [
    {
      index: 899,
      name: 'Webster County, Iowa (Fort Dodge, IA μSA)',
      population: 36626,
      area_type: 'μSA',
    },
  ],
  902: [
    {
      index: 901,
      name: 'Sioux City, IA-NE-SD MSA',
      population: 149240,
      area_type: 'MSA',
    },
    {
      index: 902,
      name: 'Woodbury County, Iowa',
      population: 105671,
      area_type: 'County',
    },
  ],
  903: [
    {
      index: 854,
      name: 'Mason City, IA μSA',
      population: 49728,
      area_type: 'μSA',
    },
    {
      index: 903,
      name: 'Worth County, Iowa',
      population: 7319,
      area_type: 'County',
    },
  ],
  905: [
    {
      index: 904,
      name: 'Kansas City-Overland Park-Kansas City, MO-KS CSA',
      population: 2545616,
      area_type: 'CSA',
    },
    {
      index: 905,
      name: 'Atchison County, Kansas (Atchison, KS μSA)',
      population: 16108,
      area_type: 'μSA',
    },
  ],
  907: [
    {
      index: 907,
      name: 'Barton County, Kansas (Great Bend, KS μSA)',
      population: 25080,
      area_type: 'μSA',
    },
  ],
  911: [
    {
      index: 909,
      name: 'Wichita-Winfield, KS CSA',
      population: 684492,
      area_type: 'CSA',
    },
    {
      index: 910,
      name: 'Wichita, KS MSA',
      population: 650039,
      area_type: 'MSA',
    },
    {
      index: 911,
      name: 'Butler County, Kansas',
      population: 68240,
      area_type: 'County',
    },
  ],
  913: [
    {
      index: 912,
      name: 'Emporia, KS μSA',
      population: 34446,
      area_type: 'μSA',
    },
    {
      index: 913,
      name: 'Chase County, Kansas',
      population: 2548,
      area_type: 'County',
    },
  ],
  914: [
    {
      index: 909,
      name: 'Wichita-Winfield, KS CSA',
      population: 684492,
      area_type: 'CSA',
    },
    {
      index: 914,
      name: 'Cowley County, Kansas (Winfield, KS μSA)',
      population: 34453,
      area_type: 'μSA',
    },
  ],
  916: [
    {
      index: 916,
      name: 'Crawford County, Kansas (Pittsburg, KS μSA)',
      population: 39078,
      area_type: 'μSA',
    },
  ],
  919: [
    {
      index: 904,
      name: 'Kansas City-Overland Park-Kansas City, MO-KS CSA',
      population: 2545616,
      area_type: 'CSA',
    },
    {
      index: 918,
      name: 'St. Joseph, MO-KS MSA',
      population: 119690,
      area_type: 'MSA',
    },
    {
      index: 919,
      name: 'Doniphan County, Kansas',
      population: 7440,
      area_type: 'County',
    },
  ],
  920: [
    {
      index: 904,
      name: 'Kansas City-Overland Park-Kansas City, MO-KS CSA',
      population: 2545616,
      area_type: 'CSA',
    },
    {
      index: 920,
      name: 'Douglas County, Kansas (Lawrence, KS MSA)',
      population: 119964,
      area_type: 'MSA',
    },
  ],
  922: [
    {
      index: 922,
      name: 'Ellis County, Kansas (Hays, KS μSA)',
      population: 28941,
      area_type: 'μSA',
    },
  ],
  925: [
    {
      index: 924,
      name: 'Garden City, KS μSA',
      population: 41505,
      area_type: 'μSA',
    },
    {
      index: 925,
      name: 'Finney County, Kansas',
      population: 37650,
      area_type: 'County',
    },
  ],
  926: [
    {
      index: 926,
      name: 'Ford County, Kansas (Dodge City, KS μSA)',
      population: 33848,
      area_type: 'μSA',
    },
  ],
  928: [
    {
      index: 904,
      name: 'Kansas City-Overland Park-Kansas City, MO-KS CSA',
      population: 2545616,
      area_type: 'CSA',
    },
    {
      index: 928,
      name: 'Franklin County, Kansas (Ottawa, KS μSA)',
      population: 25992,
      area_type: 'μSA',
    },
  ],
  931: [
    {
      index: 930,
      name: 'Manhattan, KS MSA',
      population: 133072,
      area_type: 'MSA',
    },
    {
      index: 931,
      name: 'Geary County, Kansas',
      population: 35691,
      area_type: 'County',
    },
  ],
  932: [
    {
      index: 909,
      name: 'Wichita-Winfield, KS CSA',
      population: 684492,
      area_type: 'CSA',
    },
    {
      index: 910,
      name: 'Wichita, KS MSA',
      population: 650039,
      area_type: 'MSA',
    },
    {
      index: 932,
      name: 'Harvey County, Kansas',
      population: 33801,
      area_type: 'County',
    },
  ],
  934: [
    {
      index: 933,
      name: 'Topeka, KS MSA',
      population: 231783,
      area_type: 'MSA',
    },
    {
      index: 934,
      name: 'Jackson County, Kansas',
      population: 13286,
      area_type: 'County',
    },
  ],
  935: [
    {
      index: 933,
      name: 'Topeka, KS MSA',
      population: 231783,
      area_type: 'MSA',
    },
    {
      index: 935,
      name: 'Jefferson County, Kansas',
      population: 18344,
      area_type: 'County',
    },
  ],
  937: [
    {
      index: 904,
      name: 'Kansas City-Overland Park-Kansas City, MO-KS CSA',
      population: 2545616,
      area_type: 'CSA',
    },
    {
      index: 936,
      name: 'Kansas City, MO-KS MSA',
      population: 2209494,
      area_type: 'MSA',
    },
    {
      index: 937,
      name: 'Johnson County, Kansas',
      population: 619195,
      area_type: 'County',
    },
  ],
  938: [
    {
      index: 924,
      name: 'Garden City, KS μSA',
      population: 41505,
      area_type: 'μSA',
    },
    {
      index: 938,
      name: 'Kearny County, Kansas',
      population: 3855,
      area_type: 'County',
    },
  ],
  939: [
    {
      index: 939,
      name: 'Labette County, Kansas (Parsons, KS μSA)',
      population: 19757,
      area_type: 'μSA',
    },
  ],
  941: [
    {
      index: 904,
      name: 'Kansas City-Overland Park-Kansas City, MO-KS CSA',
      population: 2545616,
      area_type: 'CSA',
    },
    {
      index: 936,
      name: 'Kansas City, MO-KS MSA',
      population: 2209494,
      area_type: 'MSA',
    },
    {
      index: 941,
      name: 'Leavenworth County, Kansas',
      population: 82892,
      area_type: 'County',
    },
  ],
  942: [
    {
      index: 904,
      name: 'Kansas City-Overland Park-Kansas City, MO-KS CSA',
      population: 2545616,
      area_type: 'CSA',
    },
    {
      index: 936,
      name: 'Kansas City, MO-KS MSA',
      population: 2209494,
      area_type: 'MSA',
    },
    {
      index: 942,
      name: 'Linn County, Kansas',
      population: 9796,
      area_type: 'County',
    },
  ],
  943: [
    {
      index: 912,
      name: 'Emporia, KS μSA',
      population: 34446,
      area_type: 'μSA',
    },
    {
      index: 943,
      name: 'Lyon County, Kansas',
      population: 31898,
      area_type: 'County',
    },
  ],
  944: [
    {
      index: 944,
      name: 'McPherson County, Kansas (McPherson, KS μSA)',
      population: 30012,
      area_type: 'μSA',
    },
  ],
  946: [
    {
      index: 904,
      name: 'Kansas City-Overland Park-Kansas City, MO-KS CSA',
      population: 2545616,
      area_type: 'CSA',
    },
    {
      index: 936,
      name: 'Kansas City, MO-KS MSA',
      population: 2209494,
      area_type: 'MSA',
    },
    {
      index: 946,
      name: 'Miami County, Kansas',
      population: 34867,
      area_type: 'County',
    },
  ],
  947: [
    {
      index: 947,
      name: 'Montgomery County, Kansas (Coffeyville, KS μSA)',
      population: 30996,
      area_type: 'μSA',
    },
  ],
  949: [
    {
      index: 933,
      name: 'Topeka, KS MSA',
      population: 231783,
      area_type: 'MSA',
    },
    {
      index: 949,
      name: 'Osage County, Kansas',
      population: 15654,
      area_type: 'County',
    },
  ],
  951: [
    {
      index: 950,
      name: 'Salina, KS μSA',
      population: 59391,
      area_type: 'μSA',
    },
    {
      index: 951,
      name: 'Ottawa County, Kansas',
      population: 5795,
      area_type: 'County',
    },
  ],
  952: [
    {
      index: 930,
      name: 'Manhattan, KS MSA',
      population: 133072,
      area_type: 'MSA',
    },
    {
      index: 952,
      name: 'Pottawatomie County, Kansas',
      population: 26273,
      area_type: 'County',
    },
  ],
  953: [
    {
      index: 953,
      name: 'Reno County, Kansas (Hutchinson, KS μSA)',
      population: 61516,
      area_type: 'μSA',
    },
  ],
  955: [
    {
      index: 930,
      name: 'Manhattan, KS MSA',
      population: 133072,
      area_type: 'MSA',
    },
    {
      index: 955,
      name: 'Riley County, Kansas',
      population: 71108,
      area_type: 'County',
    },
  ],
  956: [
    {
      index: 950,
      name: 'Salina, KS μSA',
      population: 59391,
      area_type: 'μSA',
    },
    {
      index: 956,
      name: 'Saline County, Kansas',
      population: 53596,
      area_type: 'County',
    },
  ],
  957: [
    {
      index: 909,
      name: 'Wichita-Winfield, KS CSA',
      population: 684492,
      area_type: 'CSA',
    },
    {
      index: 910,
      name: 'Wichita, KS MSA',
      population: 650039,
      area_type: 'MSA',
    },
    {
      index: 957,
      name: 'Sedgwick County, Kansas',
      population: 525525,
      area_type: 'County',
    },
  ],
  958: [
    {
      index: 958,
      name: 'Seward County, Kansas (Liberal, KS μSA)',
      population: 21358,
      area_type: 'μSA',
    },
  ],
  960: [
    {
      index: 933,
      name: 'Topeka, KS MSA',
      population: 231783,
      area_type: 'MSA',
    },
    {
      index: 960,
      name: 'Shawnee County, Kansas',
      population: 177480,
      area_type: 'County',
    },
  ],
  961: [
    {
      index: 909,
      name: 'Wichita-Winfield, KS CSA',
      population: 684492,
      area_type: 'CSA',
    },
    {
      index: 910,
      name: 'Wichita, KS MSA',
      population: 650039,
      area_type: 'MSA',
    },
    {
      index: 961,
      name: 'Sumner County, Kansas',
      population: 22473,
      area_type: 'County',
    },
  ],
  962: [
    {
      index: 933,
      name: 'Topeka, KS MSA',
      population: 231783,
      area_type: 'MSA',
    },
    {
      index: 962,
      name: 'Wabaunsee County, Kansas',
      population: 7019,
      area_type: 'County',
    },
  ],
  963: [
    {
      index: 904,
      name: 'Kansas City-Overland Park-Kansas City, MO-KS CSA',
      population: 2545616,
      area_type: 'CSA',
    },
    {
      index: 936,
      name: 'Kansas City, MO-KS MSA',
      population: 2209494,
      area_type: 'MSA',
    },
    {
      index: 963,
      name: 'Wyandotte County, Kansas',
      population: 165746,
      area_type: 'County',
    },
  ],
  966: [
    {
      index: 964,
      name: 'Bowling Green-Glasgow, KY CSA',
      population: 240906,
      area_type: 'CSA',
    },
    {
      index: 965,
      name: 'Bowling Green, KY MSA',
      population: 185682,
      area_type: 'MSA',
    },
    {
      index: 966,
      name: 'Allen County, Kentucky',
      population: 21275,
      area_type: 'County',
    },
  ],
  969: [
    {
      index: 967,
      name: 'Lexington-Fayette--Richmond--Frankfort, KY CSA',
      population: 750424,
      area_type: 'CSA',
    },
    {
      index: 968,
      name: 'Frankfort, KY μSA',
      population: 75831,
      area_type: 'μSA',
    },
    {
      index: 969,
      name: 'Anderson County, Kentucky',
      population: 24224,
      area_type: 'County',
    },
  ],
  970: [
    {
      index: 690,
      name: 'Paducah-Mayfield, KY-IL CSA',
      population: 134411,
      area_type: 'CSA',
    },
    {
      index: 691,
      name: 'Paducah, KY-IL μSA',
      population: 97999,
      area_type: 'μSA',
    },
    {
      index: 970,
      name: 'Ballard County, Kentucky',
      population: 7650,
      area_type: 'County',
    },
  ],
  972: [
    {
      index: 964,
      name: 'Bowling Green-Glasgow, KY CSA',
      population: 240906,
      area_type: 'CSA',
    },
    {
      index: 971,
      name: 'Glasgow, KY μSA',
      population: 55224,
      area_type: 'μSA',
    },
    {
      index: 972,
      name: 'Barren County, Kentucky',
      population: 44854,
      area_type: 'County',
    },
  ],
  974: [
    {
      index: 967,
      name: 'Lexington-Fayette--Richmond--Frankfort, KY CSA',
      population: 750424,
      area_type: 'CSA',
    },
    {
      index: 973,
      name: 'Mount Sterling, KY μSA',
      population: 47446,
      area_type: 'μSA',
    },
    {
      index: 974,
      name: 'Bath County, Kentucky',
      population: 12829,
      area_type: 'County',
    },
  ],
  975: [
    {
      index: 975,
      name: 'Bell County, Kentucky (Middlesborough, KY μSA)',
      population: 23568,
      area_type: 'μSA',
    },
  ],
  977: [
    {
      index: 746,
      name: 'Cincinnati-Wilmington-Maysville, OH-KY-IN CSA',
      population: 2323945,
      area_type: 'CSA',
    },
    {
      index: 747,
      name: 'Cincinnati, OH-KY-IN MSA',
      population: 2265051,
      area_type: 'MSA',
    },
    {
      index: 977,
      name: 'Boone County, Kentucky',
      population: 139093,
      area_type: 'County',
    },
  ],
  979: [
    {
      index: 967,
      name: 'Lexington-Fayette--Richmond--Frankfort, KY CSA',
      population: 750424,
      area_type: 'CSA',
    },
    {
      index: 978,
      name: 'Lexington-Fayette, KY MSA',
      population: 517916,
      area_type: 'MSA',
    },
    {
      index: 979,
      name: 'Bourbon County, Kentucky',
      population: 20093,
      area_type: 'County',
    },
  ],
  982: [
    {
      index: 980,
      name: 'Charleston-Huntington-Ashland, WV-OH-KY CSA',
      population: 763796,
      area_type: 'CSA',
    },
    {
      index: 981,
      name: 'Huntington-Ashland, WV-KY-OH MSA',
      population: 354304,
      area_type: 'MSA',
    },
    {
      index: 982,
      name: 'Boyd County, Kentucky',
      population: 48110,
      area_type: 'County',
    },
  ],
  984: [
    {
      index: 983,
      name: 'Danville, KY μSA',
      population: 55264,
      area_type: 'μSA',
    },
    {
      index: 984,
      name: 'Boyle County, Kentucky',
      population: 30904,
      area_type: 'County',
    },
  ],
  985: [
    {
      index: 746,
      name: 'Cincinnati-Wilmington-Maysville, OH-KY-IN CSA',
      population: 2323945,
      area_type: 'CSA',
    },
    {
      index: 747,
      name: 'Cincinnati, OH-KY-IN MSA',
      population: 2265051,
      area_type: 'MSA',
    },
    {
      index: 985,
      name: 'Bracken County, Kentucky',
      population: 8452,
      area_type: 'County',
    },
  ],
  986: [
    {
      index: 737,
      name: 'Louisville/Jefferson County--Elizabethtown--Bardstown, KY-IN CSA',
      population: 1513559,
      area_type: 'CSA',
    },
    {
      index: 738,
      name: 'Louisville/Jefferson County, KY-IN MSA',
      population: 1284553,
      area_type: 'MSA',
    },
    {
      index: 986,
      name: 'Bullitt County, Kentucky',
      population: 83836,
      area_type: 'County',
    },
  ],
  987: [
    {
      index: 964,
      name: 'Bowling Green-Glasgow, KY CSA',
      population: 240906,
      area_type: 'CSA',
    },
    {
      index: 965,
      name: 'Bowling Green, KY MSA',
      population: 185682,
      area_type: 'MSA',
    },
    {
      index: 987,
      name: 'Butler County, Kentucky',
      population: 12295,
      area_type: 'County',
    },
  ],
  988: [
    {
      index: 988,
      name: 'Calloway County, Kentucky (Murray, KY μSA)',
      population: 37685,
      area_type: 'μSA',
    },
  ],
  990: [
    {
      index: 746,
      name: 'Cincinnati-Wilmington-Maysville, OH-KY-IN CSA',
      population: 2323945,
      area_type: 'CSA',
    },
    {
      index: 747,
      name: 'Cincinnati, OH-KY-IN MSA',
      population: 2265051,
      area_type: 'MSA',
    },
    {
      index: 990,
      name: 'Campbell County, Kentucky',
      population: 93300,
      area_type: 'County',
    },
  ],
  991: [
    {
      index: 980,
      name: 'Charleston-Huntington-Ashland, WV-OH-KY CSA',
      population: 763796,
      area_type: 'CSA',
    },
    {
      index: 981,
      name: 'Huntington-Ashland, WV-KY-OH MSA',
      population: 354304,
      area_type: 'MSA',
    },
    {
      index: 991,
      name: 'Carter County, Kentucky',
      population: 26395,
      area_type: 'County',
    },
  ],
  993: [
    {
      index: 992,
      name: 'Clarksville, TN-KY MSA',
      population: 336605,
      area_type: 'MSA',
    },
    {
      index: 993,
      name: 'Christian County, Kentucky',
      population: 73037,
      area_type: 'County',
    },
  ],
  994: [
    {
      index: 967,
      name: 'Lexington-Fayette--Richmond--Frankfort, KY CSA',
      population: 750424,
      area_type: 'CSA',
    },
    {
      index: 978,
      name: 'Lexington-Fayette, KY MSA',
      population: 517916,
      area_type: 'MSA',
    },
    {
      index: 994,
      name: 'Clark County, Kentucky',
      population: 37061,
      area_type: 'County',
    },
  ],
  996: [
    {
      index: 995,
      name: 'London, KY μSA',
      population: 149462,
      area_type: 'μSA',
    },
    {
      index: 996,
      name: 'Clay County, Kentucky',
      population: 19913,
      area_type: 'County',
    },
  ],
  998: [
    {
      index: 997,
      name: 'Owensboro, KY MSA',
      population: 121348,
      area_type: 'MSA',
    },
    {
      index: 998,
      name: 'Daviess County, Kentucky',
      population: 103222,
      area_type: 'County',
    },
  ],
  999: [
    {
      index: 964,
      name: 'Bowling Green-Glasgow, KY CSA',
      population: 240906,
      area_type: 'CSA',
    },
    {
      index: 965,
      name: 'Bowling Green, KY MSA',
      population: 185682,
      area_type: 'MSA',
    },
    {
      index: 999,
      name: 'Edmonson County, Kentucky',
      population: 12269,
      area_type: 'County',
    },
  ],
  1001: [
    {
      index: 967,
      name: 'Lexington-Fayette--Richmond--Frankfort, KY CSA',
      population: 750424,
      area_type: 'CSA',
    },
    {
      index: 1000,
      name: 'Richmond-Berea, KY μSA',
      population: 109231,
      area_type: 'μSA',
    },
    {
      index: 1001,
      name: 'Estill County, Kentucky',
      population: 14044,
      area_type: 'County',
    },
  ],
  1002: [
    {
      index: 967,
      name: 'Lexington-Fayette--Richmond--Frankfort, KY CSA',
      population: 750424,
      area_type: 'CSA',
    },
    {
      index: 978,
      name: 'Lexington-Fayette, KY MSA',
      population: 517916,
      area_type: 'MSA',
    },
    {
      index: 1002,
      name: 'Fayette County, Kentucky',
      population: 320347,
      area_type: 'County',
    },
  ],
  1003: [
    {
      index: 967,
      name: 'Lexington-Fayette--Richmond--Frankfort, KY CSA',
      population: 750424,
      area_type: 'CSA',
    },
    {
      index: 968,
      name: 'Frankfort, KY μSA',
      population: 75831,
      area_type: 'μSA',
    },
    {
      index: 1003,
      name: 'Franklin County, Kentucky',
      population: 51607,
      area_type: 'County',
    },
  ],
  1004: [
    {
      index: 746,
      name: 'Cincinnati-Wilmington-Maysville, OH-KY-IN CSA',
      population: 2323945,
      area_type: 'CSA',
    },
    {
      index: 747,
      name: 'Cincinnati, OH-KY-IN MSA',
      population: 2265051,
      area_type: 'MSA',
    },
    {
      index: 1004,
      name: 'Gallatin County, Kentucky',
      population: 8763,
      area_type: 'County',
    },
  ],
  1005: [
    {
      index: 746,
      name: 'Cincinnati-Wilmington-Maysville, OH-KY-IN CSA',
      population: 2323945,
      area_type: 'CSA',
    },
    {
      index: 747,
      name: 'Cincinnati, OH-KY-IN MSA',
      population: 2265051,
      area_type: 'MSA',
    },
    {
      index: 1005,
      name: 'Grant County, Kentucky',
      population: 25502,
      area_type: 'County',
    },
  ],
  1006: [
    {
      index: 690,
      name: 'Paducah-Mayfield, KY-IL CSA',
      population: 134411,
      area_type: 'CSA',
    },
    {
      index: 1006,
      name: 'Graves County, Kentucky (Mayfield, KY μSA)',
      population: 36412,
      area_type: 'μSA',
    },
  ],
  1009: [
    {
      index: 1008,
      name: 'Campbellsville, KY μSA',
      population: 37772,
      area_type: 'μSA',
    },
    {
      index: 1009,
      name: 'Green County, Kentucky',
      population: 11365,
      area_type: 'County',
    },
  ],
  1010: [
    {
      index: 980,
      name: 'Charleston-Huntington-Ashland, WV-OH-KY CSA',
      population: 763796,
      area_type: 'CSA',
    },
    {
      index: 981,
      name: 'Huntington-Ashland, WV-KY-OH MSA',
      population: 354304,
      area_type: 'MSA',
    },
    {
      index: 1010,
      name: 'Greenup County, Kentucky',
      population: 35403,
      area_type: 'County',
    },
  ],
  1011: [
    {
      index: 997,
      name: 'Owensboro, KY MSA',
      population: 121348,
      area_type: 'MSA',
    },
    {
      index: 1011,
      name: 'Hancock County, Kentucky',
      population: 9021,
      area_type: 'County',
    },
  ],
  1013: [
    {
      index: 737,
      name: 'Louisville/Jefferson County--Elizabethtown--Bardstown, KY-IN CSA',
      population: 1513559,
      area_type: 'CSA',
    },
    {
      index: 1012,
      name: 'Elizabethtown-Fort Knox, KY MSA',
      population: 157026,
      area_type: 'MSA',
    },
    {
      index: 1013,
      name: 'Hardin County, Kentucky',
      population: 111862,
      area_type: 'County',
    },
  ],
  1014: [
    {
      index: 815,
      name: 'Evansville, IN-KY MSA',
      population: 314038,
      area_type: 'MSA',
    },
    {
      index: 1014,
      name: 'Henderson County, Kentucky',
      population: 44046,
      area_type: 'County',
    },
  ],
  1015: [
    {
      index: 737,
      name: 'Louisville/Jefferson County--Elizabethtown--Bardstown, KY-IN CSA',
      population: 1513559,
      area_type: 'CSA',
    },
    {
      index: 738,
      name: 'Louisville/Jefferson County, KY-IN MSA',
      population: 1284553,
      area_type: 'MSA',
    },
    {
      index: 1015,
      name: 'Henry County, Kentucky',
      population: 15771,
      area_type: 'County',
    },
  ],
  1016: [
    {
      index: 1016,
      name: 'Hopkins County, Kentucky (Madisonville, KY μSA)',
      population: 44812,
      area_type: 'μSA',
    },
  ],
  1018: [
    {
      index: 737,
      name: 'Louisville/Jefferson County--Elizabethtown--Bardstown, KY-IN CSA',
      population: 1513559,
      area_type: 'CSA',
    },
    {
      index: 738,
      name: 'Louisville/Jefferson County, KY-IN MSA',
      population: 1284553,
      area_type: 'MSA',
    },
    {
      index: 1018,
      name: 'Jefferson County, Kentucky',
      population: 773399,
      area_type: 'County',
    },
  ],
  1019: [
    {
      index: 967,
      name: 'Lexington-Fayette--Richmond--Frankfort, KY CSA',
      population: 750424,
      area_type: 'CSA',
    },
    {
      index: 978,
      name: 'Lexington-Fayette, KY MSA',
      population: 517916,
      area_type: 'MSA',
    },
    {
      index: 1019,
      name: 'Jessamine County, Kentucky',
      population: 54254,
      area_type: 'County',
    },
  ],
  1020: [
    {
      index: 746,
      name: 'Cincinnati-Wilmington-Maysville, OH-KY-IN CSA',
      population: 2323945,
      area_type: 'CSA',
    },
    {
      index: 747,
      name: 'Cincinnati, OH-KY-IN MSA',
      population: 2265051,
      area_type: 'MSA',
    },
    {
      index: 1020,
      name: 'Kenton County, Kentucky',
      population: 170313,
      area_type: 'County',
    },
  ],
  1021: [
    {
      index: 995,
      name: 'London, KY μSA',
      population: 149462,
      area_type: 'μSA',
    },
    {
      index: 1021,
      name: 'Knox County, Kentucky',
      population: 29791,
      area_type: 'County',
    },
  ],
  1022: [
    {
      index: 737,
      name: 'Louisville/Jefferson County--Elizabethtown--Bardstown, KY-IN CSA',
      population: 1513559,
      area_type: 'CSA',
    },
    {
      index: 1012,
      name: 'Elizabethtown-Fort Knox, KY MSA',
      population: 157026,
      area_type: 'MSA',
    },
    {
      index: 1022,
      name: 'Larue County, Kentucky',
      population: 15163,
      area_type: 'County',
    },
  ],
  1023: [
    {
      index: 995,
      name: 'London, KY μSA',
      population: 149462,
      area_type: 'μSA',
    },
    {
      index: 1023,
      name: 'Laurel County, Kentucky',
      population: 62885,
      area_type: 'County',
    },
  ],
  1024: [
    {
      index: 983,
      name: 'Danville, KY μSA',
      population: 55264,
      area_type: 'μSA',
    },
    {
      index: 1024,
      name: 'Lincoln County, Kentucky',
      population: 24360,
      area_type: 'County',
    },
  ],
  1025: [
    {
      index: 690,
      name: 'Paducah-Mayfield, KY-IL CSA',
      population: 134411,
      area_type: 'CSA',
    },
    {
      index: 691,
      name: 'Paducah, KY-IL μSA',
      population: 97999,
      area_type: 'μSA',
    },
    {
      index: 1025,
      name: 'Livingston County, Kentucky',
      population: 8963,
      area_type: 'County',
    },
  ],
  1026: [
    {
      index: 690,
      name: 'Paducah-Mayfield, KY-IL CSA',
      population: 134411,
      area_type: 'CSA',
    },
    {
      index: 691,
      name: 'Paducah, KY-IL μSA',
      population: 97999,
      area_type: 'μSA',
    },
    {
      index: 1026,
      name: 'McCracken County, Kentucky',
      population: 67490,
      area_type: 'County',
    },
  ],
  1027: [
    {
      index: 997,
      name: 'Owensboro, KY MSA',
      population: 121348,
      area_type: 'MSA',
    },
    {
      index: 1027,
      name: 'McLean County, Kentucky',
      population: 9105,
      area_type: 'County',
    },
  ],
  1028: [
    {
      index: 967,
      name: 'Lexington-Fayette--Richmond--Frankfort, KY CSA',
      population: 750424,
      area_type: 'CSA',
    },
    {
      index: 1000,
      name: 'Richmond-Berea, KY μSA',
      population: 109231,
      area_type: 'μSA',
    },
    {
      index: 1028,
      name: 'Madison County, Kentucky',
      population: 95187,
      area_type: 'County',
    },
  ],
  1029: [
    {
      index: 746,
      name: 'Cincinnati-Wilmington-Maysville, OH-KY-IN CSA',
      population: 2323945,
      area_type: 'CSA',
    },
    {
      index: 1029,
      name: 'Mason County, Kentucky (Maysville, KY μSA)',
      population: 16930,
      area_type: 'μSA',
    },
  ],
  1031: [
    {
      index: 737,
      name: 'Louisville/Jefferson County--Elizabethtown--Bardstown, KY-IN CSA',
      population: 1513559,
      area_type: 'CSA',
    },
    {
      index: 1012,
      name: 'Elizabethtown-Fort Knox, KY MSA',
      population: 157026,
      area_type: 'MSA',
    },
    {
      index: 1031,
      name: 'Meade County, Kentucky',
      population: 30001,
      area_type: 'County',
    },
  ],
  1032: [
    {
      index: 967,
      name: 'Lexington-Fayette--Richmond--Frankfort, KY CSA',
      population: 750424,
      area_type: 'CSA',
    },
    {
      index: 973,
      name: 'Mount Sterling, KY μSA',
      population: 47446,
      area_type: 'μSA',
    },
    {
      index: 1032,
      name: 'Menifee County, Kentucky',
      population: 6250,
      area_type: 'County',
    },
  ],
  1033: [
    {
      index: 964,
      name: 'Bowling Green-Glasgow, KY CSA',
      population: 240906,
      area_type: 'CSA',
    },
    {
      index: 971,
      name: 'Glasgow, KY μSA',
      population: 55224,
      area_type: 'μSA',
    },
    {
      index: 1033,
      name: 'Metcalfe County, Kentucky',
      population: 10370,
      area_type: 'County',
    },
  ],
  1034: [
    {
      index: 967,
      name: 'Lexington-Fayette--Richmond--Frankfort, KY CSA',
      population: 750424,
      area_type: 'CSA',
    },
    {
      index: 973,
      name: 'Mount Sterling, KY μSA',
      population: 47446,
      area_type: 'μSA',
    },
    {
      index: 1034,
      name: 'Montgomery County, Kentucky',
      population: 28367,
      area_type: 'County',
    },
  ],
  1035: [
    {
      index: 1035,
      name: 'Muhlenberg County, Kentucky (Central City, KY μSA)',
      population: 30455,
      area_type: 'μSA',
    },
  ],
  1037: [
    {
      index: 737,
      name: 'Louisville/Jefferson County--Elizabethtown--Bardstown, KY-IN CSA',
      population: 1513559,
      area_type: 'CSA',
    },
    {
      index: 1037,
      name: 'Nelson County, Kentucky (Bardstown, KY μSA)',
      population: 47392,
      area_type: 'μSA',
    },
  ],
  1039: [
    {
      index: 737,
      name: 'Louisville/Jefferson County--Elizabethtown--Bardstown, KY-IN CSA',
      population: 1513559,
      area_type: 'CSA',
    },
    {
      index: 738,
      name: 'Louisville/Jefferson County, KY-IN MSA',
      population: 1284553,
      area_type: 'MSA',
    },
    {
      index: 1039,
      name: 'Oldham County, Kentucky',
      population: 69431,
      area_type: 'County',
    },
  ],
  1040: [
    {
      index: 746,
      name: 'Cincinnati-Wilmington-Maysville, OH-KY-IN CSA',
      population: 2323945,
      area_type: 'CSA',
    },
    {
      index: 747,
      name: 'Cincinnati, OH-KY-IN MSA',
      population: 2265051,
      area_type: 'MSA',
    },
    {
      index: 1040,
      name: 'Pendleton County, Kentucky',
      population: 14676,
      area_type: 'County',
    },
  ],
  1041: [
    {
      index: 1041,
      name: 'Pulaski County, Kentucky (Somerset, KY μSA)',
      population: 65795,
      area_type: 'μSA',
    },
  ],
  1043: [
    {
      index: 967,
      name: 'Lexington-Fayette--Richmond--Frankfort, KY CSA',
      population: 750424,
      area_type: 'CSA',
    },
    {
      index: 978,
      name: 'Lexington-Fayette, KY MSA',
      population: 517916,
      area_type: 'MSA',
    },
    {
      index: 1043,
      name: 'Scott County, Kentucky',
      population: 59099,
      area_type: 'County',
    },
  ],
  1044: [
    {
      index: 737,
      name: 'Louisville/Jefferson County--Elizabethtown--Bardstown, KY-IN CSA',
      population: 1513559,
      area_type: 'CSA',
    },
    {
      index: 738,
      name: 'Louisville/Jefferson County, KY-IN MSA',
      population: 1284553,
      area_type: 'MSA',
    },
    {
      index: 1044,
      name: 'Shelby County, Kentucky',
      population: 48886,
      area_type: 'County',
    },
  ],
  1045: [
    {
      index: 737,
      name: 'Louisville/Jefferson County--Elizabethtown--Bardstown, KY-IN CSA',
      population: 1513559,
      area_type: 'CSA',
    },
    {
      index: 738,
      name: 'Louisville/Jefferson County, KY-IN MSA',
      population: 1284553,
      area_type: 'MSA',
    },
    {
      index: 1045,
      name: 'Spencer County, Kentucky',
      population: 20204,
      area_type: 'County',
    },
  ],
  1046: [
    {
      index: 1008,
      name: 'Campbellsville, KY μSA',
      population: 37772,
      area_type: 'μSA',
    },
    {
      index: 1046,
      name: 'Taylor County, Kentucky',
      population: 26407,
      area_type: 'County',
    },
  ],
  1047: [
    {
      index: 992,
      name: 'Clarksville, TN-KY MSA',
      population: 336605,
      area_type: 'MSA',
    },
    {
      index: 1047,
      name: 'Trigg County, Kentucky',
      population: 14332,
      area_type: 'County',
    },
  ],
  1048: [
    {
      index: 964,
      name: 'Bowling Green-Glasgow, KY CSA',
      population: 240906,
      area_type: 'CSA',
    },
    {
      index: 965,
      name: 'Bowling Green, KY MSA',
      population: 185682,
      area_type: 'MSA',
    },
    {
      index: 1048,
      name: 'Warren County, Kentucky',
      population: 139843,
      area_type: 'County',
    },
  ],
  1049: [
    {
      index: 995,
      name: 'London, KY μSA',
      population: 149462,
      area_type: 'μSA',
    },
    {
      index: 1049,
      name: 'Whitley County, Kentucky',
      population: 36873,
      area_type: 'County',
    },
  ],
  1050: [
    {
      index: 967,
      name: 'Lexington-Fayette--Richmond--Frankfort, KY CSA',
      population: 750424,
      area_type: 'CSA',
    },
    {
      index: 978,
      name: 'Lexington-Fayette, KY MSA',
      population: 517916,
      area_type: 'MSA',
    },
    {
      index: 1050,
      name: 'Woodford County, Kentucky',
      population: 27062,
      area_type: 'County',
    },
  ],
  1053: [
    {
      index: 1051,
      name: 'Lafayette-Opelousas-Morgan City, LA CSA',
      population: 610687,
      area_type: 'CSA',
    },
    {
      index: 1052,
      name: 'Lafayette, LA MSA',
      population: 481125,
      area_type: 'MSA',
    },
    {
      index: 1053,
      name: 'Acadia Parish, Louisiana',
      population: 56744,
      area_type: 'County',
    },
  ],
  1055: [
    {
      index: 1054,
      name: 'Baton Rouge, LA MSA',
      population: 873060,
      area_type: 'MSA',
    },
    {
      index: 1055,
      name: 'Ascension Parish, Louisiana',
      population: 130458,
      area_type: 'County',
    },
  ],
  1056: [
    {
      index: 1054,
      name: 'Baton Rouge, LA MSA',
      population: 873060,
      area_type: 'MSA',
    },
    {
      index: 1056,
      name: 'Assumption Parish, Louisiana',
      population: 20604,
      area_type: 'County',
    },
  ],
  1058: [
    {
      index: 1057,
      name: 'DeRidder-Fort Polk South, LA CSA',
      population: 83817,
      area_type: 'CSA',
    },
    {
      index: 1058,
      name: 'Beauregard Parish, Louisiana (DeRidder, LA μSA)',
      population: 36570,
      area_type: 'μSA',
    },
  ],
  1062: [
    {
      index: 1060,
      name: 'Shreveport-Bossier City-Minden, LA CSA',
      population: 420797,
      area_type: 'CSA',
    },
    {
      index: 1061,
      name: 'Shreveport-Bossier City, LA MSA',
      population: 385154,
      area_type: 'MSA',
    },
    {
      index: 1062,
      name: 'Bossier Parish, Louisiana',
      population: 129276,
      area_type: 'County',
    },
  ],
  1063: [
    {
      index: 1060,
      name: 'Shreveport-Bossier City-Minden, LA CSA',
      population: 420797,
      area_type: 'CSA',
    },
    {
      index: 1061,
      name: 'Shreveport-Bossier City, LA MSA',
      population: 385154,
      area_type: 'MSA',
    },
    {
      index: 1063,
      name: 'Caddo Parish, Louisiana',
      population: 229025,
      area_type: 'County',
    },
  ],
  1066: [
    {
      index: 1064,
      name: 'Lake Charles-Jennings, LA CSA',
      population: 239346,
      area_type: 'CSA',
    },
    {
      index: 1065,
      name: 'Lake Charles, LA MSA',
      population: 207320,
      area_type: 'MSA',
    },
    {
      index: 1066,
      name: 'Calcasieu Parish, Louisiana',
      population: 202418,
      area_type: 'County',
    },
  ],
  1067: [
    {
      index: 1064,
      name: 'Lake Charles-Jennings, LA CSA',
      population: 239346,
      area_type: 'CSA',
    },
    {
      index: 1065,
      name: 'Lake Charles, LA MSA',
      population: 207320,
      area_type: 'MSA',
    },
    {
      index: 1067,
      name: 'Cameron Parish, Louisiana',
      population: 4902,
      area_type: 'County',
    },
  ],
  1069: [
    {
      index: 1068,
      name: 'Natchez, MS-LA μSA',
      population: 46524,
      area_type: 'μSA',
    },
    {
      index: 1069,
      name: 'Concordia Parish, Louisiana',
      population: 18116,
      area_type: 'County',
    },
  ],
  1070: [
    {
      index: 1060,
      name: 'Shreveport-Bossier City-Minden, LA CSA',
      population: 420797,
      area_type: 'CSA',
    },
    {
      index: 1061,
      name: 'Shreveport-Bossier City, LA MSA',
      population: 385154,
      area_type: 'MSA',
    },
    {
      index: 1070,
      name: 'De Soto Parish, Louisiana',
      population: 26853,
      area_type: 'County',
    },
  ],
  1071: [
    {
      index: 1054,
      name: 'Baton Rouge, LA MSA',
      population: 873060,
      area_type: 'MSA',
    },
    {
      index: 1071,
      name: 'East Baton Rouge Parish, Louisiana',
      population: 450544,
      area_type: 'County',
    },
  ],
  1072: [
    {
      index: 1054,
      name: 'Baton Rouge, LA MSA',
      population: 873060,
      area_type: 'MSA',
    },
    {
      index: 1072,
      name: 'East Feliciana Parish, Louisiana',
      population: 19135,
      area_type: 'County',
    },
  ],
  1074: [
    {
      index: 1073,
      name: 'Alexandria, LA MSA',
      population: 149189,
      area_type: 'MSA',
    },
    {
      index: 1074,
      name: 'Grant Parish, Louisiana',
      population: 22000,
      area_type: 'County',
    },
  ],
  1075: [
    {
      index: 1051,
      name: 'Lafayette-Opelousas-Morgan City, LA CSA',
      population: 610687,
      area_type: 'CSA',
    },
    {
      index: 1052,
      name: 'Lafayette, LA MSA',
      population: 481125,
      area_type: 'MSA',
    },
    {
      index: 1075,
      name: 'Iberia Parish, Louisiana',
      population: 68327,
      area_type: 'County',
    },
  ],
  1076: [
    {
      index: 1054,
      name: 'Baton Rouge, LA MSA',
      population: 873060,
      area_type: 'MSA',
    },
    {
      index: 1076,
      name: 'Iberville Parish, Louisiana',
      population: 29506,
      area_type: 'County',
    },
  ],
  1079: [
    {
      index: 1077,
      name: 'New Orleans-Metairie-Hammond, LA-MS CSA',
      population: 1485510,
      area_type: 'CSA',
    },
    {
      index: 1078,
      name: 'New Orleans-Metairie, LA MSA',
      population: 1246176,
      area_type: 'MSA',
    },
    {
      index: 1079,
      name: 'Jefferson Parish, Louisiana',
      population: 425884,
      area_type: 'County',
    },
  ],
  1080: [
    {
      index: 1064,
      name: 'Lake Charles-Jennings, LA CSA',
      population: 239346,
      area_type: 'CSA',
    },
    {
      index: 1080,
      name: 'Jefferson Davis Parish, Louisiana (Jennings, LA μSA)',
      population: 32026,
      area_type: 'μSA',
    },
  ],
  1082: [
    {
      index: 1051,
      name: 'Lafayette-Opelousas-Morgan City, LA CSA',
      population: 610687,
      area_type: 'CSA',
    },
    {
      index: 1052,
      name: 'Lafayette, LA MSA',
      population: 481125,
      area_type: 'MSA',
    },
    {
      index: 1082,
      name: 'Lafayette Parish, Louisiana',
      population: 247866,
      area_type: 'County',
    },
  ],
  1084: [
    {
      index: 1083,
      name: 'Houma-Thibodaux, LA MSA',
      population: 200656,
      area_type: 'MSA',
    },
    {
      index: 1084,
      name: 'Lafourche Parish, Louisiana',
      population: 95870,
      area_type: 'County',
    },
  ],
  1086: [
    {
      index: 1085,
      name: 'Monroe-Ruston, LA CSA',
      population: 250998,
      area_type: 'CSA',
    },
    {
      index: 1086,
      name: 'Lincoln Parish, Louisiana (Ruston, LA μSA)',
      population: 48129,
      area_type: 'μSA',
    },
  ],
  1088: [
    {
      index: 1054,
      name: 'Baton Rouge, LA MSA',
      population: 873060,
      area_type: 'MSA',
    },
    {
      index: 1088,
      name: 'Livingston Parish, Louisiana',
      population: 148425,
      area_type: 'County',
    },
  ],
  1090: [
    {
      index: 1085,
      name: 'Monroe-Ruston, LA CSA',
      population: 250998,
      area_type: 'CSA',
    },
    {
      index: 1089,
      name: 'Monroe, LA MSA',
      population: 202869,
      area_type: 'MSA',
    },
    {
      index: 1090,
      name: 'Morehouse Parish, Louisiana',
      population: 24446,
      area_type: 'County',
    },
  ],
  1091: [
    {
      index: 1091,
      name: 'Natchitoches Parish, Louisiana (Natchitoches, LA μSA)',
      population: 36663,
      area_type: 'μSA',
    },
  ],
  1093: [
    {
      index: 1077,
      name: 'New Orleans-Metairie-Hammond, LA-MS CSA',
      population: 1485510,
      area_type: 'CSA',
    },
    {
      index: 1078,
      name: 'New Orleans-Metairie, LA MSA',
      population: 1246176,
      area_type: 'MSA',
    },
    {
      index: 1093,
      name: 'Orleans Parish, Louisiana',
      population: 369749,
      area_type: 'County',
    },
  ],
  1094: [
    {
      index: 1085,
      name: 'Monroe-Ruston, LA CSA',
      population: 250998,
      area_type: 'CSA',
    },
    {
      index: 1089,
      name: 'Monroe, LA MSA',
      population: 202869,
      area_type: 'MSA',
    },
    {
      index: 1094,
      name: 'Ouachita Parish, Louisiana',
      population: 157702,
      area_type: 'County',
    },
  ],
  1095: [
    {
      index: 1077,
      name: 'New Orleans-Metairie-Hammond, LA-MS CSA',
      population: 1485510,
      area_type: 'CSA',
    },
    {
      index: 1078,
      name: 'New Orleans-Metairie, LA MSA',
      population: 1246176,
      area_type: 'MSA',
    },
    {
      index: 1095,
      name: 'Plaquemines Parish, Louisiana',
      population: 22516,
      area_type: 'County',
    },
  ],
  1096: [
    {
      index: 1054,
      name: 'Baton Rouge, LA MSA',
      population: 873060,
      area_type: 'MSA',
    },
    {
      index: 1096,
      name: 'Pointe Coupee Parish, Louisiana',
      population: 20151,
      area_type: 'County',
    },
  ],
  1097: [
    {
      index: 1073,
      name: 'Alexandria, LA MSA',
      population: 149189,
      area_type: 'MSA',
    },
    {
      index: 1097,
      name: 'Rapides Parish, Louisiana',
      population: 127189,
      area_type: 'County',
    },
  ],
  1098: [
    {
      index: 1077,
      name: 'New Orleans-Metairie-Hammond, LA-MS CSA',
      population: 1485510,
      area_type: 'CSA',
    },
    {
      index: 1078,
      name: 'New Orleans-Metairie, LA MSA',
      population: 1246176,
      area_type: 'MSA',
    },
    {
      index: 1098,
      name: 'St. Bernard Parish, Louisiana',
      population: 44479,
      area_type: 'County',
    },
  ],
  1099: [
    {
      index: 1077,
      name: 'New Orleans-Metairie-Hammond, LA-MS CSA',
      population: 1485510,
      area_type: 'CSA',
    },
    {
      index: 1078,
      name: 'New Orleans-Metairie, LA MSA',
      population: 1246176,
      area_type: 'MSA',
    },
    {
      index: 1099,
      name: 'St. Charles Parish, Louisiana',
      population: 50998,
      area_type: 'County',
    },
  ],
  1100: [
    {
      index: 1054,
      name: 'Baton Rouge, LA MSA',
      population: 873060,
      area_type: 'MSA',
    },
    {
      index: 1100,
      name: 'St. Helena Parish, Louisiana',
      population: 10822,
      area_type: 'County',
    },
  ],
  1101: [
    {
      index: 1077,
      name: 'New Orleans-Metairie-Hammond, LA-MS CSA',
      population: 1485510,
      area_type: 'CSA',
    },
    {
      index: 1078,
      name: 'New Orleans-Metairie, LA MSA',
      population: 1246176,
      area_type: 'MSA',
    },
    {
      index: 1101,
      name: 'St. James Parish, Louisiana',
      population: 19423,
      area_type: 'County',
    },
  ],
  1102: [
    {
      index: 1077,
      name: 'New Orleans-Metairie-Hammond, LA-MS CSA',
      population: 1485510,
      area_type: 'CSA',
    },
    {
      index: 1078,
      name: 'New Orleans-Metairie, LA MSA',
      population: 1246176,
      area_type: 'MSA',
    },
    {
      index: 1102,
      name: 'St. John the Baptist Parish, Louisiana',
      population: 39864,
      area_type: 'County',
    },
  ],
  1103: [
    {
      index: 1051,
      name: 'Lafayette-Opelousas-Morgan City, LA CSA',
      population: 610687,
      area_type: 'CSA',
    },
    {
      index: 1103,
      name: 'St. Landry Parish, Louisiana (Opelousas, LA μSA)',
      population: 81773,
      area_type: 'μSA',
    },
  ],
  1105: [
    {
      index: 1051,
      name: 'Lafayette-Opelousas-Morgan City, LA CSA',
      population: 610687,
      area_type: 'CSA',
    },
    {
      index: 1052,
      name: 'Lafayette, LA MSA',
      population: 481125,
      area_type: 'MSA',
    },
    {
      index: 1105,
      name: 'St. Martin Parish, Louisiana',
      population: 51236,
      area_type: 'County',
    },
  ],
  1106: [
    {
      index: 1051,
      name: 'Lafayette-Opelousas-Morgan City, LA CSA',
      population: 610687,
      area_type: 'CSA',
    },
    {
      index: 1106,
      name: 'St. Mary Parish, Louisiana (Morgan City, LA μSA)',
      population: 47789,
      area_type: 'μSA',
    },
  ],
  1108: [
    {
      index: 1077,
      name: 'New Orleans-Metairie-Hammond, LA-MS CSA',
      population: 1485510,
      area_type: 'CSA',
    },
    {
      index: 1078,
      name: 'New Orleans-Metairie, LA MSA',
      population: 1246176,
      area_type: 'MSA',
    },
    {
      index: 1108,
      name: 'St. Tammany Parish, Louisiana',
      population: 273263,
      area_type: 'County',
    },
  ],
  1109: [
    {
      index: 1077,
      name: 'New Orleans-Metairie-Hammond, LA-MS CSA',
      population: 1485510,
      area_type: 'CSA',
    },
    {
      index: 1109,
      name: 'Tangipahoa Parish, Louisiana (Hammond, LA MSA)',
      population: 137048,
      area_type: 'MSA',
    },
  ],
  1111: [
    {
      index: 1083,
      name: 'Houma-Thibodaux, LA MSA',
      population: 200656,
      area_type: 'MSA',
    },
    {
      index: 1111,
      name: 'Terrebonne Parish, Louisiana',
      population: 104786,
      area_type: 'County',
    },
  ],
  1112: [
    {
      index: 1085,
      name: 'Monroe-Ruston, LA CSA',
      population: 250998,
      area_type: 'CSA',
    },
    {
      index: 1089,
      name: 'Monroe, LA MSA',
      population: 202869,
      area_type: 'MSA',
    },
    {
      index: 1112,
      name: 'Union Parish, Louisiana',
      population: 20721,
      area_type: 'County',
    },
  ],
  1113: [
    {
      index: 1051,
      name: 'Lafayette-Opelousas-Morgan City, LA CSA',
      population: 610687,
      area_type: 'CSA',
    },
    {
      index: 1052,
      name: 'Lafayette, LA MSA',
      population: 481125,
      area_type: 'MSA',
    },
    {
      index: 1113,
      name: 'Vermilion Parish, Louisiana',
      population: 56952,
      area_type: 'County',
    },
  ],
  1114: [
    {
      index: 1057,
      name: 'DeRidder-Fort Polk South, LA CSA',
      population: 83817,
      area_type: 'CSA',
    },
    {
      index: 1114,
      name: 'Vernon Parish, Louisiana (Fort Polk South, LA μSA)',
      population: 47247,
      area_type: 'μSA',
    },
  ],
  1116: [
    {
      index: 1077,
      name: 'New Orleans-Metairie-Hammond, LA-MS CSA',
      population: 1485510,
      area_type: 'CSA',
    },
    {
      index: 1116,
      name: 'Washington Parish, Louisiana (Bogalusa, LA μSA)',
      population: 45025,
      area_type: 'μSA',
    },
  ],
  1118: [
    {
      index: 1060,
      name: 'Shreveport-Bossier City-Minden, LA CSA',
      population: 420797,
      area_type: 'CSA',
    },
    {
      index: 1118,
      name: 'Webster Parish, Louisiana (Minden, LA μSA)',
      population: 35643,
      area_type: 'μSA',
    },
  ],
  1120: [
    {
      index: 1054,
      name: 'Baton Rouge, LA MSA',
      population: 873060,
      area_type: 'MSA',
    },
    {
      index: 1120,
      name: 'West Baton Rouge Parish, Louisiana',
      population: 28034,
      area_type: 'County',
    },
  ],
  1121: [
    {
      index: 1054,
      name: 'Baton Rouge, LA MSA',
      population: 873060,
      area_type: 'MSA',
    },
    {
      index: 1121,
      name: 'West Feliciana Parish, Louisiana',
      population: 15381,
      area_type: 'County',
    },
  ],
  1123: [
    {
      index: 1122,
      name: 'Portland-Lewiston-South Portland, ME CSA',
      population: 674599,
      area_type: 'CSA',
    },
    {
      index: 1123,
      name: 'Androscoggin County, Maine (Lewiston-Auburn, ME MSA)',
      population: 113023,
      area_type: 'MSA',
    },
  ],
  1126: [
    {
      index: 1122,
      name: 'Portland-Lewiston-South Portland, ME CSA',
      population: 674599,
      area_type: 'CSA',
    },
    {
      index: 1125,
      name: 'Portland-South Portland, ME MSA',
      population: 561576,
      area_type: 'MSA',
    },
    {
      index: 1126,
      name: 'Cumberland County, Maine',
      population: 307451,
      area_type: 'County',
    },
  ],
  1127: [
    {
      index: 1127,
      name: 'Kennebec County, Maine (Augusta-Waterville, ME μSA)',
      population: 125540,
      area_type: 'μSA',
    },
  ],
  1129: [
    {
      index: 1129,
      name: 'Penobscot County, Maine (Bangor, ME MSA)',
      population: 153704,
      area_type: 'MSA',
    },
  ],
  1131: [
    {
      index: 1122,
      name: 'Portland-Lewiston-South Portland, ME CSA',
      population: 674599,
      area_type: 'CSA',
    },
    {
      index: 1125,
      name: 'Portland-South Portland, ME MSA',
      population: 561576,
      area_type: 'MSA',
    },
    {
      index: 1131,
      name: 'Sagadahoc County, Maine',
      population: 37393,
      area_type: 'County',
    },
  ],
  1132: [
    {
      index: 1122,
      name: 'Portland-Lewiston-South Portland, ME CSA',
      population: 674599,
      area_type: 'CSA',
    },
    {
      index: 1125,
      name: 'Portland-South Portland, ME MSA',
      population: 561576,
      area_type: 'MSA',
    },
    {
      index: 1132,
      name: 'York County, Maine',
      population: 216732,
      area_type: 'County',
    },
  ],
  1134: [
    {
      index: 1133,
      name: 'Cumberland, MD-WV MSA',
      population: 94122,
      area_type: 'MSA',
    },
    {
      index: 1134,
      name: 'Allegany County, Maryland',
      population: 67267,
      area_type: 'County',
    },
  ],
  1136: [
    {
      index: 326,
      name: 'Washington-Baltimore-Arlington, DC-MD-VA-WV-PA CSA',
      population: 9968104,
      area_type: 'CSA',
    },
    {
      index: 1135,
      name: 'Baltimore-Columbia-Towson, MD MSA',
      population: 2835672,
      area_type: 'MSA',
    },
    {
      index: 1136,
      name: 'Anne Arundel County, Maryland',
      population: 593286,
      area_type: 'County',
    },
  ],
  1137: [
    {
      index: 326,
      name: 'Washington-Baltimore-Arlington, DC-MD-VA-WV-PA CSA',
      population: 9968104,
      area_type: 'CSA',
    },
    {
      index: 1135,
      name: 'Baltimore-Columbia-Towson, MD MSA',
      population: 2835672,
      area_type: 'MSA',
    },
    {
      index: 1137,
      name: 'Baltimore County, Maryland',
      population: 846161,
      area_type: 'County',
    },
  ],
  1138: [
    {
      index: 326,
      name: 'Washington-Baltimore-Arlington, DC-MD-VA-WV-PA CSA',
      population: 9968104,
      area_type: 'CSA',
    },
    {
      index: 327,
      name: 'Washington-Arlington-Alexandria, DC-VA-MD-WV MSA',
      population: 6373756,
      area_type: 'MSA',
    },
    {
      index: 1138,
      name: 'Calvert County, Maryland',
      population: 94573,
      area_type: 'County',
    },
  ],
  1139: [
    {
      index: 326,
      name: 'Washington-Baltimore-Arlington, DC-MD-VA-WV-PA CSA',
      population: 9968104,
      area_type: 'CSA',
    },
    {
      index: 1135,
      name: 'Baltimore-Columbia-Towson, MD MSA',
      population: 2835672,
      area_type: 'MSA',
    },
    {
      index: 1139,
      name: 'Carroll County, Maryland',
      population: 175305,
      area_type: 'County',
    },
  ],
  1140: [
    {
      index: 318,
      name: 'Philadelphia-Reading-Camden, PA-NJ-DE-MD CSA',
      population: 7381187,
      area_type: 'CSA',
    },
    {
      index: 321,
      name: 'Philadelphia-Camden-Wilmington, PA-NJ-DE-MD MSA',
      population: 6241164,
      area_type: 'MSA',
    },
    {
      index: 1140,
      name: 'Cecil County, Maryland',
      population: 104942,
      area_type: 'County',
    },
  ],
  1141: [
    {
      index: 326,
      name: 'Washington-Baltimore-Arlington, DC-MD-VA-WV-PA CSA',
      population: 9968104,
      area_type: 'CSA',
    },
    {
      index: 327,
      name: 'Washington-Arlington-Alexandria, DC-VA-MD-WV MSA',
      population: 6373756,
      area_type: 'MSA',
    },
    {
      index: 1141,
      name: 'Charles County, Maryland',
      population: 170102,
      area_type: 'County',
    },
  ],
  1142: [
    {
      index: 323,
      name: 'Salisbury-Cambridge, MD-DE CSA',
      population: 471758,
      area_type: 'CSA',
    },
    {
      index: 1142,
      name: 'Dorchester County, Maryland (Cambridge, MD μSA)',
      population: 32726,
      area_type: 'μSA',
    },
  ],
  1144: [
    {
      index: 326,
      name: 'Washington-Baltimore-Arlington, DC-MD-VA-WV-PA CSA',
      population: 9968104,
      area_type: 'CSA',
    },
    {
      index: 327,
      name: 'Washington-Arlington-Alexandria, DC-VA-MD-WV MSA',
      population: 6373756,
      area_type: 'MSA',
    },
    {
      index: 1144,
      name: 'Frederick County, Maryland',
      population: 287079,
      area_type: 'County',
    },
  ],
  1145: [
    {
      index: 326,
      name: 'Washington-Baltimore-Arlington, DC-MD-VA-WV-PA CSA',
      population: 9968104,
      area_type: 'CSA',
    },
    {
      index: 1135,
      name: 'Baltimore-Columbia-Towson, MD MSA',
      population: 2835672,
      area_type: 'MSA',
    },
    {
      index: 1145,
      name: 'Harford County, Maryland',
      population: 263867,
      area_type: 'County',
    },
  ],
  1146: [
    {
      index: 326,
      name: 'Washington-Baltimore-Arlington, DC-MD-VA-WV-PA CSA',
      population: 9968104,
      area_type: 'CSA',
    },
    {
      index: 1135,
      name: 'Baltimore-Columbia-Towson, MD MSA',
      population: 2835672,
      area_type: 'MSA',
    },
    {
      index: 1146,
      name: 'Howard County, Maryland',
      population: 335411,
      area_type: 'County',
    },
  ],
  1147: [
    {
      index: 326,
      name: 'Washington-Baltimore-Arlington, DC-MD-VA-WV-PA CSA',
      population: 9968104,
      area_type: 'CSA',
    },
    {
      index: 327,
      name: 'Washington-Arlington-Alexandria, DC-VA-MD-WV MSA',
      population: 6373756,
      area_type: 'MSA',
    },
    {
      index: 1147,
      name: 'Montgomery County, Maryland',
      population: 1052521,
      area_type: 'County',
    },
  ],
  1148: [
    {
      index: 326,
      name: 'Washington-Baltimore-Arlington, DC-MD-VA-WV-PA CSA',
      population: 9968104,
      area_type: 'CSA',
    },
    {
      index: 327,
      name: 'Washington-Arlington-Alexandria, DC-VA-MD-WV MSA',
      population: 6373756,
      area_type: 'MSA',
    },
    {
      index: 1148,
      name: "Prince George's County, Maryland",
      population: 946971,
      area_type: 'County',
    },
  ],
  1149: [
    {
      index: 326,
      name: 'Washington-Baltimore-Arlington, DC-MD-VA-WV-PA CSA',
      population: 9968104,
      area_type: 'CSA',
    },
    {
      index: 1135,
      name: 'Baltimore-Columbia-Towson, MD MSA',
      population: 2835672,
      area_type: 'MSA',
    },
    {
      index: 1149,
      name: "Queen Anne's County, Maryland",
      population: 51711,
      area_type: 'County',
    },
  ],
  1150: [
    {
      index: 326,
      name: 'Washington-Baltimore-Arlington, DC-MD-VA-WV-PA CSA',
      population: 9968104,
      area_type: 'CSA',
    },
    {
      index: 1150,
      name: "St. Mary's County, Maryland (California-Lexington Park, MD MSA)",
      population: 114877,
      area_type: 'MSA',
    },
  ],
  1152: [
    {
      index: 323,
      name: 'Salisbury-Cambridge, MD-DE CSA',
      population: 471758,
      area_type: 'CSA',
    },
    {
      index: 324,
      name: 'Salisbury, MD-DE MSA',
      population: 439032,
      area_type: 'MSA',
    },
    {
      index: 1152,
      name: 'Somerset County, Maryland',
      population: 24546,
      area_type: 'County',
    },
  ],
  1153: [
    {
      index: 326,
      name: 'Washington-Baltimore-Arlington, DC-MD-VA-WV-PA CSA',
      population: 9968104,
      area_type: 'CSA',
    },
    {
      index: 1153,
      name: 'Talbot County, Maryland (Easton, MD μSA)',
      population: 37932,
      area_type: 'μSA',
    },
  ],
  1156: [
    {
      index: 326,
      name: 'Washington-Baltimore-Arlington, DC-MD-VA-WV-PA CSA',
      population: 9968104,
      area_type: 'CSA',
    },
    {
      index: 1155,
      name: 'Hagerstown-Martinsburg, MD-WV MSA',
      population: 302510,
      area_type: 'MSA',
    },
    {
      index: 1156,
      name: 'Washington County, Maryland',
      population: 155590,
      area_type: 'County',
    },
  ],
  1157: [
    {
      index: 323,
      name: 'Salisbury-Cambridge, MD-DE CSA',
      population: 471758,
      area_type: 'CSA',
    },
    {
      index: 324,
      name: 'Salisbury, MD-DE MSA',
      population: 439032,
      area_type: 'MSA',
    },
    {
      index: 1157,
      name: 'Wicomico County, Maryland',
      population: 104664,
      area_type: 'County',
    },
  ],
  1158: [
    {
      index: 323,
      name: 'Salisbury-Cambridge, MD-DE CSA',
      population: 471758,
      area_type: 'CSA',
    },
    {
      index: 324,
      name: 'Salisbury, MD-DE MSA',
      population: 439032,
      area_type: 'MSA',
    },
    {
      index: 1158,
      name: 'Worcester County, Maryland',
      population: 53866,
      area_type: 'County',
    },
  ],
  1159: [
    {
      index: 326,
      name: 'Washington-Baltimore-Arlington, DC-MD-VA-WV-PA CSA',
      population: 9968104,
      area_type: 'CSA',
    },
    {
      index: 1135,
      name: 'Baltimore-Columbia-Towson, MD MSA',
      population: 2835672,
      area_type: 'MSA',
    },
    {
      index: 1159,
      name: 'Baltimore city, Maryland',
      population: 569931,
      area_type: 'County',
    },
  ],
  1161: [
    {
      index: 1160,
      name: 'Boston-Worcester-Providence, MA-RI-NH-CT CSA',
      population: 8434341,
      area_type: 'CSA',
    },
    {
      index: 1161,
      name: 'Barnstable County, Massachusetts (Barnstable Town, MA MSA)',
      population: 232457,
      area_type: 'MSA',
    },
  ],
  1163: [
    {
      index: 1163,
      name: 'Berkshire County, Massachusetts (Pittsfield, MA MSA)',
      population: 127859,
      area_type: 'MSA',
    },
  ],
  1166: [
    {
      index: 1160,
      name: 'Boston-Worcester-Providence, MA-RI-NH-CT CSA',
      population: 8434341,
      area_type: 'CSA',
    },
    {
      index: 1165,
      name: 'Providence-Warwick, RI-MA MSA',
      population: 1673802,
      area_type: 'MSA',
    },
    {
      index: 1166,
      name: 'Bristol County, Massachusetts',
      population: 580068,
      area_type: 'County',
    },
  ],
  1167: [
    {
      index: 1167,
      name: 'Dukes County, Massachusetts (Vineyard Haven, MA μSA)',
      population: 20868,
      area_type: 'μSA',
    },
  ],
  1170: [
    {
      index: 1160,
      name: 'Boston-Worcester-Providence, MA-RI-NH-CT CSA',
      population: 8434341,
      area_type: 'CSA',
    },
    {
      index: 1169,
      name: 'Boston-Cambridge-Newton, MA-NH MSA',
      population: 4900550,
      area_type: 'MSA',
    },
    {
      index: 1170,
      name: 'Essex County, Massachusetts',
      population: 806765,
      area_type: 'County',
    },
  ],
  1172: [
    {
      index: 1171,
      name: 'Springfield, MA MSA',
      population: 694523,
      area_type: 'MSA',
    },
    {
      index: 1172,
      name: 'Franklin County, Massachusetts',
      population: 70894,
      area_type: 'County',
    },
  ],
  1173: [
    {
      index: 1171,
      name: 'Springfield, MA MSA',
      population: 694523,
      area_type: 'MSA',
    },
    {
      index: 1173,
      name: 'Hampden County, Massachusetts',
      population: 461041,
      area_type: 'County',
    },
  ],
  1174: [
    {
      index: 1171,
      name: 'Springfield, MA MSA',
      population: 694523,
      area_type: 'MSA',
    },
    {
      index: 1174,
      name: 'Hampshire County, Massachusetts',
      population: 162588,
      area_type: 'County',
    },
  ],
  1175: [
    {
      index: 1160,
      name: 'Boston-Worcester-Providence, MA-RI-NH-CT CSA',
      population: 8434341,
      area_type: 'CSA',
    },
    {
      index: 1169,
      name: 'Boston-Cambridge-Newton, MA-NH MSA',
      population: 4900550,
      area_type: 'MSA',
    },
    {
      index: 1175,
      name: 'Middlesex County, Massachusetts',
      population: 1617105,
      area_type: 'County',
    },
  ],
  1176: [
    {
      index: 1160,
      name: 'Boston-Worcester-Providence, MA-RI-NH-CT CSA',
      population: 8434341,
      area_type: 'CSA',
    },
    {
      index: 1169,
      name: 'Boston-Cambridge-Newton, MA-NH MSA',
      population: 4900550,
      area_type: 'MSA',
    },
    {
      index: 1176,
      name: 'Norfolk County, Massachusetts',
      population: 725531,
      area_type: 'County',
    },
  ],
  1177: [
    {
      index: 1160,
      name: 'Boston-Worcester-Providence, MA-RI-NH-CT CSA',
      population: 8434341,
      area_type: 'CSA',
    },
    {
      index: 1169,
      name: 'Boston-Cambridge-Newton, MA-NH MSA',
      population: 4900550,
      area_type: 'MSA',
    },
    {
      index: 1177,
      name: 'Plymouth County, Massachusetts',
      population: 533069,
      area_type: 'County',
    },
  ],
  1178: [
    {
      index: 1160,
      name: 'Boston-Worcester-Providence, MA-RI-NH-CT CSA',
      population: 8434341,
      area_type: 'CSA',
    },
    {
      index: 1169,
      name: 'Boston-Cambridge-Newton, MA-NH MSA',
      population: 4900550,
      area_type: 'MSA',
    },
    {
      index: 1178,
      name: 'Suffolk County, Massachusetts',
      population: 766381,
      area_type: 'County',
    },
  ],
  1180: [
    {
      index: 1160,
      name: 'Boston-Worcester-Providence, MA-RI-NH-CT CSA',
      population: 8434341,
      area_type: 'CSA',
    },
    {
      index: 1179,
      name: 'Worcester, MA-CT MSA',
      population: 980137,
      area_type: 'MSA',
    },
    {
      index: 1180,
      name: 'Worcester County, Massachusetts',
      population: 862927,
      area_type: 'County',
    },
  ],
  1182: [
    {
      index: 1181,
      name: 'Grand Rapids-Kentwood-Muskegon, MI CSA',
      population: 1432693,
      area_type: 'CSA',
    },
    {
      index: 1182,
      name: 'Allegan County, Michigan (Holland, MI μSA)',
      population: 121210,
      area_type: 'μSA',
    },
  ],
  1184: [
    {
      index: 1184,
      name: 'Alpena County, Michigan (Alpena, MI μSA)',
      population: 28847,
      area_type: 'μSA',
    },
  ],
  1187: [
    {
      index: 1186,
      name: 'Saginaw-Midland-Bay City, MI CSA',
      population: 374825,
      area_type: 'CSA',
    },
    {
      index: 1187,
      name: 'Bay County, Michigan (Bay City, MI MSA)',
      population: 102821,
      area_type: 'MSA',
    },
  ],
  1190: [
    {
      index: 1189,
      name: 'Traverse City, MI μSA',
      population: 155813,
      area_type: 'μSA',
    },
    {
      index: 1190,
      name: 'Benzie County, Michigan',
      population: 18297,
      area_type: 'County',
    },
  ],
  1191: [
    {
      index: 757,
      name: 'South Bend-Elkhart-Mishawaka, IN-MI CSA',
      population: 810585,
      area_type: 'CSA',
    },
    {
      index: 1191,
      name: 'Berrien County, Michigan (Niles, MI MSA)',
      population: 152900,
      area_type: 'MSA',
    },
  ],
  1194: [
    {
      index: 1193,
      name: 'Kalamazoo-Battle Creek-Portage, MI CSA',
      population: 499867,
      area_type: 'CSA',
    },
    {
      index: 1194,
      name: 'Branch County, Michigan (Coldwater, MI μSA)',
      population: 44531,
      area_type: 'μSA',
    },
  ],
  1196: [
    {
      index: 1193,
      name: 'Kalamazoo-Battle Creek-Portage, MI CSA',
      population: 499867,
      area_type: 'CSA',
    },
    {
      index: 1196,
      name: 'Calhoun County, Michigan (Battle Creek, MI MSA)',
      population: 133289,
      area_type: 'MSA',
    },
  ],
  1198: [
    {
      index: 757,
      name: 'South Bend-Elkhart-Mishawaka, IN-MI CSA',
      population: 810585,
      area_type: 'CSA',
    },
    {
      index: 818,
      name: 'South Bend-Mishawaka, IN-MI MSA',
      population: 323637,
      area_type: 'MSA',
    },
    {
      index: 1198,
      name: 'Cass County, Michigan',
      population: 51403,
      area_type: 'County',
    },
  ],
  1199: [
    {
      index: 1199,
      name: 'Chippewa County, Michigan (Sault Ste. Marie, MI μSA)',
      population: 36293,
      area_type: 'μSA',
    },
  ],
  1202: [
    {
      index: 1201,
      name: 'Lansing-East Lansing, MI MSA',
      population: 540870,
      area_type: 'MSA',
    },
    {
      index: 1202,
      name: 'Clinton County, Michigan',
      population: 79748,
      area_type: 'County',
    },
  ],
  1203: [
    {
      index: 1203,
      name: 'Delta County, Michigan (Escanaba, MI μSA)',
      population: 36741,
      area_type: 'μSA',
    },
  ],
  1207: [
    {
      index: 1205,
      name: 'Marinette-Iron Mountain, WI-MI CSA',
      population: 95816,
      area_type: 'CSA',
    },
    {
      index: 1206,
      name: 'Iron Mountain, MI-WI μSA',
      population: 30562,
      area_type: 'μSA',
    },
    {
      index: 1207,
      name: 'Dickinson County, Michigan',
      population: 25874,
      area_type: 'County',
    },
  ],
  1208: [
    {
      index: 1201,
      name: 'Lansing-East Lansing, MI MSA',
      population: 540870,
      area_type: 'MSA',
    },
    {
      index: 1208,
      name: 'Eaton County, Michigan',
      population: 108992,
      area_type: 'County',
    },
  ],
  1210: [
    {
      index: 1209,
      name: 'Detroit-Warren-Ann Arbor, MI CSA',
      population: 5368296,
      area_type: 'CSA',
    },
    {
      index: 1210,
      name: 'Genesee County, Michigan (Flint, MI MSA)',
      population: 401983,
      area_type: 'MSA',
    },
  ],
  1212: [
    {
      index: 1189,
      name: 'Traverse City, MI μSA',
      population: 155813,
      area_type: 'μSA',
    },
    {
      index: 1212,
      name: 'Grand Traverse County, Michigan',
      population: 96464,
      area_type: 'County',
    },
  ],
  1214: [
    {
      index: 1213,
      name: 'Mount Pleasant-Alma, MI CSA',
      population: 105547,
      area_type: 'CSA',
    },
    {
      index: 1214,
      name: 'Gratiot County, Michigan (Alma, MI μSA)',
      population: 41100,
      area_type: 'μSA',
    },
  ],
  1216: [
    {
      index: 1216,
      name: 'Hillsdale County, Michigan (Hillsdale, MI μSA)',
      population: 45762,
      area_type: 'μSA',
    },
  ],
  1219: [
    {
      index: 1218,
      name: 'Houghton, MI μSA',
      population: 39215,
      area_type: 'μSA',
    },
    {
      index: 1219,
      name: 'Houghton County, Michigan',
      population: 37035,
      area_type: 'County',
    },
  ],
  1220: [
    {
      index: 1201,
      name: 'Lansing-East Lansing, MI MSA',
      population: 540870,
      area_type: 'MSA',
    },
    {
      index: 1220,
      name: 'Ingham County, Michigan',
      population: 284108,
      area_type: 'County',
    },
  ],
  1222: [
    {
      index: 1181,
      name: 'Grand Rapids-Kentwood-Muskegon, MI CSA',
      population: 1432693,
      area_type: 'CSA',
    },
    {
      index: 1221,
      name: 'Grand Rapids-Kentwood, MI MSA',
      population: 1094198,
      area_type: 'MSA',
    },
    {
      index: 1222,
      name: 'Ionia County, Michigan',
      population: 66809,
      area_type: 'County',
    },
  ],
  1223: [
    {
      index: 1213,
      name: 'Mount Pleasant-Alma, MI CSA',
      population: 105547,
      area_type: 'CSA',
    },
    {
      index: 1223,
      name: 'Isabella County, Michigan (Mount Pleasant, MI μSA)',
      population: 64447,
      area_type: 'μSA',
    },
  ],
  1225: [
    {
      index: 1225,
      name: 'Jackson County, Michigan (Jackson, MI MSA)',
      population: 160066,
      area_type: 'MSA',
    },
  ],
  1227: [
    {
      index: 1193,
      name: 'Kalamazoo-Battle Creek-Portage, MI CSA',
      population: 499867,
      area_type: 'CSA',
    },
    {
      index: 1227,
      name: 'Kalamazoo County, Michigan (Kalamazoo-Portage, MI MSA)',
      population: 261173,
      area_type: 'MSA',
    },
  ],
  1229: [
    {
      index: 1189,
      name: 'Traverse City, MI μSA',
      population: 155813,
      area_type: 'μSA',
    },
    {
      index: 1229,
      name: 'Kalkaska County, Michigan',
      population: 18182,
      area_type: 'County',
    },
  ],
  1230: [
    {
      index: 1181,
      name: 'Grand Rapids-Kentwood-Muskegon, MI CSA',
      population: 1432693,
      area_type: 'CSA',
    },
    {
      index: 1221,
      name: 'Grand Rapids-Kentwood, MI MSA',
      population: 1094198,
      area_type: 'MSA',
    },
    {
      index: 1230,
      name: 'Kent County, Michigan',
      population: 659083,
      area_type: 'County',
    },
  ],
  1231: [
    {
      index: 1218,
      name: 'Houghton, MI μSA',
      population: 39215,
      area_type: 'μSA',
    },
    {
      index: 1231,
      name: 'Keweenaw County, Michigan',
      population: 2180,
      area_type: 'County',
    },
  ],
  1233: [
    {
      index: 1209,
      name: 'Detroit-Warren-Ann Arbor, MI CSA',
      population: 5368296,
      area_type: 'CSA',
    },
    {
      index: 1232,
      name: 'Detroit-Warren-Dearborn, MI MSA',
      population: 4345761,
      area_type: 'MSA',
    },
    {
      index: 1233,
      name: 'Lapeer County, Michigan',
      population: 88780,
      area_type: 'County',
    },
  ],
  1234: [
    {
      index: 1189,
      name: 'Traverse City, MI μSA',
      population: 155813,
      area_type: 'μSA',
    },
    {
      index: 1234,
      name: 'Leelanau County, Michigan',
      population: 22870,
      area_type: 'County',
    },
  ],
  1235: [
    {
      index: 1209,
      name: 'Detroit-Warren-Ann Arbor, MI CSA',
      population: 5368296,
      area_type: 'CSA',
    },
    {
      index: 1235,
      name: 'Lenawee County, Michigan (Adrian, MI μSA)',
      population: 98567,
      area_type: 'μSA',
    },
  ],
  1237: [
    {
      index: 1209,
      name: 'Detroit-Warren-Ann Arbor, MI CSA',
      population: 5368296,
      area_type: 'CSA',
    },
    {
      index: 1232,
      name: 'Detroit-Warren-Dearborn, MI MSA',
      population: 4345761,
      area_type: 'MSA',
    },
    {
      index: 1237,
      name: 'Livingston County, Michigan',
      population: 196161,
      area_type: 'County',
    },
  ],
  1238: [
    {
      index: 1209,
      name: 'Detroit-Warren-Ann Arbor, MI CSA',
      population: 5368296,
      area_type: 'CSA',
    },
    {
      index: 1232,
      name: 'Detroit-Warren-Dearborn, MI MSA',
      population: 4345761,
      area_type: 'MSA',
    },
    {
      index: 1238,
      name: 'Macomb County, Michigan',
      population: 874195,
      area_type: 'County',
    },
  ],
  1239: [
    {
      index: 1239,
      name: 'Marquette County, Michigan (Marquette, MI μSA)',
      population: 66661,
      area_type: 'μSA',
    },
  ],
  1241: [
    {
      index: 1241,
      name: 'Mason County, Michigan (Ludington, MI μSA)',
      population: 29409,
      area_type: 'μSA',
    },
  ],
  1243: [
    {
      index: 1181,
      name: 'Grand Rapids-Kentwood-Muskegon, MI CSA',
      population: 1432693,
      area_type: 'CSA',
    },
    {
      index: 1243,
      name: 'Mecosta County, Michigan (Big Rapids, MI μSA)',
      population: 40720,
      area_type: 'μSA',
    },
  ],
  1246: [
    {
      index: 1205,
      name: 'Marinette-Iron Mountain, WI-MI CSA',
      population: 95816,
      area_type: 'CSA',
    },
    {
      index: 1245,
      name: 'Marinette, WI-MI μSA',
      population: 65254,
      area_type: 'μSA',
    },
    {
      index: 1246,
      name: 'Menominee County, Michigan',
      population: 23266,
      area_type: 'County',
    },
  ],
  1247: [
    {
      index: 1186,
      name: 'Saginaw-Midland-Bay City, MI CSA',
      population: 374825,
      area_type: 'CSA',
    },
    {
      index: 1247,
      name: 'Midland County, Michigan (Midland, MI MSA)',
      population: 83674,
      area_type: 'MSA',
    },
  ],
  1250: [
    {
      index: 1249,
      name: 'Cadillac, MI μSA',
      population: 49409,
      area_type: 'μSA',
    },
    {
      index: 1250,
      name: 'Missaukee County, Michigan',
      population: 15213,
      area_type: 'County',
    },
  ],
  1251: [
    {
      index: 1209,
      name: 'Detroit-Warren-Ann Arbor, MI CSA',
      population: 5368296,
      area_type: 'CSA',
    },
    {
      index: 1251,
      name: 'Monroe County, Michigan (Monroe, MI MSA)',
      population: 155609,
      area_type: 'MSA',
    },
  ],
  1253: [
    {
      index: 1181,
      name: 'Grand Rapids-Kentwood-Muskegon, MI CSA',
      population: 1432693,
      area_type: 'CSA',
    },
    {
      index: 1221,
      name: 'Grand Rapids-Kentwood, MI MSA',
      population: 1094198,
      area_type: 'MSA',
    },
    {
      index: 1253,
      name: 'Montcalm County, Michigan',
      population: 67433,
      area_type: 'County',
    },
  ],
  1254: [
    {
      index: 1181,
      name: 'Grand Rapids-Kentwood-Muskegon, MI CSA',
      population: 1432693,
      area_type: 'CSA',
    },
    {
      index: 1254,
      name: 'Muskegon County, Michigan (Muskegon, MI MSA)',
      population: 176565,
      area_type: 'MSA',
    },
  ],
  1256: [
    {
      index: 1209,
      name: 'Detroit-Warren-Ann Arbor, MI CSA',
      population: 5368296,
      area_type: 'CSA',
    },
    {
      index: 1232,
      name: 'Detroit-Warren-Dearborn, MI MSA',
      population: 4345761,
      area_type: 'MSA',
    },
    {
      index: 1256,
      name: 'Oakland County, Michigan',
      population: 1269431,
      area_type: 'County',
    },
  ],
  1257: [
    {
      index: 1181,
      name: 'Grand Rapids-Kentwood-Muskegon, MI CSA',
      population: 1432693,
      area_type: 'CSA',
    },
    {
      index: 1221,
      name: 'Grand Rapids-Kentwood, MI MSA',
      population: 1094198,
      area_type: 'MSA',
    },
    {
      index: 1257,
      name: 'Ottawa County, Michigan',
      population: 300873,
      area_type: 'County',
    },
  ],
  1258: [
    {
      index: 1186,
      name: 'Saginaw-Midland-Bay City, MI CSA',
      population: 374825,
      area_type: 'CSA',
    },
    {
      index: 1258,
      name: 'Saginaw County, Michigan (Saginaw, MI MSA)',
      population: 188330,
      area_type: 'MSA',
    },
  ],
  1260: [
    {
      index: 1209,
      name: 'Detroit-Warren-Ann Arbor, MI CSA',
      population: 5368296,
      area_type: 'CSA',
    },
    {
      index: 1232,
      name: 'Detroit-Warren-Dearborn, MI MSA',
      population: 4345761,
      area_type: 'MSA',
    },
    {
      index: 1260,
      name: 'St. Clair County, Michigan',
      population: 160151,
      area_type: 'County',
    },
  ],
  1261: [
    {
      index: 1193,
      name: 'Kalamazoo-Battle Creek-Portage, MI CSA',
      population: 499867,
      area_type: 'CSA',
    },
    {
      index: 1261,
      name: 'St. Joseph County, Michigan (Sturgis, MI μSA)',
      population: 60874,
      area_type: 'μSA',
    },
  ],
  1263: [
    {
      index: 1201,
      name: 'Lansing-East Lansing, MI MSA',
      population: 540870,
      area_type: 'MSA',
    },
    {
      index: 1263,
      name: 'Shiawassee County, Michigan',
      population: 68022,
      area_type: 'County',
    },
  ],
  1264: [
    {
      index: 1209,
      name: 'Detroit-Warren-Ann Arbor, MI CSA',
      population: 5368296,
      area_type: 'CSA',
    },
    {
      index: 1264,
      name: 'Washtenaw County, Michigan (Ann Arbor, MI MSA)',
      population: 366376,
      area_type: 'MSA',
    },
  ],
  1266: [
    {
      index: 1209,
      name: 'Detroit-Warren-Ann Arbor, MI CSA',
      population: 5368296,
      area_type: 'CSA',
    },
    {
      index: 1232,
      name: 'Detroit-Warren-Dearborn, MI MSA',
      population: 4345761,
      area_type: 'MSA',
    },
    {
      index: 1266,
      name: 'Wayne County, Michigan',
      population: 1757043,
      area_type: 'County',
    },
  ],
  1267: [
    {
      index: 1249,
      name: 'Cadillac, MI μSA',
      population: 49409,
      area_type: 'μSA',
    },
    {
      index: 1267,
      name: 'Wexford County, Michigan',
      population: 34196,
      area_type: 'County',
    },
  ],
  1270: [
    {
      index: 1268,
      name: 'Minneapolis-St. Paul, MN-WI CSA',
      population: 4085415,
      area_type: 'CSA',
    },
    {
      index: 1269,
      name: 'Minneapolis-St. Paul-Bloomington, MN-WI MSA',
      population: 3693729,
      area_type: 'MSA',
    },
    {
      index: 1270,
      name: 'Anoka County, Minnesota',
      population: 368864,
      area_type: 'County',
    },
  ],
  1271: [
    {
      index: 1271,
      name: 'Beltrami County, Minnesota (Bemidji, MN μSA)',
      population: 46799,
      area_type: 'μSA',
    },
  ],
  1274: [
    {
      index: 1268,
      name: 'Minneapolis-St. Paul, MN-WI CSA',
      population: 4085415,
      area_type: 'CSA',
    },
    {
      index: 1273,
      name: 'St. Cloud, MN MSA',
      population: 201868,
      area_type: 'MSA',
    },
    {
      index: 1274,
      name: 'Benton County, Minnesota',
      population: 41463,
      area_type: 'County',
    },
  ],
  1277: [
    {
      index: 1275,
      name: 'Mankato-New Ulm, MN CSA',
      population: 129795,
      area_type: 'CSA',
    },
    {
      index: 1276,
      name: 'Mankato, MN MSA',
      population: 104072,
      area_type: 'MSA',
    },
    {
      index: 1277,
      name: 'Blue Earth County, Minnesota',
      population: 69631,
      area_type: 'County',
    },
  ],
  1278: [
    {
      index: 1275,
      name: 'Mankato-New Ulm, MN CSA',
      population: 129795,
      area_type: 'CSA',
    },
    {
      index: 1278,
      name: 'Brown County, Minnesota (New Ulm, MN μSA)',
      population: 25723,
      area_type: 'μSA',
    },
  ],
  1281: [
    {
      index: 1280,
      name: 'Duluth, MN-WI MSA',
      population: 291323,
      area_type: 'MSA',
    },
    {
      index: 1281,
      name: 'Carlton County, Minnesota',
      population: 36708,
      area_type: 'County',
    },
  ],
  1282: [
    {
      index: 1268,
      name: 'Minneapolis-St. Paul, MN-WI CSA',
      population: 4085415,
      area_type: 'CSA',
    },
    {
      index: 1269,
      name: 'Minneapolis-St. Paul-Bloomington, MN-WI MSA',
      population: 3693729,
      area_type: 'MSA',
    },
    {
      index: 1282,
      name: 'Carver County, Minnesota',
      population: 110034,
      area_type: 'County',
    },
  ],
  1284: [
    {
      index: 1283,
      name: 'Brainerd, MN μSA',
      population: 99222,
      area_type: 'μSA',
    },
    {
      index: 1284,
      name: 'Cass County, Minnesota',
      population: 31274,
      area_type: 'County',
    },
  ],
  1285: [
    {
      index: 1268,
      name: 'Minneapolis-St. Paul, MN-WI CSA',
      population: 4085415,
      area_type: 'CSA',
    },
    {
      index: 1269,
      name: 'Minneapolis-St. Paul-Bloomington, MN-WI MSA',
      population: 3693729,
      area_type: 'MSA',
    },
    {
      index: 1285,
      name: 'Chisago County, Minnesota',
      population: 57988,
      area_type: 'County',
    },
  ],
  1288: [
    {
      index: 1286,
      name: 'Fargo-Wahpeton, ND-MN CSA',
      population: 281593,
      area_type: 'CSA',
    },
    {
      index: 1287,
      name: 'Fargo, ND-MN MSA',
      population: 258663,
      area_type: 'MSA',
    },
    {
      index: 1288,
      name: 'Clay County, Minnesota',
      population: 65929,
      area_type: 'County',
    },
  ],
  1289: [
    {
      index: 1283,
      name: 'Brainerd, MN μSA',
      population: 99222,
      area_type: 'μSA',
    },
    {
      index: 1289,
      name: 'Crow Wing County, Minnesota',
      population: 67948,
      area_type: 'County',
    },
  ],
  1290: [
    {
      index: 1268,
      name: 'Minneapolis-St. Paul, MN-WI CSA',
      population: 4085415,
      area_type: 'CSA',
    },
    {
      index: 1269,
      name: 'Minneapolis-St. Paul-Bloomington, MN-WI MSA',
      population: 3693729,
      area_type: 'MSA',
    },
    {
      index: 1290,
      name: 'Dakota County, Minnesota',
      population: 443341,
      area_type: 'County',
    },
  ],
  1293: [
    {
      index: 1291,
      name: 'Rochester-Austin, MN CSA',
      population: 268213,
      area_type: 'CSA',
    },
    {
      index: 1292,
      name: 'Rochester, MN MSA',
      population: 228073,
      area_type: 'MSA',
    },
    {
      index: 1293,
      name: 'Dodge County, Minnesota',
      population: 20981,
      area_type: 'County',
    },
  ],
  1294: [
    {
      index: 1294,
      name: 'Douglas County, Minnesota (Alexandria, MN μSA)',
      population: 39668,
      area_type: 'μSA',
    },
  ],
  1296: [
    {
      index: 1291,
      name: 'Rochester-Austin, MN CSA',
      population: 268213,
      area_type: 'CSA',
    },
    {
      index: 1292,
      name: 'Rochester, MN MSA',
      population: 228073,
      area_type: 'MSA',
    },
    {
      index: 1296,
      name: 'Fillmore County, Minnesota',
      population: 21414,
      area_type: 'County',
    },
  ],
  1297: [
    {
      index: 1297,
      name: 'Freeborn County, Minnesota (Albert Lea, MN μSA)',
      population: 30718,
      area_type: 'μSA',
    },
  ],
  1299: [
    {
      index: 1268,
      name: 'Minneapolis-St. Paul, MN-WI CSA',
      population: 4085415,
      area_type: 'CSA',
    },
    {
      index: 1299,
      name: 'Goodhue County, Minnesota (Red Wing, MN μSA)',
      population: 48013,
      area_type: 'μSA',
    },
  ],
  1301: [
    {
      index: 1268,
      name: 'Minneapolis-St. Paul, MN-WI CSA',
      population: 4085415,
      area_type: 'CSA',
    },
    {
      index: 1269,
      name: 'Minneapolis-St. Paul-Bloomington, MN-WI MSA',
      population: 3693729,
      area_type: 'MSA',
    },
    {
      index: 1301,
      name: 'Hennepin County, Minnesota',
      population: 1260121,
      area_type: 'County',
    },
  ],
  1303: [
    {
      index: 1302,
      name: 'La Crosse-Onalaska, WI-MN MSA',
      population: 139094,
      area_type: 'MSA',
    },
    {
      index: 1303,
      name: 'Houston County, Minnesota',
      population: 18800,
      area_type: 'County',
    },
  ],
  1304: [
    {
      index: 1268,
      name: 'Minneapolis-St. Paul, MN-WI CSA',
      population: 4085415,
      area_type: 'CSA',
    },
    {
      index: 1269,
      name: 'Minneapolis-St. Paul-Bloomington, MN-WI MSA',
      population: 3693729,
      area_type: 'MSA',
    },
    {
      index: 1304,
      name: 'Isanti County, Minnesota',
      population: 42727,
      area_type: 'County',
    },
  ],
  1305: [
    {
      index: 1305,
      name: 'Itasca County, Minnesota (Grand Rapids, MN μSA)',
      population: 45205,
      area_type: 'μSA',
    },
  ],
  1307: [
    {
      index: 1307,
      name: 'Kandiyohi County, Minnesota (Willmar, MN μSA)',
      population: 43839,
      area_type: 'μSA',
    },
  ],
  1309: [
    {
      index: 1280,
      name: 'Duluth, MN-WI MSA',
      population: 291323,
      area_type: 'MSA',
    },
    {
      index: 1309,
      name: 'Lake County, Minnesota',
      population: 10939,
      area_type: 'County',
    },
  ],
  1310: [
    {
      index: 1268,
      name: 'Minneapolis-St. Paul, MN-WI CSA',
      population: 4085415,
      area_type: 'CSA',
    },
    {
      index: 1269,
      name: 'Minneapolis-St. Paul-Bloomington, MN-WI MSA',
      population: 3693729,
      area_type: 'MSA',
    },
    {
      index: 1310,
      name: 'Le Sueur County, Minnesota',
      population: 29153,
      area_type: 'County',
    },
  ],
  1311: [
    {
      index: 1311,
      name: 'Lyon County, Minnesota (Marshall, MN μSA)',
      population: 25262,
      area_type: 'μSA',
    },
  ],
  1313: [
    {
      index: 1268,
      name: 'Minneapolis-St. Paul, MN-WI CSA',
      population: 4085415,
      area_type: 'CSA',
    },
    {
      index: 1313,
      name: 'McLeod County, Minnesota (Hutchinson, MN μSA)',
      population: 36714,
      area_type: 'μSA',
    },
  ],
  1315: [
    {
      index: 1315,
      name: 'Martin County, Minnesota (Fairmont, MN μSA)',
      population: 19650,
      area_type: 'μSA',
    },
  ],
  1317: [
    {
      index: 1268,
      name: 'Minneapolis-St. Paul, MN-WI CSA',
      population: 4085415,
      area_type: 'CSA',
    },
    {
      index: 1269,
      name: 'Minneapolis-St. Paul-Bloomington, MN-WI MSA',
      population: 3693729,
      area_type: 'MSA',
    },
    {
      index: 1317,
      name: 'Mille Lacs County, Minnesota',
      population: 27280,
      area_type: 'County',
    },
  ],
  1318: [
    {
      index: 1291,
      name: 'Rochester-Austin, MN CSA',
      population: 268213,
      area_type: 'CSA',
    },
    {
      index: 1318,
      name: 'Mower County, Minnesota (Austin, MN μSA)',
      population: 40140,
      area_type: 'μSA',
    },
  ],
  1320: [
    {
      index: 1275,
      name: 'Mankato-New Ulm, MN CSA',
      population: 129795,
      area_type: 'CSA',
    },
    {
      index: 1276,
      name: 'Mankato, MN MSA',
      population: 104072,
      area_type: 'MSA',
    },
    {
      index: 1320,
      name: 'Nicollet County, Minnesota',
      population: 34441,
      area_type: 'County',
    },
  ],
  1321: [
    {
      index: 1321,
      name: 'Nobles County, Minnesota (Worthington, MN μSA)',
      population: 21947,
      area_type: 'μSA',
    },
  ],
  1323: [
    {
      index: 1291,
      name: 'Rochester-Austin, MN CSA',
      population: 268213,
      area_type: 'CSA',
    },
    {
      index: 1292,
      name: 'Rochester, MN MSA',
      population: 228073,
      area_type: 'MSA',
    },
    {
      index: 1323,
      name: 'Olmsted County, Minnesota',
      population: 164020,
      area_type: 'County',
    },
  ],
  1324: [
    {
      index: 1324,
      name: 'Otter Tail County, Minnesota (Fergus Falls, MN μSA)',
      population: 60519,
      area_type: 'μSA',
    },
  ],
  1327: [
    {
      index: 1326,
      name: 'Grand Forks, ND-MN MSA',
      population: 103144,
      area_type: 'MSA',
    },
    {
      index: 1327,
      name: 'Polk County, Minnesota',
      population: 30731,
      area_type: 'County',
    },
  ],
  1328: [
    {
      index: 1268,
      name: 'Minneapolis-St. Paul, MN-WI CSA',
      population: 4085415,
      area_type: 'CSA',
    },
    {
      index: 1269,
      name: 'Minneapolis-St. Paul-Bloomington, MN-WI MSA',
      population: 3693729,
      area_type: 'MSA',
    },
    {
      index: 1328,
      name: 'Ramsey County, Minnesota',
      population: 536413,
      area_type: 'County',
    },
  ],
  1329: [
    {
      index: 1268,
      name: 'Minneapolis-St. Paul, MN-WI CSA',
      population: 4085415,
      area_type: 'CSA',
    },
    {
      index: 1329,
      name: 'Rice County, Minnesota (Faribault-Northfield, MN μSA)',
      population: 67693,
      area_type: 'μSA',
    },
  ],
  1331: [
    {
      index: 1280,
      name: 'Duluth, MN-WI MSA',
      population: 291323,
      area_type: 'MSA',
    },
    {
      index: 1331,
      name: 'St. Louis County, Minnesota',
      population: 199532,
      area_type: 'County',
    },
  ],
  1332: [
    {
      index: 1268,
      name: 'Minneapolis-St. Paul, MN-WI CSA',
      population: 4085415,
      area_type: 'CSA',
    },
    {
      index: 1269,
      name: 'Minneapolis-St. Paul-Bloomington, MN-WI MSA',
      population: 3693729,
      area_type: 'MSA',
    },
    {
      index: 1332,
      name: 'Scott County, Minnesota',
      population: 154520,
      area_type: 'County',
    },
  ],
  1333: [
    {
      index: 1268,
      name: 'Minneapolis-St. Paul, MN-WI CSA',
      population: 4085415,
      area_type: 'CSA',
    },
    {
      index: 1269,
      name: 'Minneapolis-St. Paul-Bloomington, MN-WI MSA',
      population: 3693729,
      area_type: 'MSA',
    },
    {
      index: 1333,
      name: 'Sherburne County, Minnesota',
      population: 100824,
      area_type: 'County',
    },
  ],
  1334: [
    {
      index: 1268,
      name: 'Minneapolis-St. Paul, MN-WI CSA',
      population: 4085415,
      area_type: 'CSA',
    },
    {
      index: 1273,
      name: 'St. Cloud, MN MSA',
      population: 201868,
      area_type: 'MSA',
    },
    {
      index: 1334,
      name: 'Stearns County, Minnesota',
      population: 160405,
      area_type: 'County',
    },
  ],
  1335: [
    {
      index: 1268,
      name: 'Minneapolis-St. Paul, MN-WI CSA',
      population: 4085415,
      area_type: 'CSA',
    },
    {
      index: 1335,
      name: 'Steele County, Minnesota (Owatonna, MN μSA)',
      population: 37398,
      area_type: 'μSA',
    },
  ],
  1337: [
    {
      index: 1291,
      name: 'Rochester-Austin, MN CSA',
      population: 268213,
      area_type: 'CSA',
    },
    {
      index: 1292,
      name: 'Rochester, MN MSA',
      population: 228073,
      area_type: 'MSA',
    },
    {
      index: 1337,
      name: 'Wabasha County, Minnesota',
      population: 21658,
      area_type: 'County',
    },
  ],
  1338: [
    {
      index: 1268,
      name: 'Minneapolis-St. Paul, MN-WI CSA',
      population: 4085415,
      area_type: 'CSA',
    },
    {
      index: 1269,
      name: 'Minneapolis-St. Paul-Bloomington, MN-WI MSA',
      population: 3693729,
      area_type: 'MSA',
    },
    {
      index: 1338,
      name: 'Washington County, Minnesota',
      population: 275912,
      area_type: 'County',
    },
  ],
  1340: [
    {
      index: 1286,
      name: 'Fargo-Wahpeton, ND-MN CSA',
      population: 281593,
      area_type: 'CSA',
    },
    {
      index: 1339,
      name: 'Wahpeton, ND-MN μSA',
      population: 22930,
      area_type: 'μSA',
    },
    {
      index: 1340,
      name: 'Wilkin County, Minnesota',
      population: 6350,
      area_type: 'County',
    },
  ],
  1341: [
    {
      index: 1341,
      name: 'Winona County, Minnesota (Winona, MN μSA)',
      population: 49478,
      area_type: 'μSA',
    },
  ],
  1343: [
    {
      index: 1268,
      name: 'Minneapolis-St. Paul, MN-WI CSA',
      population: 4085415,
      area_type: 'CSA',
    },
    {
      index: 1269,
      name: 'Minneapolis-St. Paul-Bloomington, MN-WI MSA',
      population: 3693729,
      area_type: 'MSA',
    },
    {
      index: 1343,
      name: 'Wright County, Minnesota',
      population: 148003,
      area_type: 'County',
    },
  ],
  1344: [
    {
      index: 1068,
      name: 'Natchez, MS-LA μSA',
      population: 46524,
      area_type: 'μSA',
    },
    {
      index: 1344,
      name: 'Adams County, Mississippi',
      population: 28408,
      area_type: 'County',
    },
  ],
  1346: [
    {
      index: 1345,
      name: 'Tupelo-Corinth, MS CSA',
      population: 197247,
      area_type: 'CSA',
    },
    {
      index: 1346,
      name: 'Alcorn County, Mississippi (Corinth, MS μSA)',
      population: 34204,
      area_type: 'μSA',
    },
  ],
  1349: [
    {
      index: 1348,
      name: 'Cleveland-Indianola, MS CSA',
      population: 54181,
      area_type: 'CSA',
    },
    {
      index: 1349,
      name: 'Bolivar County, Mississippi (Cleveland, MS μSA)',
      population: 29370,
      area_type: 'μSA',
    },
  ],
  1352: [
    {
      index: 1351,
      name: 'Greenwood, MS μSA',
      population: 36301,
      area_type: 'μSA',
    },
    {
      index: 1352,
      name: 'Carroll County, Mississippi',
      population: 9731,
      area_type: 'County',
    },
  ],
  1354: [
    {
      index: 1353,
      name: 'Meridian, MS μSA',
      population: 94829,
      area_type: 'μSA',
    },
    {
      index: 1354,
      name: 'Clarke County, Mississippi',
      population: 15271,
      area_type: 'County',
    },
  ],
  1356: [
    {
      index: 1355,
      name: 'Columbus-West Point, MS CSA',
      population: 75983,
      area_type: 'CSA',
    },
    {
      index: 1356,
      name: 'Clay County, Mississippi (West Point, MS μSA)',
      population: 18380,
      area_type: 'μSA',
    },
  ],
  1358: [
    {
      index: 1358,
      name: 'Coahoma County, Mississippi (Clarksdale, MS μSA)',
      population: 20197,
      area_type: 'μSA',
    },
  ],
  1362: [
    {
      index: 1360,
      name: 'Jackson-Vicksburg-Brookhaven, MS CSA',
      population: 660563,
      area_type: 'CSA',
    },
    {
      index: 1361,
      name: 'Jackson, MS MSA',
      population: 583197,
      area_type: 'MSA',
    },
    {
      index: 1362,
      name: 'Copiah County, Mississippi',
      population: 27719,
      area_type: 'County',
    },
  ],
  1365: [
    {
      index: 1363,
      name: 'Hattiesburg-Laurel, MS CSA',
      population: 256095,
      area_type: 'CSA',
    },
    {
      index: 1364,
      name: 'Hattiesburg, MS MSA',
      population: 173359,
      area_type: 'MSA',
    },
    {
      index: 1365,
      name: 'Covington County, Mississippi',
      population: 18098,
      area_type: 'County',
    },
  ],
  1366: [
    {
      index: 136,
      name: 'Memphis-Forrest City, TN-MS-AR CSA',
      population: 1354756,
      area_type: 'CSA',
    },
    {
      index: 137,
      name: 'Memphis, TN-MS-AR MSA',
      population: 1332305,
      area_type: 'MSA',
    },
    {
      index: 1366,
      name: 'DeSoto County, Mississippi',
      population: 191723,
      area_type: 'County',
    },
  ],
  1367: [
    {
      index: 1363,
      name: 'Hattiesburg-Laurel, MS CSA',
      population: 256095,
      area_type: 'CSA',
    },
    {
      index: 1364,
      name: 'Hattiesburg, MS MSA',
      population: 173359,
      area_type: 'MSA',
    },
    {
      index: 1367,
      name: 'Forrest County, Mississippi',
      population: 78110,
      area_type: 'County',
    },
  ],
  1368: [
    {
      index: 1368,
      name: 'Grenada County, Mississippi (Grenada, MS μSA)',
      population: 21088,
      area_type: 'μSA',
    },
  ],
  1371: [
    {
      index: 1370,
      name: 'Gulfport-Biloxi, MS MSA',
      population: 420782,
      area_type: 'MSA',
    },
    {
      index: 1371,
      name: 'Hancock County, Mississippi',
      population: 46094,
      area_type: 'County',
    },
  ],
  1372: [
    {
      index: 1370,
      name: 'Gulfport-Biloxi, MS MSA',
      population: 420782,
      area_type: 'MSA',
    },
    {
      index: 1372,
      name: 'Harrison County, Mississippi',
      population: 211044,
      area_type: 'County',
    },
  ],
  1373: [
    {
      index: 1360,
      name: 'Jackson-Vicksburg-Brookhaven, MS CSA',
      population: 660563,
      area_type: 'CSA',
    },
    {
      index: 1361,
      name: 'Jackson, MS MSA',
      population: 583197,
      area_type: 'MSA',
    },
    {
      index: 1373,
      name: 'Hinds County, Mississippi',
      population: 217730,
      area_type: 'County',
    },
  ],
  1374: [
    {
      index: 1360,
      name: 'Jackson-Vicksburg-Brookhaven, MS CSA',
      population: 660563,
      area_type: 'CSA',
    },
    {
      index: 1361,
      name: 'Jackson, MS MSA',
      population: 583197,
      area_type: 'MSA',
    },
    {
      index: 1374,
      name: 'Holmes County, Mississippi',
      population: 16121,
      area_type: 'County',
    },
  ],
  1376: [
    {
      index: 1345,
      name: 'Tupelo-Corinth, MS CSA',
      population: 197247,
      area_type: 'CSA',
    },
    {
      index: 1375,
      name: 'Tupelo, MS μSA',
      population: 163043,
      area_type: 'μSA',
    },
    {
      index: 1376,
      name: 'Itawamba County, Mississippi',
      population: 23903,
      area_type: 'County',
    },
  ],
  1377: [
    {
      index: 1370,
      name: 'Gulfport-Biloxi, MS MSA',
      population: 420782,
      area_type: 'MSA',
    },
    {
      index: 1377,
      name: 'Jackson County, Mississippi',
      population: 144975,
      area_type: 'County',
    },
  ],
  1379: [
    {
      index: 1363,
      name: 'Hattiesburg-Laurel, MS CSA',
      population: 256095,
      area_type: 'CSA',
    },
    {
      index: 1378,
      name: 'Laurel, MS μSA',
      population: 82736,
      area_type: 'μSA',
    },
    {
      index: 1379,
      name: 'Jasper County, Mississippi',
      population: 16167,
      area_type: 'County',
    },
  ],
  1380: [
    {
      index: 1363,
      name: 'Hattiesburg-Laurel, MS CSA',
      population: 256095,
      area_type: 'CSA',
    },
    {
      index: 1378,
      name: 'Laurel, MS μSA',
      population: 82736,
      area_type: 'μSA',
    },
    {
      index: 1380,
      name: 'Jones County, Mississippi',
      population: 66569,
      area_type: 'County',
    },
  ],
  1381: [
    {
      index: 1353,
      name: 'Meridian, MS μSA',
      population: 94829,
      area_type: 'μSA',
    },
    {
      index: 1381,
      name: 'Kemper County, Mississippi',
      population: 8654,
      area_type: 'County',
    },
  ],
  1382: [
    {
      index: 1382,
      name: 'Lafayette County, Mississippi (Oxford, MS μSA)',
      population: 57615,
      area_type: 'μSA',
    },
  ],
  1384: [
    {
      index: 1363,
      name: 'Hattiesburg-Laurel, MS CSA',
      population: 256095,
      area_type: 'CSA',
    },
    {
      index: 1364,
      name: 'Hattiesburg, MS MSA',
      population: 173359,
      area_type: 'MSA',
    },
    {
      index: 1384,
      name: 'Lamar County, Mississippi',
      population: 65783,
      area_type: 'County',
    },
  ],
  1385: [
    {
      index: 1353,
      name: 'Meridian, MS μSA',
      population: 94829,
      area_type: 'μSA',
    },
    {
      index: 1385,
      name: 'Lauderdale County, Mississippi',
      population: 70904,
      area_type: 'County',
    },
  ],
  1386: [
    {
      index: 1345,
      name: 'Tupelo-Corinth, MS CSA',
      population: 197247,
      area_type: 'CSA',
    },
    {
      index: 1375,
      name: 'Tupelo, MS μSA',
      population: 163043,
      area_type: 'μSA',
    },
    {
      index: 1386,
      name: 'Lee County, Mississippi',
      population: 82959,
      area_type: 'County',
    },
  ],
  1387: [
    {
      index: 1351,
      name: 'Greenwood, MS μSA',
      population: 36301,
      area_type: 'μSA',
    },
    {
      index: 1387,
      name: 'Leflore County, Mississippi',
      population: 26570,
      area_type: 'County',
    },
  ],
  1388: [
    {
      index: 1360,
      name: 'Jackson-Vicksburg-Brookhaven, MS CSA',
      population: 660563,
      area_type: 'CSA',
    },
    {
      index: 1388,
      name: 'Lincoln County, Mississippi (Brookhaven, MS μSA)',
      population: 34717,
      area_type: 'μSA',
    },
  ],
  1390: [
    {
      index: 1355,
      name: 'Columbus-West Point, MS CSA',
      population: 75983,
      area_type: 'CSA',
    },
    {
      index: 1390,
      name: 'Lowndes County, Mississippi (Columbus, MS μSA)',
      population: 57603,
      area_type: 'μSA',
    },
  ],
  1392: [
    {
      index: 1360,
      name: 'Jackson-Vicksburg-Brookhaven, MS CSA',
      population: 660563,
      area_type: 'CSA',
    },
    {
      index: 1361,
      name: 'Jackson, MS MSA',
      population: 583197,
      area_type: 'MSA',
    },
    {
      index: 1392,
      name: 'Madison County, Mississippi',
      population: 111113,
      area_type: 'County',
    },
  ],
  1393: [
    {
      index: 136,
      name: 'Memphis-Forrest City, TN-MS-AR CSA',
      population: 1354756,
      area_type: 'CSA',
    },
    {
      index: 137,
      name: 'Memphis, TN-MS-AR MSA',
      population: 1332305,
      area_type: 'MSA',
    },
    {
      index: 1393,
      name: 'Marshall County, Mississippi',
      population: 34110,
      area_type: 'County',
    },
  ],
  1395: [
    {
      index: 1394,
      name: 'Starkville, MS μSA',
      population: 61420,
      area_type: 'μSA',
    },
    {
      index: 1395,
      name: 'Oktibbeha County, Mississippi',
      population: 51427,
      area_type: 'County',
    },
  ],
  1396: [
    {
      index: 1077,
      name: 'New Orleans-Metairie-Hammond, LA-MS CSA',
      population: 1485510,
      area_type: 'CSA',
    },
    {
      index: 1396,
      name: 'Pearl River County, Mississippi (Picayune, MS μSA)',
      population: 57261,
      area_type: 'μSA',
    },
  ],
  1398: [
    {
      index: 1363,
      name: 'Hattiesburg-Laurel, MS CSA',
      population: 256095,
      area_type: 'CSA',
    },
    {
      index: 1364,
      name: 'Hattiesburg, MS MSA',
      population: 173359,
      area_type: 'MSA',
    },
    {
      index: 1398,
      name: 'Perry County, Mississippi',
      population: 11368,
      area_type: 'County',
    },
  ],
  1399: [
    {
      index: 1399,
      name: 'Pike County, Mississippi (McComb, MS μSA)',
      population: 39644,
      area_type: 'μSA',
    },
  ],
  1401: [
    {
      index: 1345,
      name: 'Tupelo-Corinth, MS CSA',
      population: 197247,
      area_type: 'CSA',
    },
    {
      index: 1375,
      name: 'Tupelo, MS μSA',
      population: 163043,
      area_type: 'μSA',
    },
    {
      index: 1401,
      name: 'Pontotoc County, Mississippi',
      population: 31389,
      area_type: 'County',
    },
  ],
  1402: [
    {
      index: 1345,
      name: 'Tupelo-Corinth, MS CSA',
      population: 197247,
      area_type: 'CSA',
    },
    {
      index: 1375,
      name: 'Tupelo, MS μSA',
      population: 163043,
      area_type: 'μSA',
    },
    {
      index: 1402,
      name: 'Prentiss County, Mississippi',
      population: 24792,
      area_type: 'County',
    },
  ],
  1403: [
    {
      index: 1360,
      name: 'Jackson-Vicksburg-Brookhaven, MS CSA',
      population: 660563,
      area_type: 'CSA',
    },
    {
      index: 1361,
      name: 'Jackson, MS MSA',
      population: 583197,
      area_type: 'MSA',
    },
    {
      index: 1403,
      name: 'Rankin County, Mississippi',
      population: 158979,
      area_type: 'County',
    },
  ],
  1404: [
    {
      index: 1360,
      name: 'Jackson-Vicksburg-Brookhaven, MS CSA',
      population: 660563,
      area_type: 'CSA',
    },
    {
      index: 1361,
      name: 'Jackson, MS MSA',
      population: 583197,
      area_type: 'MSA',
    },
    {
      index: 1404,
      name: 'Simpson County, Mississippi',
      population: 25587,
      area_type: 'County',
    },
  ],
  1405: [
    {
      index: 1370,
      name: 'Gulfport-Biloxi, MS MSA',
      population: 420782,
      area_type: 'MSA',
    },
    {
      index: 1405,
      name: 'Stone County, Mississippi',
      population: 18669,
      area_type: 'County',
    },
  ],
  1406: [
    {
      index: 1348,
      name: 'Cleveland-Indianola, MS CSA',
      population: 54181,
      area_type: 'CSA',
    },
    {
      index: 1406,
      name: 'Sunflower County, Mississippi (Indianola, MS μSA)',
      population: 24811,
      area_type: 'μSA',
    },
  ],
  1408: [
    {
      index: 136,
      name: 'Memphis-Forrest City, TN-MS-AR CSA',
      population: 1354756,
      area_type: 'CSA',
    },
    {
      index: 137,
      name: 'Memphis, TN-MS-AR MSA',
      population: 1332305,
      area_type: 'MSA',
    },
    {
      index: 1408,
      name: 'Tate County, Mississippi',
      population: 28296,
      area_type: 'County',
    },
  ],
  1409: [
    {
      index: 136,
      name: 'Memphis-Forrest City, TN-MS-AR CSA',
      population: 1354756,
      area_type: 'CSA',
    },
    {
      index: 137,
      name: 'Memphis, TN-MS-AR MSA',
      population: 1332305,
      area_type: 'MSA',
    },
    {
      index: 1409,
      name: 'Tunica County, Mississippi',
      population: 9458,
      area_type: 'County',
    },
  ],
  1410: [
    {
      index: 1360,
      name: 'Jackson-Vicksburg-Brookhaven, MS CSA',
      population: 660563,
      area_type: 'CSA',
    },
    {
      index: 1410,
      name: 'Warren County, Mississippi (Vicksburg, MS μSA)',
      population: 42649,
      area_type: 'μSA',
    },
  ],
  1412: [
    {
      index: 1412,
      name: 'Washington County, Mississippi (Greenville, MS μSA)',
      population: 42514,
      area_type: 'μSA',
    },
  ],
  1414: [
    {
      index: 1394,
      name: 'Starkville, MS μSA',
      population: 61420,
      area_type: 'μSA',
    },
    {
      index: 1414,
      name: 'Webster County, Mississippi',
      population: 9993,
      area_type: 'County',
    },
  ],
  1415: [
    {
      index: 1360,
      name: 'Jackson-Vicksburg-Brookhaven, MS CSA',
      population: 660563,
      area_type: 'CSA',
    },
    {
      index: 1361,
      name: 'Jackson, MS MSA',
      population: 583197,
      area_type: 'MSA',
    },
    {
      index: 1415,
      name: 'Yazoo County, Mississippi',
      population: 25948,
      area_type: 'County',
    },
  ],
  1417: [
    {
      index: 1416,
      name: 'Kirksville, MO μSA',
      population: 29167,
      area_type: 'μSA',
    },
    {
      index: 1417,
      name: 'Adair County, Missouri',
      population: 25165,
      area_type: 'County',
    },
  ],
  1418: [
    {
      index: 904,
      name: 'Kansas City-Overland Park-Kansas City, MO-KS CSA',
      population: 2545616,
      area_type: 'CSA',
    },
    {
      index: 918,
      name: 'St. Joseph, MO-KS MSA',
      population: 119690,
      area_type: 'MSA',
    },
    {
      index: 1418,
      name: 'Andrew County, Missouri',
      population: 18003,
      area_type: 'County',
    },
  ],
  1420: [
    {
      index: 1419,
      name: 'Columbia-Moberly-Mexico, MO CSA',
      population: 263686,
      area_type: 'CSA',
    },
    {
      index: 1420,
      name: 'Audrain County, Missouri (Mexico, MO μSA)',
      population: 24434,
      area_type: 'μSA',
    },
  ],
  1422: [
    {
      index: 904,
      name: 'Kansas City-Overland Park-Kansas City, MO-KS CSA',
      population: 2545616,
      area_type: 'CSA',
    },
    {
      index: 936,
      name: 'Kansas City, MO-KS MSA',
      population: 2209494,
      area_type: 'MSA',
    },
    {
      index: 1422,
      name: 'Bates County, Missouri',
      population: 16177,
      area_type: 'County',
    },
  ],
  1423: [
    {
      index: 617,
      name: 'Cape Girardeau-Sikeston, MO-IL CSA',
      population: 136115,
      area_type: 'CSA',
    },
    {
      index: 618,
      name: 'Cape Girardeau, MO-IL MSA',
      population: 98275,
      area_type: 'MSA',
    },
    {
      index: 1423,
      name: 'Bollinger County, Missouri',
      population: 10518,
      area_type: 'County',
    },
  ],
  1425: [
    {
      index: 1419,
      name: 'Columbia-Moberly-Mexico, MO CSA',
      population: 263686,
      area_type: 'CSA',
    },
    {
      index: 1424,
      name: 'Columbia, MO MSA',
      population: 214630,
      area_type: 'MSA',
    },
    {
      index: 1425,
      name: 'Boone County, Missouri',
      population: 187690,
      area_type: 'County',
    },
  ],
  1426: [
    {
      index: 904,
      name: 'Kansas City-Overland Park-Kansas City, MO-KS CSA',
      population: 2545616,
      area_type: 'CSA',
    },
    {
      index: 918,
      name: 'St. Joseph, MO-KS MSA',
      population: 119690,
      area_type: 'MSA',
    },
    {
      index: 1426,
      name: 'Buchanan County, Missouri',
      population: 82911,
      area_type: 'County',
    },
  ],
  1428: [
    {
      index: 1427,
      name: 'Poplar Bluff, MO μSA',
      population: 52882,
      area_type: 'μSA',
    },
    {
      index: 1428,
      name: 'Butler County, Missouri',
      population: 42179,
      area_type: 'County',
    },
  ],
  1429: [
    {
      index: 904,
      name: 'Kansas City-Overland Park-Kansas City, MO-KS CSA',
      population: 2545616,
      area_type: 'CSA',
    },
    {
      index: 936,
      name: 'Kansas City, MO-KS MSA',
      population: 2209494,
      area_type: 'MSA',
    },
    {
      index: 1429,
      name: 'Caldwell County, Missouri',
      population: 8933,
      area_type: 'County',
    },
  ],
  1431: [
    {
      index: 1430,
      name: 'Jefferson City, MO MSA',
      population: 150350,
      area_type: 'MSA',
    },
    {
      index: 1431,
      name: 'Callaway County, Missouri',
      population: 44762,
      area_type: 'County',
    },
  ],
  1432: [
    {
      index: 617,
      name: 'Cape Girardeau-Sikeston, MO-IL CSA',
      population: 136115,
      area_type: 'CSA',
    },
    {
      index: 618,
      name: 'Cape Girardeau, MO-IL MSA',
      population: 98275,
      area_type: 'MSA',
    },
    {
      index: 1432,
      name: 'Cape Girardeau County, Missouri',
      population: 82899,
      area_type: 'County',
    },
  ],
  1433: [
    {
      index: 904,
      name: 'Kansas City-Overland Park-Kansas City, MO-KS CSA',
      population: 2545616,
      area_type: 'CSA',
    },
    {
      index: 936,
      name: 'Kansas City, MO-KS MSA',
      population: 2209494,
      area_type: 'MSA',
    },
    {
      index: 1433,
      name: 'Cass County, Missouri',
      population: 110394,
      area_type: 'County',
    },
  ],
  1435: [
    {
      index: 1434,
      name: 'Springfield, MO MSA',
      population: 487061,
      area_type: 'MSA',
    },
    {
      index: 1435,
      name: 'Christian County, Missouri',
      population: 93114,
      area_type: 'County',
    },
  ],
  1436: [
    {
      index: 648,
      name: 'Burlington-Fort Madison-Keokuk, IA-IL-MO CSA',
      population: 101251,
      area_type: 'CSA',
    },
    {
      index: 649,
      name: 'Fort Madison-Keokuk, IA-IL-MO μSA',
      population: 56807,
      area_type: 'μSA',
    },
    {
      index: 1436,
      name: 'Clark County, Missouri',
      population: 6723,
      area_type: 'County',
    },
  ],
  1437: [
    {
      index: 904,
      name: 'Kansas City-Overland Park-Kansas City, MO-KS CSA',
      population: 2545616,
      area_type: 'CSA',
    },
    {
      index: 936,
      name: 'Kansas City, MO-KS MSA',
      population: 2209494,
      area_type: 'MSA',
    },
    {
      index: 1437,
      name: 'Clay County, Missouri',
      population: 257033,
      area_type: 'County',
    },
  ],
  1438: [
    {
      index: 904,
      name: 'Kansas City-Overland Park-Kansas City, MO-KS CSA',
      population: 2545616,
      area_type: 'CSA',
    },
    {
      index: 936,
      name: 'Kansas City, MO-KS MSA',
      population: 2209494,
      area_type: 'MSA',
    },
    {
      index: 1438,
      name: 'Clinton County, Missouri',
      population: 21328,
      area_type: 'County',
    },
  ],
  1439: [
    {
      index: 1430,
      name: 'Jefferson City, MO MSA',
      population: 150350,
      area_type: 'MSA',
    },
    {
      index: 1439,
      name: 'Cole County, Missouri',
      population: 76969,
      area_type: 'County',
    },
  ],
  1440: [
    {
      index: 1419,
      name: 'Columbia-Moberly-Mexico, MO CSA',
      population: 263686,
      area_type: 'CSA',
    },
    {
      index: 1424,
      name: 'Columbia, MO MSA',
      population: 214630,
      area_type: 'MSA',
    },
    {
      index: 1440,
      name: 'Cooper County, Missouri',
      population: 16772,
      area_type: 'County',
    },
  ],
  1441: [
    {
      index: 1434,
      name: 'Springfield, MO MSA',
      population: 487061,
      area_type: 'MSA',
    },
    {
      index: 1441,
      name: 'Dallas County, Missouri',
      population: 17626,
      area_type: 'County',
    },
  ],
  1442: [
    {
      index: 904,
      name: 'Kansas City-Overland Park-Kansas City, MO-KS CSA',
      population: 2545616,
      area_type: 'CSA',
    },
    {
      index: 918,
      name: 'St. Joseph, MO-KS MSA',
      population: 119690,
      area_type: 'MSA',
    },
    {
      index: 1442,
      name: 'DeKalb County, Missouri',
      population: 11336,
      area_type: 'County',
    },
  ],
  1443: [
    {
      index: 1443,
      name: 'Dunklin County, Missouri (Kennett, MO μSA)',
      population: 27406,
      area_type: 'μSA',
    },
  ],
  1445: [
    {
      index: 620,
      name: 'St. Louis-St. Charles-Farmington, MO-IL CSA',
      population: 2905202,
      area_type: 'CSA',
    },
    {
      index: 621,
      name: 'St. Louis, MO-IL MSA',
      population: 2801319,
      area_type: 'MSA',
    },
    {
      index: 1445,
      name: 'Franklin County, Missouri',
      population: 105879,
      area_type: 'County',
    },
  ],
  1446: [
    {
      index: 1434,
      name: 'Springfield, MO MSA',
      population: 487061,
      area_type: 'MSA',
    },
    {
      index: 1446,
      name: 'Greene County, Missouri',
      population: 303293,
      area_type: 'County',
    },
  ],
  1447: [
    {
      index: 1419,
      name: 'Columbia-Moberly-Mexico, MO CSA',
      population: 263686,
      area_type: 'CSA',
    },
    {
      index: 1424,
      name: 'Columbia, MO MSA',
      population: 214630,
      area_type: 'MSA',
    },
    {
      index: 1447,
      name: 'Howard County, Missouri',
      population: 10168,
      area_type: 'County',
    },
  ],
  1448: [
    {
      index: 1448,
      name: 'Howell County, Missouri (West Plains, MO μSA)',
      population: 40631,
      area_type: 'μSA',
    },
  ],
  1450: [
    {
      index: 904,
      name: 'Kansas City-Overland Park-Kansas City, MO-KS CSA',
      population: 2545616,
      area_type: 'CSA',
    },
    {
      index: 936,
      name: 'Kansas City, MO-KS MSA',
      population: 2209494,
      area_type: 'MSA',
    },
    {
      index: 1450,
      name: 'Jackson County, Missouri',
      population: 716531,
      area_type: 'County',
    },
  ],
  1453: [
    {
      index: 1451,
      name: 'Joplin-Miami, MO-OK CSA',
      population: 214424,
      area_type: 'CSA',
    },
    {
      index: 1452,
      name: 'Joplin, MO MSA',
      population: 184086,
      area_type: 'MSA',
    },
    {
      index: 1453,
      name: 'Jasper County, Missouri',
      population: 124075,
      area_type: 'County',
    },
  ],
  1454: [
    {
      index: 620,
      name: 'St. Louis-St. Charles-Farmington, MO-IL CSA',
      population: 2905202,
      area_type: 'CSA',
    },
    {
      index: 621,
      name: 'St. Louis, MO-IL MSA',
      population: 2801319,
      area_type: 'MSA',
    },
    {
      index: 1454,
      name: 'Jefferson County, Missouri',
      population: 229336,
      area_type: 'County',
    },
  ],
  1455: [
    {
      index: 904,
      name: 'Kansas City-Overland Park-Kansas City, MO-KS CSA',
      population: 2545616,
      area_type: 'CSA',
    },
    {
      index: 1455,
      name: 'Johnson County, Missouri (Warrensburg, MO μSA)',
      population: 54368,
      area_type: 'μSA',
    },
  ],
  1457: [
    {
      index: 1457,
      name: 'Laclede County, Missouri (Lebanon, MO μSA)',
      population: 36313,
      area_type: 'μSA',
    },
  ],
  1459: [
    {
      index: 904,
      name: 'Kansas City-Overland Park-Kansas City, MO-KS CSA',
      population: 2545616,
      area_type: 'CSA',
    },
    {
      index: 936,
      name: 'Kansas City, MO-KS MSA',
      population: 2209494,
      area_type: 'MSA',
    },
    {
      index: 1459,
      name: 'Lafayette County, Missouri',
      population: 32961,
      area_type: 'County',
    },
  ],
  1460: [
    {
      index: 614,
      name: 'Quincy-Hannibal, IL-MO CSA',
      population: 113474,
      area_type: 'CSA',
    },
    {
      index: 615,
      name: 'Quincy, IL-MO μSA',
      population: 74616,
      area_type: 'μSA',
    },
    {
      index: 1460,
      name: 'Lewis County, Missouri',
      population: 9891,
      area_type: 'County',
    },
  ],
  1461: [
    {
      index: 620,
      name: 'St. Louis-St. Charles-Farmington, MO-IL CSA',
      population: 2905202,
      area_type: 'CSA',
    },
    {
      index: 621,
      name: 'St. Louis, MO-IL MSA',
      population: 2801319,
      area_type: 'MSA',
    },
    {
      index: 1461,
      name: 'Lincoln County, Missouri',
      population: 63155,
      area_type: 'County',
    },
  ],
  1463: [
    {
      index: 614,
      name: 'Quincy-Hannibal, IL-MO CSA',
      population: 113474,
      area_type: 'CSA',
    },
    {
      index: 1462,
      name: 'Hannibal, MO μSA',
      population: 38858,
      area_type: 'μSA',
    },
    {
      index: 1463,
      name: 'Marion County, Missouri',
      population: 28438,
      area_type: 'County',
    },
  ],
  1464: [
    {
      index: 1430,
      name: 'Jefferson City, MO MSA',
      population: 150350,
      area_type: 'MSA',
    },
    {
      index: 1464,
      name: 'Moniteau County, Missouri',
      population: 15220,
      area_type: 'County',
    },
  ],
  1465: [
    {
      index: 1451,
      name: 'Joplin-Miami, MO-OK CSA',
      population: 214424,
      area_type: 'CSA',
    },
    {
      index: 1452,
      name: 'Joplin, MO MSA',
      population: 184086,
      area_type: 'MSA',
    },
    {
      index: 1465,
      name: 'Newton County, Missouri',
      population: 60011,
      area_type: 'County',
    },
  ],
  1466: [
    {
      index: 1466,
      name: 'Nodaway County, Missouri (Maryville, MO μSA)',
      population: 20670,
      area_type: 'μSA',
    },
  ],
  1468: [
    {
      index: 1430,
      name: 'Jefferson City, MO MSA',
      population: 150350,
      area_type: 'MSA',
    },
    {
      index: 1468,
      name: 'Osage County, Missouri',
      population: 13399,
      area_type: 'County',
    },
  ],
  1469: [
    {
      index: 1469,
      name: 'Pettis County, Missouri (Sedalia, MO μSA)',
      population: 43353,
      area_type: 'μSA',
    },
  ],
  1471: [
    {
      index: 1471,
      name: 'Phelps County, Missouri (Rolla, MO μSA)',
      population: 45313,
      area_type: 'μSA',
    },
  ],
  1473: [
    {
      index: 904,
      name: 'Kansas City-Overland Park-Kansas City, MO-KS CSA',
      population: 2545616,
      area_type: 'CSA',
    },
    {
      index: 936,
      name: 'Kansas City, MO-KS MSA',
      population: 2209494,
      area_type: 'MSA',
    },
    {
      index: 1473,
      name: 'Platte County, Missouri',
      population: 110534,
      area_type: 'County',
    },
  ],
  1474: [
    {
      index: 1434,
      name: 'Springfield, MO MSA',
      population: 487061,
      area_type: 'MSA',
    },
    {
      index: 1474,
      name: 'Polk County, Missouri',
      population: 32693,
      area_type: 'County',
    },
  ],
  1475: [
    {
      index: 1475,
      name: 'Pulaski County, Missouri (Fort Leonard Wood, MO μSA)',
      population: 53941,
      area_type: 'μSA',
    },
  ],
  1477: [
    {
      index: 614,
      name: 'Quincy-Hannibal, IL-MO CSA',
      population: 113474,
      area_type: 'CSA',
    },
    {
      index: 1462,
      name: 'Hannibal, MO μSA',
      population: 38858,
      area_type: 'μSA',
    },
    {
      index: 1477,
      name: 'Ralls County, Missouri',
      population: 10420,
      area_type: 'County',
    },
  ],
  1478: [
    {
      index: 1419,
      name: 'Columbia-Moberly-Mexico, MO CSA',
      population: 263686,
      area_type: 'CSA',
    },
    {
      index: 1478,
      name: 'Randolph County, Missouri (Moberly, MO μSA)',
      population: 24622,
      area_type: 'μSA',
    },
  ],
  1480: [
    {
      index: 904,
      name: 'Kansas City-Overland Park-Kansas City, MO-KS CSA',
      population: 2545616,
      area_type: 'CSA',
    },
    {
      index: 936,
      name: 'Kansas City, MO-KS MSA',
      population: 2209494,
      area_type: 'MSA',
    },
    {
      index: 1480,
      name: 'Ray County, Missouri',
      population: 23107,
      area_type: 'County',
    },
  ],
  1481: [
    {
      index: 1427,
      name: 'Poplar Bluff, MO μSA',
      population: 52882,
      area_type: 'μSA',
    },
    {
      index: 1481,
      name: 'Ripley County, Missouri',
      population: 10703,
      area_type: 'County',
    },
  ],
  1482: [
    {
      index: 620,
      name: 'St. Louis-St. Charles-Farmington, MO-IL CSA',
      population: 2905202,
      area_type: 'CSA',
    },
    {
      index: 621,
      name: 'St. Louis, MO-IL MSA',
      population: 2801319,
      area_type: 'MSA',
    },
    {
      index: 1482,
      name: 'St. Charles County, Missouri',
      population: 413803,
      area_type: 'County',
    },
  ],
  1483: [
    {
      index: 620,
      name: 'St. Louis-St. Charles-Farmington, MO-IL CSA',
      population: 2905202,
      area_type: 'CSA',
    },
    {
      index: 1483,
      name: 'St. Francois County, Missouri (Farmington, MO μSA)',
      population: 66969,
      area_type: 'μSA',
    },
  ],
  1485: [
    {
      index: 620,
      name: 'St. Louis-St. Charles-Farmington, MO-IL CSA',
      population: 2905202,
      area_type: 'CSA',
    },
    {
      index: 621,
      name: 'St. Louis, MO-IL MSA',
      population: 2801319,
      area_type: 'MSA',
    },
    {
      index: 1485,
      name: 'St. Louis County, Missouri',
      population: 990414,
      area_type: 'County',
    },
  ],
  1486: [
    {
      index: 1486,
      name: 'Saline County, Missouri (Marshall, MO μSA)',
      population: 23007,
      area_type: 'μSA',
    },
  ],
  1488: [
    {
      index: 1416,
      name: 'Kirksville, MO μSA',
      population: 29167,
      area_type: 'μSA',
    },
    {
      index: 1488,
      name: 'Schuyler County, Missouri',
      population: 4002,
      area_type: 'County',
    },
  ],
  1489: [
    {
      index: 617,
      name: 'Cape Girardeau-Sikeston, MO-IL CSA',
      population: 136115,
      area_type: 'CSA',
    },
    {
      index: 1489,
      name: 'Scott County, Missouri (Sikeston, MO μSA)',
      population: 37840,
      area_type: 'μSA',
    },
  ],
  1491: [
    {
      index: 1491,
      name: 'Taney County, Missouri (Branson, MO μSA)',
      population: 56821,
      area_type: 'μSA',
    },
  ],
  1493: [
    {
      index: 620,
      name: 'St. Louis-St. Charles-Farmington, MO-IL CSA',
      population: 2905202,
      area_type: 'CSA',
    },
    {
      index: 621,
      name: 'St. Louis, MO-IL MSA',
      population: 2801319,
      area_type: 'MSA',
    },
    {
      index: 1493,
      name: 'Warren County, Missouri',
      population: 37260,
      area_type: 'County',
    },
  ],
  1494: [
    {
      index: 1434,
      name: 'Springfield, MO MSA',
      population: 487061,
      area_type: 'MSA',
    },
    {
      index: 1494,
      name: 'Webster County, Missouri',
      population: 40335,
      area_type: 'County',
    },
  ],
  1495: [
    {
      index: 620,
      name: 'St. Louis-St. Charles-Farmington, MO-IL CSA',
      population: 2905202,
      area_type: 'CSA',
    },
    {
      index: 621,
      name: 'St. Louis, MO-IL MSA',
      population: 2801319,
      area_type: 'MSA',
    },
    {
      index: 1495,
      name: 'St. Louis city, Missouri',
      population: 286578,
      area_type: 'County',
    },
  ],
  1497: [
    {
      index: 1496,
      name: 'Billings, MT MSA',
      population: 190208,
      area_type: 'MSA',
    },
    {
      index: 1497,
      name: 'Carbon County, Montana',
      population: 11179,
      area_type: 'County',
    },
  ],
  1498: [
    {
      index: 1498,
      name: 'Cascade County, Montana (Great Falls, MT MSA)',
      population: 84864,
      area_type: 'MSA',
    },
  ],
  1500: [
    {
      index: 1500,
      name: 'Flathead County, Montana (Kalispell, MT μSA)',
      population: 111814,
      area_type: 'μSA',
    },
  ],
  1502: [
    {
      index: 1502,
      name: 'Gallatin County, Montana (Bozeman, MT μSA)',
      population: 124857,
      area_type: 'μSA',
    },
  ],
  1505: [
    {
      index: 1504,
      name: 'Helena, MT μSA',
      population: 86658,
      area_type: 'μSA',
    },
    {
      index: 1505,
      name: 'Jefferson County, Montana',
      population: 12826,
      area_type: 'County',
    },
  ],
  1506: [
    {
      index: 1504,
      name: 'Helena, MT μSA',
      population: 86658,
      area_type: 'μSA',
    },
    {
      index: 1506,
      name: 'Lewis and Clark County, Montana',
      population: 73832,
      area_type: 'County',
    },
  ],
  1507: [
    {
      index: 1507,
      name: 'Missoula County, Montana (Missoula, MT MSA)',
      population: 121041,
      area_type: 'MSA',
    },
  ],
  1509: [
    {
      index: 1509,
      name: 'Silver Bow County, Montana (Butte-Silver Bow, MT μSA)',
      population: 36068,
      area_type: 'μSA',
    },
  ],
  1511: [
    {
      index: 1496,
      name: 'Billings, MT MSA',
      population: 190208,
      area_type: 'MSA',
    },
    {
      index: 1511,
      name: 'Stillwater County, Montana',
      population: 9177,
      area_type: 'County',
    },
  ],
  1512: [
    {
      index: 1496,
      name: 'Billings, MT MSA',
      population: 190208,
      area_type: 'MSA',
    },
    {
      index: 1512,
      name: 'Yellowstone County, Montana',
      population: 169852,
      area_type: 'County',
    },
  ],
  1513: [
    {
      index: 1513,
      name: 'Adams County, Nebraska (Hastings, NE μSA)',
      population: 30970,
      area_type: 'μSA',
    },
  ],
  1516: [
    {
      index: 1515,
      name: 'Scottsbluff, NE μSA',
      population: 37390,
      area_type: 'μSA',
    },
    {
      index: 1516,
      name: 'Banner County, Nebraska',
      population: 660,
      area_type: 'County',
    },
  ],
  1518: [
    {
      index: 1517,
      name: 'Kearney, NE μSA',
      population: 57276,
      area_type: 'μSA',
    },
    {
      index: 1518,
      name: 'Buffalo County, Nebraska',
      population: 50586,
      area_type: 'County',
    },
  ],
  1519: [
    {
      index: 870,
      name: 'Omaha-Council Bluffs-Fremont, NE-IA CSA',
      population: 1013668,
      area_type: 'CSA',
    },
    {
      index: 871,
      name: 'Omaha-Council Bluffs, NE-IA MSA',
      population: 976671,
      area_type: 'MSA',
    },
    {
      index: 1519,
      name: 'Cass County, Nebraska',
      population: 27122,
      area_type: 'County',
    },
  ],
  1520: [
    {
      index: 901,
      name: 'Sioux City, IA-NE-SD MSA',
      population: 149240,
      area_type: 'MSA',
    },
    {
      index: 1520,
      name: 'Dakota County, Nebraska',
      population: 21042,
      area_type: 'County',
    },
  ],
  1522: [
    {
      index: 1521,
      name: 'Lexington, NE μSA',
      population: 25692,
      area_type: 'μSA',
    },
    {
      index: 1522,
      name: 'Dawson County, Nebraska',
      population: 23884,
      area_type: 'County',
    },
  ],
  1523: [
    {
      index: 901,
      name: 'Sioux City, IA-NE-SD MSA',
      population: 149240,
      area_type: 'MSA',
    },
    {
      index: 1523,
      name: 'Dixon County, Nebraska',
      population: 5464,
      area_type: 'County',
    },
  ],
  1524: [
    {
      index: 870,
      name: 'Omaha-Council Bluffs-Fremont, NE-IA CSA',
      population: 1013668,
      area_type: 'CSA',
    },
    {
      index: 1524,
      name: 'Dodge County, Nebraska (Fremont, NE μSA)',
      population: 36997,
      area_type: 'μSA',
    },
  ],
  1526: [
    {
      index: 870,
      name: 'Omaha-Council Bluffs-Fremont, NE-IA CSA',
      population: 1013668,
      area_type: 'CSA',
    },
    {
      index: 871,
      name: 'Omaha-Council Bluffs, NE-IA MSA',
      population: 976671,
      area_type: 'MSA',
    },
    {
      index: 1526,
      name: 'Douglas County, Nebraska',
      population: 586327,
      area_type: 'County',
    },
  ],
  1528: [
    {
      index: 1527,
      name: 'Lincoln-Beatrice, NE CSA',
      population: 364031,
      area_type: 'CSA',
    },
    {
      index: 1528,
      name: 'Gage County, Nebraska (Beatrice, NE μSA)',
      population: 21583,
      area_type: 'μSA',
    },
  ],
  1530: [
    {
      index: 1521,
      name: 'Lexington, NE μSA',
      population: 25692,
      area_type: 'μSA',
    },
    {
      index: 1530,
      name: 'Gosper County, Nebraska',
      population: 1808,
      area_type: 'County',
    },
  ],
  1532: [
    {
      index: 1531,
      name: 'Grand Island, NE MSA',
      population: 76333,
      area_type: 'MSA',
    },
    {
      index: 1532,
      name: 'Hall County, Nebraska',
      population: 62097,
      area_type: 'County',
    },
  ],
  1533: [
    {
      index: 1531,
      name: 'Grand Island, NE MSA',
      population: 76333,
      area_type: 'MSA',
    },
    {
      index: 1533,
      name: 'Howard County, Nebraska',
      population: 6515,
      area_type: 'County',
    },
  ],
  1534: [
    {
      index: 1517,
      name: 'Kearney, NE μSA',
      population: 57276,
      area_type: 'μSA',
    },
    {
      index: 1534,
      name: 'Kearney County, Nebraska',
      population: 6690,
      area_type: 'County',
    },
  ],
  1536: [
    {
      index: 1527,
      name: 'Lincoln-Beatrice, NE CSA',
      population: 364031,
      area_type: 'CSA',
    },
    {
      index: 1535,
      name: 'Lincoln, NE MSA',
      population: 342448,
      area_type: 'MSA',
    },
    {
      index: 1536,
      name: 'Lancaster County, Nebraska',
      population: 324756,
      area_type: 'County',
    },
  ],
  1538: [
    {
      index: 1537,
      name: 'North Platte, NE μSA',
      population: 34732,
      area_type: 'μSA',
    },
    {
      index: 1538,
      name: 'Lincoln County, Nebraska',
      population: 33685,
      area_type: 'County',
    },
  ],
  1539: [
    {
      index: 1537,
      name: 'North Platte, NE μSA',
      population: 34732,
      area_type: 'μSA',
    },
    {
      index: 1539,
      name: 'Logan County, Nebraska',
      population: 675,
      area_type: 'County',
    },
  ],
  1540: [
    {
      index: 1537,
      name: 'North Platte, NE μSA',
      population: 34732,
      area_type: 'μSA',
    },
    {
      index: 1540,
      name: 'McPherson County, Nebraska',
      population: 372,
      area_type: 'County',
    },
  ],
  1542: [
    {
      index: 1541,
      name: 'Norfolk, NE μSA',
      population: 48417,
      area_type: 'μSA',
    },
    {
      index: 1542,
      name: 'Madison County, Nebraska',
      population: 35368,
      area_type: 'County',
    },
  ],
  1543: [
    {
      index: 1531,
      name: 'Grand Island, NE MSA',
      population: 76333,
      area_type: 'MSA',
    },
    {
      index: 1543,
      name: 'Merrick County, Nebraska',
      population: 7721,
      area_type: 'County',
    },
  ],
  1544: [
    {
      index: 1541,
      name: 'Norfolk, NE μSA',
      population: 48417,
      area_type: 'μSA',
    },
    {
      index: 1544,
      name: 'Pierce County, Nebraska',
      population: 7332,
      area_type: 'County',
    },
  ],
  1545: [
    {
      index: 1545,
      name: 'Platte County, Nebraska (Columbus, NE μSA)',
      population: 34296,
      area_type: 'μSA',
    },
  ],
  1547: [
    {
      index: 870,
      name: 'Omaha-Council Bluffs-Fremont, NE-IA CSA',
      population: 1013668,
      area_type: 'CSA',
    },
    {
      index: 871,
      name: 'Omaha-Council Bluffs, NE-IA MSA',
      population: 976671,
      area_type: 'MSA',
    },
    {
      index: 1547,
      name: 'Sarpy County, Nebraska',
      population: 196553,
      area_type: 'County',
    },
  ],
  1548: [
    {
      index: 870,
      name: 'Omaha-Council Bluffs-Fremont, NE-IA CSA',
      population: 1013668,
      area_type: 'CSA',
    },
    {
      index: 871,
      name: 'Omaha-Council Bluffs, NE-IA MSA',
      population: 976671,
      area_type: 'MSA',
    },
    {
      index: 1548,
      name: 'Saunders County, Nebraska',
      population: 23118,
      area_type: 'County',
    },
  ],
  1549: [
    {
      index: 1515,
      name: 'Scottsbluff, NE μSA',
      population: 37390,
      area_type: 'μSA',
    },
    {
      index: 1549,
      name: 'Scotts Bluff County, Nebraska',
      population: 35603,
      area_type: 'County',
    },
  ],
  1550: [
    {
      index: 1527,
      name: 'Lincoln-Beatrice, NE CSA',
      population: 364031,
      area_type: 'CSA',
    },
    {
      index: 1535,
      name: 'Lincoln, NE MSA',
      population: 342448,
      area_type: 'MSA',
    },
    {
      index: 1550,
      name: 'Seward County, Nebraska',
      population: 17692,
      area_type: 'County',
    },
  ],
  1551: [
    {
      index: 1515,
      name: 'Scottsbluff, NE μSA',
      population: 37390,
      area_type: 'μSA',
    },
    {
      index: 1551,
      name: 'Sioux County, Nebraska',
      population: 1127,
      area_type: 'County',
    },
  ],
  1552: [
    {
      index: 1541,
      name: 'Norfolk, NE μSA',
      population: 48417,
      area_type: 'μSA',
    },
    {
      index: 1552,
      name: 'Stanton County, Nebraska',
      population: 5717,
      area_type: 'County',
    },
  ],
  1553: [
    {
      index: 870,
      name: 'Omaha-Council Bluffs-Fremont, NE-IA CSA',
      population: 1013668,
      area_type: 'CSA',
    },
    {
      index: 871,
      name: 'Omaha-Council Bluffs, NE-IA MSA',
      population: 976671,
      area_type: 'MSA',
    },
    {
      index: 1553,
      name: 'Washington County, Nebraska',
      population: 21167,
      area_type: 'County',
    },
  ],
  1554: [
    {
      index: 1554,
      name: 'Churchill County, Nevada (Fallon, NV μSA)',
      population: 25843,
      area_type: 'μSA',
    },
  ],
  1557: [
    {
      index: 1556,
      name: 'Las Vegas-Henderson, NV CSA',
      population: 2377723,
      area_type: 'CSA',
    },
    {
      index: 1557,
      name: 'Clark County, Nevada (Las Vegas-Henderson-Paradise, NV MSA)',
      population: 2322985,
      area_type: 'MSA',
    },
  ],
  1560: [
    {
      index: 1559,
      name: 'Reno-Carson City-Fernley, NV CSA',
      population: 670258,
      area_type: 'CSA',
    },
    {
      index: 1560,
      name: 'Douglas County, Nevada (Gardnerville Ranchos, NV μSA)',
      population: 49628,
      area_type: 'μSA',
    },
  ],
  1563: [
    {
      index: 1562,
      name: 'Elko, NV μSA',
      population: 55909,
      area_type: 'μSA',
    },
    {
      index: 1563,
      name: 'Elko County, Nevada',
      population: 54046,
      area_type: 'County',
    },
  ],
  1564: [
    {
      index: 1562,
      name: 'Elko, NV μSA',
      population: 55909,
      area_type: 'μSA',
    },
    {
      index: 1564,
      name: 'Eureka County, Nevada',
      population: 1863,
      area_type: 'County',
    },
  ],
  1565: [
    {
      index: 1565,
      name: 'Humboldt County, Nevada (Winnemucca, NV μSA)',
      population: 17272,
      area_type: 'μSA',
    },
  ],
  1567: [
    {
      index: 1559,
      name: 'Reno-Carson City-Fernley, NV CSA',
      population: 670258,
      area_type: 'CSA',
    },
    {
      index: 1567,
      name: 'Lyon County, Nevada (Fernley, NV μSA)',
      population: 61585,
      area_type: 'μSA',
    },
  ],
  1569: [
    {
      index: 1556,
      name: 'Las Vegas-Henderson, NV CSA',
      population: 2377723,
      area_type: 'CSA',
    },
    {
      index: 1569,
      name: 'Nye County, Nevada (Pahrump, NV μSA)',
      population: 54738,
      area_type: 'μSA',
    },
  ],
  1572: [
    {
      index: 1559,
      name: 'Reno-Carson City-Fernley, NV CSA',
      population: 670258,
      area_type: 'CSA',
    },
    {
      index: 1571,
      name: 'Reno, NV MSA',
      population: 500915,
      area_type: 'MSA',
    },
    {
      index: 1572,
      name: 'Storey County, Nevada',
      population: 4170,
      area_type: 'County',
    },
  ],
  1573: [
    {
      index: 1559,
      name: 'Reno-Carson City-Fernley, NV CSA',
      population: 670258,
      area_type: 'CSA',
    },
    {
      index: 1571,
      name: 'Reno, NV MSA',
      population: 500915,
      area_type: 'MSA',
    },
    {
      index: 1573,
      name: 'Washoe County, Nevada',
      population: 496745,
      area_type: 'County',
    },
  ],
  1574: [
    {
      index: 1559,
      name: 'Reno-Carson City-Fernley, NV CSA',
      population: 670258,
      area_type: 'CSA',
    },
    {
      index: 1574,
      name: 'Carson City, Nevada (Carson City, NV MSA)',
      population: 58130,
      area_type: 'MSA',
    },
  ],
  1576: [
    {
      index: 1160,
      name: 'Boston-Worcester-Providence, MA-RI-NH-CT CSA',
      population: 8434341,
      area_type: 'CSA',
    },
    {
      index: 1576,
      name: 'Belknap County, New Hampshire (Laconia, NH μSA)',
      population: 64781,
      area_type: 'μSA',
    },
  ],
  1578: [
    {
      index: 1578,
      name: 'Cheshire County, New Hampshire (Keene, NH μSA)',
      population: 77350,
      area_type: 'μSA',
    },
  ],
  1580: [
    {
      index: 1580,
      name: 'Coos County, New Hampshire (Berlin, NH μSA)',
      population: 31504,
      area_type: 'μSA',
    },
  ],
  1583: [
    {
      index: 1582,
      name: 'Lebanon, NH-VT μSA',
      population: 223072,
      area_type: 'μSA',
    },
    {
      index: 1583,
      name: 'Grafton County, New Hampshire',
      population: 91126,
      area_type: 'County',
    },
  ],
  1584: [
    {
      index: 1160,
      name: 'Boston-Worcester-Providence, MA-RI-NH-CT CSA',
      population: 8434341,
      area_type: 'CSA',
    },
    {
      index: 1584,
      name: 'Hillsborough County, New Hampshire (Manchester-Nashua, NH MSA)',
      population: 426594,
      area_type: 'MSA',
    },
  ],
  1586: [
    {
      index: 1160,
      name: 'Boston-Worcester-Providence, MA-RI-NH-CT CSA',
      population: 8434341,
      area_type: 'CSA',
    },
    {
      index: 1586,
      name: 'Merrimack County, New Hampshire (Concord, NH μSA)',
      population: 156020,
      area_type: 'μSA',
    },
  ],
  1588: [
    {
      index: 1160,
      name: 'Boston-Worcester-Providence, MA-RI-NH-CT CSA',
      population: 8434341,
      area_type: 'CSA',
    },
    {
      index: 1169,
      name: 'Boston-Cambridge-Newton, MA-NH MSA',
      population: 4900550,
      area_type: 'MSA',
    },
    {
      index: 1588,
      name: 'Rockingham County, New Hampshire',
      population: 319424,
      area_type: 'County',
    },
  ],
  1589: [
    {
      index: 1160,
      name: 'Boston-Worcester-Providence, MA-RI-NH-CT CSA',
      population: 8434341,
      area_type: 'CSA',
    },
    {
      index: 1169,
      name: 'Boston-Cambridge-Newton, MA-NH MSA',
      population: 4900550,
      area_type: 'MSA',
    },
    {
      index: 1589,
      name: 'Strafford County, New Hampshire',
      population: 132275,
      area_type: 'County',
    },
  ],
  1590: [
    {
      index: 1582,
      name: 'Lebanon, NH-VT μSA',
      population: 223072,
      area_type: 'μSA',
    },
    {
      index: 1590,
      name: 'Sullivan County, New Hampshire',
      population: 43958,
      area_type: 'County',
    },
  ],
  1591: [
    {
      index: 318,
      name: 'Philadelphia-Reading-Camden, PA-NJ-DE-MD CSA',
      population: 7381187,
      area_type: 'CSA',
    },
    {
      index: 1591,
      name: 'Atlantic County, New Jersey (Atlantic City-Hammonton, NJ MSA)',
      population: 275638,
      area_type: 'MSA',
    },
  ],
  1595: [
    {
      index: 1593,
      name: 'New York-Newark, NY-NJ-CT-PA CSA',
      population: 23070149,
      area_type: 'CSA',
    },
    {
      index: 1594,
      name: 'New York-Newark-Jersey City, NY-NJ-PA MSA',
      population: 19617869,
      area_type: 'MSA',
    },
    {
      index: 1595,
      name: 'Bergen County, New Jersey',
      population: 952997,
      area_type: 'County',
    },
  ],
  1596: [
    {
      index: 318,
      name: 'Philadelphia-Reading-Camden, PA-NJ-DE-MD CSA',
      population: 7381187,
      area_type: 'CSA',
    },
    {
      index: 321,
      name: 'Philadelphia-Camden-Wilmington, PA-NJ-DE-MD MSA',
      population: 6241164,
      area_type: 'MSA',
    },
    {
      index: 1596,
      name: 'Burlington County, New Jersey',
      population: 466103,
      area_type: 'County',
    },
  ],
  1597: [
    {
      index: 318,
      name: 'Philadelphia-Reading-Camden, PA-NJ-DE-MD CSA',
      population: 7381187,
      area_type: 'CSA',
    },
    {
      index: 321,
      name: 'Philadelphia-Camden-Wilmington, PA-NJ-DE-MD MSA',
      population: 6241164,
      area_type: 'MSA',
    },
    {
      index: 1597,
      name: 'Camden County, New Jersey',
      population: 524907,
      area_type: 'County',
    },
  ],
  1598: [
    {
      index: 318,
      name: 'Philadelphia-Reading-Camden, PA-NJ-DE-MD CSA',
      population: 7381187,
      area_type: 'CSA',
    },
    {
      index: 1598,
      name: 'Cape May County, New Jersey (Ocean City, NJ MSA)',
      population: 95634,
      area_type: 'MSA',
    },
  ],
  1600: [
    {
      index: 318,
      name: 'Philadelphia-Reading-Camden, PA-NJ-DE-MD CSA',
      population: 7381187,
      area_type: 'CSA',
    },
    {
      index: 1600,
      name: 'Cumberland County, New Jersey (Vineland-Bridgeton, NJ MSA)',
      population: 151356,
      area_type: 'MSA',
    },
  ],
  1602: [
    {
      index: 1593,
      name: 'New York-Newark, NY-NJ-CT-PA CSA',
      population: 23070149,
      area_type: 'CSA',
    },
    {
      index: 1594,
      name: 'New York-Newark-Jersey City, NY-NJ-PA MSA',
      population: 19617869,
      area_type: 'MSA',
    },
    {
      index: 1602,
      name: 'Essex County, New Jersey',
      population: 849477,
      area_type: 'County',
    },
  ],
  1603: [
    {
      index: 318,
      name: 'Philadelphia-Reading-Camden, PA-NJ-DE-MD CSA',
      population: 7381187,
      area_type: 'CSA',
    },
    {
      index: 321,
      name: 'Philadelphia-Camden-Wilmington, PA-NJ-DE-MD MSA',
      population: 6241164,
      area_type: 'MSA',
    },
    {
      index: 1603,
      name: 'Gloucester County, New Jersey',
      population: 306601,
      area_type: 'County',
    },
  ],
  1604: [
    {
      index: 1593,
      name: 'New York-Newark, NY-NJ-CT-PA CSA',
      population: 23070149,
      area_type: 'CSA',
    },
    {
      index: 1594,
      name: 'New York-Newark-Jersey City, NY-NJ-PA MSA',
      population: 19617869,
      area_type: 'MSA',
    },
    {
      index: 1604,
      name: 'Hudson County, New Jersey',
      population: 703366,
      area_type: 'County',
    },
  ],
  1605: [
    {
      index: 1593,
      name: 'New York-Newark, NY-NJ-CT-PA CSA',
      population: 23070149,
      area_type: 'CSA',
    },
    {
      index: 1594,
      name: 'New York-Newark-Jersey City, NY-NJ-PA MSA',
      population: 19617869,
      area_type: 'MSA',
    },
    {
      index: 1605,
      name: 'Hunterdon County, New Jersey',
      population: 129777,
      area_type: 'County',
    },
  ],
  1606: [
    {
      index: 1593,
      name: 'New York-Newark, NY-NJ-CT-PA CSA',
      population: 23070149,
      area_type: 'CSA',
    },
    {
      index: 1606,
      name: 'Mercer County, New Jersey (Trenton-Princeton, NJ MSA)',
      population: 380688,
      area_type: 'MSA',
    },
  ],
  1608: [
    {
      index: 1593,
      name: 'New York-Newark, NY-NJ-CT-PA CSA',
      population: 23070149,
      area_type: 'CSA',
    },
    {
      index: 1594,
      name: 'New York-Newark-Jersey City, NY-NJ-PA MSA',
      population: 19617869,
      area_type: 'MSA',
    },
    {
      index: 1608,
      name: 'Middlesex County, New Jersey',
      population: 861418,
      area_type: 'County',
    },
  ],
  1609: [
    {
      index: 1593,
      name: 'New York-Newark, NY-NJ-CT-PA CSA',
      population: 23070149,
      area_type: 'CSA',
    },
    {
      index: 1594,
      name: 'New York-Newark-Jersey City, NY-NJ-PA MSA',
      population: 19617869,
      area_type: 'MSA',
    },
    {
      index: 1609,
      name: 'Monmouth County, New Jersey',
      population: 644098,
      area_type: 'County',
    },
  ],
  1610: [
    {
      index: 1593,
      name: 'New York-Newark, NY-NJ-CT-PA CSA',
      population: 23070149,
      area_type: 'CSA',
    },
    {
      index: 1594,
      name: 'New York-Newark-Jersey City, NY-NJ-PA MSA',
      population: 19617869,
      area_type: 'MSA',
    },
    {
      index: 1610,
      name: 'Morris County, New Jersey',
      population: 511151,
      area_type: 'County',
    },
  ],
  1611: [
    {
      index: 1593,
      name: 'New York-Newark, NY-NJ-CT-PA CSA',
      population: 23070149,
      area_type: 'CSA',
    },
    {
      index: 1594,
      name: 'New York-Newark-Jersey City, NY-NJ-PA MSA',
      population: 19617869,
      area_type: 'MSA',
    },
    {
      index: 1611,
      name: 'Ocean County, New Jersey',
      population: 655735,
      area_type: 'County',
    },
  ],
  1612: [
    {
      index: 1593,
      name: 'New York-Newark, NY-NJ-CT-PA CSA',
      population: 23070149,
      area_type: 'CSA',
    },
    {
      index: 1594,
      name: 'New York-Newark-Jersey City, NY-NJ-PA MSA',
      population: 19617869,
      area_type: 'MSA',
    },
    {
      index: 1612,
      name: 'Passaic County, New Jersey',
      population: 513936,
      area_type: 'County',
    },
  ],
  1613: [
    {
      index: 318,
      name: 'Philadelphia-Reading-Camden, PA-NJ-DE-MD CSA',
      population: 7381187,
      area_type: 'CSA',
    },
    {
      index: 321,
      name: 'Philadelphia-Camden-Wilmington, PA-NJ-DE-MD MSA',
      population: 6241164,
      area_type: 'MSA',
    },
    {
      index: 1613,
      name: 'Salem County, New Jersey',
      population: 65117,
      area_type: 'County',
    },
  ],
  1614: [
    {
      index: 1593,
      name: 'New York-Newark, NY-NJ-CT-PA CSA',
      population: 23070149,
      area_type: 'CSA',
    },
    {
      index: 1594,
      name: 'New York-Newark-Jersey City, NY-NJ-PA MSA',
      population: 19617869,
      area_type: 'MSA',
    },
    {
      index: 1614,
      name: 'Somerset County, New Jersey',
      population: 346875,
      area_type: 'County',
    },
  ],
  1615: [
    {
      index: 1593,
      name: 'New York-Newark, NY-NJ-CT-PA CSA',
      population: 23070149,
      area_type: 'CSA',
    },
    {
      index: 1594,
      name: 'New York-Newark-Jersey City, NY-NJ-PA MSA',
      population: 19617869,
      area_type: 'MSA',
    },
    {
      index: 1615,
      name: 'Sussex County, New Jersey',
      population: 146084,
      area_type: 'County',
    },
  ],
  1616: [
    {
      index: 1593,
      name: 'New York-Newark, NY-NJ-CT-PA CSA',
      population: 23070149,
      area_type: 'CSA',
    },
    {
      index: 1594,
      name: 'New York-Newark-Jersey City, NY-NJ-PA MSA',
      population: 19617869,
      area_type: 'MSA',
    },
    {
      index: 1616,
      name: 'Union County, New Jersey',
      population: 569815,
      area_type: 'County',
    },
  ],
  1618: [
    {
      index: 1617,
      name: 'Allentown-Bethlehem-Easton, PA-NJ MSA',
      population: 871229,
      area_type: 'MSA',
    },
    {
      index: 1618,
      name: 'Warren County, New Jersey',
      population: 110926,
      area_type: 'County',
    },
  ],
  1621: [
    {
      index: 1619,
      name: 'Albuquerque-Santa Fe-Las Vegas, NM CSA',
      population: 1165564,
      area_type: 'CSA',
    },
    {
      index: 1620,
      name: 'Albuquerque, NM MSA',
      population: 919543,
      area_type: 'MSA',
    },
    {
      index: 1621,
      name: 'Bernalillo County, New Mexico',
      population: 672508,
      area_type: 'County',
    },
  ],
  1622: [
    {
      index: 1622,
      name: 'Chaves County, New Mexico (Roswell, NM μSA)',
      population: 63894,
      area_type: 'μSA',
    },
  ],
  1624: [
    {
      index: 1624,
      name: 'Cibola County, New Mexico (Grants, NM μSA)',
      population: 26950,
      area_type: 'μSA',
    },
  ],
  1627: [
    {
      index: 1626,
      name: 'Clovis-Portales, NM CSA',
      population: 66466,
      area_type: 'CSA',
    },
    {
      index: 1627,
      name: 'Curry County, New Mexico (Clovis, NM μSA)',
      population: 47532,
      area_type: 'μSA',
    },
  ],
  1630: [
    {
      index: 1629,
      name: 'El Paso-Las Cruces, TX-NM CSA',
      population: 1095532,
      area_type: 'CSA',
    },
    {
      index: 1630,
      name: 'Doña Ana County, New Mexico (Las Cruces, NM MSA)',
      population: 223337,
      area_type: 'MSA',
    },
  ],
  1632: [
    {
      index: 1632,
      name: 'Eddy County, New Mexico (Carlsbad-Artesia, NM μSA)',
      population: 60400,
      area_type: 'μSA',
    },
  ],
  1634: [
    {
      index: 1634,
      name: 'Grant County, New Mexico (Silver City, NM μSA)',
      population: 27686,
      area_type: 'μSA',
    },
  ],
  1636: [
    {
      index: 1636,
      name: 'Lea County, New Mexico (Hobbs, NM μSA)',
      population: 72452,
      area_type: 'μSA',
    },
  ],
  1638: [
    {
      index: 1638,
      name: 'Lincoln County, New Mexico (Ruidoso, NM μSA)',
      population: 20411,
      area_type: 'μSA',
    },
  ],
  1640: [
    {
      index: 1619,
      name: 'Albuquerque-Santa Fe-Las Vegas, NM CSA',
      population: 1165564,
      area_type: 'CSA',
    },
    {
      index: 1640,
      name: 'Los Alamos County, New Mexico (Los Alamos, NM μSA)',
      population: 19187,
      area_type: 'μSA',
    },
  ],
  1642: [
    {
      index: 1642,
      name: 'Luna County, New Mexico (Deming, NM μSA)',
      population: 25749,
      area_type: 'μSA',
    },
  ],
  1644: [
    {
      index: 1644,
      name: 'McKinley County, New Mexico (Gallup, NM μSA)',
      population: 69830,
      area_type: 'μSA',
    },
  ],
  1647: [
    {
      index: 1619,
      name: 'Albuquerque-Santa Fe-Las Vegas, NM CSA',
      population: 1165564,
      area_type: 'CSA',
    },
    {
      index: 1646,
      name: 'Las Vegas, NM μSA',
      population: 31122,
      area_type: 'μSA',
    },
    {
      index: 1647,
      name: 'Mora County, New Mexico',
      population: 4169,
      area_type: 'County',
    },
  ],
  1648: [
    {
      index: 1648,
      name: 'Otero County, New Mexico (Alamogordo, NM μSA)',
      population: 68823,
      area_type: 'μSA',
    },
  ],
  1650: [
    {
      index: 1619,
      name: 'Albuquerque-Santa Fe-Las Vegas, NM CSA',
      population: 1165564,
      area_type: 'CSA',
    },
    {
      index: 1650,
      name: 'Rio Arriba County, New Mexico (Española, NM μSA)',
      population: 40048,
      area_type: 'μSA',
    },
  ],
  1652: [
    {
      index: 1626,
      name: 'Clovis-Portales, NM CSA',
      population: 66466,
      area_type: 'CSA',
    },
    {
      index: 1652,
      name: 'Roosevelt County, New Mexico (Portales, NM μSA)',
      population: 18934,
      area_type: 'μSA',
    },
  ],
  1654: [
    {
      index: 1619,
      name: 'Albuquerque-Santa Fe-Las Vegas, NM CSA',
      population: 1165564,
      area_type: 'CSA',
    },
    {
      index: 1620,
      name: 'Albuquerque, NM MSA',
      population: 919543,
      area_type: 'MSA',
    },
    {
      index: 1654,
      name: 'Sandoval County, New Mexico',
      population: 153501,
      area_type: 'County',
    },
  ],
  1655: [
    {
      index: 1655,
      name: 'San Juan County, New Mexico (Farmington, NM MSA)',
      population: 120418,
      area_type: 'MSA',
    },
  ],
  1657: [
    {
      index: 1619,
      name: 'Albuquerque-Santa Fe-Las Vegas, NM CSA',
      population: 1165564,
      area_type: 'CSA',
    },
    {
      index: 1646,
      name: 'Las Vegas, NM μSA',
      population: 31122,
      area_type: 'μSA',
    },
    {
      index: 1657,
      name: 'San Miguel County, New Mexico',
      population: 26953,
      area_type: 'County',
    },
  ],
  1658: [
    {
      index: 1619,
      name: 'Albuquerque-Santa Fe-Las Vegas, NM CSA',
      population: 1165564,
      area_type: 'CSA',
    },
    {
      index: 1658,
      name: 'Santa Fe County, New Mexico (Santa Fe, NM MSA)',
      population: 155664,
      area_type: 'MSA',
    },
  ],
  1660: [
    {
      index: 1660,
      name: 'Taos County, New Mexico (Taos, NM μSA)',
      population: 34580,
      area_type: 'μSA',
    },
  ],
  1662: [
    {
      index: 1619,
      name: 'Albuquerque-Santa Fe-Las Vegas, NM CSA',
      population: 1165564,
      area_type: 'CSA',
    },
    {
      index: 1620,
      name: 'Albuquerque, NM MSA',
      population: 919543,
      area_type: 'MSA',
    },
    {
      index: 1662,
      name: 'Torrance County, New Mexico',
      population: 15454,
      area_type: 'County',
    },
  ],
  1663: [
    {
      index: 1619,
      name: 'Albuquerque-Santa Fe-Las Vegas, NM CSA',
      population: 1165564,
      area_type: 'CSA',
    },
    {
      index: 1620,
      name: 'Albuquerque, NM MSA',
      population: 919543,
      area_type: 'MSA',
    },
    {
      index: 1663,
      name: 'Valencia County, New Mexico',
      population: 78080,
      area_type: 'County',
    },
  ],
  1666: [
    {
      index: 1664,
      name: 'Albany-Schenectady, NY CSA',
      population: 1194635,
      area_type: 'CSA',
    },
    {
      index: 1665,
      name: 'Albany-Schenectady-Troy, NY MSA',
      population: 904617,
      area_type: 'MSA',
    },
    {
      index: 1666,
      name: 'Albany County, New York',
      population: 315811,
      area_type: 'County',
    },
  ],
  1667: [
    {
      index: 1593,
      name: 'New York-Newark, NY-NJ-CT-PA CSA',
      population: 23070149,
      area_type: 'CSA',
    },
    {
      index: 1594,
      name: 'New York-Newark-Jersey City, NY-NJ-PA MSA',
      population: 19617869,
      area_type: 'MSA',
    },
    {
      index: 1667,
      name: 'Bronx County, New York',
      population: 1379946,
      area_type: 'County',
    },
  ],
  1669: [
    {
      index: 1668,
      name: 'Binghamton, NY MSA',
      population: 244889,
      area_type: 'MSA',
    },
    {
      index: 1669,
      name: 'Broome County, New York',
      population: 197117,
      area_type: 'County',
    },
  ],
  1671: [
    {
      index: 1670,
      name: 'Buffalo-Cheektowaga-Olean, NY CSA',
      population: 1237631,
      area_type: 'CSA',
    },
    {
      index: 1671,
      name: 'Cattaraugus County, New York (Olean, NY μSA)',
      population: 76439,
      area_type: 'μSA',
    },
  ],
  1674: [
    {
      index: 1673,
      name: 'Syracuse-Auburn, NY CSA',
      population: 728631,
      area_type: 'CSA',
    },
    {
      index: 1674,
      name: 'Cayuga County, New York (Auburn, NY μSA)',
      population: 74998,
      area_type: 'μSA',
    },
  ],
  1676: [
    {
      index: 1676,
      name: 'Chautauqua County, New York (Jamestown-Dunkirk-Fredonia, NY μSA)',
      population: 126027,
      area_type: 'μSA',
    },
  ],
  1679: [
    {
      index: 1678,
      name: 'Elmira-Corning, NY CSA',
      population: 174025,
      area_type: 'CSA',
    },
    {
      index: 1679,
      name: 'Chemung County, New York (Elmira, NY MSA)',
      population: 81426,
      area_type: 'MSA',
    },
  ],
  1681: [
    {
      index: 1681,
      name: 'Clinton County, New York (Plattsburgh, NY μSA)',
      population: 78753,
      area_type: 'μSA',
    },
  ],
  1683: [
    {
      index: 1664,
      name: 'Albany-Schenectady, NY CSA',
      population: 1194635,
      area_type: 'CSA',
    },
    {
      index: 1683,
      name: 'Columbia County, New York (Hudson, NY μSA)',
      population: 61286,
      area_type: 'μSA',
    },
  ],
  1686: [
    {
      index: 1685,
      name: 'Ithaca-Cortland, NY CSA',
      population: 150903,
      area_type: 'CSA',
    },
    {
      index: 1686,
      name: 'Cortland County, New York (Cortland, NY μSA)',
      population: 46126,
      area_type: 'μSA',
    },
  ],
  1689: [
    {
      index: 1593,
      name: 'New York-Newark, NY-NJ-CT-PA CSA',
      population: 23070149,
      area_type: 'CSA',
    },
    {
      index: 1688,
      name: 'Poughkeepsie-Newburgh-Middletown, NY MSA',
      population: 703486,
      area_type: 'MSA',
    },
    {
      index: 1689,
      name: 'Dutchess County, New York',
      population: 297545,
      area_type: 'County',
    },
  ],
  1691: [
    {
      index: 1670,
      name: 'Buffalo-Cheektowaga-Olean, NY CSA',
      population: 1237631,
      area_type: 'CSA',
    },
    {
      index: 1690,
      name: 'Buffalo-Cheektowaga, NY MSA',
      population: 1161192,
      area_type: 'MSA',
    },
    {
      index: 1691,
      name: 'Erie County, New York',
      population: 950312,
      area_type: 'County',
    },
  ],
  1692: [
    {
      index: 1692,
      name: 'Franklin County, New York (Malone, NY μSA)',
      population: 46373,
      area_type: 'μSA',
    },
  ],
  1694: [
    {
      index: 1664,
      name: 'Albany-Schenectady, NY CSA',
      population: 1194635,
      area_type: 'CSA',
    },
    {
      index: 1694,
      name: 'Fulton County, New York (Gloversville, NY μSA)',
      population: 52669,
      area_type: 'μSA',
    },
  ],
  1697: [
    {
      index: 1696,
      name: 'Rochester-Batavia-Seneca Falls, NY CSA',
      population: 1171569,
      area_type: 'CSA',
    },
    {
      index: 1697,
      name: 'Genesee County, New York (Batavia, NY μSA)',
      population: 57535,
      area_type: 'μSA',
    },
  ],
  1700: [
    {
      index: 1699,
      name: 'Utica-Rome, NY MSA',
      population: 288668,
      area_type: 'MSA',
    },
    {
      index: 1700,
      name: 'Herkimer County, New York',
      population: 59822,
      area_type: 'County',
    },
  ],
  1701: [
    {
      index: 1701,
      name: 'Jefferson County, New York (Watertown-Fort Drum, NY MSA)',
      population: 116637,
      area_type: 'MSA',
    },
  ],
  1703: [
    {
      index: 1593,
      name: 'New York-Newark, NY-NJ-CT-PA CSA',
      population: 23070149,
      area_type: 'CSA',
    },
    {
      index: 1594,
      name: 'New York-Newark-Jersey City, NY-NJ-PA MSA',
      population: 19617869,
      area_type: 'MSA',
    },
    {
      index: 1703,
      name: 'Kings County, New York',
      population: 2590516,
      area_type: 'County',
    },
  ],
  1705: [
    {
      index: 1696,
      name: 'Rochester-Batavia-Seneca Falls, NY CSA',
      population: 1171569,
      area_type: 'CSA',
    },
    {
      index: 1704,
      name: 'Rochester, NY MSA',
      population: 1081152,
      area_type: 'MSA',
    },
    {
      index: 1705,
      name: 'Livingston County, New York',
      population: 61516,
      area_type: 'County',
    },
  ],
  1707: [
    {
      index: 1673,
      name: 'Syracuse-Auburn, NY CSA',
      population: 728631,
      area_type: 'CSA',
    },
    {
      index: 1706,
      name: 'Syracuse, NY MSA',
      population: 653633,
      area_type: 'MSA',
    },
    {
      index: 1707,
      name: 'Madison County, New York',
      population: 67097,
      area_type: 'County',
    },
  ],
  1708: [
    {
      index: 1696,
      name: 'Rochester-Batavia-Seneca Falls, NY CSA',
      population: 1171569,
      area_type: 'CSA',
    },
    {
      index: 1704,
      name: 'Rochester, NY MSA',
      population: 1081152,
      area_type: 'MSA',
    },
    {
      index: 1708,
      name: 'Monroe County, New York',
      population: 752035,
      area_type: 'County',
    },
  ],
  1709: [
    {
      index: 1664,
      name: 'Albany-Schenectady, NY CSA',
      population: 1194635,
      area_type: 'CSA',
    },
    {
      index: 1709,
      name: 'Montgomery County, New York (Amsterdam, NY μSA)',
      population: 49623,
      area_type: 'μSA',
    },
  ],
  1711: [
    {
      index: 1593,
      name: 'New York-Newark, NY-NJ-CT-PA CSA',
      population: 23070149,
      area_type: 'CSA',
    },
    {
      index: 1594,
      name: 'New York-Newark-Jersey City, NY-NJ-PA MSA',
      population: 19617869,
      area_type: 'MSA',
    },
    {
      index: 1711,
      name: 'Nassau County, New York',
      population: 1383726,
      area_type: 'County',
    },
  ],
  1712: [
    {
      index: 1593,
      name: 'New York-Newark, NY-NJ-CT-PA CSA',
      population: 23070149,
      area_type: 'CSA',
    },
    {
      index: 1594,
      name: 'New York-Newark-Jersey City, NY-NJ-PA MSA',
      population: 19617869,
      area_type: 'MSA',
    },
    {
      index: 1712,
      name: 'New York County, New York',
      population: 1596273,
      area_type: 'County',
    },
  ],
  1713: [
    {
      index: 1670,
      name: 'Buffalo-Cheektowaga-Olean, NY CSA',
      population: 1237631,
      area_type: 'CSA',
    },
    {
      index: 1690,
      name: 'Buffalo-Cheektowaga, NY MSA',
      population: 1161192,
      area_type: 'MSA',
    },
    {
      index: 1713,
      name: 'Niagara County, New York',
      population: 210880,
      area_type: 'County',
    },
  ],
  1714: [
    {
      index: 1699,
      name: 'Utica-Rome, NY MSA',
      population: 288668,
      area_type: 'MSA',
    },
    {
      index: 1714,
      name: 'Oneida County, New York',
      population: 228846,
      area_type: 'County',
    },
  ],
  1715: [
    {
      index: 1673,
      name: 'Syracuse-Auburn, NY CSA',
      population: 728631,
      area_type: 'CSA',
    },
    {
      index: 1706,
      name: 'Syracuse, NY MSA',
      population: 653633,
      area_type: 'MSA',
    },
    {
      index: 1715,
      name: 'Onondaga County, New York',
      population: 468249,
      area_type: 'County',
    },
  ],
  1716: [
    {
      index: 1696,
      name: 'Rochester-Batavia-Seneca Falls, NY CSA',
      population: 1171569,
      area_type: 'CSA',
    },
    {
      index: 1704,
      name: 'Rochester, NY MSA',
      population: 1081152,
      area_type: 'MSA',
    },
    {
      index: 1716,
      name: 'Ontario County, New York',
      population: 112707,
      area_type: 'County',
    },
  ],
  1717: [
    {
      index: 1593,
      name: 'New York-Newark, NY-NJ-CT-PA CSA',
      population: 23070149,
      area_type: 'CSA',
    },
    {
      index: 1688,
      name: 'Poughkeepsie-Newburgh-Middletown, NY MSA',
      population: 703486,
      area_type: 'MSA',
    },
    {
      index: 1717,
      name: 'Orange County, New York',
      population: 405941,
      area_type: 'County',
    },
  ],
  1718: [
    {
      index: 1696,
      name: 'Rochester-Batavia-Seneca Falls, NY CSA',
      population: 1171569,
      area_type: 'CSA',
    },
    {
      index: 1704,
      name: 'Rochester, NY MSA',
      population: 1081152,
      area_type: 'MSA',
    },
    {
      index: 1718,
      name: 'Orleans County, New York',
      population: 39318,
      area_type: 'County',
    },
  ],
  1719: [
    {
      index: 1673,
      name: 'Syracuse-Auburn, NY CSA',
      population: 728631,
      area_type: 'CSA',
    },
    {
      index: 1706,
      name: 'Syracuse, NY MSA',
      population: 653633,
      area_type: 'MSA',
    },
    {
      index: 1719,
      name: 'Oswego County, New York',
      population: 118287,
      area_type: 'County',
    },
  ],
  1720: [
    {
      index: 1720,
      name: 'Otsego County, New York (Oneonta, NY μSA)',
      population: 60636,
      area_type: 'μSA',
    },
  ],
  1722: [
    {
      index: 1593,
      name: 'New York-Newark, NY-NJ-CT-PA CSA',
      population: 23070149,
      area_type: 'CSA',
    },
    {
      index: 1594,
      name: 'New York-Newark-Jersey City, NY-NJ-PA MSA',
      population: 19617869,
      area_type: 'MSA',
    },
    {
      index: 1722,
      name: 'Putnam County, New York',
      population: 98045,
      area_type: 'County',
    },
  ],
  1723: [
    {
      index: 1593,
      name: 'New York-Newark, NY-NJ-CT-PA CSA',
      population: 23070149,
      area_type: 'CSA',
    },
    {
      index: 1594,
      name: 'New York-Newark-Jersey City, NY-NJ-PA MSA',
      population: 19617869,
      area_type: 'MSA',
    },
    {
      index: 1723,
      name: 'Queens County, New York',
      population: 2278029,
      area_type: 'County',
    },
  ],
  1724: [
    {
      index: 1664,
      name: 'Albany-Schenectady, NY CSA',
      population: 1194635,
      area_type: 'CSA',
    },
    {
      index: 1665,
      name: 'Albany-Schenectady-Troy, NY MSA',
      population: 904617,
      area_type: 'MSA',
    },
    {
      index: 1724,
      name: 'Rensselaer County, New York',
      population: 159853,
      area_type: 'County',
    },
  ],
  1725: [
    {
      index: 1593,
      name: 'New York-Newark, NY-NJ-CT-PA CSA',
      population: 23070149,
      area_type: 'CSA',
    },
    {
      index: 1594,
      name: 'New York-Newark-Jersey City, NY-NJ-PA MSA',
      population: 19617869,
      area_type: 'MSA',
    },
    {
      index: 1725,
      name: 'Richmond County, New York',
      population: 491133,
      area_type: 'County',
    },
  ],
  1726: [
    {
      index: 1593,
      name: 'New York-Newark, NY-NJ-CT-PA CSA',
      population: 23070149,
      area_type: 'CSA',
    },
    {
      index: 1594,
      name: 'New York-Newark-Jersey City, NY-NJ-PA MSA',
      population: 19617869,
      area_type: 'MSA',
    },
    {
      index: 1726,
      name: 'Rockland County, New York',
      population: 339022,
      area_type: 'County',
    },
  ],
  1727: [
    {
      index: 1727,
      name: 'St. Lawrence County, New York (Ogdensburg-Massena, NY μSA)',
      population: 107733,
      area_type: 'μSA',
    },
  ],
  1729: [
    {
      index: 1664,
      name: 'Albany-Schenectady, NY CSA',
      population: 1194635,
      area_type: 'CSA',
    },
    {
      index: 1665,
      name: 'Albany-Schenectady-Troy, NY MSA',
      population: 904617,
      area_type: 'MSA',
    },
    {
      index: 1729,
      name: 'Saratoga County, New York',
      population: 238797,
      area_type: 'County',
    },
  ],
  1730: [
    {
      index: 1664,
      name: 'Albany-Schenectady, NY CSA',
      population: 1194635,
      area_type: 'CSA',
    },
    {
      index: 1665,
      name: 'Albany-Schenectady-Troy, NY MSA',
      population: 904617,
      area_type: 'MSA',
    },
    {
      index: 1730,
      name: 'Schenectady County, New York',
      population: 160093,
      area_type: 'County',
    },
  ],
  1731: [
    {
      index: 1664,
      name: 'Albany-Schenectady, NY CSA',
      population: 1194635,
      area_type: 'CSA',
    },
    {
      index: 1665,
      name: 'Albany-Schenectady-Troy, NY MSA',
      population: 904617,
      area_type: 'MSA',
    },
    {
      index: 1731,
      name: 'Schoharie County, New York',
      population: 30063,
      area_type: 'County',
    },
  ],
  1732: [
    {
      index: 1696,
      name: 'Rochester-Batavia-Seneca Falls, NY CSA',
      population: 1171569,
      area_type: 'CSA',
    },
    {
      index: 1732,
      name: 'Seneca County, New York (Seneca Falls, NY μSA)',
      population: 32882,
      area_type: 'μSA',
    },
  ],
  1734: [
    {
      index: 1678,
      name: 'Elmira-Corning, NY CSA',
      population: 174025,
      area_type: 'CSA',
    },
    {
      index: 1734,
      name: 'Steuben County, New York (Corning, NY μSA)',
      population: 92599,
      area_type: 'μSA',
    },
  ],
  1736: [
    {
      index: 1593,
      name: 'New York-Newark, NY-NJ-CT-PA CSA',
      population: 23070149,
      area_type: 'CSA',
    },
    {
      index: 1594,
      name: 'New York-Newark-Jersey City, NY-NJ-PA MSA',
      population: 19617869,
      area_type: 'MSA',
    },
    {
      index: 1736,
      name: 'Suffolk County, New York',
      population: 1525465,
      area_type: 'County',
    },
  ],
  1737: [
    {
      index: 1668,
      name: 'Binghamton, NY MSA',
      population: 244889,
      area_type: 'MSA',
    },
    {
      index: 1737,
      name: 'Tioga County, New York',
      population: 47772,
      area_type: 'County',
    },
  ],
  1738: [
    {
      index: 1685,
      name: 'Ithaca-Cortland, NY CSA',
      population: 150903,
      area_type: 'CSA',
    },
    {
      index: 1738,
      name: 'Tompkins County, New York (Ithaca, NY MSA)',
      population: 104777,
      area_type: 'MSA',
    },
  ],
  1740: [
    {
      index: 1593,
      name: 'New York-Newark, NY-NJ-CT-PA CSA',
      population: 23070149,
      area_type: 'CSA',
    },
    {
      index: 1740,
      name: 'Ulster County, New York (Kingston, NY MSA)',
      population: 182319,
      area_type: 'MSA',
    },
  ],
  1743: [
    {
      index: 1664,
      name: 'Albany-Schenectady, NY CSA',
      population: 1194635,
      area_type: 'CSA',
    },
    {
      index: 1742,
      name: 'Glens Falls, NY MSA',
      population: 126440,
      area_type: 'MSA',
    },
    {
      index: 1743,
      name: 'Warren County, New York',
      population: 65599,
      area_type: 'County',
    },
  ],
  1744: [
    {
      index: 1664,
      name: 'Albany-Schenectady, NY CSA',
      population: 1194635,
      area_type: 'CSA',
    },
    {
      index: 1742,
      name: 'Glens Falls, NY MSA',
      population: 126440,
      area_type: 'MSA',
    },
    {
      index: 1744,
      name: 'Washington County, New York',
      population: 60841,
      area_type: 'County',
    },
  ],
  1745: [
    {
      index: 1696,
      name: 'Rochester-Batavia-Seneca Falls, NY CSA',
      population: 1171569,
      area_type: 'CSA',
    },
    {
      index: 1704,
      name: 'Rochester, NY MSA',
      population: 1081152,
      area_type: 'MSA',
    },
    {
      index: 1745,
      name: 'Wayne County, New York',
      population: 91125,
      area_type: 'County',
    },
  ],
  1746: [
    {
      index: 1593,
      name: 'New York-Newark, NY-NJ-CT-PA CSA',
      population: 23070149,
      area_type: 'CSA',
    },
    {
      index: 1594,
      name: 'New York-Newark-Jersey City, NY-NJ-PA MSA',
      population: 19617869,
      area_type: 'MSA',
    },
    {
      index: 1746,
      name: 'Westchester County, New York',
      population: 990427,
      area_type: 'County',
    },
  ],
  1747: [
    {
      index: 1696,
      name: 'Rochester-Batavia-Seneca Falls, NY CSA',
      population: 1171569,
      area_type: 'CSA',
    },
    {
      index: 1704,
      name: 'Rochester, NY MSA',
      population: 1081152,
      area_type: 'MSA',
    },
    {
      index: 1747,
      name: 'Yates County, New York',
      population: 24451,
      area_type: 'County',
    },
  ],
  1749: [
    {
      index: 1748,
      name: 'Greensboro--Winston-Salem--High Point, NC CSA',
      population: 1720328,
      area_type: 'CSA',
    },
    {
      index: 1749,
      name: 'Alamance County, North Carolina (Burlington, NC MSA)',
      population: 176353,
      area_type: 'MSA',
    },
  ],
  1752: [
    {
      index: 1751,
      name: 'Hickory-Lenoir-Morganton, NC MSA',
      population: 368347,
      area_type: 'MSA',
    },
    {
      index: 1752,
      name: 'Alexander County, North Carolina',
      population: 36512,
      area_type: 'County',
    },
  ],
  1755: [
    {
      index: 1753,
      name: 'Charlotte-Concord, NC-SC CSA',
      population: 2920892,
      area_type: 'CSA',
    },
    {
      index: 1754,
      name: 'Charlotte-Concord-Gastonia, NC-SC MSA',
      population: 2756069,
      area_type: 'MSA',
    },
    {
      index: 1755,
      name: 'Anson County, North Carolina',
      population: 22202,
      area_type: 'County',
    },
  ],
  1757: [
    {
      index: 1756,
      name: 'Greenville-Kinston-Washington, NC CSA',
      population: 272447,
      area_type: 'CSA',
    },
    {
      index: 1757,
      name: 'Beaufort County, North Carolina (Washington, NC μSA)',
      population: 44272,
      area_type: 'μSA',
    },
  ],
  1761: [
    {
      index: 1759,
      name: 'Myrtle Beach-Conway, SC-NC CSA',
      population: 600887,
      area_type: 'CSA',
    },
    {
      index: 1760,
      name: 'Myrtle Beach-Conway-North Myrtle Beach, SC-NC MSA',
      population: 536165,
      area_type: 'MSA',
    },
    {
      index: 1761,
      name: 'Brunswick County, North Carolina',
      population: 153064,
      area_type: 'County',
    },
  ],
  1764: [
    {
      index: 1762,
      name: 'Asheville-Marion-Brevard, NC CSA',
      population: 554180,
      area_type: 'CSA',
    },
    {
      index: 1763,
      name: 'Asheville, NC MSA',
      population: 476072,
      area_type: 'MSA',
    },
    {
      index: 1764,
      name: 'Buncombe County, North Carolina',
      population: 273589,
      area_type: 'County',
    },
  ],
  1765: [
    {
      index: 1751,
      name: 'Hickory-Lenoir-Morganton, NC MSA',
      population: 368347,
      area_type: 'MSA',
    },
    {
      index: 1765,
      name: 'Burke County, North Carolina',
      population: 87881,
      area_type: 'County',
    },
  ],
  1766: [
    {
      index: 1753,
      name: 'Charlotte-Concord, NC-SC CSA',
      population: 2920892,
      area_type: 'CSA',
    },
    {
      index: 1754,
      name: 'Charlotte-Concord-Gastonia, NC-SC MSA',
      population: 2756069,
      area_type: 'MSA',
    },
    {
      index: 1766,
      name: 'Cabarrus County, North Carolina',
      population: 235797,
      area_type: 'County',
    },
  ],
  1767: [
    {
      index: 1751,
      name: 'Hickory-Lenoir-Morganton, NC MSA',
      population: 368347,
      area_type: 'MSA',
    },
    {
      index: 1767,
      name: 'Caldwell County, North Carolina',
      population: 80492,
      area_type: 'County',
    },
  ],
  1770: [
    {
      index: 1768,
      name: 'Virginia Beach-Norfolk, VA-NC CSA',
      population: 1898944,
      area_type: 'CSA',
    },
    {
      index: 1769,
      name: 'Virginia Beach-Norfolk-Newport News, VA-NC MSA',
      population: 1806840,
      area_type: 'MSA',
    },
    {
      index: 1770,
      name: 'Camden County, North Carolina',
      population: 11088,
      area_type: 'County',
    },
  ],
  1772: [
    {
      index: 1771,
      name: 'New Bern-Morehead City, NC CSA',
      population: 191868,
      area_type: 'CSA',
    },
    {
      index: 1772,
      name: 'Carteret County, North Carolina (Morehead City, NC μSA)',
      population: 69380,
      area_type: 'μSA',
    },
  ],
  1774: [
    {
      index: 1751,
      name: 'Hickory-Lenoir-Morganton, NC MSA',
      population: 368347,
      area_type: 'MSA',
    },
    {
      index: 1774,
      name: 'Catawba County, North Carolina',
      population: 163462,
      area_type: 'County',
    },
  ],
  1777: [
    {
      index: 1775,
      name: 'Raleigh-Durham-Cary, NC CSA',
      population: 2190786,
      area_type: 'CSA',
    },
    {
      index: 1776,
      name: 'Durham-Chapel Hill, NC MSA',
      population: 664310,
      area_type: 'MSA',
    },
    {
      index: 1777,
      name: 'Chatham County, North Carolina',
      population: 79864,
      area_type: 'County',
    },
  ],
  1778: [
    {
      index: 1753,
      name: 'Charlotte-Concord, NC-SC CSA',
      population: 2920892,
      area_type: 'CSA',
    },
    {
      index: 1778,
      name: 'Cleveland County, North Carolina (Shelby, NC μSA)',
      population: 100670,
      area_type: 'μSA',
    },
  ],
  1781: [
    {
      index: 1771,
      name: 'New Bern-Morehead City, NC CSA',
      population: 191868,
      area_type: 'CSA',
    },
    {
      index: 1780,
      name: 'New Bern, NC MSA',
      population: 122488,
      area_type: 'MSA',
    },
    {
      index: 1781,
      name: 'Craven County, North Carolina',
      population: 100874,
      area_type: 'County',
    },
  ],
  1784: [
    {
      index: 1782,
      name: 'Fayetteville-Sanford-Lumberton, NC CSA',
      population: 851150,
      area_type: 'CSA',
    },
    {
      index: 1783,
      name: 'Fayetteville, NC MSA',
      population: 529318,
      area_type: 'MSA',
    },
    {
      index: 1784,
      name: 'Cumberland County, North Carolina',
      population: 336699,
      area_type: 'County',
    },
  ],
  1785: [
    {
      index: 1768,
      name: 'Virginia Beach-Norfolk, VA-NC CSA',
      population: 1898944,
      area_type: 'CSA',
    },
    {
      index: 1769,
      name: 'Virginia Beach-Norfolk-Newport News, VA-NC MSA',
      population: 1806840,
      area_type: 'MSA',
    },
    {
      index: 1785,
      name: 'Currituck County, North Carolina',
      population: 31015,
      area_type: 'County',
    },
  ],
  1786: [
    {
      index: 1768,
      name: 'Virginia Beach-Norfolk, VA-NC CSA',
      population: 1898944,
      area_type: 'CSA',
    },
    {
      index: 1786,
      name: 'Dare County, North Carolina (Kill Devil Hills, NC μSA)',
      population: 37956,
      area_type: 'μSA',
    },
  ],
  1789: [
    {
      index: 1748,
      name: 'Greensboro--Winston-Salem--High Point, NC CSA',
      population: 1720328,
      area_type: 'CSA',
    },
    {
      index: 1788,
      name: 'Winston-Salem, NC MSA',
      population: 688471,
      area_type: 'MSA',
    },
    {
      index: 1789,
      name: 'Davidson County, North Carolina',
      population: 172586,
      area_type: 'County',
    },
  ],
  1790: [
    {
      index: 1748,
      name: 'Greensboro--Winston-Salem--High Point, NC CSA',
      population: 1720328,
      area_type: 'CSA',
    },
    {
      index: 1788,
      name: 'Winston-Salem, NC MSA',
      population: 688471,
      area_type: 'MSA',
    },
    {
      index: 1790,
      name: 'Davie County, North Carolina',
      population: 44090,
      area_type: 'County',
    },
  ],
  1791: [
    {
      index: 1775,
      name: 'Raleigh-Durham-Cary, NC CSA',
      population: 2190786,
      area_type: 'CSA',
    },
    {
      index: 1776,
      name: 'Durham-Chapel Hill, NC MSA',
      population: 664310,
      area_type: 'MSA',
    },
    {
      index: 1791,
      name: 'Durham County, North Carolina',
      population: 332680,
      area_type: 'County',
    },
  ],
  1794: [
    {
      index: 1792,
      name: 'Rocky Mount-Wilson-Roanoke Rapids, NC CSA',
      population: 287166,
      area_type: 'CSA',
    },
    {
      index: 1793,
      name: 'Rocky Mount, NC MSA',
      population: 144090,
      area_type: 'MSA',
    },
    {
      index: 1794,
      name: 'Edgecombe County, North Carolina',
      population: 48301,
      area_type: 'County',
    },
  ],
  1795: [
    {
      index: 1748,
      name: 'Greensboro--Winston-Salem--High Point, NC CSA',
      population: 1720328,
      area_type: 'CSA',
    },
    {
      index: 1788,
      name: 'Winston-Salem, NC MSA',
      population: 688471,
      area_type: 'MSA',
    },
    {
      index: 1795,
      name: 'Forsyth County, North Carolina',
      population: 389157,
      area_type: 'County',
    },
  ],
  1797: [
    {
      index: 1775,
      name: 'Raleigh-Durham-Cary, NC CSA',
      population: 2190786,
      area_type: 'CSA',
    },
    {
      index: 1796,
      name: 'Raleigh-Cary, NC MSA',
      population: 1484338,
      area_type: 'MSA',
    },
    {
      index: 1797,
      name: 'Franklin County, North Carolina',
      population: 74539,
      area_type: 'County',
    },
  ],
  1798: [
    {
      index: 1753,
      name: 'Charlotte-Concord, NC-SC CSA',
      population: 2920892,
      area_type: 'CSA',
    },
    {
      index: 1754,
      name: 'Charlotte-Concord-Gastonia, NC-SC MSA',
      population: 2756069,
      area_type: 'MSA',
    },
    {
      index: 1798,
      name: 'Gaston County, North Carolina',
      population: 234215,
      area_type: 'County',
    },
  ],
  1799: [
    {
      index: 1768,
      name: 'Virginia Beach-Norfolk, VA-NC CSA',
      population: 1898944,
      area_type: 'CSA',
    },
    {
      index: 1769,
      name: 'Virginia Beach-Norfolk-Newport News, VA-NC MSA',
      population: 1806840,
      area_type: 'MSA',
    },
    {
      index: 1799,
      name: 'Gates County, North Carolina',
      population: 10383,
      area_type: 'County',
    },
  ],
  1800: [
    {
      index: 1775,
      name: 'Raleigh-Durham-Cary, NC CSA',
      population: 2190786,
      area_type: 'CSA',
    },
    {
      index: 1776,
      name: 'Durham-Chapel Hill, NC MSA',
      population: 664310,
      area_type: 'MSA',
    },
    {
      index: 1800,
      name: 'Granville County, North Carolina',
      population: 61903,
      area_type: 'County',
    },
  ],
  1802: [
    {
      index: 1748,
      name: 'Greensboro--Winston-Salem--High Point, NC CSA',
      population: 1720328,
      area_type: 'CSA',
    },
    {
      index: 1801,
      name: 'Greensboro-High Point, NC MSA',
      population: 784101,
      area_type: 'MSA',
    },
    {
      index: 1802,
      name: 'Guilford County, North Carolina',
      population: 546101,
      area_type: 'County',
    },
  ],
  1804: [
    {
      index: 1792,
      name: 'Rocky Mount-Wilson-Roanoke Rapids, NC CSA',
      population: 287166,
      area_type: 'CSA',
    },
    {
      index: 1803,
      name: 'Roanoke Rapids, NC μSA',
      population: 64627,
      area_type: 'μSA',
    },
    {
      index: 1804,
      name: 'Halifax County, North Carolina',
      population: 47848,
      area_type: 'County',
    },
  ],
  1805: [
    {
      index: 1782,
      name: 'Fayetteville-Sanford-Lumberton, NC CSA',
      population: 851150,
      area_type: 'CSA',
    },
    {
      index: 1783,
      name: 'Fayetteville, NC MSA',
      population: 529318,
      area_type: 'MSA',
    },
    {
      index: 1805,
      name: 'Harnett County, North Carolina',
      population: 138832,
      area_type: 'County',
    },
  ],
  1806: [
    {
      index: 1762,
      name: 'Asheville-Marion-Brevard, NC CSA',
      population: 554180,
      area_type: 'CSA',
    },
    {
      index: 1763,
      name: 'Asheville, NC MSA',
      population: 476072,
      area_type: 'MSA',
    },
    {
      index: 1806,
      name: 'Haywood County, North Carolina',
      population: 62609,
      area_type: 'County',
    },
  ],
  1807: [
    {
      index: 1762,
      name: 'Asheville-Marion-Brevard, NC CSA',
      population: 554180,
      area_type: 'CSA',
    },
    {
      index: 1763,
      name: 'Asheville, NC MSA',
      population: 476072,
      area_type: 'MSA',
    },
    {
      index: 1807,
      name: 'Henderson County, North Carolina',
      population: 118106,
      area_type: 'County',
    },
  ],
  1808: [
    {
      index: 1782,
      name: 'Fayetteville-Sanford-Lumberton, NC CSA',
      population: 851150,
      area_type: 'CSA',
    },
    {
      index: 1783,
      name: 'Fayetteville, NC MSA',
      population: 529318,
      area_type: 'MSA',
    },
    {
      index: 1808,
      name: 'Hoke County, North Carolina',
      population: 53787,
      area_type: 'County',
    },
  ],
  1809: [
    {
      index: 1753,
      name: 'Charlotte-Concord, NC-SC CSA',
      population: 2920892,
      area_type: 'CSA',
    },
    {
      index: 1754,
      name: 'Charlotte-Concord-Gastonia, NC-SC MSA',
      population: 2756069,
      area_type: 'MSA',
    },
    {
      index: 1809,
      name: 'Iredell County, North Carolina',
      population: 195897,
      area_type: 'County',
    },
  ],
  1811: [
    {
      index: 1810,
      name: 'Cullowhee, NC μSA',
      population: 56922,
      area_type: 'μSA',
    },
    {
      index: 1811,
      name: 'Jackson County, North Carolina',
      population: 42955,
      area_type: 'County',
    },
  ],
  1812: [
    {
      index: 1775,
      name: 'Raleigh-Durham-Cary, NC CSA',
      population: 2190786,
      area_type: 'CSA',
    },
    {
      index: 1796,
      name: 'Raleigh-Cary, NC MSA',
      population: 1484338,
      area_type: 'MSA',
    },
    {
      index: 1812,
      name: 'Johnston County, North Carolina',
      population: 234778,
      area_type: 'County',
    },
  ],
  1813: [
    {
      index: 1771,
      name: 'New Bern-Morehead City, NC CSA',
      population: 191868,
      area_type: 'CSA',
    },
    {
      index: 1780,
      name: 'New Bern, NC MSA',
      population: 122488,
      area_type: 'MSA',
    },
    {
      index: 1813,
      name: 'Jones County, North Carolina',
      population: 9233,
      area_type: 'County',
    },
  ],
  1814: [
    {
      index: 1782,
      name: 'Fayetteville-Sanford-Lumberton, NC CSA',
      population: 851150,
      area_type: 'CSA',
    },
    {
      index: 1814,
      name: 'Lee County, North Carolina (Sanford, NC μSA)',
      population: 65476,
      area_type: 'μSA',
    },
  ],
  1816: [
    {
      index: 1756,
      name: 'Greenville-Kinston-Washington, NC CSA',
      population: 272447,
      area_type: 'CSA',
    },
    {
      index: 1816,
      name: 'Lenoir County, North Carolina (Kinston, NC μSA)',
      population: 54633,
      area_type: 'μSA',
    },
  ],
  1818: [
    {
      index: 1753,
      name: 'Charlotte-Concord, NC-SC CSA',
      population: 2920892,
      area_type: 'CSA',
    },
    {
      index: 1754,
      name: 'Charlotte-Concord-Gastonia, NC-SC MSA',
      population: 2756069,
      area_type: 'MSA',
    },
    {
      index: 1818,
      name: 'Lincoln County, North Carolina',
      population: 93095,
      area_type: 'County',
    },
  ],
  1819: [
    {
      index: 1762,
      name: 'Asheville-Marion-Brevard, NC CSA',
      population: 554180,
      area_type: 'CSA',
    },
    {
      index: 1819,
      name: 'McDowell County, North Carolina (Marion, NC μSA)',
      population: 44753,
      area_type: 'μSA',
    },
  ],
  1821: [
    {
      index: 1762,
      name: 'Asheville-Marion-Brevard, NC CSA',
      population: 554180,
      area_type: 'CSA',
    },
    {
      index: 1763,
      name: 'Asheville, NC MSA',
      population: 476072,
      area_type: 'MSA',
    },
    {
      index: 1821,
      name: 'Madison County, North Carolina',
      population: 21768,
      area_type: 'County',
    },
  ],
  1822: [
    {
      index: 1753,
      name: 'Charlotte-Concord, NC-SC CSA',
      population: 2920892,
      area_type: 'CSA',
    },
    {
      index: 1754,
      name: 'Charlotte-Concord-Gastonia, NC-SC MSA',
      population: 2756069,
      area_type: 'MSA',
    },
    {
      index: 1822,
      name: 'Mecklenburg County, North Carolina',
      population: 1145392,
      area_type: 'County',
    },
  ],
  1823: [
    {
      index: 1782,
      name: 'Fayetteville-Sanford-Lumberton, NC CSA',
      population: 851150,
      area_type: 'CSA',
    },
    {
      index: 1823,
      name: 'Moore County, North Carolina (Pinehurst-Southern Pines, NC μSA)',
      population: 105531,
      area_type: 'μSA',
    },
  ],
  1825: [
    {
      index: 1792,
      name: 'Rocky Mount-Wilson-Roanoke Rapids, NC CSA',
      population: 287166,
      area_type: 'CSA',
    },
    {
      index: 1793,
      name: 'Rocky Mount, NC MSA',
      population: 144090,
      area_type: 'MSA',
    },
    {
      index: 1825,
      name: 'Nash County, North Carolina',
      population: 95789,
      area_type: 'County',
    },
  ],
  1827: [
    {
      index: 1826,
      name: 'Wilmington, NC MSA',
      population: 300658,
      area_type: 'MSA',
    },
    {
      index: 1827,
      name: 'New Hanover County, North Carolina',
      population: 234921,
      area_type: 'County',
    },
  ],
  1828: [
    {
      index: 1792,
      name: 'Rocky Mount-Wilson-Roanoke Rapids, NC CSA',
      population: 287166,
      area_type: 'CSA',
    },
    {
      index: 1803,
      name: 'Roanoke Rapids, NC μSA',
      population: 64627,
      area_type: 'μSA',
    },
    {
      index: 1828,
      name: 'Northampton County, North Carolina',
      population: 16779,
      area_type: 'County',
    },
  ],
  1829: [
    {
      index: 1829,
      name: 'Onslow County, North Carolina (Jacksonville, NC MSA)',
      population: 207298,
      area_type: 'MSA',
    },
  ],
  1831: [
    {
      index: 1775,
      name: 'Raleigh-Durham-Cary, NC CSA',
      population: 2190786,
      area_type: 'CSA',
    },
    {
      index: 1776,
      name: 'Durham-Chapel Hill, NC MSA',
      population: 664310,
      area_type: 'MSA',
    },
    {
      index: 1831,
      name: 'Orange County, North Carolina',
      population: 150477,
      area_type: 'County',
    },
  ],
  1832: [
    {
      index: 1771,
      name: 'New Bern-Morehead City, NC CSA',
      population: 191868,
      area_type: 'CSA',
    },
    {
      index: 1780,
      name: 'New Bern, NC MSA',
      population: 122488,
      area_type: 'MSA',
    },
    {
      index: 1832,
      name: 'Pamlico County, North Carolina',
      population: 12381,
      area_type: 'County',
    },
  ],
  1834: [
    {
      index: 1768,
      name: 'Virginia Beach-Norfolk, VA-NC CSA',
      population: 1898944,
      area_type: 'CSA',
    },
    {
      index: 1833,
      name: 'Elizabeth City, NC μSA',
      population: 54148,
      area_type: 'μSA',
    },
    {
      index: 1834,
      name: 'Pasquotank County, North Carolina',
      population: 40938,
      area_type: 'County',
    },
  ],
  1835: [
    {
      index: 1826,
      name: 'Wilmington, NC MSA',
      population: 300658,
      area_type: 'MSA',
    },
    {
      index: 1835,
      name: 'Pender County, North Carolina',
      population: 65737,
      area_type: 'County',
    },
  ],
  1836: [
    {
      index: 1768,
      name: 'Virginia Beach-Norfolk, VA-NC CSA',
      population: 1898944,
      area_type: 'CSA',
    },
    {
      index: 1833,
      name: 'Elizabeth City, NC μSA',
      population: 54148,
      area_type: 'μSA',
    },
    {
      index: 1836,
      name: 'Perquimans County, North Carolina',
      population: 13210,
      area_type: 'County',
    },
  ],
  1837: [
    {
      index: 1775,
      name: 'Raleigh-Durham-Cary, NC CSA',
      population: 2190786,
      area_type: 'CSA',
    },
    {
      index: 1776,
      name: 'Durham-Chapel Hill, NC MSA',
      population: 664310,
      area_type: 'MSA',
    },
    {
      index: 1837,
      name: 'Person County, North Carolina',
      population: 39386,
      area_type: 'County',
    },
  ],
  1838: [
    {
      index: 1756,
      name: 'Greenville-Kinston-Washington, NC CSA',
      population: 272447,
      area_type: 'CSA',
    },
    {
      index: 1838,
      name: 'Pitt County, North Carolina (Greenville, NC MSA)',
      population: 173542,
      area_type: 'MSA',
    },
  ],
  1840: [
    {
      index: 1748,
      name: 'Greensboro--Winston-Salem--High Point, NC CSA',
      population: 1720328,
      area_type: 'CSA',
    },
    {
      index: 1801,
      name: 'Greensboro-High Point, NC MSA',
      population: 784101,
      area_type: 'MSA',
    },
    {
      index: 1840,
      name: 'Randolph County, North Carolina',
      population: 146043,
      area_type: 'County',
    },
  ],
  1841: [
    {
      index: 1841,
      name: 'Richmond County, North Carolina (Rockingham, NC μSA)',
      population: 42778,
      area_type: 'μSA',
    },
  ],
  1843: [
    {
      index: 1782,
      name: 'Fayetteville-Sanford-Lumberton, NC CSA',
      population: 851150,
      area_type: 'CSA',
    },
    {
      index: 1843,
      name: 'Robeson County, North Carolina (Lumberton, NC μSA)',
      population: 116663,
      area_type: 'μSA',
    },
  ],
  1845: [
    {
      index: 1748,
      name: 'Greensboro--Winston-Salem--High Point, NC CSA',
      population: 1720328,
      area_type: 'CSA',
    },
    {
      index: 1801,
      name: 'Greensboro-High Point, NC MSA',
      population: 784101,
      area_type: 'MSA',
    },
    {
      index: 1845,
      name: 'Rockingham County, North Carolina',
      population: 91957,
      area_type: 'County',
    },
  ],
  1846: [
    {
      index: 1753,
      name: 'Charlotte-Concord, NC-SC CSA',
      population: 2920892,
      area_type: 'CSA',
    },
    {
      index: 1754,
      name: 'Charlotte-Concord-Gastonia, NC-SC MSA',
      population: 2756069,
      area_type: 'MSA',
    },
    {
      index: 1846,
      name: 'Rowan County, North Carolina',
      population: 149645,
      area_type: 'County',
    },
  ],
  1847: [
    {
      index: 1847,
      name: 'Rutherford County, North Carolina (Forest City, NC μSA)',
      population: 64963,
      area_type: 'μSA',
    },
  ],
  1849: [
    {
      index: 1782,
      name: 'Fayetteville-Sanford-Lumberton, NC CSA',
      population: 851150,
      area_type: 'CSA',
    },
    {
      index: 1849,
      name: 'Scotland County, North Carolina (Laurinburg, NC μSA)',
      population: 34162,
      area_type: 'μSA',
    },
  ],
  1851: [
    {
      index: 1753,
      name: 'Charlotte-Concord, NC-SC CSA',
      population: 2920892,
      area_type: 'CSA',
    },
    {
      index: 1851,
      name: 'Stanly County, North Carolina (Albemarle, NC μSA)',
      population: 64153,
      area_type: 'μSA',
    },
  ],
  1853: [
    {
      index: 1748,
      name: 'Greensboro--Winston-Salem--High Point, NC CSA',
      population: 1720328,
      area_type: 'CSA',
    },
    {
      index: 1788,
      name: 'Winston-Salem, NC MSA',
      population: 688471,
      area_type: 'MSA',
    },
    {
      index: 1853,
      name: 'Stokes County, North Carolina',
      population: 45175,
      area_type: 'County',
    },
  ],
  1854: [
    {
      index: 1748,
      name: 'Greensboro--Winston-Salem--High Point, NC CSA',
      population: 1720328,
      area_type: 'CSA',
    },
    {
      index: 1854,
      name: 'Surry County, North Carolina (Mount Airy, NC μSA)',
      population: 71403,
      area_type: 'μSA',
    },
  ],
  1856: [
    {
      index: 1810,
      name: 'Cullowhee, NC μSA',
      population: 56922,
      area_type: 'μSA',
    },
    {
      index: 1856,
      name: 'Swain County, North Carolina',
      population: 13967,
      area_type: 'County',
    },
  ],
  1857: [
    {
      index: 1762,
      name: 'Asheville-Marion-Brevard, NC CSA',
      population: 554180,
      area_type: 'CSA',
    },
    {
      index: 1857,
      name: 'Transylvania County, North Carolina (Brevard, NC μSA)',
      population: 33355,
      area_type: 'μSA',
    },
  ],
  1859: [
    {
      index: 1753,
      name: 'Charlotte-Concord, NC-SC CSA',
      population: 2920892,
      area_type: 'CSA',
    },
    {
      index: 1754,
      name: 'Charlotte-Concord-Gastonia, NC-SC MSA',
      population: 2756069,
      area_type: 'MSA',
    },
    {
      index: 1859,
      name: 'Union County, North Carolina',
      population: 249070,
      area_type: 'County',
    },
  ],
  1860: [
    {
      index: 1775,
      name: 'Raleigh-Durham-Cary, NC CSA',
      population: 2190786,
      area_type: 'CSA',
    },
    {
      index: 1860,
      name: 'Vance County, North Carolina (Henderson, NC μSA)',
      population: 42138,
      area_type: 'μSA',
    },
  ],
  1862: [
    {
      index: 1775,
      name: 'Raleigh-Durham-Cary, NC CSA',
      population: 2190786,
      area_type: 'CSA',
    },
    {
      index: 1796,
      name: 'Raleigh-Cary, NC MSA',
      population: 1484338,
      area_type: 'MSA',
    },
    {
      index: 1862,
      name: 'Wake County, North Carolina',
      population: 1175021,
      area_type: 'County',
    },
  ],
  1863: [
    {
      index: 1863,
      name: 'Watauga County, North Carolina (Boone, NC μSA)',
      population: 55089,
      area_type: 'μSA',
    },
  ],
  1865: [
    {
      index: 1865,
      name: 'Wayne County, North Carolina (Goldsboro, NC MSA)',
      population: 117286,
      area_type: 'MSA',
    },
  ],
  1867: [
    {
      index: 1867,
      name: 'Wilkes County, North Carolina (North Wilkesboro, NC μSA)',
      population: 65784,
      area_type: 'μSA',
    },
  ],
  1869: [
    {
      index: 1792,
      name: 'Rocky Mount-Wilson-Roanoke Rapids, NC CSA',
      population: 287166,
      area_type: 'CSA',
    },
    {
      index: 1869,
      name: 'Wilson County, North Carolina (Wilson, NC μSA)',
      population: 78449,
      area_type: 'μSA',
    },
  ],
  1871: [
    {
      index: 1748,
      name: 'Greensboro--Winston-Salem--High Point, NC CSA',
      population: 1720328,
      area_type: 'CSA',
    },
    {
      index: 1788,
      name: 'Winston-Salem, NC MSA',
      population: 688471,
      area_type: 'MSA',
    },
    {
      index: 1871,
      name: 'Yadkin County, North Carolina',
      population: 37463,
      area_type: 'County',
    },
  ],
  1873: [
    {
      index: 1872,
      name: 'Dickinson, ND μSA',
      population: 33821,
      area_type: 'μSA',
    },
    {
      index: 1873,
      name: 'Billings County, North Dakota',
      population: 1018,
      area_type: 'County',
    },
  ],
  1875: [
    {
      index: 1874,
      name: 'Bismarck, ND MSA',
      population: 134846,
      area_type: 'MSA',
    },
    {
      index: 1875,
      name: 'Burleigh County, North Dakota',
      population: 99280,
      area_type: 'County',
    },
  ],
  1876: [
    {
      index: 1286,
      name: 'Fargo-Wahpeton, ND-MN CSA',
      population: 281593,
      area_type: 'CSA',
    },
    {
      index: 1287,
      name: 'Fargo, ND-MN MSA',
      population: 258663,
      area_type: 'MSA',
    },
    {
      index: 1876,
      name: 'Cass County, North Dakota',
      population: 192734,
      area_type: 'County',
    },
  ],
  1877: [
    {
      index: 1326,
      name: 'Grand Forks, ND-MN MSA',
      population: 103144,
      area_type: 'MSA',
    },
    {
      index: 1877,
      name: 'Grand Forks County, North Dakota',
      population: 72413,
      area_type: 'County',
    },
  ],
  1879: [
    {
      index: 1878,
      name: 'Minot, ND μSA',
      population: 76279,
      area_type: 'μSA',
    },
    {
      index: 1879,
      name: 'McHenry County, North Dakota',
      population: 5189,
      area_type: 'County',
    },
  ],
  1880: [
    {
      index: 1874,
      name: 'Bismarck, ND MSA',
      population: 134846,
      area_type: 'MSA',
    },
    {
      index: 1880,
      name: 'Morton County, North Dakota',
      population: 33710,
      area_type: 'County',
    },
  ],
  1881: [
    {
      index: 1874,
      name: 'Bismarck, ND MSA',
      population: 134846,
      area_type: 'MSA',
    },
    {
      index: 1881,
      name: 'Oliver County, North Dakota',
      population: 1856,
      area_type: 'County',
    },
  ],
  1882: [
    {
      index: 1878,
      name: 'Minot, ND μSA',
      population: 76279,
      area_type: 'μSA',
    },
    {
      index: 1882,
      name: 'Renville County, North Dakota',
      population: 2220,
      area_type: 'County',
    },
  ],
  1883: [
    {
      index: 1286,
      name: 'Fargo-Wahpeton, ND-MN CSA',
      population: 281593,
      area_type: 'CSA',
    },
    {
      index: 1339,
      name: 'Wahpeton, ND-MN μSA',
      population: 22930,
      area_type: 'μSA',
    },
    {
      index: 1883,
      name: 'Richland County, North Dakota',
      population: 16580,
      area_type: 'County',
    },
  ],
  1884: [
    {
      index: 1872,
      name: 'Dickinson, ND μSA',
      population: 33821,
      area_type: 'μSA',
    },
    {
      index: 1884,
      name: 'Stark County, North Dakota',
      population: 32803,
      area_type: 'County',
    },
  ],
  1885: [
    {
      index: 1885,
      name: 'Stutsman County, North Dakota (Jamestown, ND μSA)',
      population: 21487,
      area_type: 'μSA',
    },
  ],
  1887: [
    {
      index: 1878,
      name: 'Minot, ND μSA',
      population: 76279,
      area_type: 'μSA',
    },
    {
      index: 1887,
      name: 'Ward County, North Dakota',
      population: 68870,
      area_type: 'County',
    },
  ],
  1888: [
    {
      index: 1888,
      name: 'Williams County, North Dakota (Williston, ND μSA)',
      population: 38109,
      area_type: 'μSA',
    },
  ],
  1891: [
    {
      index: 1890,
      name: 'Lima-Van Wert-Celina, OH CSA',
      population: 218180,
      area_type: 'CSA',
    },
    {
      index: 1891,
      name: 'Allen County, Ohio (Lima, OH MSA)',
      population: 101115,
      area_type: 'MSA',
    },
  ],
  1894: [
    {
      index: 1893,
      name: 'Mansfield-Ashland-Bucyrus, OH CSA',
      population: 219022,
      area_type: 'CSA',
    },
    {
      index: 1894,
      name: 'Ashland County, Ohio (Ashland, OH μSA)',
      population: 52181,
      area_type: 'μSA',
    },
  ],
  1897: [
    {
      index: 1896,
      name: 'Cleveland-Akron-Canton, OH CSA',
      population: 3598304,
      area_type: 'CSA',
    },
    {
      index: 1897,
      name: 'Ashtabula County, Ohio (Ashtabula, OH μSA)',
      population: 97014,
      area_type: 'μSA',
    },
  ],
  1899: [
    {
      index: 1899,
      name: 'Athens County, Ohio (Athens, OH μSA)',
      population: 58979,
      area_type: 'μSA',
    },
  ],
  1901: [
    {
      index: 1890,
      name: 'Lima-Van Wert-Celina, OH CSA',
      population: 218180,
      area_type: 'CSA',
    },
    {
      index: 1901,
      name: 'Auglaize County, Ohio (Wapakoneta, OH μSA)',
      population: 45948,
      area_type: 'μSA',
    },
  ],
  1904: [
    {
      index: 1903,
      name: 'Wheeling, WV-OH MSA',
      population: 136708,
      area_type: 'MSA',
    },
    {
      index: 1904,
      name: 'Belmont County, Ohio',
      population: 65509,
      area_type: 'County',
    },
  ],
  1905: [
    {
      index: 746,
      name: 'Cincinnati-Wilmington-Maysville, OH-KY-IN CSA',
      population: 2323945,
      area_type: 'CSA',
    },
    {
      index: 747,
      name: 'Cincinnati, OH-KY-IN MSA',
      population: 2265051,
      area_type: 'MSA',
    },
    {
      index: 1905,
      name: 'Brown County, Ohio',
      population: 43680,
      area_type: 'County',
    },
  ],
  1906: [
    {
      index: 746,
      name: 'Cincinnati-Wilmington-Maysville, OH-KY-IN CSA',
      population: 2323945,
      area_type: 'CSA',
    },
    {
      index: 747,
      name: 'Cincinnati, OH-KY-IN MSA',
      population: 2265051,
      area_type: 'MSA',
    },
    {
      index: 1906,
      name: 'Butler County, Ohio',
      population: 388420,
      area_type: 'County',
    },
  ],
  1908: [
    {
      index: 1896,
      name: 'Cleveland-Akron-Canton, OH CSA',
      population: 3598304,
      area_type: 'CSA',
    },
    {
      index: 1907,
      name: 'Canton-Massillon, OH MSA',
      population: 399316,
      area_type: 'MSA',
    },
    {
      index: 1908,
      name: 'Carroll County, Ohio',
      population: 26659,
      area_type: 'County',
    },
  ],
  1910: [
    {
      index: 1909,
      name: 'Dayton-Springfield-Kettering, OH CSA',
      population: 1085335,
      area_type: 'CSA',
    },
    {
      index: 1910,
      name: 'Champaign County, Ohio (Urbana, OH μSA)',
      population: 38709,
      area_type: 'μSA',
    },
  ],
  1912: [
    {
      index: 1909,
      name: 'Dayton-Springfield-Kettering, OH CSA',
      population: 1085335,
      area_type: 'CSA',
    },
    {
      index: 1912,
      name: 'Clark County, Ohio (Springfield, OH MSA)',
      population: 134831,
      area_type: 'MSA',
    },
  ],
  1914: [
    {
      index: 746,
      name: 'Cincinnati-Wilmington-Maysville, OH-KY-IN CSA',
      population: 2323945,
      area_type: 'CSA',
    },
    {
      index: 747,
      name: 'Cincinnati, OH-KY-IN MSA',
      population: 2265051,
      area_type: 'MSA',
    },
    {
      index: 1914,
      name: 'Clermont County, Ohio',
      population: 210805,
      area_type: 'County',
    },
  ],
  1915: [
    {
      index: 746,
      name: 'Cincinnati-Wilmington-Maysville, OH-KY-IN CSA',
      population: 2323945,
      area_type: 'CSA',
    },
    {
      index: 1915,
      name: 'Clinton County, Ohio (Wilmington, OH μSA)',
      population: 41964,
      area_type: 'μSA',
    },
  ],
  1918: [
    {
      index: 1917,
      name: 'Youngstown-Warren, OH-PA CSA',
      population: 636010,
      area_type: 'CSA',
    },
    {
      index: 1918,
      name: 'Columbiana County, Ohio (Salem, OH μSA)',
      population: 100511,
      area_type: 'μSA',
    },
  ],
  1920: [
    {
      index: 1920,
      name: 'Coshocton County, Ohio (Coshocton, OH μSA)',
      population: 36571,
      area_type: 'μSA',
    },
  ],
  1922: [
    {
      index: 1893,
      name: 'Mansfield-Ashland-Bucyrus, OH CSA',
      population: 219022,
      area_type: 'CSA',
    },
    {
      index: 1922,
      name: 'Crawford County, Ohio (Bucyrus-Galion, OH μSA)',
      population: 41522,
      area_type: 'μSA',
    },
  ],
  1925: [
    {
      index: 1896,
      name: 'Cleveland-Akron-Canton, OH CSA',
      population: 3598304,
      area_type: 'CSA',
    },
    {
      index: 1924,
      name: 'Cleveland-Elyria, OH MSA',
      population: 2063132,
      area_type: 'MSA',
    },
    {
      index: 1925,
      name: 'Cuyahoga County, Ohio',
      population: 1236041,
      area_type: 'County',
    },
  ],
  1926: [
    {
      index: 1909,
      name: 'Dayton-Springfield-Kettering, OH CSA',
      population: 1085335,
      area_type: 'CSA',
    },
    {
      index: 1926,
      name: 'Darke County, Ohio (Greenville, OH μSA)',
      population: 51529,
      area_type: 'μSA',
    },
  ],
  1928: [
    {
      index: 1928,
      name: 'Defiance County, Ohio (Defiance, OH μSA)',
      population: 38187,
      area_type: 'μSA',
    },
  ],
  1932: [
    {
      index: 1930,
      name: 'Columbus-Marion-Zanesville, OH CSA',
      population: 2565032,
      area_type: 'CSA',
    },
    {
      index: 1931,
      name: 'Columbus, OH MSA',
      population: 2161511,
      area_type: 'MSA',
    },
    {
      index: 1932,
      name: 'Delaware County, Ohio',
      population: 226296,
      area_type: 'County',
    },
  ],
  1933: [
    {
      index: 1896,
      name: 'Cleveland-Akron-Canton, OH CSA',
      population: 3598304,
      area_type: 'CSA',
    },
    {
      index: 1933,
      name: 'Erie County, Ohio (Sandusky, OH μSA)',
      population: 74501,
      area_type: 'μSA',
    },
  ],
  1935: [
    {
      index: 1930,
      name: 'Columbus-Marion-Zanesville, OH CSA',
      population: 2565032,
      area_type: 'CSA',
    },
    {
      index: 1931,
      name: 'Columbus, OH MSA',
      population: 2161511,
      area_type: 'MSA',
    },
    {
      index: 1935,
      name: 'Fairfield County, Ohio',
      population: 162898,
      area_type: 'County',
    },
  ],
  1936: [
    {
      index: 1930,
      name: 'Columbus-Marion-Zanesville, OH CSA',
      population: 2565032,
      area_type: 'CSA',
    },
    {
      index: 1936,
      name: 'Fayette County, Ohio (Washington Court House, OH μSA)',
      population: 28839,
      area_type: 'μSA',
    },
  ],
  1938: [
    {
      index: 1930,
      name: 'Columbus-Marion-Zanesville, OH CSA',
      population: 2565032,
      area_type: 'CSA',
    },
    {
      index: 1931,
      name: 'Columbus, OH MSA',
      population: 2161511,
      area_type: 'MSA',
    },
    {
      index: 1938,
      name: 'Franklin County, Ohio',
      population: 1321820,
      area_type: 'County',
    },
  ],
  1941: [
    {
      index: 1939,
      name: 'Toledo-Findlay-Tiffin, OH CSA',
      population: 828544,
      area_type: 'CSA',
    },
    {
      index: 1940,
      name: 'Toledo, OH MSA',
      population: 640384,
      area_type: 'MSA',
    },
    {
      index: 1941,
      name: 'Fulton County, Ohio',
      population: 42171,
      area_type: 'County',
    },
  ],
  1943: [
    {
      index: 980,
      name: 'Charleston-Huntington-Ashland, WV-OH-KY CSA',
      population: 763796,
      area_type: 'CSA',
    },
    {
      index: 1942,
      name: 'Point Pleasant, WV-OH μSA',
      population: 54068,
      area_type: 'μSA',
    },
    {
      index: 1943,
      name: 'Gallia County, Ohio',
      population: 29068,
      area_type: 'County',
    },
  ],
  1944: [
    {
      index: 1896,
      name: 'Cleveland-Akron-Canton, OH CSA',
      population: 3598304,
      area_type: 'CSA',
    },
    {
      index: 1924,
      name: 'Cleveland-Elyria, OH MSA',
      population: 2063132,
      area_type: 'MSA',
    },
    {
      index: 1944,
      name: 'Geauga County, Ohio',
      population: 95469,
      area_type: 'County',
    },
  ],
  1946: [
    {
      index: 1909,
      name: 'Dayton-Springfield-Kettering, OH CSA',
      population: 1085335,
      area_type: 'CSA',
    },
    {
      index: 1945,
      name: 'Dayton-Kettering, OH MSA',
      population: 812595,
      area_type: 'MSA',
    },
    {
      index: 1946,
      name: 'Greene County, Ohio',
      population: 168456,
      area_type: 'County',
    },
  ],
  1947: [
    {
      index: 1930,
      name: 'Columbus-Marion-Zanesville, OH CSA',
      population: 2565032,
      area_type: 'CSA',
    },
    {
      index: 1947,
      name: 'Guernsey County, Ohio (Cambridge, OH μSA)',
      population: 38098,
      area_type: 'μSA',
    },
  ],
  1949: [
    {
      index: 746,
      name: 'Cincinnati-Wilmington-Maysville, OH-KY-IN CSA',
      population: 2323945,
      area_type: 'CSA',
    },
    {
      index: 747,
      name: 'Cincinnati, OH-KY-IN MSA',
      population: 2265051,
      area_type: 'MSA',
    },
    {
      index: 1949,
      name: 'Hamilton County, Ohio',
      population: 825037,
      area_type: 'County',
    },
  ],
  1950: [
    {
      index: 1939,
      name: 'Toledo-Findlay-Tiffin, OH CSA',
      population: 828544,
      area_type: 'CSA',
    },
    {
      index: 1950,
      name: 'Hancock County, Ohio (Findlay, OH μSA)',
      population: 74861,
      area_type: 'μSA',
    },
  ],
  1952: [
    {
      index: 1930,
      name: 'Columbus-Marion-Zanesville, OH CSA',
      population: 2565032,
      area_type: 'CSA',
    },
    {
      index: 1931,
      name: 'Columbus, OH MSA',
      population: 2161511,
      area_type: 'MSA',
    },
    {
      index: 1952,
      name: 'Hocking County, Ohio',
      population: 27858,
      area_type: 'County',
    },
  ],
  1953: [
    {
      index: 1896,
      name: 'Cleveland-Akron-Canton, OH CSA',
      population: 3598304,
      area_type: 'CSA',
    },
    {
      index: 1953,
      name: 'Huron County, Ohio (Norwalk, OH μSA)',
      population: 58218,
      area_type: 'μSA',
    },
  ],
  1955: [
    {
      index: 1955,
      name: 'Jackson County, Ohio (Jackson, OH μSA)',
      population: 32586,
      area_type: 'μSA',
    },
  ],
  1959: [
    {
      index: 1957,
      name: 'Pittsburgh-New Castle-Weirton, PA-OH-WV CSA',
      population: 2631213,
      area_type: 'CSA',
    },
    {
      index: 1958,
      name: 'Weirton-Steubenville, WV-OH MSA',
      population: 114235,
      area_type: 'MSA',
    },
    {
      index: 1959,
      name: 'Jefferson County, Ohio',
      population: 64330,
      area_type: 'County',
    },
  ],
  1960: [
    {
      index: 1930,
      name: 'Columbus-Marion-Zanesville, OH CSA',
      population: 2565032,
      area_type: 'CSA',
    },
    {
      index: 1960,
      name: 'Knox County, Ohio (Mount Vernon, OH μSA)',
      population: 63183,
      area_type: 'μSA',
    },
  ],
  1962: [
    {
      index: 1896,
      name: 'Cleveland-Akron-Canton, OH CSA',
      population: 3598304,
      area_type: 'CSA',
    },
    {
      index: 1924,
      name: 'Cleveland-Elyria, OH MSA',
      population: 2063132,
      area_type: 'MSA',
    },
    {
      index: 1962,
      name: 'Lake County, Ohio',
      population: 231842,
      area_type: 'County',
    },
  ],
  1963: [
    {
      index: 980,
      name: 'Charleston-Huntington-Ashland, WV-OH-KY CSA',
      population: 763796,
      area_type: 'CSA',
    },
    {
      index: 981,
      name: 'Huntington-Ashland, WV-KY-OH MSA',
      population: 354304,
      area_type: 'MSA',
    },
    {
      index: 1963,
      name: 'Lawrence County, Ohio',
      population: 56653,
      area_type: 'County',
    },
  ],
  1964: [
    {
      index: 1930,
      name: 'Columbus-Marion-Zanesville, OH CSA',
      population: 2565032,
      area_type: 'CSA',
    },
    {
      index: 1931,
      name: 'Columbus, OH MSA',
      population: 2161511,
      area_type: 'MSA',
    },
    {
      index: 1964,
      name: 'Licking County, Ohio',
      population: 181359,
      area_type: 'County',
    },
  ],
  1965: [
    {
      index: 1930,
      name: 'Columbus-Marion-Zanesville, OH CSA',
      population: 2565032,
      area_type: 'CSA',
    },
    {
      index: 1965,
      name: 'Logan County, Ohio (Bellefontaine, OH μSA)',
      population: 46040,
      area_type: 'μSA',
    },
  ],
  1967: [
    {
      index: 1896,
      name: 'Cleveland-Akron-Canton, OH CSA',
      population: 3598304,
      area_type: 'CSA',
    },
    {
      index: 1924,
      name: 'Cleveland-Elyria, OH MSA',
      population: 2063132,
      area_type: 'MSA',
    },
    {
      index: 1967,
      name: 'Lorain County, Ohio',
      population: 316268,
      area_type: 'County',
    },
  ],
  1968: [
    {
      index: 1939,
      name: 'Toledo-Findlay-Tiffin, OH CSA',
      population: 828544,
      area_type: 'CSA',
    },
    {
      index: 1940,
      name: 'Toledo, OH MSA',
      population: 640384,
      area_type: 'MSA',
    },
    {
      index: 1968,
      name: 'Lucas County, Ohio',
      population: 426643,
      area_type: 'County',
    },
  ],
  1969: [
    {
      index: 1930,
      name: 'Columbus-Marion-Zanesville, OH CSA',
      population: 2565032,
      area_type: 'CSA',
    },
    {
      index: 1931,
      name: 'Columbus, OH MSA',
      population: 2161511,
      area_type: 'MSA',
    },
    {
      index: 1969,
      name: 'Madison County, Ohio',
      population: 43540,
      area_type: 'County',
    },
  ],
  1971: [
    {
      index: 1917,
      name: 'Youngstown-Warren, OH-PA CSA',
      population: 636010,
      area_type: 'CSA',
    },
    {
      index: 1970,
      name: 'Youngstown-Warren-Boardman, OH-PA MSA',
      population: 535499,
      area_type: 'MSA',
    },
    {
      index: 1971,
      name: 'Mahoning County, Ohio',
      population: 225636,
      area_type: 'County',
    },
  ],
  1972: [
    {
      index: 1930,
      name: 'Columbus-Marion-Zanesville, OH CSA',
      population: 2565032,
      area_type: 'CSA',
    },
    {
      index: 1972,
      name: 'Marion County, Ohio (Marion, OH μSA)',
      population: 64642,
      area_type: 'μSA',
    },
  ],
  1974: [
    {
      index: 1896,
      name: 'Cleveland-Akron-Canton, OH CSA',
      population: 3598304,
      area_type: 'CSA',
    },
    {
      index: 1924,
      name: 'Cleveland-Elyria, OH MSA',
      population: 2063132,
      area_type: 'MSA',
    },
    {
      index: 1974,
      name: 'Medina County, Ohio',
      population: 183512,
      area_type: 'County',
    },
  ],
  1975: [
    {
      index: 1890,
      name: 'Lima-Van Wert-Celina, OH CSA',
      population: 218180,
      area_type: 'CSA',
    },
    {
      index: 1975,
      name: 'Mercer County, Ohio (Celina, OH μSA)',
      population: 42348,
      area_type: 'μSA',
    },
  ],
  1977: [
    {
      index: 1909,
      name: 'Dayton-Springfield-Kettering, OH CSA',
      population: 1085335,
      area_type: 'CSA',
    },
    {
      index: 1945,
      name: 'Dayton-Kettering, OH MSA',
      population: 812595,
      area_type: 'MSA',
    },
    {
      index: 1977,
      name: 'Miami County, Ohio',
      population: 110247,
      area_type: 'County',
    },
  ],
  1978: [
    {
      index: 1909,
      name: 'Dayton-Springfield-Kettering, OH CSA',
      population: 1085335,
      area_type: 'CSA',
    },
    {
      index: 1945,
      name: 'Dayton-Kettering, OH MSA',
      population: 812595,
      area_type: 'MSA',
    },
    {
      index: 1978,
      name: 'Montgomery County, Ohio',
      population: 533892,
      area_type: 'County',
    },
  ],
  1979: [
    {
      index: 1930,
      name: 'Columbus-Marion-Zanesville, OH CSA',
      population: 2565032,
      area_type: 'CSA',
    },
    {
      index: 1931,
      name: 'Columbus, OH MSA',
      population: 2161511,
      area_type: 'MSA',
    },
    {
      index: 1979,
      name: 'Morrow County, Ohio',
      population: 35339,
      area_type: 'County',
    },
  ],
  1980: [
    {
      index: 1930,
      name: 'Columbus-Marion-Zanesville, OH CSA',
      population: 2565032,
      area_type: 'CSA',
    },
    {
      index: 1980,
      name: 'Muskingum County, Ohio (Zanesville, OH μSA)',
      population: 86113,
      area_type: 'μSA',
    },
  ],
  1982: [
    {
      index: 1939,
      name: 'Toledo-Findlay-Tiffin, OH CSA',
      population: 828544,
      area_type: 'CSA',
    },
    {
      index: 1940,
      name: 'Toledo, OH MSA',
      population: 640384,
      area_type: 'MSA',
    },
    {
      index: 1982,
      name: 'Ottawa County, Ohio',
      population: 39978,
      area_type: 'County',
    },
  ],
  1983: [
    {
      index: 1930,
      name: 'Columbus-Marion-Zanesville, OH CSA',
      population: 2565032,
      area_type: 'CSA',
    },
    {
      index: 1931,
      name: 'Columbus, OH MSA',
      population: 2161511,
      area_type: 'MSA',
    },
    {
      index: 1983,
      name: 'Perry County, Ohio',
      population: 35480,
      area_type: 'County',
    },
  ],
  1984: [
    {
      index: 1930,
      name: 'Columbus-Marion-Zanesville, OH CSA',
      population: 2565032,
      area_type: 'CSA',
    },
    {
      index: 1931,
      name: 'Columbus, OH MSA',
      population: 2161511,
      area_type: 'MSA',
    },
    {
      index: 1984,
      name: 'Pickaway County, Ohio',
      population: 60023,
      area_type: 'County',
    },
  ],
  1986: [
    {
      index: 1896,
      name: 'Cleveland-Akron-Canton, OH CSA',
      population: 3598304,
      area_type: 'CSA',
    },
    {
      index: 1985,
      name: 'Akron, OH MSA',
      population: 697627,
      area_type: 'MSA',
    },
    {
      index: 1986,
      name: 'Portage County, Ohio',
      population: 161745,
      area_type: 'County',
    },
  ],
  1987: [
    {
      index: 1893,
      name: 'Mansfield-Ashland-Bucyrus, OH CSA',
      population: 219022,
      area_type: 'CSA',
    },
    {
      index: 1987,
      name: 'Richland County, Ohio (Mansfield, OH MSA)',
      population: 125319,
      area_type: 'MSA',
    },
  ],
  1989: [
    {
      index: 1930,
      name: 'Columbus-Marion-Zanesville, OH CSA',
      population: 2565032,
      area_type: 'CSA',
    },
    {
      index: 1989,
      name: 'Ross County, Ohio (Chillicothe, OH μSA)',
      population: 76606,
      area_type: 'μSA',
    },
  ],
  1991: [
    {
      index: 1939,
      name: 'Toledo-Findlay-Tiffin, OH CSA',
      population: 828544,
      area_type: 'CSA',
    },
    {
      index: 1991,
      name: 'Sandusky County, Ohio (Fremont, OH μSA)',
      population: 58667,
      area_type: 'μSA',
    },
  ],
  1993: [
    {
      index: 980,
      name: 'Charleston-Huntington-Ashland, WV-OH-KY CSA',
      population: 763796,
      area_type: 'CSA',
    },
    {
      index: 1993,
      name: 'Scioto County, Ohio (Portsmouth, OH μSA)',
      population: 72194,
      area_type: 'μSA',
    },
  ],
  1995: [
    {
      index: 1939,
      name: 'Toledo-Findlay-Tiffin, OH CSA',
      population: 828544,
      area_type: 'CSA',
    },
    {
      index: 1995,
      name: 'Seneca County, Ohio (Tiffin, OH μSA)',
      population: 54632,
      area_type: 'μSA',
    },
  ],
  1997: [
    {
      index: 1909,
      name: 'Dayton-Springfield-Kettering, OH CSA',
      population: 1085335,
      area_type: 'CSA',
    },
    {
      index: 1997,
      name: 'Shelby County, Ohio (Sidney, OH μSA)',
      population: 47671,
      area_type: 'μSA',
    },
  ],
  1999: [
    {
      index: 1896,
      name: 'Cleveland-Akron-Canton, OH CSA',
      population: 3598304,
      area_type: 'CSA',
    },
    {
      index: 1907,
      name: 'Canton-Massillon, OH MSA',
      population: 399316,
      area_type: 'MSA',
    },
    {
      index: 1999,
      name: 'Stark County, Ohio',
      population: 372657,
      area_type: 'County',
    },
  ],
  2000: [
    {
      index: 1896,
      name: 'Cleveland-Akron-Canton, OH CSA',
      population: 3598304,
      area_type: 'CSA',
    },
    {
      index: 1985,
      name: 'Akron, OH MSA',
      population: 697627,
      area_type: 'MSA',
    },
    {
      index: 2000,
      name: 'Summit County, Ohio',
      population: 535882,
      area_type: 'County',
    },
  ],
  2001: [
    {
      index: 1917,
      name: 'Youngstown-Warren, OH-PA CSA',
      population: 636010,
      area_type: 'CSA',
    },
    {
      index: 1970,
      name: 'Youngstown-Warren-Boardman, OH-PA MSA',
      population: 535499,
      area_type: 'MSA',
    },
    {
      index: 2001,
      name: 'Trumbull County, Ohio',
      population: 200643,
      area_type: 'County',
    },
  ],
  2002: [
    {
      index: 1896,
      name: 'Cleveland-Akron-Canton, OH CSA',
      population: 3598304,
      area_type: 'CSA',
    },
    {
      index: 2002,
      name: 'Tuscarawas County, Ohio (New Philadelphia-Dover, OH μSA)',
      population: 91937,
      area_type: 'μSA',
    },
  ],
  2004: [
    {
      index: 1930,
      name: 'Columbus-Marion-Zanesville, OH CSA',
      population: 2565032,
      area_type: 'CSA',
    },
    {
      index: 1931,
      name: 'Columbus, OH MSA',
      population: 2161511,
      area_type: 'MSA',
    },
    {
      index: 2004,
      name: 'Union County, Ohio',
      population: 66898,
      area_type: 'County',
    },
  ],
  2005: [
    {
      index: 1890,
      name: 'Lima-Van Wert-Celina, OH CSA',
      population: 218180,
      area_type: 'CSA',
    },
    {
      index: 2005,
      name: 'Van Wert County, Ohio (Van Wert, OH μSA)',
      population: 28769,
      area_type: 'μSA',
    },
  ],
  2007: [
    {
      index: 746,
      name: 'Cincinnati-Wilmington-Maysville, OH-KY-IN CSA',
      population: 2323945,
      area_type: 'CSA',
    },
    {
      index: 747,
      name: 'Cincinnati, OH-KY-IN MSA',
      population: 2265051,
      area_type: 'MSA',
    },
    {
      index: 2007,
      name: 'Warren County, Ohio',
      population: 249778,
      area_type: 'County',
    },
  ],
  2009: [
    {
      index: 2008,
      name: 'Parkersburg-Marietta-Vienna, WV-OH CSA',
      population: 147332,
      area_type: 'CSA',
    },
    {
      index: 2009,
      name: 'Washington County, Ohio (Marietta, OH μSA)',
      population: 58901,
      area_type: 'μSA',
    },
  ],
  2011: [
    {
      index: 1896,
      name: 'Cleveland-Akron-Canton, OH CSA',
      population: 3598304,
      area_type: 'CSA',
    },
    {
      index: 2011,
      name: 'Wayne County, Ohio (Wooster, OH μSA)',
      population: 116559,
      area_type: 'μSA',
    },
  ],
  2013: [
    {
      index: 1939,
      name: 'Toledo-Findlay-Tiffin, OH CSA',
      population: 828544,
      area_type: 'CSA',
    },
    {
      index: 1940,
      name: 'Toledo, OH MSA',
      population: 640384,
      area_type: 'MSA',
    },
    {
      index: 2013,
      name: 'Wood County, Ohio',
      population: 131592,
      area_type: 'County',
    },
  ],
  2014: [
    {
      index: 2014,
      name: 'Beckham County, Oklahoma (Elk City, OK μSA)',
      population: 22009,
      area_type: 'μSA',
    },
  ],
  2017: [
    {
      index: 2016,
      name: 'Dallas-Fort Worth, TX-OK CSA',
      population: 8449932,
      area_type: 'CSA',
    },
    {
      index: 2017,
      name: 'Bryan County, Oklahoma (Durant, OK μSA)',
      population: 48182,
      area_type: 'μSA',
    },
  ],
  2021: [
    {
      index: 2019,
      name: 'Oklahoma City-Shawnee, OK CSA',
      population: 1532913,
      area_type: 'CSA',
    },
    {
      index: 2020,
      name: 'Oklahoma City, OK MSA',
      population: 1459380,
      area_type: 'MSA',
    },
    {
      index: 2021,
      name: 'Canadian County, Oklahoma',
      population: 169149,
      area_type: 'County',
    },
  ],
  2023: [
    {
      index: 2022,
      name: 'Ardmore, OK μSA',
      population: 58728,
      area_type: 'μSA',
    },
    {
      index: 2023,
      name: 'Carter County, Oklahoma',
      population: 48510,
      area_type: 'County',
    },
  ],
  2024: [
    {
      index: 2024,
      name: 'Cherokee County, Oklahoma (Tahlequah, OK μSA)',
      population: 48098,
      area_type: 'μSA',
    },
  ],
  2026: [
    {
      index: 2019,
      name: 'Oklahoma City-Shawnee, OK CSA',
      population: 1532913,
      area_type: 'CSA',
    },
    {
      index: 2020,
      name: 'Oklahoma City, OK MSA',
      population: 1459380,
      area_type: 'MSA',
    },
    {
      index: 2026,
      name: 'Cleveland County, Oklahoma',
      population: 299587,
      area_type: 'County',
    },
  ],
  2028: [
    {
      index: 2027,
      name: 'Lawton, OK MSA',
      population: 128523,
      area_type: 'MSA',
    },
    {
      index: 2028,
      name: 'Comanche County, Oklahoma',
      population: 123046,
      area_type: 'County',
    },
  ],
  2029: [
    {
      index: 2027,
      name: 'Lawton, OK MSA',
      population: 128523,
      area_type: 'MSA',
    },
    {
      index: 2029,
      name: 'Cotton County, Oklahoma',
      population: 5477,
      area_type: 'County',
    },
  ],
  2032: [
    {
      index: 2030,
      name: 'Tulsa-Muskogee-Bartlesville, OK CSA',
      population: 1153719,
      area_type: 'CSA',
    },
    {
      index: 2031,
      name: 'Tulsa, OK MSA',
      population: 1034123,
      area_type: 'MSA',
    },
    {
      index: 2032,
      name: 'Creek County, Oklahoma',
      population: 72699,
      area_type: 'County',
    },
  ],
  2033: [
    {
      index: 2033,
      name: 'Custer County, Oklahoma (Weatherford, OK μSA)',
      population: 27886,
      area_type: 'μSA',
    },
  ],
  2036: [
    {
      index: 2035,
      name: 'Woodward, OK μSA',
      population: 23647,
      area_type: 'μSA',
    },
    {
      index: 2036,
      name: 'Ellis County, Oklahoma',
      population: 3657,
      area_type: 'County',
    },
  ],
  2037: [
    {
      index: 2037,
      name: 'Garfield County, Oklahoma (Enid, OK MSA)',
      population: 61920,
      area_type: 'MSA',
    },
  ],
  2039: [
    {
      index: 2019,
      name: 'Oklahoma City-Shawnee, OK CSA',
      population: 1532913,
      area_type: 'CSA',
    },
    {
      index: 2020,
      name: 'Oklahoma City, OK MSA',
      population: 1459380,
      area_type: 'MSA',
    },
    {
      index: 2039,
      name: 'Grady County, Oklahoma',
      population: 56658,
      area_type: 'County',
    },
  ],
  2040: [
    {
      index: 2040,
      name: 'Jackson County, Oklahoma (Altus, OK μSA)',
      population: 24556,
      area_type: 'μSA',
    },
  ],
  2042: [
    {
      index: 2042,
      name: 'Kay County, Oklahoma (Ponca City, OK μSA)',
      population: 43668,
      area_type: 'μSA',
    },
  ],
  2044: [
    {
      index: 2019,
      name: 'Oklahoma City-Shawnee, OK CSA',
      population: 1532913,
      area_type: 'CSA',
    },
    {
      index: 2020,
      name: 'Oklahoma City, OK MSA',
      population: 1459380,
      area_type: 'MSA',
    },
    {
      index: 2044,
      name: 'Lincoln County, Oklahoma',
      population: 34188,
      area_type: 'County',
    },
  ],
  2045: [
    {
      index: 2019,
      name: 'Oklahoma City-Shawnee, OK CSA',
      population: 1532913,
      area_type: 'CSA',
    },
    {
      index: 2020,
      name: 'Oklahoma City, OK MSA',
      population: 1459380,
      area_type: 'MSA',
    },
    {
      index: 2045,
      name: 'Logan County, Oklahoma',
      population: 51933,
      area_type: 'County',
    },
  ],
  2046: [
    {
      index: 2022,
      name: 'Ardmore, OK μSA',
      population: 58728,
      area_type: 'μSA',
    },
    {
      index: 2046,
      name: 'Love County, Oklahoma',
      population: 10218,
      area_type: 'County',
    },
  ],
  2047: [
    {
      index: 2019,
      name: 'Oklahoma City-Shawnee, OK CSA',
      population: 1532913,
      area_type: 'CSA',
    },
    {
      index: 2020,
      name: 'Oklahoma City, OK MSA',
      population: 1459380,
      area_type: 'MSA',
    },
    {
      index: 2047,
      name: 'McClain County, Oklahoma',
      population: 45306,
      area_type: 'County',
    },
  ],
  2048: [
    {
      index: 2030,
      name: 'Tulsa-Muskogee-Bartlesville, OK CSA',
      population: 1153719,
      area_type: 'CSA',
    },
    {
      index: 2048,
      name: 'Muskogee County, Oklahoma (Muskogee, OK μSA)',
      population: 66354,
      area_type: 'μSA',
    },
  ],
  2050: [
    {
      index: 2019,
      name: 'Oklahoma City-Shawnee, OK CSA',
      population: 1532913,
      area_type: 'CSA',
    },
    {
      index: 2020,
      name: 'Oklahoma City, OK MSA',
      population: 1459380,
      area_type: 'MSA',
    },
    {
      index: 2050,
      name: 'Oklahoma County, Oklahoma',
      population: 802559,
      area_type: 'County',
    },
  ],
  2051: [
    {
      index: 2030,
      name: 'Tulsa-Muskogee-Bartlesville, OK CSA',
      population: 1153719,
      area_type: 'CSA',
    },
    {
      index: 2031,
      name: 'Tulsa, OK MSA',
      population: 1034123,
      area_type: 'MSA',
    },
    {
      index: 2051,
      name: 'Okmulgee County, Oklahoma',
      population: 36990,
      area_type: 'County',
    },
  ],
  2052: [
    {
      index: 2030,
      name: 'Tulsa-Muskogee-Bartlesville, OK CSA',
      population: 1153719,
      area_type: 'CSA',
    },
    {
      index: 2031,
      name: 'Tulsa, OK MSA',
      population: 1034123,
      area_type: 'MSA',
    },
    {
      index: 2052,
      name: 'Osage County, Oklahoma',
      population: 45839,
      area_type: 'County',
    },
  ],
  2053: [
    {
      index: 1451,
      name: 'Joplin-Miami, MO-OK CSA',
      population: 214424,
      area_type: 'CSA',
    },
    {
      index: 2053,
      name: 'Ottawa County, Oklahoma (Miami, OK μSA)',
      population: 30338,
      area_type: 'μSA',
    },
  ],
  2055: [
    {
      index: 2030,
      name: 'Tulsa-Muskogee-Bartlesville, OK CSA',
      population: 1153719,
      area_type: 'CSA',
    },
    {
      index: 2031,
      name: 'Tulsa, OK MSA',
      population: 1034123,
      area_type: 'MSA',
    },
    {
      index: 2055,
      name: 'Pawnee County, Oklahoma',
      population: 15757,
      area_type: 'County',
    },
  ],
  2056: [
    {
      index: 2056,
      name: 'Payne County, Oklahoma (Stillwater, OK μSA)',
      population: 82794,
      area_type: 'μSA',
    },
  ],
  2058: [
    {
      index: 2058,
      name: 'Pittsburg County, Oklahoma (McAlester, OK μSA)',
      population: 43613,
      area_type: 'μSA',
    },
  ],
  2060: [
    {
      index: 2060,
      name: 'Pontotoc County, Oklahoma (Ada, OK μSA)',
      population: 38141,
      area_type: 'μSA',
    },
  ],
  2062: [
    {
      index: 2019,
      name: 'Oklahoma City-Shawnee, OK CSA',
      population: 1532913,
      area_type: 'CSA',
    },
    {
      index: 2062,
      name: 'Pottawatomie County, Oklahoma (Shawnee, OK μSA)',
      population: 73533,
      area_type: 'μSA',
    },
  ],
  2064: [
    {
      index: 2030,
      name: 'Tulsa-Muskogee-Bartlesville, OK CSA',
      population: 1153719,
      area_type: 'CSA',
    },
    {
      index: 2031,
      name: 'Tulsa, OK MSA',
      population: 1034123,
      area_type: 'MSA',
    },
    {
      index: 2064,
      name: 'Rogers County, Oklahoma',
      population: 98836,
      area_type: 'County',
    },
  ],
  2065: [
    {
      index: 134,
      name: 'Fort Smith, AR-OK MSA',
      population: 247072,
      area_type: 'MSA',
    },
    {
      index: 2065,
      name: 'Sequoyah County, Oklahoma',
      population: 39667,
      area_type: 'County',
    },
  ],
  2066: [
    {
      index: 2066,
      name: 'Stephens County, Oklahoma (Duncan, OK μSA)',
      population: 43710,
      area_type: 'μSA',
    },
  ],
  2068: [
    {
      index: 2068,
      name: 'Texas County, Oklahoma (Guymon, OK μSA)',
      population: 20495,
      area_type: 'μSA',
    },
  ],
  2070: [
    {
      index: 2030,
      name: 'Tulsa-Muskogee-Bartlesville, OK CSA',
      population: 1153719,
      area_type: 'CSA',
    },
    {
      index: 2031,
      name: 'Tulsa, OK MSA',
      population: 1034123,
      area_type: 'MSA',
    },
    {
      index: 2070,
      name: 'Tulsa County, Oklahoma',
      population: 677358,
      area_type: 'County',
    },
  ],
  2071: [
    {
      index: 2030,
      name: 'Tulsa-Muskogee-Bartlesville, OK CSA',
      population: 1153719,
      area_type: 'CSA',
    },
    {
      index: 2031,
      name: 'Tulsa, OK MSA',
      population: 1034123,
      area_type: 'MSA',
    },
    {
      index: 2071,
      name: 'Wagoner County, Oklahoma',
      population: 86644,
      area_type: 'County',
    },
  ],
  2072: [
    {
      index: 2030,
      name: 'Tulsa-Muskogee-Bartlesville, OK CSA',
      population: 1153719,
      area_type: 'CSA',
    },
    {
      index: 2072,
      name: 'Washington County, Oklahoma (Bartlesville, OK μSA)',
      population: 53242,
      area_type: 'μSA',
    },
  ],
  2074: [
    {
      index: 2035,
      name: 'Woodward, OK μSA',
      population: 23647,
      area_type: 'μSA',
    },
    {
      index: 2074,
      name: 'Woodward County, Oklahoma',
      population: 19990,
      area_type: 'County',
    },
  ],
  2076: [
    {
      index: 2075,
      name: 'Portland-Vancouver-Salem, OR-WA CSA',
      population: 3285859,
      area_type: 'CSA',
    },
    {
      index: 2076,
      name: 'Benton County, Oregon (Corvallis, OR MSA)',
      population: 97630,
      area_type: 'MSA',
    },
  ],
  2079: [
    {
      index: 2075,
      name: 'Portland-Vancouver-Salem, OR-WA CSA',
      population: 3285859,
      area_type: 'CSA',
    },
    {
      index: 2078,
      name: 'Portland-Vancouver-Hillsboro, OR-WA MSA',
      population: 2509489,
      area_type: 'MSA',
    },
    {
      index: 2079,
      name: 'Clackamas County, Oregon',
      population: 423177,
      area_type: 'County',
    },
  ],
  2080: [
    {
      index: 2080,
      name: 'Clatsop County, Oregon (Astoria, OR μSA)',
      population: 41695,
      area_type: 'μSA',
    },
  ],
  2082: [
    {
      index: 2075,
      name: 'Portland-Vancouver-Salem, OR-WA CSA',
      population: 3285859,
      area_type: 'CSA',
    },
    {
      index: 2078,
      name: 'Portland-Vancouver-Hillsboro, OR-WA MSA',
      population: 2509489,
      area_type: 'MSA',
    },
    {
      index: 2082,
      name: 'Columbia County, Oregon',
      population: 53588,
      area_type: 'County',
    },
  ],
  2083: [
    {
      index: 2083,
      name: 'Coos County, Oregon (Coos Bay, OR μSA)',
      population: 64990,
      area_type: 'μSA',
    },
  ],
  2086: [
    {
      index: 2085,
      name: 'Bend-Prineville, OR CSA',
      population: 232924,
      area_type: 'CSA',
    },
    {
      index: 2086,
      name: 'Crook County, Oregon (Prineville, OR μSA)',
      population: 26375,
      area_type: 'μSA',
    },
  ],
  2088: [
    {
      index: 2088,
      name: 'Curry County, Oregon (Brookings, OR μSA)',
      population: 23598,
      area_type: 'μSA',
    },
  ],
  2090: [
    {
      index: 2085,
      name: 'Bend-Prineville, OR CSA',
      population: 232924,
      area_type: 'CSA',
    },
    {
      index: 2090,
      name: 'Deschutes County, Oregon (Bend, OR MSA)',
      population: 206549,
      area_type: 'MSA',
    },
  ],
  2092: [
    {
      index: 2092,
      name: 'Douglas County, Oregon (Roseburg, OR μSA)',
      population: 112297,
      area_type: 'μSA',
    },
  ],
  2094: [
    {
      index: 2094,
      name: 'Hood River County, Oregon (Hood River, OR μSA)',
      population: 24048,
      area_type: 'μSA',
    },
  ],
  2097: [
    {
      index: 2096,
      name: 'Medford-Grants Pass, OR CSA',
      population: 309374,
      area_type: 'CSA',
    },
    {
      index: 2097,
      name: 'Jackson County, Oregon (Medford, OR MSA)',
      population: 221644,
      area_type: 'MSA',
    },
  ],
  2099: [
    {
      index: 2096,
      name: 'Medford-Grants Pass, OR CSA',
      population: 309374,
      area_type: 'CSA',
    },
    {
      index: 2099,
      name: 'Josephine County, Oregon (Grants Pass, OR MSA)',
      population: 87730,
      area_type: 'MSA',
    },
  ],
  2101: [
    {
      index: 2101,
      name: 'Klamath County, Oregon (Klamath Falls, OR μSA)',
      population: 70212,
      area_type: 'μSA',
    },
  ],
  2103: [
    {
      index: 2103,
      name: 'Lane County, Oregon (Eugene-Springfield, OR MSA)',
      population: 382353,
      area_type: 'MSA',
    },
  ],
  2105: [
    {
      index: 2105,
      name: 'Lincoln County, Oregon (Newport, OR μSA)',
      population: 50813,
      area_type: 'μSA',
    },
  ],
  2107: [
    {
      index: 2075,
      name: 'Portland-Vancouver-Salem, OR-WA CSA',
      population: 3285859,
      area_type: 'CSA',
    },
    {
      index: 2107,
      name: 'Linn County, Oregon (Albany-Lebanon, OR MSA)',
      population: 130467,
      area_type: 'MSA',
    },
  ],
  2109: [
    {
      index: 567,
      name: 'Boise City-Mountain Home-Ontario, ID-OR CSA',
      population: 899574,
      area_type: 'CSA',
    },
    {
      index: 608,
      name: 'Ontario, OR-ID μSA',
      population: 58835,
      area_type: 'μSA',
    },
    {
      index: 2109,
      name: 'Malheur County, Oregon',
      population: 31879,
      area_type: 'County',
    },
  ],
  2111: [
    {
      index: 2075,
      name: 'Portland-Vancouver-Salem, OR-WA CSA',
      population: 3285859,
      area_type: 'CSA',
    },
    {
      index: 2110,
      name: 'Salem, OR MSA',
      population: 436317,
      area_type: 'MSA',
    },
    {
      index: 2111,
      name: 'Marion County, Oregon',
      population: 346703,
      area_type: 'County',
    },
  ],
  2113: [
    {
      index: 2112,
      name: 'Hermiston-Pendleton, OR μSA',
      population: 92515,
      area_type: 'μSA',
    },
    {
      index: 2113,
      name: 'Morrow County, Oregon',
      population: 12300,
      area_type: 'County',
    },
  ],
  2114: [
    {
      index: 2075,
      name: 'Portland-Vancouver-Salem, OR-WA CSA',
      population: 3285859,
      area_type: 'CSA',
    },
    {
      index: 2078,
      name: 'Portland-Vancouver-Hillsboro, OR-WA MSA',
      population: 2509489,
      area_type: 'MSA',
    },
    {
      index: 2114,
      name: 'Multnomah County, Oregon',
      population: 795083,
      area_type: 'County',
    },
  ],
  2115: [
    {
      index: 2075,
      name: 'Portland-Vancouver-Salem, OR-WA CSA',
      population: 3285859,
      area_type: 'CSA',
    },
    {
      index: 2110,
      name: 'Salem, OR MSA',
      population: 436317,
      area_type: 'MSA',
    },
    {
      index: 2115,
      name: 'Polk County, Oregon',
      population: 89614,
      area_type: 'County',
    },
  ],
  2116: [
    {
      index: 2112,
      name: 'Hermiston-Pendleton, OR μSA',
      population: 92515,
      area_type: 'μSA',
    },
    {
      index: 2116,
      name: 'Umatilla County, Oregon',
      population: 80215,
      area_type: 'County',
    },
  ],
  2117: [
    {
      index: 2117,
      name: 'Union County, Oregon (La Grande, OR μSA)',
      population: 26177,
      area_type: 'μSA',
    },
  ],
  2119: [
    {
      index: 2119,
      name: 'Wasco County, Oregon (The Dalles, OR μSA)',
      population: 26561,
      area_type: 'μSA',
    },
  ],
  2121: [
    {
      index: 2075,
      name: 'Portland-Vancouver-Salem, OR-WA CSA',
      population: 3285859,
      area_type: 'CSA',
    },
    {
      index: 2078,
      name: 'Portland-Vancouver-Hillsboro, OR-WA MSA',
      population: 2509489,
      area_type: 'MSA',
    },
    {
      index: 2121,
      name: 'Washington County, Oregon',
      population: 600176,
      area_type: 'County',
    },
  ],
  2122: [
    {
      index: 2075,
      name: 'Portland-Vancouver-Salem, OR-WA CSA',
      population: 3285859,
      area_type: 'CSA',
    },
    {
      index: 2078,
      name: 'Portland-Vancouver-Hillsboro, OR-WA MSA',
      population: 2509489,
      area_type: 'MSA',
    },
    {
      index: 2122,
      name: 'Yamhill County, Oregon',
      population: 108226,
      area_type: 'County',
    },
  ],
  2124: [
    {
      index: 2123,
      name: 'Harrisburg-York-Lebanon, PA CSA',
      population: 1314589,
      area_type: 'CSA',
    },
    {
      index: 2124,
      name: 'Adams County, Pennsylvania (Gettysburg, PA MSA)',
      population: 106027,
      area_type: 'MSA',
    },
  ],
  2127: [
    {
      index: 1957,
      name: 'Pittsburgh-New Castle-Weirton, PA-OH-WV CSA',
      population: 2631213,
      area_type: 'CSA',
    },
    {
      index: 2126,
      name: 'Pittsburgh, PA MSA',
      population: 2349172,
      area_type: 'MSA',
    },
    {
      index: 2127,
      name: 'Allegheny County, Pennsylvania',
      population: 1233253,
      area_type: 'County',
    },
  ],
  2128: [
    {
      index: 1957,
      name: 'Pittsburgh-New Castle-Weirton, PA-OH-WV CSA',
      population: 2631213,
      area_type: 'CSA',
    },
    {
      index: 2126,
      name: 'Pittsburgh, PA MSA',
      population: 2349172,
      area_type: 'MSA',
    },
    {
      index: 2128,
      name: 'Armstrong County, Pennsylvania',
      population: 64747,
      area_type: 'County',
    },
  ],
  2129: [
    {
      index: 1957,
      name: 'Pittsburgh-New Castle-Weirton, PA-OH-WV CSA',
      population: 2631213,
      area_type: 'CSA',
    },
    {
      index: 2126,
      name: 'Pittsburgh, PA MSA',
      population: 2349172,
      area_type: 'MSA',
    },
    {
      index: 2129,
      name: 'Beaver County, Pennsylvania',
      population: 165677,
      area_type: 'County',
    },
  ],
  2130: [
    {
      index: 318,
      name: 'Philadelphia-Reading-Camden, PA-NJ-DE-MD CSA',
      population: 7381187,
      area_type: 'CSA',
    },
    {
      index: 2130,
      name: 'Berks County, Pennsylvania (Reading, PA MSA)',
      population: 430449,
      area_type: 'MSA',
    },
  ],
  2133: [
    {
      index: 2132,
      name: 'Altoona-Huntingdon, PA CSA',
      population: 164313,
      area_type: 'CSA',
    },
    {
      index: 2133,
      name: 'Blair County, Pennsylvania (Altoona, PA MSA)',
      population: 121032,
      area_type: 'MSA',
    },
  ],
  2135: [
    {
      index: 2135,
      name: 'Bradford County, Pennsylvania (Sayre, PA μSA)',
      population: 59866,
      area_type: 'μSA',
    },
  ],
  2137: [
    {
      index: 318,
      name: 'Philadelphia-Reading-Camden, PA-NJ-DE-MD CSA',
      population: 7381187,
      area_type: 'CSA',
    },
    {
      index: 321,
      name: 'Philadelphia-Camden-Wilmington, PA-NJ-DE-MD MSA',
      population: 6241164,
      area_type: 'MSA',
    },
    {
      index: 2137,
      name: 'Bucks County, Pennsylvania',
      population: 645054,
      area_type: 'County',
    },
  ],
  2138: [
    {
      index: 1957,
      name: 'Pittsburgh-New Castle-Weirton, PA-OH-WV CSA',
      population: 2631213,
      area_type: 'CSA',
    },
    {
      index: 2126,
      name: 'Pittsburgh, PA MSA',
      population: 2349172,
      area_type: 'MSA',
    },
    {
      index: 2138,
      name: 'Butler County, Pennsylvania',
      population: 197300,
      area_type: 'County',
    },
  ],
  2140: [
    {
      index: 2139,
      name: 'Johnstown-Somerset, PA CSA',
      population: 204151,
      area_type: 'CSA',
    },
    {
      index: 2140,
      name: 'Cambria County, Pennsylvania (Johnstown, PA MSA)',
      population: 131441,
      area_type: 'MSA',
    },
  ],
  2142: [
    {
      index: 1617,
      name: 'Allentown-Bethlehem-Easton, PA-NJ MSA',
      population: 871229,
      area_type: 'MSA',
    },
    {
      index: 2142,
      name: 'Carbon County, Pennsylvania',
      population: 65460,
      area_type: 'County',
    },
  ],
  2144: [
    {
      index: 2143,
      name: 'State College-DuBois, PA CSA',
      population: 236329,
      area_type: 'CSA',
    },
    {
      index: 2144,
      name: 'Centre County, Pennsylvania (State College, PA MSA)',
      population: 158425,
      area_type: 'MSA',
    },
  ],
  2146: [
    {
      index: 318,
      name: 'Philadelphia-Reading-Camden, PA-NJ-DE-MD CSA',
      population: 7381187,
      area_type: 'CSA',
    },
    {
      index: 321,
      name: 'Philadelphia-Camden-Wilmington, PA-NJ-DE-MD MSA',
      population: 6241164,
      area_type: 'MSA',
    },
    {
      index: 2146,
      name: 'Chester County, Pennsylvania',
      population: 545823,
      area_type: 'County',
    },
  ],
  2147: [
    {
      index: 2143,
      name: 'State College-DuBois, PA CSA',
      population: 236329,
      area_type: 'CSA',
    },
    {
      index: 2147,
      name: 'Clearfield County, Pennsylvania (DuBois, PA μSA)',
      population: 77904,
      area_type: 'μSA',
    },
  ],
  2150: [
    {
      index: 2149,
      name: 'Williamsport-Lock Haven, PA CSA',
      population: 151035,
      area_type: 'CSA',
    },
    {
      index: 2150,
      name: 'Clinton County, Pennsylvania (Lock Haven, PA μSA)',
      population: 37931,
      area_type: 'μSA',
    },
  ],
  2154: [
    {
      index: 2152,
      name: 'Bloomsburg-Berwick-Sunbury, PA CSA',
      population: 255546,
      area_type: 'CSA',
    },
    {
      index: 2153,
      name: 'Bloomsburg-Berwick, PA MSA',
      population: 83017,
      area_type: 'MSA',
    },
    {
      index: 2154,
      name: 'Columbia County, Pennsylvania',
      population: 64926,
      area_type: 'County',
    },
  ],
  2156: [
    {
      index: 2155,
      name: 'Erie-Meadville, PA CSA',
      population: 350359,
      area_type: 'CSA',
    },
    {
      index: 2156,
      name: 'Crawford County, Pennsylvania (Meadville, PA μSA)',
      population: 82670,
      area_type: 'μSA',
    },
  ],
  2159: [
    {
      index: 2123,
      name: 'Harrisburg-York-Lebanon, PA CSA',
      population: 1314589,
      area_type: 'CSA',
    },
    {
      index: 2158,
      name: 'Harrisburg-Carlisle, PA MSA',
      population: 603493,
      area_type: 'MSA',
    },
    {
      index: 2159,
      name: 'Cumberland County, Pennsylvania',
      population: 268579,
      area_type: 'County',
    },
  ],
  2160: [
    {
      index: 2123,
      name: 'Harrisburg-York-Lebanon, PA CSA',
      population: 1314589,
      area_type: 'CSA',
    },
    {
      index: 2158,
      name: 'Harrisburg-Carlisle, PA MSA',
      population: 603493,
      area_type: 'MSA',
    },
    {
      index: 2160,
      name: 'Dauphin County, Pennsylvania',
      population: 288800,
      area_type: 'County',
    },
  ],
  2161: [
    {
      index: 318,
      name: 'Philadelphia-Reading-Camden, PA-NJ-DE-MD CSA',
      population: 7381187,
      area_type: 'CSA',
    },
    {
      index: 321,
      name: 'Philadelphia-Camden-Wilmington, PA-NJ-DE-MD MSA',
      population: 6241164,
      area_type: 'MSA',
    },
    {
      index: 2161,
      name: 'Delaware County, Pennsylvania',
      population: 575182,
      area_type: 'County',
    },
  ],
  2162: [
    {
      index: 2162,
      name: 'Elk County, Pennsylvania (St. Marys, PA μSA)',
      population: 30477,
      area_type: 'μSA',
    },
  ],
  2164: [
    {
      index: 2155,
      name: 'Erie-Meadville, PA CSA',
      population: 350359,
      area_type: 'CSA',
    },
    {
      index: 2164,
      name: 'Erie County, Pennsylvania (Erie, PA MSA)',
      population: 267689,
      area_type: 'MSA',
    },
  ],
  2166: [
    {
      index: 1957,
      name: 'Pittsburgh-New Castle-Weirton, PA-OH-WV CSA',
      population: 2631213,
      area_type: 'CSA',
    },
    {
      index: 2126,
      name: 'Pittsburgh, PA MSA',
      population: 2349172,
      area_type: 'MSA',
    },
    {
      index: 2166,
      name: 'Fayette County, Pennsylvania',
      population: 125755,
      area_type: 'County',
    },
  ],
  2167: [
    {
      index: 326,
      name: 'Washington-Baltimore-Arlington, DC-MD-VA-WV-PA CSA',
      population: 9968104,
      area_type: 'CSA',
    },
    {
      index: 2167,
      name: 'Franklin County, Pennsylvania (Chambersburg-Waynesboro, PA MSA)',
      population: 156902,
      area_type: 'MSA',
    },
  ],
  2169: [
    {
      index: 2132,
      name: 'Altoona-Huntingdon, PA CSA',
      population: 164313,
      area_type: 'CSA',
    },
    {
      index: 2169,
      name: 'Huntingdon County, Pennsylvania (Huntingdon, PA μSA)',
      population: 43281,
      area_type: 'μSA',
    },
  ],
  2171: [
    {
      index: 1957,
      name: 'Pittsburgh-New Castle-Weirton, PA-OH-WV CSA',
      population: 2631213,
      area_type: 'CSA',
    },
    {
      index: 2171,
      name: 'Indiana County, Pennsylvania (Indiana, PA μSA)',
      population: 82957,
      area_type: 'μSA',
    },
  ],
  2174: [
    {
      index: 2173,
      name: 'Scranton--Wilkes-Barre, PA MSA',
      population: 567998,
      area_type: 'MSA',
    },
    {
      index: 2174,
      name: 'Lackawanna County, Pennsylvania',
      population: 215615,
      area_type: 'County',
    },
  ],
  2175: [
    {
      index: 2175,
      name: 'Lancaster County, Pennsylvania (Lancaster, PA MSA)',
      population: 556629,
      area_type: 'MSA',
    },
  ],
  2177: [
    {
      index: 1957,
      name: 'Pittsburgh-New Castle-Weirton, PA-OH-WV CSA',
      population: 2631213,
      area_type: 'CSA',
    },
    {
      index: 2177,
      name: 'Lawrence County, Pennsylvania (New Castle, PA μSA)',
      population: 84849,
      area_type: 'μSA',
    },
  ],
  2179: [
    {
      index: 2123,
      name: 'Harrisburg-York-Lebanon, PA CSA',
      population: 1314589,
      area_type: 'CSA',
    },
    {
      index: 2179,
      name: 'Lebanon County, Pennsylvania (Lebanon, PA MSA)',
      population: 144011,
      area_type: 'MSA',
    },
  ],
  2181: [
    {
      index: 1617,
      name: 'Allentown-Bethlehem-Easton, PA-NJ MSA',
      population: 871229,
      area_type: 'MSA',
    },
    {
      index: 2181,
      name: 'Lehigh County, Pennsylvania',
      population: 376317,
      area_type: 'County',
    },
  ],
  2182: [
    {
      index: 2173,
      name: 'Scranton--Wilkes-Barre, PA MSA',
      population: 567998,
      area_type: 'MSA',
    },
    {
      index: 2182,
      name: 'Luzerne County, Pennsylvania',
      population: 326369,
      area_type: 'County',
    },
  ],
  2183: [
    {
      index: 2149,
      name: 'Williamsport-Lock Haven, PA CSA',
      population: 151035,
      area_type: 'CSA',
    },
    {
      index: 2183,
      name: 'Lycoming County, Pennsylvania (Williamsport, PA MSA)',
      population: 113104,
      area_type: 'MSA',
    },
  ],
  2185: [
    {
      index: 2185,
      name: 'McKean County, Pennsylvania (Bradford, PA μSA)',
      population: 39866,
      area_type: 'μSA',
    },
  ],
  2187: [
    {
      index: 1917,
      name: 'Youngstown-Warren, OH-PA CSA',
      population: 636010,
      area_type: 'CSA',
    },
    {
      index: 1970,
      name: 'Youngstown-Warren-Boardman, OH-PA MSA',
      population: 535499,
      area_type: 'MSA',
    },
    {
      index: 2187,
      name: 'Mercer County, Pennsylvania',
      population: 109220,
      area_type: 'County',
    },
  ],
  2188: [
    {
      index: 2188,
      name: 'Mifflin County, Pennsylvania (Lewistown, PA μSA)',
      population: 45988,
      area_type: 'μSA',
    },
  ],
  2190: [
    {
      index: 1593,
      name: 'New York-Newark, NY-NJ-CT-PA CSA',
      population: 23070149,
      area_type: 'CSA',
    },
    {
      index: 2190,
      name: 'Monroe County, Pennsylvania (East Stroudsburg, PA MSA)',
      population: 167198,
      area_type: 'MSA',
    },
  ],
  2192: [
    {
      index: 318,
      name: 'Philadelphia-Reading-Camden, PA-NJ-DE-MD CSA',
      population: 7381187,
      area_type: 'CSA',
    },
    {
      index: 321,
      name: 'Philadelphia-Camden-Wilmington, PA-NJ-DE-MD MSA',
      population: 6241164,
      area_type: 'MSA',
    },
    {
      index: 2192,
      name: 'Montgomery County, Pennsylvania',
      population: 864683,
      area_type: 'County',
    },
  ],
  2193: [
    {
      index: 2152,
      name: 'Bloomsburg-Berwick-Sunbury, PA CSA',
      population: 255546,
      area_type: 'CSA',
    },
    {
      index: 2153,
      name: 'Bloomsburg-Berwick, PA MSA',
      population: 83017,
      area_type: 'MSA',
    },
    {
      index: 2193,
      name: 'Montour County, Pennsylvania',
      population: 18091,
      area_type: 'County',
    },
  ],
  2194: [
    {
      index: 1617,
      name: 'Allentown-Bethlehem-Easton, PA-NJ MSA',
      population: 871229,
      area_type: 'MSA',
    },
    {
      index: 2194,
      name: 'Northampton County, Pennsylvania',
      population: 318526,
      area_type: 'County',
    },
  ],
  2195: [
    {
      index: 2152,
      name: 'Bloomsburg-Berwick-Sunbury, PA CSA',
      population: 255546,
      area_type: 'CSA',
    },
    {
      index: 2195,
      name: 'Northumberland County, Pennsylvania (Sunbury, PA μSA)',
      population: 90133,
      area_type: 'μSA',
    },
  ],
  2197: [
    {
      index: 2123,
      name: 'Harrisburg-York-Lebanon, PA CSA',
      population: 1314589,
      area_type: 'CSA',
    },
    {
      index: 2158,
      name: 'Harrisburg-Carlisle, PA MSA',
      population: 603493,
      area_type: 'MSA',
    },
    {
      index: 2197,
      name: 'Perry County, Pennsylvania',
      population: 46114,
      area_type: 'County',
    },
  ],
  2198: [
    {
      index: 318,
      name: 'Philadelphia-Reading-Camden, PA-NJ-DE-MD CSA',
      population: 7381187,
      area_type: 'CSA',
    },
    {
      index: 321,
      name: 'Philadelphia-Camden-Wilmington, PA-NJ-DE-MD MSA',
      population: 6241164,
      area_type: 'MSA',
    },
    {
      index: 2198,
      name: 'Philadelphia County, Pennsylvania',
      population: 1567258,
      area_type: 'County',
    },
  ],
  2199: [
    {
      index: 1593,
      name: 'New York-Newark, NY-NJ-CT-PA CSA',
      population: 23070149,
      area_type: 'CSA',
    },
    {
      index: 1594,
      name: 'New York-Newark-Jersey City, NY-NJ-PA MSA',
      population: 19617869,
      area_type: 'MSA',
    },
    {
      index: 2199,
      name: 'Pike County, Pennsylvania',
      population: 60558,
      area_type: 'County',
    },
  ],
  2200: [
    {
      index: 2200,
      name: 'Schuylkill County, Pennsylvania (Pottsville, PA μSA)',
      population: 143104,
      area_type: 'μSA',
    },
  ],
  2202: [
    {
      index: 2152,
      name: 'Bloomsburg-Berwick-Sunbury, PA CSA',
      population: 255546,
      area_type: 'CSA',
    },
    {
      index: 2202,
      name: 'Snyder County, Pennsylvania (Selinsgrove, PA μSA)',
      population: 39652,
      area_type: 'μSA',
    },
  ],
  2204: [
    {
      index: 2139,
      name: 'Johnstown-Somerset, PA CSA',
      population: 204151,
      area_type: 'CSA',
    },
    {
      index: 2204,
      name: 'Somerset County, Pennsylvania (Somerset, PA μSA)',
      population: 72710,
      area_type: 'μSA',
    },
  ],
  2206: [
    {
      index: 2152,
      name: 'Bloomsburg-Berwick-Sunbury, PA CSA',
      population: 255546,
      area_type: 'CSA',
    },
    {
      index: 2206,
      name: 'Union County, Pennsylvania (Lewisburg, PA μSA)',
      population: 42744,
      area_type: 'μSA',
    },
  ],
  2208: [
    {
      index: 2208,
      name: 'Venango County, Pennsylvania (Oil City, PA μSA)',
      population: 49777,
      area_type: 'μSA',
    },
  ],
  2210: [
    {
      index: 2210,
      name: 'Warren County, Pennsylvania (Warren, PA μSA)',
      population: 37808,
      area_type: 'μSA',
    },
  ],
  2212: [
    {
      index: 1957,
      name: 'Pittsburgh-New Castle-Weirton, PA-OH-WV CSA',
      population: 2631213,
      area_type: 'CSA',
    },
    {
      index: 2126,
      name: 'Pittsburgh, PA MSA',
      population: 2349172,
      area_type: 'MSA',
    },
    {
      index: 2212,
      name: 'Washington County, Pennsylvania',
      population: 210383,
      area_type: 'County',
    },
  ],
  2213: [
    {
      index: 1957,
      name: 'Pittsburgh-New Castle-Weirton, PA-OH-WV CSA',
      population: 2631213,
      area_type: 'CSA',
    },
    {
      index: 2126,
      name: 'Pittsburgh, PA MSA',
      population: 2349172,
      area_type: 'MSA',
    },
    {
      index: 2213,
      name: 'Westmoreland County, Pennsylvania',
      population: 352057,
      area_type: 'County',
    },
  ],
  2214: [
    {
      index: 2173,
      name: 'Scranton--Wilkes-Barre, PA MSA',
      population: 567998,
      area_type: 'MSA',
    },
    {
      index: 2214,
      name: 'Wyoming County, Pennsylvania',
      population: 26014,
      area_type: 'County',
    },
  ],
  2215: [
    {
      index: 2123,
      name: 'Harrisburg-York-Lebanon, PA CSA',
      population: 1314589,
      area_type: 'CSA',
    },
    {
      index: 2215,
      name: 'York County, Pennsylvania (York-Hanover, PA MSA)',
      population: 461058,
      area_type: 'MSA',
    },
  ],
  2217: [
    {
      index: 1160,
      name: 'Boston-Worcester-Providence, MA-RI-NH-CT CSA',
      population: 8434341,
      area_type: 'CSA',
    },
    {
      index: 1165,
      name: 'Providence-Warwick, RI-MA MSA',
      population: 1673802,
      area_type: 'MSA',
    },
    {
      index: 2217,
      name: 'Bristol County, Rhode Island',
      population: 50360,
      area_type: 'County',
    },
  ],
  2218: [
    {
      index: 1160,
      name: 'Boston-Worcester-Providence, MA-RI-NH-CT CSA',
      population: 8434341,
      area_type: 'CSA',
    },
    {
      index: 1165,
      name: 'Providence-Warwick, RI-MA MSA',
      population: 1673802,
      area_type: 'MSA',
    },
    {
      index: 2218,
      name: 'Kent County, Rhode Island',
      population: 171275,
      area_type: 'County',
    },
  ],
  2219: [
    {
      index: 1160,
      name: 'Boston-Worcester-Providence, MA-RI-NH-CT CSA',
      population: 8434341,
      area_type: 'CSA',
    },
    {
      index: 1165,
      name: 'Providence-Warwick, RI-MA MSA',
      population: 1673802,
      area_type: 'MSA',
    },
    {
      index: 2219,
      name: 'Newport County, Rhode Island',
      population: 84481,
      area_type: 'County',
    },
  ],
  2220: [
    {
      index: 1160,
      name: 'Boston-Worcester-Providence, MA-RI-NH-CT CSA',
      population: 8434341,
      area_type: 'CSA',
    },
    {
      index: 1165,
      name: 'Providence-Warwick, RI-MA MSA',
      population: 1673802,
      area_type: 'MSA',
    },
    {
      index: 2220,
      name: 'Providence County, Rhode Island',
      population: 657288,
      area_type: 'County',
    },
  ],
  2221: [
    {
      index: 1160,
      name: 'Boston-Worcester-Providence, MA-RI-NH-CT CSA',
      population: 8434341,
      area_type: 'CSA',
    },
    {
      index: 1165,
      name: 'Providence-Warwick, RI-MA MSA',
      population: 1673802,
      area_type: 'MSA',
    },
    {
      index: 2221,
      name: 'Washington County, Rhode Island',
      population: 130330,
      area_type: 'County',
    },
  ],
  2222: [
    {
      index: 436,
      name: 'Augusta-Richmond County, GA-SC MSA',
      population: 624083,
      area_type: 'MSA',
    },
    {
      index: 2222,
      name: 'Aiken County, South Carolina',
      population: 174150,
      area_type: 'County',
    },
  ],
  2225: [
    {
      index: 2223,
      name: 'Greenville-Spartanburg-Anderson, SC CSA',
      population: 1537109,
      area_type: 'CSA',
    },
    {
      index: 2224,
      name: 'Greenville-Anderson, SC MSA',
      population: 958958,
      area_type: 'MSA',
    },
    {
      index: 2225,
      name: 'Anderson County, South Carolina',
      population: 209581,
      area_type: 'County',
    },
  ],
  2227: [
    {
      index: 2226,
      name: 'Hilton Head Island-Bluffton, SC MSA',
      population: 228410,
      area_type: 'MSA',
    },
    {
      index: 2227,
      name: 'Beaufort County, South Carolina',
      population: 196371,
      area_type: 'County',
    },
  ],
  2229: [
    {
      index: 2228,
      name: 'Charleston-North Charleston, SC MSA',
      population: 830529,
      area_type: 'MSA',
    },
    {
      index: 2229,
      name: 'Berkeley County, South Carolina',
      population: 245117,
      area_type: 'County',
    },
  ],
  2232: [
    {
      index: 2230,
      name: 'Columbia-Orangeburg-Newberry, SC CSA',
      population: 969027,
      area_type: 'CSA',
    },
    {
      index: 2231,
      name: 'Columbia, SC MSA',
      population: 847686,
      area_type: 'MSA',
    },
    {
      index: 2232,
      name: 'Calhoun County, South Carolina',
      population: 14179,
      area_type: 'County',
    },
  ],
  2233: [
    {
      index: 2228,
      name: 'Charleston-North Charleston, SC MSA',
      population: 830529,
      area_type: 'MSA',
    },
    {
      index: 2233,
      name: 'Charleston County, South Carolina',
      population: 419279,
      area_type: 'County',
    },
  ],
  2234: [
    {
      index: 2223,
      name: 'Greenville-Spartanburg-Anderson, SC CSA',
      population: 1537109,
      area_type: 'CSA',
    },
    {
      index: 2234,
      name: 'Cherokee County, South Carolina (Gaffney, SC μSA)',
      population: 56121,
      area_type: 'μSA',
    },
  ],
  2236: [
    {
      index: 1753,
      name: 'Charlotte-Concord, NC-SC CSA',
      population: 2920892,
      area_type: 'CSA',
    },
    {
      index: 1754,
      name: 'Charlotte-Concord-Gastonia, NC-SC MSA',
      population: 2756069,
      area_type: 'MSA',
    },
    {
      index: 2236,
      name: 'Chester County, South Carolina',
      population: 31931,
      area_type: 'County',
    },
  ],
  2238: [
    {
      index: 2237,
      name: 'Sumter, SC MSA',
      population: 134925,
      area_type: 'MSA',
    },
    {
      index: 2238,
      name: 'Clarendon County, South Carolina',
      population: 30913,
      area_type: 'County',
    },
  ],
  2240: [
    {
      index: 2239,
      name: 'Florence, SC MSA',
      population: 199119,
      area_type: 'MSA',
    },
    {
      index: 2240,
      name: 'Darlington County, South Carolina',
      population: 62398,
      area_type: 'County',
    },
  ],
  2241: [
    {
      index: 2228,
      name: 'Charleston-North Charleston, SC MSA',
      population: 830529,
      area_type: 'MSA',
    },
    {
      index: 2241,
      name: 'Dorchester County, South Carolina',
      population: 166133,
      area_type: 'County',
    },
  ],
  2242: [
    {
      index: 436,
      name: 'Augusta-Richmond County, GA-SC MSA',
      population: 624083,
      area_type: 'MSA',
    },
    {
      index: 2242,
      name: 'Edgefield County, South Carolina',
      population: 26932,
      area_type: 'County',
    },
  ],
  2243: [
    {
      index: 2230,
      name: 'Columbia-Orangeburg-Newberry, SC CSA',
      population: 969027,
      area_type: 'CSA',
    },
    {
      index: 2231,
      name: 'Columbia, SC MSA',
      population: 847686,
      area_type: 'MSA',
    },
    {
      index: 2243,
      name: 'Fairfield County, South Carolina',
      population: 20455,
      area_type: 'County',
    },
  ],
  2244: [
    {
      index: 2239,
      name: 'Florence, SC MSA',
      population: 199119,
      area_type: 'MSA',
    },
    {
      index: 2244,
      name: 'Florence County, South Carolina',
      population: 136721,
      area_type: 'County',
    },
  ],
  2245: [
    {
      index: 1759,
      name: 'Myrtle Beach-Conway, SC-NC CSA',
      population: 600887,
      area_type: 'CSA',
    },
    {
      index: 2245,
      name: 'Georgetown County, South Carolina (Georgetown, SC μSA)',
      population: 64722,
      area_type: 'μSA',
    },
  ],
  2247: [
    {
      index: 2223,
      name: 'Greenville-Spartanburg-Anderson, SC CSA',
      population: 1537109,
      area_type: 'CSA',
    },
    {
      index: 2224,
      name: 'Greenville-Anderson, SC MSA',
      population: 958958,
      area_type: 'MSA',
    },
    {
      index: 2247,
      name: 'Greenville County, South Carolina',
      population: 547950,
      area_type: 'County',
    },
  ],
  2248: [
    {
      index: 2223,
      name: 'Greenville-Spartanburg-Anderson, SC CSA',
      population: 1537109,
      area_type: 'CSA',
    },
    {
      index: 2248,
      name: 'Greenwood County, South Carolina (Greenwood, SC μSA)',
      population: 69267,
      area_type: 'μSA',
    },
  ],
  2250: [
    {
      index: 1759,
      name: 'Myrtle Beach-Conway, SC-NC CSA',
      population: 600887,
      area_type: 'CSA',
    },
    {
      index: 1760,
      name: 'Myrtle Beach-Conway-North Myrtle Beach, SC-NC MSA',
      population: 536165,
      area_type: 'MSA',
    },
    {
      index: 2250,
      name: 'Horry County, South Carolina',
      population: 383101,
      area_type: 'County',
    },
  ],
  2251: [
    {
      index: 2226,
      name: 'Hilton Head Island-Bluffton, SC MSA',
      population: 228410,
      area_type: 'MSA',
    },
    {
      index: 2251,
      name: 'Jasper County, South Carolina',
      population: 32039,
      area_type: 'County',
    },
  ],
  2252: [
    {
      index: 2230,
      name: 'Columbia-Orangeburg-Newberry, SC CSA',
      population: 969027,
      area_type: 'CSA',
    },
    {
      index: 2231,
      name: 'Columbia, SC MSA',
      population: 847686,
      area_type: 'MSA',
    },
    {
      index: 2252,
      name: 'Kershaw County, South Carolina',
      population: 67751,
      area_type: 'County',
    },
  ],
  2253: [
    {
      index: 1753,
      name: 'Charlotte-Concord, NC-SC CSA',
      population: 2920892,
      area_type: 'CSA',
    },
    {
      index: 1754,
      name: 'Charlotte-Concord-Gastonia, NC-SC MSA',
      population: 2756069,
      area_type: 'MSA',
    },
    {
      index: 2253,
      name: 'Lancaster County, South Carolina',
      population: 104577,
      area_type: 'County',
    },
  ],
  2254: [
    {
      index: 2223,
      name: 'Greenville-Spartanburg-Anderson, SC CSA',
      population: 1537109,
      area_type: 'CSA',
    },
    {
      index: 2224,
      name: 'Greenville-Anderson, SC MSA',
      population: 958958,
      area_type: 'MSA',
    },
    {
      index: 2254,
      name: 'Laurens County, South Carolina',
      population: 67965,
      area_type: 'County',
    },
  ],
  2255: [
    {
      index: 2230,
      name: 'Columbia-Orangeburg-Newberry, SC CSA',
      population: 969027,
      area_type: 'CSA',
    },
    {
      index: 2231,
      name: 'Columbia, SC MSA',
      population: 847686,
      area_type: 'MSA',
    },
    {
      index: 2255,
      name: 'Lexington County, South Carolina',
      population: 304797,
      area_type: 'County',
    },
  ],
  2256: [
    {
      index: 2256,
      name: 'Marlboro County, South Carolina (Bennettsville, SC μSA)',
      population: 26039,
      area_type: 'μSA',
    },
  ],
  2258: [
    {
      index: 2230,
      name: 'Columbia-Orangeburg-Newberry, SC CSA',
      population: 969027,
      area_type: 'CSA',
    },
    {
      index: 2258,
      name: 'Newberry County, South Carolina (Newberry, SC μSA)',
      population: 38247,
      area_type: 'μSA',
    },
  ],
  2260: [
    {
      index: 2223,
      name: 'Greenville-Spartanburg-Anderson, SC CSA',
      population: 1537109,
      area_type: 'CSA',
    },
    {
      index: 2260,
      name: 'Oconee County, South Carolina (Seneca, SC μSA)',
      population: 80180,
      area_type: 'μSA',
    },
  ],
  2262: [
    {
      index: 2230,
      name: 'Columbia-Orangeburg-Newberry, SC CSA',
      population: 969027,
      area_type: 'CSA',
    },
    {
      index: 2262,
      name: 'Orangeburg County, South Carolina (Orangeburg, SC μSA)',
      population: 83094,
      area_type: 'μSA',
    },
  ],
  2264: [
    {
      index: 2223,
      name: 'Greenville-Spartanburg-Anderson, SC CSA',
      population: 1537109,
      area_type: 'CSA',
    },
    {
      index: 2224,
      name: 'Greenville-Anderson, SC MSA',
      population: 958958,
      area_type: 'MSA',
    },
    {
      index: 2264,
      name: 'Pickens County, South Carolina',
      population: 133462,
      area_type: 'County',
    },
  ],
  2265: [
    {
      index: 2230,
      name: 'Columbia-Orangeburg-Newberry, SC CSA',
      population: 969027,
      area_type: 'CSA',
    },
    {
      index: 2231,
      name: 'Columbia, SC MSA',
      population: 847686,
      area_type: 'MSA',
    },
    {
      index: 2265,
      name: 'Richland County, South Carolina',
      population: 421566,
      area_type: 'County',
    },
  ],
  2266: [
    {
      index: 2230,
      name: 'Columbia-Orangeburg-Newberry, SC CSA',
      population: 969027,
      area_type: 'CSA',
    },
    {
      index: 2231,
      name: 'Columbia, SC MSA',
      population: 847686,
      area_type: 'MSA',
    },
    {
      index: 2266,
      name: 'Saluda County, South Carolina',
      population: 18938,
      area_type: 'County',
    },
  ],
  2267: [
    {
      index: 2223,
      name: 'Greenville-Spartanburg-Anderson, SC CSA',
      population: 1537109,
      area_type: 'CSA',
    },
    {
      index: 2267,
      name: 'Spartanburg County, South Carolina (Spartanburg, SC MSA)',
      population: 345831,
      area_type: 'MSA',
    },
  ],
  2269: [
    {
      index: 2237,
      name: 'Sumter, SC MSA',
      population: 134925,
      area_type: 'MSA',
    },
    {
      index: 2269,
      name: 'Sumter County, South Carolina',
      population: 104012,
      area_type: 'County',
    },
  ],
  2270: [
    {
      index: 2223,
      name: 'Greenville-Spartanburg-Anderson, SC CSA',
      population: 1537109,
      area_type: 'CSA',
    },
    {
      index: 2270,
      name: 'Union County, South Carolina (Union, SC μSA)',
      population: 26752,
      area_type: 'μSA',
    },
  ],
  2272: [
    {
      index: 1753,
      name: 'Charlotte-Concord, NC-SC CSA',
      population: 2920892,
      area_type: 'CSA',
    },
    {
      index: 1754,
      name: 'Charlotte-Concord-Gastonia, NC-SC MSA',
      population: 2756069,
      area_type: 'MSA',
    },
    {
      index: 2272,
      name: 'York County, South Carolina',
      population: 294248,
      area_type: 'County',
    },
  ],
  2274: [
    {
      index: 2273,
      name: 'Huron, SD μSA',
      population: 21026,
      area_type: 'μSA',
    },
    {
      index: 2274,
      name: 'Beadle County, South Dakota',
      population: 19376,
      area_type: 'County',
    },
  ],
  2275: [
    {
      index: 2275,
      name: 'Brookings County, South Dakota (Brookings, SD μSA)',
      population: 35484,
      area_type: 'μSA',
    },
  ],
  2278: [
    {
      index: 2277,
      name: 'Aberdeen, SD μSA',
      population: 42037,
      area_type: 'μSA',
    },
    {
      index: 2278,
      name: 'Brown County, South Dakota',
      population: 37972,
      area_type: 'County',
    },
  ],
  2279: [
    {
      index: 2279,
      name: 'Clay County, South Dakota (Vermillion, SD μSA)',
      population: 15280,
      area_type: 'μSA',
    },
  ],
  2282: [
    {
      index: 2281,
      name: 'Watertown, SD μSA',
      population: 35073,
      area_type: 'μSA',
    },
    {
      index: 2282,
      name: 'Codington County, South Dakota',
      population: 28721,
      area_type: 'County',
    },
  ],
  2284: [
    {
      index: 2283,
      name: 'Mitchell, SD μSA',
      population: 23434,
      area_type: 'μSA',
    },
    {
      index: 2284,
      name: 'Davison County, South Dakota',
      population: 19973,
      area_type: 'County',
    },
  ],
  2285: [
    {
      index: 2277,
      name: 'Aberdeen, SD μSA',
      population: 42037,
      area_type: 'μSA',
    },
    {
      index: 2285,
      name: 'Edmunds County, South Dakota',
      population: 4065,
      area_type: 'County',
    },
  ],
  2286: [
    {
      index: 2281,
      name: 'Watertown, SD μSA',
      population: 35073,
      area_type: 'μSA',
    },
    {
      index: 2286,
      name: 'Hamlin County, South Dakota',
      population: 6352,
      area_type: 'County',
    },
  ],
  2287: [
    {
      index: 2283,
      name: 'Mitchell, SD μSA',
      population: 23434,
      area_type: 'μSA',
    },
    {
      index: 2287,
      name: 'Hanson County, South Dakota',
      population: 3461,
      area_type: 'County',
    },
  ],
  2289: [
    {
      index: 2288,
      name: 'Pierre, SD μSA',
      population: 20691,
      area_type: 'μSA',
    },
    {
      index: 2289,
      name: 'Hughes County, South Dakota',
      population: 17692,
      area_type: 'County',
    },
  ],
  2290: [
    {
      index: 2273,
      name: 'Huron, SD μSA',
      population: 21026,
      area_type: 'μSA',
    },
    {
      index: 2290,
      name: 'Jerauld County, South Dakota',
      population: 1650,
      area_type: 'County',
    },
  ],
  2292: [
    {
      index: 2291,
      name: 'Rapid City-Spearfish, SD CSA',
      population: 172373,
      area_type: 'CSA',
    },
    {
      index: 2292,
      name: 'Lawrence County, South Dakota (Spearfish, SD μSA)',
      population: 27214,
      area_type: 'μSA',
    },
  ],
  2295: [
    {
      index: 2294,
      name: 'Sioux Falls, SD MSA',
      population: 289592,
      area_type: 'MSA',
    },
    {
      index: 2295,
      name: 'Lincoln County, South Dakota',
      population: 70987,
      area_type: 'County',
    },
  ],
  2296: [
    {
      index: 2294,
      name: 'Sioux Falls, SD MSA',
      population: 289592,
      area_type: 'MSA',
    },
    {
      index: 2296,
      name: 'McCook County, South Dakota',
      population: 5778,
      area_type: 'County',
    },
  ],
  2298: [
    {
      index: 2291,
      name: 'Rapid City-Spearfish, SD CSA',
      population: 172373,
      area_type: 'CSA',
    },
    {
      index: 2297,
      name: 'Rapid City, SD MSA',
      population: 145159,
      area_type: 'MSA',
    },
    {
      index: 2298,
      name: 'Meade County, South Dakota',
      population: 30698,
      area_type: 'County',
    },
  ],
  2299: [
    {
      index: 2294,
      name: 'Sioux Falls, SD MSA',
      population: 289592,
      area_type: 'MSA',
    },
    {
      index: 2299,
      name: 'Minnehaha County, South Dakota',
      population: 203971,
      area_type: 'County',
    },
  ],
  2300: [
    {
      index: 2291,
      name: 'Rapid City-Spearfish, SD CSA',
      population: 172373,
      area_type: 'CSA',
    },
    {
      index: 2297,
      name: 'Rapid City, SD MSA',
      population: 145159,
      area_type: 'MSA',
    },
    {
      index: 2300,
      name: 'Pennington County, South Dakota',
      population: 114461,
      area_type: 'County',
    },
  ],
  2301: [
    {
      index: 2288,
      name: 'Pierre, SD μSA',
      population: 20691,
      area_type: 'μSA',
    },
    {
      index: 2301,
      name: 'Stanley County, South Dakota',
      population: 2999,
      area_type: 'County',
    },
  ],
  2302: [
    {
      index: 2294,
      name: 'Sioux Falls, SD MSA',
      population: 289592,
      area_type: 'MSA',
    },
    {
      index: 2302,
      name: 'Turner County, South Dakota',
      population: 8856,
      area_type: 'County',
    },
  ],
  2303: [
    {
      index: 901,
      name: 'Sioux City, IA-NE-SD MSA',
      population: 149240,
      area_type: 'MSA',
    },
    {
      index: 2303,
      name: 'Union County, South Dakota',
      population: 17063,
      area_type: 'County',
    },
  ],
  2304: [
    {
      index: 2304,
      name: 'Yankton County, South Dakota (Yankton, SD μSA)',
      population: 23373,
      area_type: 'μSA',
    },
  ],
  2308: [
    {
      index: 2306,
      name: 'Knoxville-Morristown-Sevierville, TN CSA',
      population: 1189808,
      area_type: 'CSA',
    },
    {
      index: 2307,
      name: 'Knoxville, TN MSA',
      population: 907968,
      area_type: 'MSA',
    },
    {
      index: 2308,
      name: 'Anderson County, Tennessee',
      population: 78913,
      area_type: 'County',
    },
  ],
  2310: [
    {
      index: 2309,
      name: 'Nashville-Davidson--Murfreesboro, TN CSA',
      population: 2180071,
      area_type: 'CSA',
    },
    {
      index: 2310,
      name: 'Bedford County, Tennessee (Shelbyville, TN μSA)',
      population: 51950,
      area_type: 'μSA',
    },
  ],
  2312: [
    {
      index: 2306,
      name: 'Knoxville-Morristown-Sevierville, TN CSA',
      population: 1189808,
      area_type: 'CSA',
    },
    {
      index: 2307,
      name: 'Knoxville, TN MSA',
      population: 907968,
      area_type: 'MSA',
    },
    {
      index: 2312,
      name: 'Blount County, Tennessee',
      population: 139958,
      area_type: 'County',
    },
  ],
  2314: [
    {
      index: 442,
      name: 'Chattanooga-Cleveland-Dalton, TN-GA CSA',
      population: 1018929,
      area_type: 'CSA',
    },
    {
      index: 2313,
      name: 'Cleveland, TN MSA',
      population: 128479,
      area_type: 'MSA',
    },
    {
      index: 2314,
      name: 'Bradley County, Tennessee',
      population: 110616,
      area_type: 'County',
    },
  ],
  2315: [
    {
      index: 2306,
      name: 'Knoxville-Morristown-Sevierville, TN CSA',
      population: 1189808,
      area_type: 'CSA',
    },
    {
      index: 2307,
      name: 'Knoxville, TN MSA',
      population: 907968,
      area_type: 'MSA',
    },
    {
      index: 2315,
      name: 'Campbell County, Tennessee',
      population: 39584,
      area_type: 'County',
    },
  ],
  2317: [
    {
      index: 2309,
      name: 'Nashville-Davidson--Murfreesboro, TN CSA',
      population: 2180071,
      area_type: 'CSA',
    },
    {
      index: 2316,
      name: 'Nashville-Davidson--Murfreesboro--Franklin, TN MSA',
      population: 2046828,
      area_type: 'MSA',
    },
    {
      index: 2317,
      name: 'Cannon County, Tennessee',
      population: 14788,
      area_type: 'County',
    },
  ],
  2320: [
    {
      index: 2318,
      name: 'Johnson City-Kingsport-Bristol, TN-VA CSA',
      population: 521528,
      area_type: 'CSA',
    },
    {
      index: 2319,
      name: 'Johnson City, TN MSA',
      population: 210256,
      area_type: 'MSA',
    },
    {
      index: 2320,
      name: 'Carter County, Tennessee',
      population: 56410,
      area_type: 'County',
    },
  ],
  2321: [
    {
      index: 2309,
      name: 'Nashville-Davidson--Murfreesboro, TN CSA',
      population: 2180071,
      area_type: 'CSA',
    },
    {
      index: 2316,
      name: 'Nashville-Davidson--Murfreesboro--Franklin, TN MSA',
      population: 2046828,
      area_type: 'MSA',
    },
    {
      index: 2321,
      name: 'Cheatham County, Tennessee',
      population: 41830,
      area_type: 'County',
    },
  ],
  2324: [
    {
      index: 2322,
      name: 'Jackson-Brownsville, TN CSA',
      population: 199129,
      area_type: 'CSA',
    },
    {
      index: 2323,
      name: 'Jackson, TN MSA',
      population: 181579,
      area_type: 'MSA',
    },
    {
      index: 2324,
      name: 'Chester County, Tennessee',
      population: 17609,
      area_type: 'County',
    },
  ],
  2325: [
    {
      index: 2306,
      name: 'Knoxville-Morristown-Sevierville, TN CSA',
      population: 1189808,
      area_type: 'CSA',
    },
    {
      index: 2325,
      name: 'Cocke County, Tennessee (Newport, TN μSA)',
      population: 36879,
      area_type: 'μSA',
    },
  ],
  2328: [
    {
      index: 2327,
      name: 'Tullahoma-Manchester, TN μSA',
      population: 110412,
      area_type: 'μSA',
    },
    {
      index: 2328,
      name: 'Coffee County, Tennessee',
      population: 59728,
      area_type: 'County',
    },
  ],
  2329: [
    {
      index: 2322,
      name: 'Jackson-Brownsville, TN CSA',
      population: 199129,
      area_type: 'CSA',
    },
    {
      index: 2323,
      name: 'Jackson, TN MSA',
      population: 181579,
      area_type: 'MSA',
    },
    {
      index: 2329,
      name: 'Crockett County, Tennessee',
      population: 13888,
      area_type: 'County',
    },
  ],
  2330: [
    {
      index: 2330,
      name: 'Cumberland County, Tennessee (Crossville, TN μSA)',
      population: 63522,
      area_type: 'μSA',
    },
  ],
  2332: [
    {
      index: 2309,
      name: 'Nashville-Davidson--Murfreesboro, TN CSA',
      population: 2180071,
      area_type: 'CSA',
    },
    {
      index: 2316,
      name: 'Nashville-Davidson--Murfreesboro--Franklin, TN MSA',
      population: 2046828,
      area_type: 'MSA',
    },
    {
      index: 2332,
      name: 'Davidson County, Tennessee',
      population: 708144,
      area_type: 'County',
    },
  ],
  2333: [
    {
      index: 2309,
      name: 'Nashville-Davidson--Murfreesboro, TN CSA',
      population: 2180071,
      area_type: 'CSA',
    },
    {
      index: 2316,
      name: 'Nashville-Davidson--Murfreesboro--Franklin, TN MSA',
      population: 2046828,
      area_type: 'MSA',
    },
    {
      index: 2333,
      name: 'Dickson County, Tennessee',
      population: 55761,
      area_type: 'County',
    },
  ],
  2334: [
    {
      index: 2334,
      name: 'Dyer County, Tennessee (Dyersburg, TN μSA)',
      population: 36410,
      area_type: 'μSA',
    },
  ],
  2336: [
    {
      index: 136,
      name: 'Memphis-Forrest City, TN-MS-AR CSA',
      population: 1354756,
      area_type: 'CSA',
    },
    {
      index: 137,
      name: 'Memphis, TN-MS-AR MSA',
      population: 1332305,
      area_type: 'MSA',
    },
    {
      index: 2336,
      name: 'Fayette County, Tennessee',
      population: 43630,
      area_type: 'County',
    },
  ],
  2337: [
    {
      index: 2327,
      name: 'Tullahoma-Manchester, TN μSA',
      population: 110412,
      area_type: 'μSA',
    },
    {
      index: 2337,
      name: 'Franklin County, Tennessee',
      population: 43942,
      area_type: 'County',
    },
  ],
  2338: [
    {
      index: 2322,
      name: 'Jackson-Brownsville, TN CSA',
      population: 199129,
      area_type: 'CSA',
    },
    {
      index: 2323,
      name: 'Jackson, TN MSA',
      population: 181579,
      area_type: 'MSA',
    },
    {
      index: 2338,
      name: 'Gibson County, Tennessee',
      population: 50837,
      area_type: 'County',
    },
  ],
  2340: [
    {
      index: 2306,
      name: 'Knoxville-Morristown-Sevierville, TN CSA',
      population: 1189808,
      area_type: 'CSA',
    },
    {
      index: 2339,
      name: 'Morristown, TN MSA',
      population: 146172,
      area_type: 'MSA',
    },
    {
      index: 2340,
      name: 'Grainger County, Tennessee',
      population: 24277,
      area_type: 'County',
    },
  ],
  2341: [
    {
      index: 2341,
      name: 'Greene County, Tennessee (Greeneville, TN μSA)',
      population: 71405,
      area_type: 'μSA',
    },
  ],
  2343: [
    {
      index: 2306,
      name: 'Knoxville-Morristown-Sevierville, TN CSA',
      population: 1189808,
      area_type: 'CSA',
    },
    {
      index: 2339,
      name: 'Morristown, TN MSA',
      population: 146172,
      area_type: 'MSA',
    },
    {
      index: 2343,
      name: 'Hamblen County, Tennessee',
      population: 65168,
      area_type: 'County',
    },
  ],
  2344: [
    {
      index: 442,
      name: 'Chattanooga-Cleveland-Dalton, TN-GA CSA',
      population: 1018929,
      area_type: 'CSA',
    },
    {
      index: 443,
      name: 'Chattanooga, TN-GA MSA',
      population: 574507,
      area_type: 'MSA',
    },
    {
      index: 2344,
      name: 'Hamilton County, Tennessee',
      population: 374682,
      area_type: 'County',
    },
  ],
  2346: [
    {
      index: 2318,
      name: 'Johnson City-Kingsport-Bristol, TN-VA CSA',
      population: 521528,
      area_type: 'CSA',
    },
    {
      index: 2345,
      name: 'Kingsport-Bristol, TN-VA MSA',
      population: 311272,
      area_type: 'MSA',
    },
    {
      index: 2346,
      name: 'Hawkins County, Tennessee',
      population: 58043,
      area_type: 'County',
    },
  ],
  2347: [
    {
      index: 2322,
      name: 'Jackson-Brownsville, TN CSA',
      population: 199129,
      area_type: 'CSA',
    },
    {
      index: 2347,
      name: 'Haywood County, Tennessee (Brownsville, TN μSA)',
      population: 17550,
      area_type: 'μSA',
    },
  ],
  2349: [
    {
      index: 2349,
      name: 'Henry County, Tennessee (Paris, TN μSA)',
      population: 32379,
      area_type: 'μSA',
    },
  ],
  2352: [
    {
      index: 2351,
      name: 'Cookeville, TN μSA',
      population: 117415,
      area_type: 'μSA',
    },
    {
      index: 2352,
      name: 'Jackson County, Tennessee',
      population: 11989,
      area_type: 'County',
    },
  ],
  2353: [
    {
      index: 2306,
      name: 'Knoxville-Morristown-Sevierville, TN CSA',
      population: 1189808,
      area_type: 'CSA',
    },
    {
      index: 2339,
      name: 'Morristown, TN MSA',
      population: 146172,
      area_type: 'MSA',
    },
    {
      index: 2353,
      name: 'Jefferson County, Tennessee',
      population: 56727,
      area_type: 'County',
    },
  ],
  2354: [
    {
      index: 2306,
      name: 'Knoxville-Morristown-Sevierville, TN CSA',
      population: 1189808,
      area_type: 'CSA',
    },
    {
      index: 2307,
      name: 'Knoxville, TN MSA',
      population: 907968,
      area_type: 'MSA',
    },
    {
      index: 2354,
      name: 'Knox County, Tennessee',
      population: 494574,
      area_type: 'County',
    },
  ],
  2355: [
    {
      index: 2309,
      name: 'Nashville-Davidson--Murfreesboro, TN CSA',
      population: 2180071,
      area_type: 'CSA',
    },
    {
      index: 2355,
      name: 'Lawrence County, Tennessee (Lawrenceburg, TN μSA)',
      population: 45415,
      area_type: 'μSA',
    },
  ],
  2357: [
    {
      index: 2306,
      name: 'Knoxville-Morristown-Sevierville, TN CSA',
      population: 1189808,
      area_type: 'CSA',
    },
    {
      index: 2307,
      name: 'Knoxville, TN MSA',
      population: 907968,
      area_type: 'MSA',
    },
    {
      index: 2357,
      name: 'Loudon County, Tennessee',
      population: 58181,
      area_type: 'County',
    },
  ],
  2358: [
    {
      index: 442,
      name: 'Chattanooga-Cleveland-Dalton, TN-GA CSA',
      population: 1018929,
      area_type: 'CSA',
    },
    {
      index: 2358,
      name: 'McMinn County, Tennessee (Athens, TN μSA)',
      population: 54719,
      area_type: 'μSA',
    },
  ],
  2360: [
    {
      index: 2309,
      name: 'Nashville-Davidson--Murfreesboro, TN CSA',
      population: 2180071,
      area_type: 'CSA',
    },
    {
      index: 2316,
      name: 'Nashville-Davidson--Murfreesboro--Franklin, TN MSA',
      population: 2046828,
      area_type: 'MSA',
    },
    {
      index: 2360,
      name: 'Macon County, Tennessee',
      population: 26229,
      area_type: 'County',
    },
  ],
  2361: [
    {
      index: 2322,
      name: 'Jackson-Brownsville, TN CSA',
      population: 199129,
      area_type: 'CSA',
    },
    {
      index: 2323,
      name: 'Jackson, TN MSA',
      population: 181579,
      area_type: 'MSA',
    },
    {
      index: 2361,
      name: 'Madison County, Tennessee',
      population: 99245,
      area_type: 'County',
    },
  ],
  2362: [
    {
      index: 442,
      name: 'Chattanooga-Cleveland-Dalton, TN-GA CSA',
      population: 1018929,
      area_type: 'CSA',
    },
    {
      index: 443,
      name: 'Chattanooga, TN-GA MSA',
      population: 574507,
      area_type: 'MSA',
    },
    {
      index: 2362,
      name: 'Marion County, Tennessee',
      population: 29094,
      area_type: 'County',
    },
  ],
  2363: [
    {
      index: 2309,
      name: 'Nashville-Davidson--Murfreesboro, TN CSA',
      population: 2180071,
      area_type: 'CSA',
    },
    {
      index: 2363,
      name: 'Marshall County, Tennessee (Lewisburg, TN μSA)',
      population: 35878,
      area_type: 'μSA',
    },
  ],
  2365: [
    {
      index: 2309,
      name: 'Nashville-Davidson--Murfreesboro, TN CSA',
      population: 2180071,
      area_type: 'CSA',
    },
    {
      index: 2316,
      name: 'Nashville-Davidson--Murfreesboro--Franklin, TN MSA',
      population: 2046828,
      area_type: 'MSA',
    },
    {
      index: 2365,
      name: 'Maury County, Tennessee',
      population: 108159,
      area_type: 'County',
    },
  ],
  2366: [
    {
      index: 992,
      name: 'Clarksville, TN-KY MSA',
      population: 336605,
      area_type: 'MSA',
    },
    {
      index: 2366,
      name: 'Montgomery County, Tennessee',
      population: 235201,
      area_type: 'County',
    },
  ],
  2367: [
    {
      index: 2327,
      name: 'Tullahoma-Manchester, TN μSA',
      population: 110412,
      area_type: 'μSA',
    },
    {
      index: 2367,
      name: 'Moore County, Tennessee',
      population: 6742,
      area_type: 'County',
    },
  ],
  2368: [
    {
      index: 2306,
      name: 'Knoxville-Morristown-Sevierville, TN CSA',
      population: 1189808,
      area_type: 'CSA',
    },
    {
      index: 2307,
      name: 'Knoxville, TN MSA',
      population: 907968,
      area_type: 'MSA',
    },
    {
      index: 2368,
      name: 'Morgan County, Tennessee',
      population: 21224,
      area_type: 'County',
    },
  ],
  2370: [
    {
      index: 2369,
      name: 'Martin-Union City, TN CSA',
      population: 63457,
      area_type: 'CSA',
    },
    {
      index: 2370,
      name: 'Obion County, Tennessee (Union City, TN μSA)',
      population: 30394,
      area_type: 'μSA',
    },
  ],
  2372: [
    {
      index: 2351,
      name: 'Cookeville, TN μSA',
      population: 117415,
      area_type: 'μSA',
    },
    {
      index: 2372,
      name: 'Overton County, Tennessee',
      population: 23044,
      area_type: 'County',
    },
  ],
  2373: [
    {
      index: 442,
      name: 'Chattanooga-Cleveland-Dalton, TN-GA CSA',
      population: 1018929,
      area_type: 'CSA',
    },
    {
      index: 2313,
      name: 'Cleveland, TN MSA',
      population: 128479,
      area_type: 'MSA',
    },
    {
      index: 2373,
      name: 'Polk County, Tennessee',
      population: 17863,
      area_type: 'County',
    },
  ],
  2374: [
    {
      index: 2351,
      name: 'Cookeville, TN μSA',
      population: 117415,
      area_type: 'μSA',
    },
    {
      index: 2374,
      name: 'Putnam County, Tennessee',
      population: 82382,
      area_type: 'County',
    },
  ],
  2375: [
    {
      index: 442,
      name: 'Chattanooga-Cleveland-Dalton, TN-GA CSA',
      population: 1018929,
      area_type: 'CSA',
    },
    {
      index: 2375,
      name: 'Rhea County, Tennessee (Dayton, TN μSA)',
      population: 33730,
      area_type: 'μSA',
    },
  ],
  2377: [
    {
      index: 2306,
      name: 'Knoxville-Morristown-Sevierville, TN CSA',
      population: 1189808,
      area_type: 'CSA',
    },
    {
      index: 2307,
      name: 'Knoxville, TN MSA',
      population: 907968,
      area_type: 'MSA',
    },
    {
      index: 2377,
      name: 'Roane County, Tennessee',
      population: 55082,
      area_type: 'County',
    },
  ],
  2378: [
    {
      index: 2309,
      name: 'Nashville-Davidson--Murfreesboro, TN CSA',
      population: 2180071,
      area_type: 'CSA',
    },
    {
      index: 2316,
      name: 'Nashville-Davidson--Murfreesboro--Franklin, TN MSA',
      population: 2046828,
      area_type: 'MSA',
    },
    {
      index: 2378,
      name: 'Robertson County, Tennessee',
      population: 75470,
      area_type: 'County',
    },
  ],
  2379: [
    {
      index: 2309,
      name: 'Nashville-Davidson--Murfreesboro, TN CSA',
      population: 2180071,
      area_type: 'CSA',
    },
    {
      index: 2316,
      name: 'Nashville-Davidson--Murfreesboro--Franklin, TN MSA',
      population: 2046828,
      area_type: 'MSA',
    },
    {
      index: 2379,
      name: 'Rutherford County, Tennessee',
      population: 360619,
      area_type: 'County',
    },
  ],
  2380: [
    {
      index: 442,
      name: 'Chattanooga-Cleveland-Dalton, TN-GA CSA',
      population: 1018929,
      area_type: 'CSA',
    },
    {
      index: 443,
      name: 'Chattanooga, TN-GA MSA',
      population: 574507,
      area_type: 'MSA',
    },
    {
      index: 2380,
      name: 'Sequatchie County, Tennessee',
      population: 16909,
      area_type: 'County',
    },
  ],
  2381: [
    {
      index: 2306,
      name: 'Knoxville-Morristown-Sevierville, TN CSA',
      population: 1189808,
      area_type: 'CSA',
    },
    {
      index: 2381,
      name: 'Sevier County, Tennessee (Sevierville, TN μSA)',
      population: 98789,
      area_type: 'μSA',
    },
  ],
  2383: [
    {
      index: 136,
      name: 'Memphis-Forrest City, TN-MS-AR CSA',
      population: 1354756,
      area_type: 'CSA',
    },
    {
      index: 137,
      name: 'Memphis, TN-MS-AR MSA',
      population: 1332305,
      area_type: 'MSA',
    },
    {
      index: 2383,
      name: 'Shelby County, Tennessee',
      population: 916371,
      area_type: 'County',
    },
  ],
  2384: [
    {
      index: 2309,
      name: 'Nashville-Davidson--Murfreesboro, TN CSA',
      population: 2180071,
      area_type: 'CSA',
    },
    {
      index: 2316,
      name: 'Nashville-Davidson--Murfreesboro--Franklin, TN MSA',
      population: 2046828,
      area_type: 'MSA',
    },
    {
      index: 2384,
      name: 'Smith County, Tennessee',
      population: 20489,
      area_type: 'County',
    },
  ],
  2385: [
    {
      index: 992,
      name: 'Clarksville, TN-KY MSA',
      population: 336605,
      area_type: 'MSA',
    },
    {
      index: 2385,
      name: 'Stewart County, Tennessee',
      population: 14035,
      area_type: 'County',
    },
  ],
  2386: [
    {
      index: 2318,
      name: 'Johnson City-Kingsport-Bristol, TN-VA CSA',
      population: 521528,
      area_type: 'CSA',
    },
    {
      index: 2345,
      name: 'Kingsport-Bristol, TN-VA MSA',
      population: 311272,
      area_type: 'MSA',
    },
    {
      index: 2386,
      name: 'Sullivan County, Tennessee',
      population: 160820,
      area_type: 'County',
    },
  ],
  2387: [
    {
      index: 2309,
      name: 'Nashville-Davidson--Murfreesboro, TN CSA',
      population: 2180071,
      area_type: 'CSA',
    },
    {
      index: 2316,
      name: 'Nashville-Davidson--Murfreesboro--Franklin, TN MSA',
      population: 2046828,
      area_type: 'MSA',
    },
    {
      index: 2387,
      name: 'Sumner County, Tennessee',
      population: 203858,
      area_type: 'County',
    },
  ],
  2388: [
    {
      index: 136,
      name: 'Memphis-Forrest City, TN-MS-AR CSA',
      population: 1354756,
      area_type: 'CSA',
    },
    {
      index: 137,
      name: 'Memphis, TN-MS-AR MSA',
      population: 1332305,
      area_type: 'MSA',
    },
    {
      index: 2388,
      name: 'Tipton County, Tennessee',
      population: 61656,
      area_type: 'County',
    },
  ],
  2389: [
    {
      index: 2309,
      name: 'Nashville-Davidson--Murfreesboro, TN CSA',
      population: 2180071,
      area_type: 'CSA',
    },
    {
      index: 2316,
      name: 'Nashville-Davidson--Murfreesboro--Franklin, TN MSA',
      population: 2046828,
      area_type: 'MSA',
    },
    {
      index: 2389,
      name: 'Trousdale County, Tennessee',
      population: 12111,
      area_type: 'County',
    },
  ],
  2390: [
    {
      index: 2318,
      name: 'Johnson City-Kingsport-Bristol, TN-VA CSA',
      population: 521528,
      area_type: 'CSA',
    },
    {
      index: 2319,
      name: 'Johnson City, TN MSA',
      population: 210256,
      area_type: 'MSA',
    },
    {
      index: 2390,
      name: 'Unicoi County, Tennessee',
      population: 17674,
      area_type: 'County',
    },
  ],
  2391: [
    {
      index: 2306,
      name: 'Knoxville-Morristown-Sevierville, TN CSA',
      population: 1189808,
      area_type: 'CSA',
    },
    {
      index: 2307,
      name: 'Knoxville, TN MSA',
      population: 907968,
      area_type: 'MSA',
    },
    {
      index: 2391,
      name: 'Union County, Tennessee',
      population: 20452,
      area_type: 'County',
    },
  ],
  2392: [
    {
      index: 2392,
      name: 'Warren County, Tennessee (McMinnville, TN μSA)',
      population: 42026,
      area_type: 'μSA',
    },
  ],
  2394: [
    {
      index: 2318,
      name: 'Johnson City-Kingsport-Bristol, TN-VA CSA',
      population: 521528,
      area_type: 'CSA',
    },
    {
      index: 2319,
      name: 'Johnson City, TN MSA',
      population: 210256,
      area_type: 'MSA',
    },
    {
      index: 2394,
      name: 'Washington County, Tennessee',
      population: 136172,
      area_type: 'County',
    },
  ],
  2395: [
    {
      index: 2369,
      name: 'Martin-Union City, TN CSA',
      population: 63457,
      area_type: 'CSA',
    },
    {
      index: 2395,
      name: 'Weakley County, Tennessee (Martin, TN μSA)',
      population: 33063,
      area_type: 'μSA',
    },
  ],
  2397: [
    {
      index: 2309,
      name: 'Nashville-Davidson--Murfreesboro, TN CSA',
      population: 2180071,
      area_type: 'CSA',
    },
    {
      index: 2316,
      name: 'Nashville-Davidson--Murfreesboro--Franklin, TN MSA',
      population: 2046828,
      area_type: 'MSA',
    },
    {
      index: 2397,
      name: 'Williamson County, Tennessee',
      population: 260815,
      area_type: 'County',
    },
  ],
  2398: [
    {
      index: 2309,
      name: 'Nashville-Davidson--Murfreesboro, TN CSA',
      population: 2180071,
      area_type: 'CSA',
    },
    {
      index: 2316,
      name: 'Nashville-Davidson--Murfreesboro--Franklin, TN MSA',
      population: 2046828,
      area_type: 'MSA',
    },
    {
      index: 2398,
      name: 'Wilson County, Tennessee',
      population: 158555,
      area_type: 'County',
    },
  ],
  2399: [
    {
      index: 2399,
      name: 'Anderson County, Texas (Palestine, TX μSA)',
      population: 58064,
      area_type: 'μSA',
    },
  ],
  2401: [
    {
      index: 2401,
      name: 'Andrews County, Texas (Andrews, TX μSA)',
      population: 18334,
      area_type: 'μSA',
    },
  ],
  2403: [
    {
      index: 2403,
      name: 'Angelina County, Texas (Lufkin, TX μSA)',
      population: 87101,
      area_type: 'μSA',
    },
  ],
  2406: [
    {
      index: 2405,
      name: 'Corpus Christi-Kingsville-Alice, TX CSA',
      population: 526006,
      area_type: 'CSA',
    },
    {
      index: 2406,
      name: 'Aransas County, Texas (Rockport, TX μSA)',
      population: 24944,
      area_type: 'μSA',
    },
  ],
  2409: [
    {
      index: 2408,
      name: 'Wichita Falls, TX MSA',
      population: 149299,
      area_type: 'MSA',
    },
    {
      index: 2409,
      name: 'Archer County, Texas',
      population: 8835,
      area_type: 'County',
    },
  ],
  2412: [
    {
      index: 2410,
      name: 'Amarillo-Pampa-Borger, TX CSA',
      population: 313204,
      area_type: 'CSA',
    },
    {
      index: 2411,
      name: 'Amarillo, TX MSA',
      population: 271171,
      area_type: 'MSA',
    },
    {
      index: 2412,
      name: 'Armstrong County, Texas',
      population: 1850,
      area_type: 'County',
    },
  ],
  2415: [
    {
      index: 2413,
      name: 'San Antonio-New Braunfels-Pearsall, TX CSA',
      population: 2673157,
      area_type: 'CSA',
    },
    {
      index: 2414,
      name: 'San Antonio-New Braunfels, TX MSA',
      population: 2655342,
      area_type: 'MSA',
    },
    {
      index: 2415,
      name: 'Atascosa County, Texas',
      population: 50864,
      area_type: 'County',
    },
  ],
  2418: [
    {
      index: 2416,
      name: 'Houston-The Woodlands, TX CSA',
      population: 7533096,
      area_type: 'CSA',
    },
    {
      index: 2417,
      name: 'Houston-The Woodlands-Sugar Land, TX MSA',
      population: 7340118,
      area_type: 'MSA',
    },
    {
      index: 2418,
      name: 'Austin County, Texas',
      population: 31097,
      area_type: 'County',
    },
  ],
  2419: [
    {
      index: 2413,
      name: 'San Antonio-New Braunfels-Pearsall, TX CSA',
      population: 2673157,
      area_type: 'CSA',
    },
    {
      index: 2414,
      name: 'San Antonio-New Braunfels, TX MSA',
      population: 2655342,
      area_type: 'MSA',
    },
    {
      index: 2419,
      name: 'Bandera County, Texas',
      population: 22115,
      area_type: 'County',
    },
  ],
  2421: [
    {
      index: 2420,
      name: 'Austin-Round Rock-Georgetown, TX MSA',
      population: 2421115,
      area_type: 'MSA',
    },
    {
      index: 2421,
      name: 'Bastrop County, Texas',
      population: 106188,
      area_type: 'County',
    },
  ],
  2422: [
    {
      index: 2422,
      name: 'Bee County, Texas (Beeville, TX μSA)',
      population: 30394,
      area_type: 'μSA',
    },
  ],
  2425: [
    {
      index: 2424,
      name: 'Killeen-Temple, TX MSA',
      population: 496228,
      area_type: 'MSA',
    },
    {
      index: 2425,
      name: 'Bell County, Texas',
      population: 388386,
      area_type: 'County',
    },
  ],
  2426: [
    {
      index: 2413,
      name: 'San Antonio-New Braunfels-Pearsall, TX CSA',
      population: 2673157,
      area_type: 'CSA',
    },
    {
      index: 2414,
      name: 'San Antonio-New Braunfels, TX MSA',
      population: 2655342,
      area_type: 'MSA',
    },
    {
      index: 2426,
      name: 'Bexar County, Texas',
      population: 2059530,
      area_type: 'County',
    },
  ],
  2427: [
    {
      index: 156,
      name: 'Texarkana, TX-AR MSA',
      population: 146408,
      area_type: 'MSA',
    },
    {
      index: 2427,
      name: 'Bowie County, Texas',
      population: 92035,
      area_type: 'County',
    },
  ],
  2428: [
    {
      index: 2416,
      name: 'Houston-The Woodlands, TX CSA',
      population: 7533096,
      area_type: 'CSA',
    },
    {
      index: 2417,
      name: 'Houston-The Woodlands-Sugar Land, TX MSA',
      population: 7340118,
      area_type: 'MSA',
    },
    {
      index: 2428,
      name: 'Brazoria County, Texas',
      population: 388181,
      area_type: 'County',
    },
  ],
  2430: [
    {
      index: 2429,
      name: 'College Station-Bryan, TX MSA',
      population: 277824,
      area_type: 'MSA',
    },
    {
      index: 2430,
      name: 'Brazos County, Texas',
      population: 242014,
      area_type: 'County',
    },
  ],
  2431: [
    {
      index: 2431,
      name: 'Brown County, Texas (Brownwood, TX μSA)',
      population: 38373,
      area_type: 'μSA',
    },
  ],
  2433: [
    {
      index: 2429,
      name: 'College Station-Bryan, TX MSA',
      population: 277824,
      area_type: 'MSA',
    },
    {
      index: 2433,
      name: 'Burleson County, Texas',
      population: 18657,
      area_type: 'County',
    },
  ],
  2434: [
    {
      index: 2420,
      name: 'Austin-Round Rock-Georgetown, TX MSA',
      population: 2421115,
      area_type: 'MSA',
    },
    {
      index: 2434,
      name: 'Caldwell County, Texas',
      population: 47848,
      area_type: 'County',
    },
  ],
  2436: [
    {
      index: 2435,
      name: 'Victoria-Port Lavaca, TX CSA',
      population: 117902,
      area_type: 'CSA',
    },
    {
      index: 2436,
      name: 'Calhoun County, Texas (Port Lavaca, TX μSA)',
      population: 19706,
      area_type: 'μSA',
    },
  ],
  2439: [
    {
      index: 2438,
      name: 'Abilene, TX MSA',
      population: 179308,
      area_type: 'MSA',
    },
    {
      index: 2439,
      name: 'Callahan County, Texas',
      population: 14210,
      area_type: 'County',
    },
  ],
  2441: [
    {
      index: 2440,
      name: 'Brownsville-Harlingen-Raymondville, TX CSA',
      population: 445351,
      area_type: 'CSA',
    },
    {
      index: 2441,
      name: 'Cameron County, Texas (Brownsville-Harlingen, TX MSA)',
      population: 425208,
      area_type: 'MSA',
    },
  ],
  2444: [
    {
      index: 2443,
      name: 'Mount Pleasant, TX μSA',
      population: 43924,
      area_type: 'μSA',
    },
    {
      index: 2444,
      name: 'Camp County, Texas',
      population: 12716,
      area_type: 'County',
    },
  ],
  2445: [
    {
      index: 2410,
      name: 'Amarillo-Pampa-Borger, TX CSA',
      population: 313204,
      area_type: 'CSA',
    },
    {
      index: 2411,
      name: 'Amarillo, TX MSA',
      population: 271171,
      area_type: 'MSA',
    },
    {
      index: 2445,
      name: 'Carson County, Texas',
      population: 5784,
      area_type: 'County',
    },
  ],
  2446: [
    {
      index: 2416,
      name: 'Houston-The Woodlands, TX CSA',
      population: 7533096,
      area_type: 'CSA',
    },
    {
      index: 2417,
      name: 'Houston-The Woodlands-Sugar Land, TX MSA',
      population: 7340118,
      area_type: 'MSA',
    },
    {
      index: 2446,
      name: 'Chambers County, Texas',
      population: 51288,
      area_type: 'County',
    },
  ],
  2448: [
    {
      index: 2447,
      name: 'Tyler-Jacksonville, TX CSA',
      population: 293567,
      area_type: 'CSA',
    },
    {
      index: 2448,
      name: 'Cherokee County, Texas (Jacksonville, TX μSA)',
      population: 51645,
      area_type: 'μSA',
    },
  ],
  2450: [
    {
      index: 2408,
      name: 'Wichita Falls, TX MSA',
      population: 149299,
      area_type: 'MSA',
    },
    {
      index: 2450,
      name: 'Clay County, Texas',
      population: 10486,
      area_type: 'County',
    },
  ],
  2452: [
    {
      index: 2016,
      name: 'Dallas-Fort Worth, TX-OK CSA',
      population: 8449932,
      area_type: 'CSA',
    },
    {
      index: 2451,
      name: 'Dallas-Fort Worth-Arlington, TX MSA',
      population: 7943685,
      area_type: 'MSA',
    },
    {
      index: 2452,
      name: 'Collin County, Texas',
      population: 1158696,
      area_type: 'County',
    },
  ],
  2453: [
    {
      index: 2413,
      name: 'San Antonio-New Braunfels-Pearsall, TX CSA',
      population: 2673157,
      area_type: 'CSA',
    },
    {
      index: 2414,
      name: 'San Antonio-New Braunfels, TX MSA',
      population: 2655342,
      area_type: 'MSA',
    },
    {
      index: 2453,
      name: 'Comal County, Texas',
      population: 184642,
      area_type: 'County',
    },
  ],
  2454: [
    {
      index: 2016,
      name: 'Dallas-Fort Worth, TX-OK CSA',
      population: 8449932,
      area_type: 'CSA',
    },
    {
      index: 2454,
      name: 'Cooke County, Texas (Gainesville, TX μSA)',
      population: 43050,
      area_type: 'μSA',
    },
  ],
  2456: [
    {
      index: 2424,
      name: 'Killeen-Temple, TX MSA',
      population: 496228,
      area_type: 'MSA',
    },
    {
      index: 2456,
      name: 'Coryell County, Texas',
      population: 85057,
      area_type: 'County',
    },
  ],
  2459: [
    {
      index: 2457,
      name: 'Lubbock-Plainview-Levelland, TX CSA',
      population: 381271,
      area_type: 'CSA',
    },
    {
      index: 2458,
      name: 'Lubbock, TX MSA',
      population: 328283,
      area_type: 'MSA',
    },
    {
      index: 2459,
      name: 'Crosby County, Texas',
      population: 4998,
      area_type: 'County',
    },
  ],
  2460: [
    {
      index: 2016,
      name: 'Dallas-Fort Worth, TX-OK CSA',
      population: 8449932,
      area_type: 'CSA',
    },
    {
      index: 2451,
      name: 'Dallas-Fort Worth-Arlington, TX MSA',
      population: 7943685,
      area_type: 'MSA',
    },
    {
      index: 2460,
      name: 'Dallas County, Texas',
      population: 2600840,
      area_type: 'County',
    },
  ],
  2461: [
    {
      index: 2461,
      name: 'Dawson County, Texas (Lamesa, TX μSA)',
      population: 12130,
      area_type: 'μSA',
    },
  ],
  2463: [
    {
      index: 2463,
      name: 'Deaf Smith County, Texas (Hereford, TX μSA)',
      population: 18377,
      area_type: 'μSA',
    },
  ],
  2465: [
    {
      index: 2016,
      name: 'Dallas-Fort Worth, TX-OK CSA',
      population: 8449932,
      area_type: 'CSA',
    },
    {
      index: 2451,
      name: 'Dallas-Fort Worth-Arlington, TX MSA',
      population: 7943685,
      area_type: 'MSA',
    },
    {
      index: 2465,
      name: 'Denton County, Texas',
      population: 977281,
      area_type: 'County',
    },
  ],
  2467: [
    {
      index: 2405,
      name: 'Corpus Christi-Kingsville-Alice, TX CSA',
      population: 526006,
      area_type: 'CSA',
    },
    {
      index: 2466,
      name: 'Alice, TX μSA',
      population: 48714,
      area_type: 'μSA',
    },
    {
      index: 2467,
      name: 'Duval County, Texas',
      population: 9888,
      area_type: 'County',
    },
  ],
  2469: [
    {
      index: 2468,
      name: 'Midland-Odessa, TX CSA',
      population: 338085,
      area_type: 'CSA',
    },
    {
      index: 2469,
      name: 'Ector County, Texas (Odessa, TX MSA)',
      population: 160869,
      area_type: 'MSA',
    },
  ],
  2471: [
    {
      index: 2016,
      name: 'Dallas-Fort Worth, TX-OK CSA',
      population: 8449932,
      area_type: 'CSA',
    },
    {
      index: 2451,
      name: 'Dallas-Fort Worth-Arlington, TX MSA',
      population: 7943685,
      area_type: 'MSA',
    },
    {
      index: 2471,
      name: 'Ellis County, Texas',
      population: 212182,
      area_type: 'County',
    },
  ],
  2473: [
    {
      index: 1629,
      name: 'El Paso-Las Cruces, TX-NM CSA',
      population: 1095532,
      area_type: 'CSA',
    },
    {
      index: 2472,
      name: 'El Paso, TX MSA',
      population: 872195,
      area_type: 'MSA',
    },
    {
      index: 2473,
      name: 'El Paso County, Texas',
      population: 868763,
      area_type: 'County',
    },
  ],
  2474: [
    {
      index: 2474,
      name: 'Erath County, Texas (Stephenville, TX μSA)',
      population: 43895,
      area_type: 'μSA',
    },
  ],
  2477: [
    {
      index: 2476,
      name: 'Waco, TX MSA',
      population: 283885,
      area_type: 'MSA',
    },
    {
      index: 2477,
      name: 'Falls County, Texas',
      population: 17049,
      area_type: 'County',
    },
  ],
  2478: [
    {
      index: 2016,
      name: 'Dallas-Fort Worth, TX-OK CSA',
      population: 8449932,
      area_type: 'CSA',
    },
    {
      index: 2478,
      name: 'Fannin County, Texas (Bonham, TX μSA)',
      population: 37125,
      area_type: 'μSA',
    },
  ],
  2480: [
    {
      index: 2416,
      name: 'Houston-The Woodlands, TX CSA',
      population: 7533096,
      area_type: 'CSA',
    },
    {
      index: 2417,
      name: 'Houston-The Woodlands-Sugar Land, TX MSA',
      population: 7340118,
      area_type: 'MSA',
    },
    {
      index: 2480,
      name: 'Fort Bend County, Texas',
      population: 889146,
      area_type: 'County',
    },
  ],
  2481: [
    {
      index: 2413,
      name: 'San Antonio-New Braunfels-Pearsall, TX CSA',
      population: 2673157,
      area_type: 'CSA',
    },
    {
      index: 2481,
      name: 'Frio County, Texas (Pearsall, TX μSA)',
      population: 17815,
      area_type: 'μSA',
    },
  ],
  2483: [
    {
      index: 2416,
      name: 'Houston-The Woodlands, TX CSA',
      population: 7533096,
      area_type: 'CSA',
    },
    {
      index: 2417,
      name: 'Houston-The Woodlands-Sugar Land, TX MSA',
      population: 7340118,
      area_type: 'MSA',
    },
    {
      index: 2483,
      name: 'Galveston County, Texas',
      population: 357117,
      area_type: 'County',
    },
  ],
  2485: [
    {
      index: 2484,
      name: 'Kerrville-Fredericksburg, TX CSA',
      population: 81218,
      area_type: 'CSA',
    },
    {
      index: 2485,
      name: 'Gillespie County, Texas (Fredericksburg, TX μSA)',
      population: 27477,
      area_type: 'μSA',
    },
  ],
  2488: [
    {
      index: 2435,
      name: 'Victoria-Port Lavaca, TX CSA',
      population: 117902,
      area_type: 'CSA',
    },
    {
      index: 2487,
      name: 'Victoria, TX MSA',
      population: 98196,
      area_type: 'MSA',
    },
    {
      index: 2488,
      name: 'Goliad County, Texas',
      population: 7131,
      area_type: 'County',
    },
  ],
  2490: [
    {
      index: 2410,
      name: 'Amarillo-Pampa-Borger, TX CSA',
      population: 313204,
      area_type: 'CSA',
    },
    {
      index: 2489,
      name: 'Pampa, TX μSA',
      population: 21818,
      area_type: 'μSA',
    },
    {
      index: 2490,
      name: 'Gray County, Texas',
      population: 21015,
      area_type: 'County',
    },
  ],
  2491: [
    {
      index: 2016,
      name: 'Dallas-Fort Worth, TX-OK CSA',
      population: 8449932,
      area_type: 'CSA',
    },
    {
      index: 2491,
      name: 'Grayson County, Texas (Sherman-Denison, TX MSA)',
      population: 143131,
      area_type: 'MSA',
    },
  ],
  2494: [
    {
      index: 2493,
      name: 'Longview, TX MSA',
      population: 291219,
      area_type: 'MSA',
    },
    {
      index: 2494,
      name: 'Gregg County, Texas',
      population: 125443,
      area_type: 'County',
    },
  ],
  2495: [
    {
      index: 2413,
      name: 'San Antonio-New Braunfels-Pearsall, TX CSA',
      population: 2673157,
      area_type: 'CSA',
    },
    {
      index: 2414,
      name: 'San Antonio-New Braunfels, TX MSA',
      population: 2655342,
      area_type: 'MSA',
    },
    {
      index: 2495,
      name: 'Guadalupe County, Texas',
      population: 182760,
      area_type: 'County',
    },
  ],
  2496: [
    {
      index: 2457,
      name: 'Lubbock-Plainview-Levelland, TX CSA',
      population: 381271,
      area_type: 'CSA',
    },
    {
      index: 2496,
      name: 'Hale County, Texas (Plainview, TX μSA)',
      population: 31827,
      area_type: 'μSA',
    },
  ],
  2499: [
    {
      index: 2498,
      name: 'Beaumont-Port Arthur, TX MSA',
      population: 393575,
      area_type: 'MSA',
    },
    {
      index: 2499,
      name: 'Hardin County, Texas',
      population: 57811,
      area_type: 'County',
    },
  ],
  2500: [
    {
      index: 2416,
      name: 'Houston-The Woodlands, TX CSA',
      population: 7533096,
      area_type: 'CSA',
    },
    {
      index: 2417,
      name: 'Houston-The Woodlands-Sugar Land, TX MSA',
      population: 7340118,
      area_type: 'MSA',
    },
    {
      index: 2500,
      name: 'Harris County, Texas',
      population: 4780913,
      area_type: 'County',
    },
  ],
  2501: [
    {
      index: 2493,
      name: 'Longview, TX MSA',
      population: 291219,
      area_type: 'MSA',
    },
    {
      index: 2501,
      name: 'Harrison County, Texas',
      population: 69955,
      area_type: 'County',
    },
  ],
  2502: [
    {
      index: 2420,
      name: 'Austin-Round Rock-Georgetown, TX MSA',
      population: 2421115,
      area_type: 'MSA',
    },
    {
      index: 2502,
      name: 'Hays County, Texas',
      population: 269225,
      area_type: 'County',
    },
  ],
  2503: [
    {
      index: 2016,
      name: 'Dallas-Fort Worth, TX-OK CSA',
      population: 8449932,
      area_type: 'CSA',
    },
    {
      index: 2503,
      name: 'Henderson County, Texas (Athens, TX μSA)',
      population: 84511,
      area_type: 'μSA',
    },
  ],
  2506: [
    {
      index: 2505,
      name: 'McAllen-Edinburg, TX CSA',
      population: 954095,
      area_type: 'CSA',
    },
    {
      index: 2506,
      name: 'Hidalgo County, Texas (McAllen-Edinburg-Mission, TX MSA)',
      population: 888367,
      area_type: 'MSA',
    },
  ],
  2508: [
    {
      index: 2457,
      name: 'Lubbock-Plainview-Levelland, TX CSA',
      population: 381271,
      area_type: 'CSA',
    },
    {
      index: 2508,
      name: 'Hockley County, Texas (Levelland, TX μSA)',
      population: 21161,
      area_type: 'μSA',
    },
  ],
  2510: [
    {
      index: 2016,
      name: 'Dallas-Fort Worth, TX-OK CSA',
      population: 8449932,
      area_type: 'CSA',
    },
    {
      index: 2510,
      name: 'Hood County, Texas (Granbury, TX μSA)',
      population: 66373,
      area_type: 'μSA',
    },
  ],
  2512: [
    {
      index: 2512,
      name: 'Hopkins County, Texas (Sulphur Springs, TX μSA)',
      population: 37804,
      area_type: 'μSA',
    },
  ],
  2514: [
    {
      index: 2514,
      name: 'Howard County, Texas (Big Spring, TX μSA)',
      population: 33672,
      area_type: 'μSA',
    },
  ],
  2516: [
    {
      index: 1629,
      name: 'El Paso-Las Cruces, TX-NM CSA',
      population: 1095532,
      area_type: 'CSA',
    },
    {
      index: 2472,
      name: 'El Paso, TX MSA',
      population: 872195,
      area_type: 'MSA',
    },
    {
      index: 2516,
      name: 'Hudspeth County, Texas',
      population: 3432,
      area_type: 'County',
    },
  ],
  2517: [
    {
      index: 2016,
      name: 'Dallas-Fort Worth, TX-OK CSA',
      population: 8449932,
      area_type: 'CSA',
    },
    {
      index: 2451,
      name: 'Dallas-Fort Worth-Arlington, TX MSA',
      population: 7943685,
      area_type: 'MSA',
    },
    {
      index: 2517,
      name: 'Hunt County, Texas',
      population: 108282,
      area_type: 'County',
    },
  ],
  2518: [
    {
      index: 2410,
      name: 'Amarillo-Pampa-Borger, TX CSA',
      population: 313204,
      area_type: 'CSA',
    },
    {
      index: 2518,
      name: 'Hutchinson County, Texas (Borger, TX μSA)',
      population: 20215,
      area_type: 'μSA',
    },
  ],
  2521: [
    {
      index: 2520,
      name: 'San Angelo, TX MSA',
      population: 121839,
      area_type: 'MSA',
    },
    {
      index: 2521,
      name: 'Irion County, Texas',
      population: 1530,
      area_type: 'County',
    },
  ],
  2522: [
    {
      index: 2498,
      name: 'Beaumont-Port Arthur, TX MSA',
      population: 393575,
      area_type: 'MSA',
    },
    {
      index: 2522,
      name: 'Jefferson County, Texas',
      population: 250830,
      area_type: 'County',
    },
  ],
  2523: [
    {
      index: 2405,
      name: 'Corpus Christi-Kingsville-Alice, TX CSA',
      population: 526006,
      area_type: 'CSA',
    },
    {
      index: 2466,
      name: 'Alice, TX μSA',
      population: 48714,
      area_type: 'μSA',
    },
    {
      index: 2523,
      name: 'Jim Wells County, Texas',
      population: 38826,
      area_type: 'County',
    },
  ],
  2524: [
    {
      index: 2016,
      name: 'Dallas-Fort Worth, TX-OK CSA',
      population: 8449932,
      area_type: 'CSA',
    },
    {
      index: 2451,
      name: 'Dallas-Fort Worth-Arlington, TX MSA',
      population: 7943685,
      area_type: 'MSA',
    },
    {
      index: 2524,
      name: 'Johnson County, Texas',
      population: 195506,
      area_type: 'County',
    },
  ],
  2525: [
    {
      index: 2438,
      name: 'Abilene, TX MSA',
      population: 179308,
      area_type: 'MSA',
    },
    {
      index: 2525,
      name: 'Jones County, Texas',
      population: 19935,
      area_type: 'County',
    },
  ],
  2526: [
    {
      index: 2016,
      name: 'Dallas-Fort Worth, TX-OK CSA',
      population: 8449932,
      area_type: 'CSA',
    },
    {
      index: 2451,
      name: 'Dallas-Fort Worth-Arlington, TX MSA',
      population: 7943685,
      area_type: 'MSA',
    },
    {
      index: 2526,
      name: 'Kaufman County, Texas',
      population: 172366,
      area_type: 'County',
    },
  ],
  2527: [
    {
      index: 2413,
      name: 'San Antonio-New Braunfels-Pearsall, TX CSA',
      population: 2673157,
      area_type: 'CSA',
    },
    {
      index: 2414,
      name: 'San Antonio-New Braunfels, TX MSA',
      population: 2655342,
      area_type: 'MSA',
    },
    {
      index: 2527,
      name: 'Kendall County, Texas',
      population: 48973,
      area_type: 'County',
    },
  ],
  2529: [
    {
      index: 2405,
      name: 'Corpus Christi-Kingsville-Alice, TX CSA',
      population: 526006,
      area_type: 'CSA',
    },
    {
      index: 2528,
      name: 'Kingsville, TX μSA',
      population: 30720,
      area_type: 'μSA',
    },
    {
      index: 2529,
      name: 'Kenedy County, Texas',
      population: 358,
      area_type: 'County',
    },
  ],
  2530: [
    {
      index: 2484,
      name: 'Kerrville-Fredericksburg, TX CSA',
      population: 81218,
      area_type: 'CSA',
    },
    {
      index: 2530,
      name: 'Kerr County, Texas (Kerrville, TX μSA)',
      population: 53741,
      area_type: 'μSA',
    },
  ],
  2532: [
    {
      index: 2405,
      name: 'Corpus Christi-Kingsville-Alice, TX CSA',
      population: 526006,
      area_type: 'CSA',
    },
    {
      index: 2528,
      name: 'Kingsville, TX μSA',
      population: 30720,
      area_type: 'μSA',
    },
    {
      index: 2532,
      name: 'Kleberg County, Texas',
      population: 30362,
      area_type: 'County',
    },
  ],
  2533: [
    {
      index: 2533,
      name: 'Lamar County, Texas (Paris, TX μSA)',
      population: 50484,
      area_type: 'μSA',
    },
  ],
  2535: [
    {
      index: 2424,
      name: 'Killeen-Temple, TX MSA',
      population: 496228,
      area_type: 'MSA',
    },
    {
      index: 2535,
      name: 'Lampasas County, Texas',
      population: 22785,
      area_type: 'County',
    },
  ],
  2536: [
    {
      index: 2416,
      name: 'Houston-The Woodlands, TX CSA',
      population: 7533096,
      area_type: 'CSA',
    },
    {
      index: 2417,
      name: 'Houston-The Woodlands-Sugar Land, TX MSA',
      population: 7340118,
      area_type: 'MSA',
    },
    {
      index: 2536,
      name: 'Liberty County, Texas',
      population: 101992,
      area_type: 'County',
    },
  ],
  2538: [
    {
      index: 2537,
      name: 'Pecos, TX μSA',
      population: 12956,
      area_type: 'μSA',
    },
    {
      index: 2538,
      name: 'Loving County, Texas',
      population: 51,
      area_type: 'County',
    },
  ],
  2539: [
    {
      index: 2457,
      name: 'Lubbock-Plainview-Levelland, TX CSA',
      population: 381271,
      area_type: 'CSA',
    },
    {
      index: 2458,
      name: 'Lubbock, TX MSA',
      population: 328283,
      area_type: 'MSA',
    },
    {
      index: 2539,
      name: 'Lubbock County, Texas',
      population: 317561,
      area_type: 'County',
    },
  ],
  2540: [
    {
      index: 2457,
      name: 'Lubbock-Plainview-Levelland, TX CSA',
      population: 381271,
      area_type: 'CSA',
    },
    {
      index: 2458,
      name: 'Lubbock, TX MSA',
      population: 328283,
      area_type: 'MSA',
    },
    {
      index: 2540,
      name: 'Lynn County, Texas',
      population: 5724,
      area_type: 'County',
    },
  ],
  2541: [
    {
      index: 2476,
      name: 'Waco, TX MSA',
      population: 283885,
      area_type: 'MSA',
    },
    {
      index: 2541,
      name: 'McLennan County, Texas',
      population: 266836,
      area_type: 'County',
    },
  ],
  2543: [
    {
      index: 2468,
      name: 'Midland-Odessa, TX CSA',
      population: 338085,
      area_type: 'CSA',
    },
    {
      index: 2542,
      name: 'Midland, TX MSA',
      population: 177216,
      area_type: 'MSA',
    },
    {
      index: 2543,
      name: 'Martin County, Texas',
      population: 5217,
      area_type: 'County',
    },
  ],
  2544: [
    {
      index: 2416,
      name: 'Houston-The Woodlands, TX CSA',
      population: 7533096,
      area_type: 'CSA',
    },
    {
      index: 2544,
      name: 'Matagorda County, Texas (Bay City, TX μSA)',
      population: 36125,
      area_type: 'μSA',
    },
  ],
  2546: [
    {
      index: 2546,
      name: 'Maverick County, Texas (Eagle Pass, TX μSA)',
      population: 57843,
      area_type: 'μSA',
    },
  ],
  2548: [
    {
      index: 2413,
      name: 'San Antonio-New Braunfels-Pearsall, TX CSA',
      population: 2673157,
      area_type: 'CSA',
    },
    {
      index: 2414,
      name: 'San Antonio-New Braunfels, TX MSA',
      population: 2655342,
      area_type: 'MSA',
    },
    {
      index: 2548,
      name: 'Medina County, Texas',
      population: 53723,
      area_type: 'County',
    },
  ],
  2549: [
    {
      index: 2468,
      name: 'Midland-Odessa, TX CSA',
      population: 338085,
      area_type: 'CSA',
    },
    {
      index: 2542,
      name: 'Midland, TX MSA',
      population: 177216,
      area_type: 'MSA',
    },
    {
      index: 2549,
      name: 'Midland County, Texas',
      population: 171999,
      area_type: 'County',
    },
  ],
  2550: [
    {
      index: 2416,
      name: 'Houston-The Woodlands, TX CSA',
      population: 7533096,
      area_type: 'CSA',
    },
    {
      index: 2417,
      name: 'Houston-The Woodlands-Sugar Land, TX MSA',
      population: 7340118,
      area_type: 'MSA',
    },
    {
      index: 2550,
      name: 'Montgomery County, Texas',
      population: 678490,
      area_type: 'County',
    },
  ],
  2551: [
    {
      index: 2551,
      name: 'Moore County, Texas (Dumas, TX μSA)',
      population: 20996,
      area_type: 'μSA',
    },
  ],
  2553: [
    {
      index: 2553,
      name: 'Nacogdoches County, Texas (Nacogdoches, TX μSA)',
      population: 64862,
      area_type: 'μSA',
    },
  ],
  2555: [
    {
      index: 2016,
      name: 'Dallas-Fort Worth, TX-OK CSA',
      population: 8449932,
      area_type: 'CSA',
    },
    {
      index: 2555,
      name: 'Navarro County, Texas (Corsicana, TX μSA)',
      population: 54636,
      area_type: 'μSA',
    },
  ],
  2557: [
    {
      index: 2557,
      name: 'Nolan County, Texas (Sweetwater, TX μSA)',
      population: 14473,
      area_type: 'μSA',
    },
  ],
  2560: [
    {
      index: 2405,
      name: 'Corpus Christi-Kingsville-Alice, TX CSA',
      population: 526006,
      area_type: 'CSA',
    },
    {
      index: 2559,
      name: 'Corpus Christi, TX MSA',
      population: 421628,
      area_type: 'MSA',
    },
    {
      index: 2560,
      name: 'Nueces County, Texas',
      population: 351674,
      area_type: 'County',
    },
  ],
  2561: [
    {
      index: 2410,
      name: 'Amarillo-Pampa-Borger, TX CSA',
      population: 313204,
      area_type: 'CSA',
    },
    {
      index: 2411,
      name: 'Amarillo, TX MSA',
      population: 271171,
      area_type: 'MSA',
    },
    {
      index: 2561,
      name: 'Oldham County, Texas',
      population: 1752,
      area_type: 'County',
    },
  ],
  2562: [
    {
      index: 2498,
      name: 'Beaumont-Port Arthur, TX MSA',
      population: 393575,
      area_type: 'MSA',
    },
    {
      index: 2562,
      name: 'Orange County, Texas',
      population: 84934,
      area_type: 'County',
    },
  ],
  2563: [
    {
      index: 2016,
      name: 'Dallas-Fort Worth, TX-OK CSA',
      population: 8449932,
      area_type: 'CSA',
    },
    {
      index: 2563,
      name: 'Palo Pinto County, Texas (Mineral Wells, TX μSA)',
      population: 29239,
      area_type: 'μSA',
    },
  ],
  2565: [
    {
      index: 2016,
      name: 'Dallas-Fort Worth, TX-OK CSA',
      population: 8449932,
      area_type: 'CSA',
    },
    {
      index: 2451,
      name: 'Dallas-Fort Worth-Arlington, TX MSA',
      population: 7943685,
      area_type: 'MSA',
    },
    {
      index: 2565,
      name: 'Parker County, Texas',
      population: 165834,
      area_type: 'County',
    },
  ],
  2566: [
    {
      index: 2410,
      name: 'Amarillo-Pampa-Borger, TX CSA',
      population: 313204,
      area_type: 'CSA',
    },
    {
      index: 2411,
      name: 'Amarillo, TX MSA',
      population: 271171,
      area_type: 'MSA',
    },
    {
      index: 2566,
      name: 'Potter County, Texas',
      population: 115645,
      area_type: 'County',
    },
  ],
  2567: [
    {
      index: 2410,
      name: 'Amarillo-Pampa-Borger, TX CSA',
      population: 313204,
      area_type: 'CSA',
    },
    {
      index: 2411,
      name: 'Amarillo, TX MSA',
      population: 271171,
      area_type: 'MSA',
    },
    {
      index: 2567,
      name: 'Randall County, Texas',
      population: 146140,
      area_type: 'County',
    },
  ],
  2568: [
    {
      index: 2537,
      name: 'Pecos, TX μSA',
      population: 12956,
      area_type: 'μSA',
    },
    {
      index: 2568,
      name: 'Reeves County, Texas',
      population: 12905,
      area_type: 'County',
    },
  ],
  2569: [
    {
      index: 2410,
      name: 'Amarillo-Pampa-Borger, TX CSA',
      population: 313204,
      area_type: 'CSA',
    },
    {
      index: 2489,
      name: 'Pampa, TX μSA',
      population: 21818,
      area_type: 'μSA',
    },
    {
      index: 2569,
      name: 'Roberts County, Texas',
      population: 803,
      area_type: 'County',
    },
  ],
  2570: [
    {
      index: 2429,
      name: 'College Station-Bryan, TX MSA',
      population: 277824,
      area_type: 'MSA',
    },
    {
      index: 2570,
      name: 'Robertson County, Texas',
      population: 17153,
      area_type: 'County',
    },
  ],
  2571: [
    {
      index: 2016,
      name: 'Dallas-Fort Worth, TX-OK CSA',
      population: 8449932,
      area_type: 'CSA',
    },
    {
      index: 2451,
      name: 'Dallas-Fort Worth-Arlington, TX MSA',
      population: 7943685,
      area_type: 'MSA',
    },
    {
      index: 2571,
      name: 'Rockwall County, Texas',
      population: 123208,
      area_type: 'County',
    },
  ],
  2572: [
    {
      index: 2493,
      name: 'Longview, TX MSA',
      population: 291219,
      area_type: 'MSA',
    },
    {
      index: 2572,
      name: 'Rusk County, Texas',
      population: 53333,
      area_type: 'County',
    },
  ],
  2573: [
    {
      index: 2405,
      name: 'Corpus Christi-Kingsville-Alice, TX CSA',
      population: 526006,
      area_type: 'CSA',
    },
    {
      index: 2559,
      name: 'Corpus Christi, TX MSA',
      population: 421628,
      area_type: 'MSA',
    },
    {
      index: 2573,
      name: 'San Patricio County, Texas',
      population: 69954,
      area_type: 'County',
    },
  ],
  2574: [
    {
      index: 2574,
      name: 'Scurry County, Texas (Snyder, TX μSA)',
      population: 16686,
      area_type: 'μSA',
    },
  ],
  2576: [
    {
      index: 2447,
      name: 'Tyler-Jacksonville, TX CSA',
      population: 293567,
      area_type: 'CSA',
    },
    {
      index: 2576,
      name: 'Smith County, Texas (Tyler, TX MSA)',
      population: 241922,
      area_type: 'MSA',
    },
  ],
  2578: [
    {
      index: 2505,
      name: 'McAllen-Edinburg, TX CSA',
      population: 954095,
      area_type: 'CSA',
    },
    {
      index: 2578,
      name: 'Starr County, Texas (Rio Grande City-Roma, TX μSA)',
      population: 65728,
      area_type: 'μSA',
    },
  ],
  2580: [
    {
      index: 2520,
      name: 'San Angelo, TX MSA',
      population: 121839,
      area_type: 'MSA',
    },
    {
      index: 2580,
      name: 'Sterling County, Texas',
      population: 1417,
      area_type: 'County',
    },
  ],
  2581: [
    {
      index: 2016,
      name: 'Dallas-Fort Worth, TX-OK CSA',
      population: 8449932,
      area_type: 'CSA',
    },
    {
      index: 2451,
      name: 'Dallas-Fort Worth-Arlington, TX MSA',
      population: 7943685,
      area_type: 'MSA',
    },
    {
      index: 2581,
      name: 'Tarrant County, Texas',
      population: 2154595,
      area_type: 'County',
    },
  ],
  2582: [
    {
      index: 2438,
      name: 'Abilene, TX MSA',
      population: 179308,
      area_type: 'MSA',
    },
    {
      index: 2582,
      name: 'Taylor County, Texas',
      population: 145163,
      area_type: 'County',
    },
  ],
  2583: [
    {
      index: 2443,
      name: 'Mount Pleasant, TX μSA',
      population: 43924,
      area_type: 'μSA',
    },
    {
      index: 2583,
      name: 'Titus County, Texas',
      population: 31208,
      area_type: 'County',
    },
  ],
  2584: [
    {
      index: 2520,
      name: 'San Angelo, TX MSA',
      population: 121839,
      area_type: 'MSA',
    },
    {
      index: 2584,
      name: 'Tom Green County, Texas',
      population: 118892,
      area_type: 'County',
    },
  ],
  2585: [
    {
      index: 2420,
      name: 'Austin-Round Rock-Georgetown, TX MSA',
      population: 2421115,
      area_type: 'MSA',
    },
    {
      index: 2585,
      name: 'Travis County, Texas',
      population: 1326436,
      area_type: 'County',
    },
  ],
  2586: [
    {
      index: 2493,
      name: 'Longview, TX MSA',
      population: 291219,
      area_type: 'MSA',
    },
    {
      index: 2586,
      name: 'Upshur County, Texas',
      population: 42488,
      area_type: 'County',
    },
  ],
  2587: [
    {
      index: 2587,
      name: 'Uvalde County, Texas (Uvalde, TX μSA)',
      population: 24940,
      area_type: 'μSA',
    },
  ],
  2589: [
    {
      index: 2589,
      name: 'Val Verde County, Texas (Del Rio, TX μSA)',
      population: 47606,
      area_type: 'μSA',
    },
  ],
  2591: [
    {
      index: 2435,
      name: 'Victoria-Port Lavaca, TX CSA',
      population: 117902,
      area_type: 'CSA',
    },
    {
      index: 2487,
      name: 'Victoria, TX MSA',
      population: 98196,
      area_type: 'MSA',
    },
    {
      index: 2591,
      name: 'Victoria County, Texas',
      population: 91065,
      area_type: 'County',
    },
  ],
  2592: [
    {
      index: 2416,
      name: 'Houston-The Woodlands, TX CSA',
      population: 7533096,
      area_type: 'CSA',
    },
    {
      index: 2592,
      name: 'Walker County, Texas (Huntsville, TX μSA)',
      population: 78870,
      area_type: 'μSA',
    },
  ],
  2594: [
    {
      index: 2416,
      name: 'Houston-The Woodlands, TX CSA',
      population: 7533096,
      area_type: 'CSA',
    },
    {
      index: 2417,
      name: 'Houston-The Woodlands-Sugar Land, TX MSA',
      population: 7340118,
      area_type: 'MSA',
    },
    {
      index: 2594,
      name: 'Waller County, Texas',
      population: 61894,
      area_type: 'County',
    },
  ],
  2595: [
    {
      index: 2416,
      name: 'Houston-The Woodlands, TX CSA',
      population: 7533096,
      area_type: 'CSA',
    },
    {
      index: 2595,
      name: 'Washington County, Texas (Brenham, TX μSA)',
      population: 36159,
      area_type: 'μSA',
    },
  ],
  2597: [
    {
      index: 2597,
      name: 'Webb County, Texas (Laredo, TX MSA)',
      population: 267780,
      area_type: 'MSA',
    },
  ],
  2599: [
    {
      index: 2416,
      name: 'Houston-The Woodlands, TX CSA',
      population: 7533096,
      area_type: 'CSA',
    },
    {
      index: 2599,
      name: 'Wharton County, Texas (El Campo, TX μSA)',
      population: 41824,
      area_type: 'μSA',
    },
  ],
  2601: [
    {
      index: 2408,
      name: 'Wichita Falls, TX MSA',
      population: 149299,
      area_type: 'MSA',
    },
    {
      index: 2601,
      name: 'Wichita County, Texas',
      population: 129978,
      area_type: 'County',
    },
  ],
  2602: [
    {
      index: 2602,
      name: 'Wilbarger County, Texas (Vernon, TX μSA)',
      population: 12491,
      area_type: 'μSA',
    },
  ],
  2604: [
    {
      index: 2440,
      name: 'Brownsville-Harlingen-Raymondville, TX CSA',
      population: 445351,
      area_type: 'CSA',
    },
    {
      index: 2604,
      name: 'Willacy County, Texas (Raymondville, TX μSA)',
      population: 20143,
      area_type: 'μSA',
    },
  ],
  2606: [
    {
      index: 2420,
      name: 'Austin-Round Rock-Georgetown, TX MSA',
      population: 2421115,
      area_type: 'MSA',
    },
    {
      index: 2606,
      name: 'Williamson County, Texas',
      population: 671418,
      area_type: 'County',
    },
  ],
  2607: [
    {
      index: 2413,
      name: 'San Antonio-New Braunfels-Pearsall, TX CSA',
      population: 2673157,
      area_type: 'CSA',
    },
    {
      index: 2414,
      name: 'San Antonio-New Braunfels, TX MSA',
      population: 2655342,
      area_type: 'MSA',
    },
    {
      index: 2607,
      name: 'Wilson County, Texas',
      population: 52735,
      area_type: 'County',
    },
  ],
  2608: [
    {
      index: 2016,
      name: 'Dallas-Fort Worth, TX-OK CSA',
      population: 8449932,
      area_type: 'CSA',
    },
    {
      index: 2451,
      name: 'Dallas-Fort Worth-Arlington, TX MSA',
      population: 7943685,
      area_type: 'MSA',
    },
    {
      index: 2608,
      name: 'Wise County, Texas',
      population: 74895,
      area_type: 'County',
    },
  ],
  2609: [
    {
      index: 2609,
      name: 'Zapata County, Texas (Zapata, TX μSA)',
      population: 13849,
      area_type: 'μSA',
    },
  ],
  2613: [
    {
      index: 2611,
      name: 'Salt Lake City-Provo-Orem, UT CSA',
      population: 2774686,
      area_type: 'CSA',
    },
    {
      index: 2612,
      name: 'Ogden-Clearfield, UT MSA',
      population: 713839,
      area_type: 'MSA',
    },
    {
      index: 2613,
      name: 'Box Elder County, Utah',
      population: 61498,
      area_type: 'County',
    },
  ],
  2614: [
    {
      index: 589,
      name: 'Logan, UT-ID MSA',
      population: 155362,
      area_type: 'MSA',
    },
    {
      index: 2614,
      name: 'Cache County, Utah',
      population: 140173,
      area_type: 'County',
    },
  ],
  2615: [
    {
      index: 2615,
      name: 'Carbon County, Utah (Price, UT μSA)',
      population: 20571,
      area_type: 'μSA',
    },
  ],
  2617: [
    {
      index: 2611,
      name: 'Salt Lake City-Provo-Orem, UT CSA',
      population: 2774686,
      area_type: 'CSA',
    },
    {
      index: 2612,
      name: 'Ogden-Clearfield, UT MSA',
      population: 713839,
      area_type: 'MSA',
    },
    {
      index: 2617,
      name: 'Davis County, Utah',
      population: 369948,
      area_type: 'County',
    },
  ],
  2618: [
    {
      index: 2618,
      name: 'Iron County, Utah (Cedar City, UT μSA)',
      population: 62429,
      area_type: 'μSA',
    },
  ],
  2621: [
    {
      index: 2611,
      name: 'Salt Lake City-Provo-Orem, UT CSA',
      population: 2774686,
      area_type: 'CSA',
    },
    {
      index: 2620,
      name: 'Provo-Orem, UT MSA',
      population: 715001,
      area_type: 'MSA',
    },
    {
      index: 2621,
      name: 'Juab County, Utah',
      population: 12567,
      area_type: 'County',
    },
  ],
  2622: [
    {
      index: 2611,
      name: 'Salt Lake City-Provo-Orem, UT CSA',
      population: 2774686,
      area_type: 'CSA',
    },
    {
      index: 2612,
      name: 'Ogden-Clearfield, UT MSA',
      population: 713839,
      area_type: 'MSA',
    },
    {
      index: 2622,
      name: 'Morgan County, Utah',
      population: 12832,
      area_type: 'County',
    },
  ],
  2624: [
    {
      index: 2611,
      name: 'Salt Lake City-Provo-Orem, UT CSA',
      population: 2774686,
      area_type: 'CSA',
    },
    {
      index: 2623,
      name: 'Salt Lake City, UT MSA',
      population: 1266191,
      area_type: 'MSA',
    },
    {
      index: 2624,
      name: 'Salt Lake County, Utah',
      population: 1186257,
      area_type: 'County',
    },
  ],
  2626: [
    {
      index: 2611,
      name: 'Salt Lake City-Provo-Orem, UT CSA',
      population: 2774686,
      area_type: 'CSA',
    },
    {
      index: 2625,
      name: 'Heber, UT μSA',
      population: 79655,
      area_type: 'μSA',
    },
    {
      index: 2626,
      name: 'Summit County, Utah',
      population: 43036,
      area_type: 'County',
    },
  ],
  2627: [
    {
      index: 2611,
      name: 'Salt Lake City-Provo-Orem, UT CSA',
      population: 2774686,
      area_type: 'CSA',
    },
    {
      index: 2623,
      name: 'Salt Lake City, UT MSA',
      population: 1266191,
      area_type: 'MSA',
    },
    {
      index: 2627,
      name: 'Tooele County, Utah',
      population: 79934,
      area_type: 'County',
    },
  ],
  2628: [
    {
      index: 2628,
      name: 'Uintah County, Utah (Vernal, UT μSA)',
      population: 37141,
      area_type: 'μSA',
    },
  ],
  2630: [
    {
      index: 2611,
      name: 'Salt Lake City-Provo-Orem, UT CSA',
      population: 2774686,
      area_type: 'CSA',
    },
    {
      index: 2620,
      name: 'Provo-Orem, UT MSA',
      population: 715001,
      area_type: 'MSA',
    },
    {
      index: 2630,
      name: 'Utah County, Utah',
      population: 702434,
      area_type: 'County',
    },
  ],
  2631: [
    {
      index: 2611,
      name: 'Salt Lake City-Provo-Orem, UT CSA',
      population: 2774686,
      area_type: 'CSA',
    },
    {
      index: 2625,
      name: 'Heber, UT μSA',
      population: 79655,
      area_type: 'μSA',
    },
    {
      index: 2631,
      name: 'Wasatch County, Utah',
      population: 36619,
      area_type: 'County',
    },
  ],
  2632: [
    {
      index: 2632,
      name: 'Washington County, Utah (St. George, UT MSA)',
      population: 197680,
      area_type: 'MSA',
    },
  ],
  2634: [
    {
      index: 2611,
      name: 'Salt Lake City-Provo-Orem, UT CSA',
      population: 2774686,
      area_type: 'CSA',
    },
    {
      index: 2612,
      name: 'Ogden-Clearfield, UT MSA',
      population: 713839,
      area_type: 'MSA',
    },
    {
      index: 2634,
      name: 'Weber County, Utah',
      population: 269561,
      area_type: 'County',
    },
  ],
  2635: [
    {
      index: 2635,
      name: 'Bennington County, Vermont (Bennington, VT μSA)',
      population: 37392,
      area_type: 'μSA',
    },
  ],
  2639: [
    {
      index: 2637,
      name: 'Burlington-South Burlington-Barre, VT CSA',
      population: 287569,
      area_type: 'CSA',
    },
    {
      index: 2638,
      name: 'Burlington-South Burlington, VT MSA',
      population: 227521,
      area_type: 'MSA',
    },
    {
      index: 2639,
      name: 'Chittenden County, Vermont',
      population: 169301,
      area_type: 'County',
    },
  ],
  2640: [
    {
      index: 2637,
      name: 'Burlington-South Burlington-Barre, VT CSA',
      population: 287569,
      area_type: 'CSA',
    },
    {
      index: 2638,
      name: 'Burlington-South Burlington, VT MSA',
      population: 227521,
      area_type: 'MSA',
    },
    {
      index: 2640,
      name: 'Franklin County, Vermont',
      population: 50731,
      area_type: 'County',
    },
  ],
  2641: [
    {
      index: 2637,
      name: 'Burlington-South Burlington-Barre, VT CSA',
      population: 287569,
      area_type: 'CSA',
    },
    {
      index: 2638,
      name: 'Burlington-South Burlington, VT MSA',
      population: 227521,
      area_type: 'MSA',
    },
    {
      index: 2641,
      name: 'Grand Isle County, Vermont',
      population: 7489,
      area_type: 'County',
    },
  ],
  2642: [
    {
      index: 1582,
      name: 'Lebanon, NH-VT μSA',
      population: 223072,
      area_type: 'μSA',
    },
    {
      index: 2642,
      name: 'Orange County, Vermont',
      population: 29846,
      area_type: 'County',
    },
  ],
  2643: [
    {
      index: 2643,
      name: 'Rutland County, Vermont (Rutland, VT μSA)',
      population: 60366,
      area_type: 'μSA',
    },
  ],
  2645: [
    {
      index: 2637,
      name: 'Burlington-South Burlington-Barre, VT CSA',
      population: 287569,
      area_type: 'CSA',
    },
    {
      index: 2645,
      name: 'Washington County, Vermont (Barre, VT μSA)',
      population: 60048,
      area_type: 'μSA',
    },
  ],
  2647: [
    {
      index: 1582,
      name: 'Lebanon, NH-VT μSA',
      population: 223072,
      area_type: 'μSA',
    },
    {
      index: 2647,
      name: 'Windsor County, Vermont',
      population: 58142,
      area_type: 'County',
    },
  ],
  2649: [
    {
      index: 2648,
      name: 'Charlottesville, VA MSA',
      population: 223825,
      area_type: 'MSA',
    },
    {
      index: 2649,
      name: 'Albemarle County, Virginia',
      population: 114534,
      area_type: 'County',
    },
  ],
  2651: [
    {
      index: 2650,
      name: 'Richmond, VA MSA',
      population: 1339182,
      area_type: 'MSA',
    },
    {
      index: 2651,
      name: 'Amelia County, Virginia',
      population: 13455,
      area_type: 'County',
    },
  ],
  2653: [
    {
      index: 2652,
      name: 'Lynchburg, VA MSA',
      population: 263613,
      area_type: 'MSA',
    },
    {
      index: 2653,
      name: 'Amherst County, Virginia',
      population: 31589,
      area_type: 'County',
    },
  ],
  2654: [
    {
      index: 2652,
      name: 'Lynchburg, VA MSA',
      population: 263613,
      area_type: 'MSA',
    },
    {
      index: 2654,
      name: 'Appomattox County, Virginia',
      population: 16748,
      area_type: 'County',
    },
  ],
  2655: [
    {
      index: 326,
      name: 'Washington-Baltimore-Arlington, DC-MD-VA-WV-PA CSA',
      population: 9968104,
      area_type: 'CSA',
    },
    {
      index: 327,
      name: 'Washington-Arlington-Alexandria, DC-VA-MD-WV MSA',
      population: 6373756,
      area_type: 'MSA',
    },
    {
      index: 2655,
      name: 'Arlington County, Virginia',
      population: 234000,
      area_type: 'County',
    },
  ],
  2658: [
    {
      index: 2656,
      name: 'Harrisonburg-Staunton, VA CSA',
      population: 263331,
      area_type: 'CSA',
    },
    {
      index: 2657,
      name: 'Staunton, VA MSA',
      population: 126776,
      area_type: 'MSA',
    },
    {
      index: 2658,
      name: 'Augusta County, Virginia',
      population: 78064,
      area_type: 'County',
    },
  ],
  2659: [
    {
      index: 2652,
      name: 'Lynchburg, VA MSA',
      population: 263613,
      area_type: 'MSA',
    },
    {
      index: 2659,
      name: 'Bedford County, Virginia',
      population: 80848,
      area_type: 'County',
    },
  ],
  2661: [
    {
      index: 2660,
      name: 'Bluefield, WV-VA μSA',
      population: 104669,
      area_type: 'μSA',
    },
    {
      index: 2661,
      name: 'Bland County, Virginia',
      population: 6148,
      area_type: 'County',
    },
  ],
  2663: [
    {
      index: 2662,
      name: 'Roanoke, VA MSA',
      population: 314340,
      area_type: 'MSA',
    },
    {
      index: 2663,
      name: 'Botetourt County, Virginia',
      population: 34135,
      area_type: 'County',
    },
  ],
  2664: [
    {
      index: 2652,
      name: 'Lynchburg, VA MSA',
      population: 263613,
      area_type: 'MSA',
    },
    {
      index: 2664,
      name: 'Campbell County, Virginia',
      population: 55141,
      area_type: 'County',
    },
  ],
  2665: [
    {
      index: 2650,
      name: 'Richmond, VA MSA',
      population: 1339182,
      area_type: 'MSA',
    },
    {
      index: 2665,
      name: 'Charles City County, Virginia',
      population: 6605,
      area_type: 'County',
    },
  ],
  2666: [
    {
      index: 2650,
      name: 'Richmond, VA MSA',
      population: 1339182,
      area_type: 'MSA',
    },
    {
      index: 2666,
      name: 'Chesterfield County, Virginia',
      population: 378408,
      area_type: 'County',
    },
  ],
  2667: [
    {
      index: 326,
      name: 'Washington-Baltimore-Arlington, DC-MD-VA-WV-PA CSA',
      population: 9968104,
      area_type: 'CSA',
    },
    {
      index: 327,
      name: 'Washington-Arlington-Alexandria, DC-VA-MD-WV MSA',
      population: 6373756,
      area_type: 'MSA',
    },
    {
      index: 2667,
      name: 'Clarke County, Virginia',
      population: 15266,
      area_type: 'County',
    },
  ],
  2668: [
    {
      index: 2662,
      name: 'Roanoke, VA MSA',
      population: 314340,
      area_type: 'MSA',
    },
    {
      index: 2668,
      name: 'Craig County, Virginia',
      population: 4847,
      area_type: 'County',
    },
  ],
  2669: [
    {
      index: 326,
      name: 'Washington-Baltimore-Arlington, DC-MD-VA-WV-PA CSA',
      population: 9968104,
      area_type: 'CSA',
    },
    {
      index: 327,
      name: 'Washington-Arlington-Alexandria, DC-VA-MD-WV MSA',
      population: 6373756,
      area_type: 'MSA',
    },
    {
      index: 2669,
      name: 'Culpeper County, Virginia',
      population: 54381,
      area_type: 'County',
    },
  ],
  2670: [
    {
      index: 2650,
      name: 'Richmond, VA MSA',
      population: 1339182,
      area_type: 'MSA',
    },
    {
      index: 2670,
      name: 'Dinwiddie County, Virginia',
      population: 28161,
      area_type: 'County',
    },
  ],
  2671: [
    {
      index: 326,
      name: 'Washington-Baltimore-Arlington, DC-MD-VA-WV-PA CSA',
      population: 9968104,
      area_type: 'CSA',
    },
    {
      index: 327,
      name: 'Washington-Arlington-Alexandria, DC-VA-MD-WV MSA',
      population: 6373756,
      area_type: 'MSA',
    },
    {
      index: 2671,
      name: 'Fairfax County, Virginia',
      population: 1138331,
      area_type: 'County',
    },
  ],
  2672: [
    {
      index: 326,
      name: 'Washington-Baltimore-Arlington, DC-MD-VA-WV-PA CSA',
      population: 9968104,
      area_type: 'CSA',
    },
    {
      index: 327,
      name: 'Washington-Arlington-Alexandria, DC-VA-MD-WV MSA',
      population: 6373756,
      area_type: 'MSA',
    },
    {
      index: 2672,
      name: 'Fauquier County, Virginia',
      population: 74664,
      area_type: 'County',
    },
  ],
  2673: [
    {
      index: 2648,
      name: 'Charlottesville, VA MSA',
      population: 223825,
      area_type: 'MSA',
    },
    {
      index: 2673,
      name: 'Fluvanna County, Virginia',
      population: 28159,
      area_type: 'County',
    },
  ],
  2674: [
    {
      index: 2662,
      name: 'Roanoke, VA MSA',
      population: 314340,
      area_type: 'MSA',
    },
    {
      index: 2674,
      name: 'Franklin County, Virginia',
      population: 55074,
      area_type: 'County',
    },
  ],
  2676: [
    {
      index: 326,
      name: 'Washington-Baltimore-Arlington, DC-MD-VA-WV-PA CSA',
      population: 9968104,
      area_type: 'CSA',
    },
    {
      index: 2675,
      name: 'Winchester, VA-WV MSA',
      population: 146455,
      area_type: 'MSA',
    },
    {
      index: 2676,
      name: 'Frederick County, Virginia',
      population: 95051,
      area_type: 'County',
    },
  ],
  2678: [
    {
      index: 2677,
      name: 'Blacksburg-Christiansburg, VA MSA',
      population: 165812,
      area_type: 'MSA',
    },
    {
      index: 2678,
      name: 'Giles County, Virginia',
      population: 16453,
      area_type: 'County',
    },
  ],
  2679: [
    {
      index: 1768,
      name: 'Virginia Beach-Norfolk, VA-NC CSA',
      population: 1898944,
      area_type: 'CSA',
    },
    {
      index: 1769,
      name: 'Virginia Beach-Norfolk-Newport News, VA-NC MSA',
      population: 1806840,
      area_type: 'MSA',
    },
    {
      index: 2679,
      name: 'Gloucester County, Virginia',
      population: 39493,
      area_type: 'County',
    },
  ],
  2680: [
    {
      index: 2650,
      name: 'Richmond, VA MSA',
      population: 1339182,
      area_type: 'MSA',
    },
    {
      index: 2680,
      name: 'Goochland County, Virginia',
      population: 26109,
      area_type: 'County',
    },
  ],
  2681: [
    {
      index: 2648,
      name: 'Charlottesville, VA MSA',
      population: 223825,
      area_type: 'MSA',
    },
    {
      index: 2681,
      name: 'Greene County, Virginia',
      population: 21107,
      area_type: 'County',
    },
  ],
  2682: [
    {
      index: 2650,
      name: 'Richmond, VA MSA',
      population: 1339182,
      area_type: 'MSA',
    },
    {
      index: 2682,
      name: 'Hanover County, Virginia',
      population: 112938,
      area_type: 'County',
    },
  ],
  2683: [
    {
      index: 2650,
      name: 'Richmond, VA MSA',
      population: 1339182,
      area_type: 'MSA',
    },
    {
      index: 2683,
      name: 'Henrico County, Virginia',
      population: 333962,
      area_type: 'County',
    },
  ],
  2685: [
    {
      index: 2684,
      name: 'Martinsville, VA μSA',
      population: 63631,
      area_type: 'μSA',
    },
    {
      index: 2685,
      name: 'Henry County, Virginia',
      population: 49906,
      area_type: 'County',
    },
  ],
  2686: [
    {
      index: 1768,
      name: 'Virginia Beach-Norfolk, VA-NC CSA',
      population: 1898944,
      area_type: 'CSA',
    },
    {
      index: 1769,
      name: 'Virginia Beach-Norfolk-Newport News, VA-NC MSA',
      population: 1806840,
      area_type: 'MSA',
    },
    {
      index: 2686,
      name: 'Isle of Wight County, Virginia',
      population: 40151,
      area_type: 'County',
    },
  ],
  2687: [
    {
      index: 1768,
      name: 'Virginia Beach-Norfolk, VA-NC CSA',
      population: 1898944,
      area_type: 'CSA',
    },
    {
      index: 1769,
      name: 'Virginia Beach-Norfolk-Newport News, VA-NC MSA',
      population: 1806840,
      area_type: 'MSA',
    },
    {
      index: 2687,
      name: 'James City County, Virginia',
      population: 81199,
      area_type: 'County',
    },
  ],
  2688: [
    {
      index: 2650,
      name: 'Richmond, VA MSA',
      population: 1339182,
      area_type: 'MSA',
    },
    {
      index: 2688,
      name: 'King and Queen County, Virginia',
      population: 6718,
      area_type: 'County',
    },
  ],
  2689: [
    {
      index: 2650,
      name: 'Richmond, VA MSA',
      population: 1339182,
      area_type: 'MSA',
    },
    {
      index: 2689,
      name: 'King William County, Virginia',
      population: 18492,
      area_type: 'County',
    },
  ],
  2690: [
    {
      index: 326,
      name: 'Washington-Baltimore-Arlington, DC-MD-VA-WV-PA CSA',
      population: 9968104,
      area_type: 'CSA',
    },
    {
      index: 327,
      name: 'Washington-Arlington-Alexandria, DC-VA-MD-WV MSA',
      population: 6373756,
      area_type: 'MSA',
    },
    {
      index: 2690,
      name: 'Loudoun County, Virginia',
      population: 432085,
      area_type: 'County',
    },
  ],
  2691: [
    {
      index: 326,
      name: 'Washington-Baltimore-Arlington, DC-MD-VA-WV-PA CSA',
      population: 9968104,
      area_type: 'CSA',
    },
    {
      index: 327,
      name: 'Washington-Arlington-Alexandria, DC-VA-MD-WV MSA',
      population: 6373756,
      area_type: 'MSA',
    },
    {
      index: 2691,
      name: 'Madison County, Virginia',
      population: 14000,
      area_type: 'County',
    },
  ],
  2692: [
    {
      index: 1768,
      name: 'Virginia Beach-Norfolk, VA-NC CSA',
      population: 1898944,
      area_type: 'CSA',
    },
    {
      index: 1769,
      name: 'Virginia Beach-Norfolk-Newport News, VA-NC MSA',
      population: 1806840,
      area_type: 'MSA',
    },
    {
      index: 2692,
      name: 'Mathews County, Virginia',
      population: 8490,
      area_type: 'County',
    },
  ],
  2693: [
    {
      index: 2677,
      name: 'Blacksburg-Christiansburg, VA MSA',
      population: 165812,
      area_type: 'MSA',
    },
    {
      index: 2693,
      name: 'Montgomery County, Virginia',
      population: 98915,
      area_type: 'County',
    },
  ],
  2694: [
    {
      index: 2648,
      name: 'Charlottesville, VA MSA',
      population: 223825,
      area_type: 'MSA',
    },
    {
      index: 2694,
      name: 'Nelson County, Virginia',
      population: 14652,
      area_type: 'County',
    },
  ],
  2695: [
    {
      index: 2650,
      name: 'Richmond, VA MSA',
      population: 1339182,
      area_type: 'MSA',
    },
    {
      index: 2695,
      name: 'New Kent County, Virginia',
      population: 24986,
      area_type: 'County',
    },
  ],
  2697: [
    {
      index: 2696,
      name: 'Danville, VA μSA',
      population: 102181,
      area_type: 'μSA',
    },
    {
      index: 2697,
      name: 'Pittsylvania County, Virginia',
      population: 59952,
      area_type: 'County',
    },
  ],
  2698: [
    {
      index: 2650,
      name: 'Richmond, VA MSA',
      population: 1339182,
      area_type: 'MSA',
    },
    {
      index: 2698,
      name: 'Powhatan County, Virginia',
      population: 31489,
      area_type: 'County',
    },
  ],
  2699: [
    {
      index: 2650,
      name: 'Richmond, VA MSA',
      population: 1339182,
      area_type: 'MSA',
    },
    {
      index: 2699,
      name: 'Prince George County, Virginia',
      population: 43134,
      area_type: 'County',
    },
  ],
  2700: [
    {
      index: 326,
      name: 'Washington-Baltimore-Arlington, DC-MD-VA-WV-PA CSA',
      population: 9968104,
      area_type: 'CSA',
    },
    {
      index: 327,
      name: 'Washington-Arlington-Alexandria, DC-VA-MD-WV MSA',
      population: 6373756,
      area_type: 'MSA',
    },
    {
      index: 2700,
      name: 'Prince William County, Virginia',
      population: 486943,
      area_type: 'County',
    },
  ],
  2701: [
    {
      index: 2677,
      name: 'Blacksburg-Christiansburg, VA MSA',
      population: 165812,
      area_type: 'MSA',
    },
    {
      index: 2701,
      name: 'Pulaski County, Virginia',
      population: 33706,
      area_type: 'County',
    },
  ],
  2702: [
    {
      index: 326,
      name: 'Washington-Baltimore-Arlington, DC-MD-VA-WV-PA CSA',
      population: 9968104,
      area_type: 'CSA',
    },
    {
      index: 327,
      name: 'Washington-Arlington-Alexandria, DC-VA-MD-WV MSA',
      population: 6373756,
      area_type: 'MSA',
    },
    {
      index: 2702,
      name: 'Rappahannock County, Virginia',
      population: 7502,
      area_type: 'County',
    },
  ],
  2703: [
    {
      index: 2662,
      name: 'Roanoke, VA MSA',
      population: 314340,
      area_type: 'MSA',
    },
    {
      index: 2703,
      name: 'Roanoke County, Virginia',
      population: 96914,
      area_type: 'County',
    },
  ],
  2705: [
    {
      index: 2656,
      name: 'Harrisonburg-Staunton, VA CSA',
      population: 263331,
      area_type: 'CSA',
    },
    {
      index: 2704,
      name: 'Harrisonburg, VA MSA',
      population: 136555,
      area_type: 'MSA',
    },
    {
      index: 2705,
      name: 'Rockingham County, Virginia',
      population: 85397,
      area_type: 'County',
    },
  ],
  2706: [
    {
      index: 2318,
      name: 'Johnson City-Kingsport-Bristol, TN-VA CSA',
      population: 521528,
      area_type: 'CSA',
    },
    {
      index: 2345,
      name: 'Kingsport-Bristol, TN-VA MSA',
      population: 311272,
      area_type: 'MSA',
    },
    {
      index: 2706,
      name: 'Scott County, Virginia',
      population: 21476,
      area_type: 'County',
    },
  ],
  2707: [
    {
      index: 1768,
      name: 'Virginia Beach-Norfolk, VA-NC CSA',
      population: 1898944,
      area_type: 'CSA',
    },
    {
      index: 1769,
      name: 'Virginia Beach-Norfolk-Newport News, VA-NC MSA',
      population: 1806840,
      area_type: 'MSA',
    },
    {
      index: 2707,
      name: 'Southampton County, Virginia',
      population: 17932,
      area_type: 'County',
    },
  ],
  2708: [
    {
      index: 326,
      name: 'Washington-Baltimore-Arlington, DC-MD-VA-WV-PA CSA',
      population: 9968104,
      area_type: 'CSA',
    },
    {
      index: 327,
      name: 'Washington-Arlington-Alexandria, DC-VA-MD-WV MSA',
      population: 6373756,
      area_type: 'MSA',
    },
    {
      index: 2708,
      name: 'Spotsylvania County, Virginia',
      population: 146688,
      area_type: 'County',
    },
  ],
  2709: [
    {
      index: 326,
      name: 'Washington-Baltimore-Arlington, DC-MD-VA-WV-PA CSA',
      population: 9968104,
      area_type: 'CSA',
    },
    {
      index: 327,
      name: 'Washington-Arlington-Alexandria, DC-VA-MD-WV MSA',
      population: 6373756,
      area_type: 'MSA',
    },
    {
      index: 2709,
      name: 'Stafford County, Virginia',
      population: 163380,
      area_type: 'County',
    },
  ],
  2710: [
    {
      index: 2650,
      name: 'Richmond, VA MSA',
      population: 1339182,
      area_type: 'MSA',
    },
    {
      index: 2710,
      name: 'Sussex County, Virginia',
      population: 10680,
      area_type: 'County',
    },
  ],
  2711: [
    {
      index: 2660,
      name: 'Bluefield, WV-VA μSA',
      population: 104669,
      area_type: 'μSA',
    },
    {
      index: 2711,
      name: 'Tazewell County, Virginia',
      population: 39821,
      area_type: 'County',
    },
  ],
  2712: [
    {
      index: 326,
      name: 'Washington-Baltimore-Arlington, DC-MD-VA-WV-PA CSA',
      population: 9968104,
      area_type: 'CSA',
    },
    {
      index: 327,
      name: 'Washington-Arlington-Alexandria, DC-VA-MD-WV MSA',
      population: 6373756,
      area_type: 'MSA',
    },
    {
      index: 2712,
      name: 'Warren County, Virginia',
      population: 41440,
      area_type: 'County',
    },
  ],
  2713: [
    {
      index: 2318,
      name: 'Johnson City-Kingsport-Bristol, TN-VA CSA',
      population: 521528,
      area_type: 'CSA',
    },
    {
      index: 2345,
      name: 'Kingsport-Bristol, TN-VA MSA',
      population: 311272,
      area_type: 'MSA',
    },
    {
      index: 2713,
      name: 'Washington County, Virginia',
      population: 53958,
      area_type: 'County',
    },
  ],
  2715: [
    {
      index: 2714,
      name: 'Big Stone Gap, VA μSA',
      population: 39030,
      area_type: 'μSA',
    },
    {
      index: 2715,
      name: 'Wise County, Virginia',
      population: 35421,
      area_type: 'County',
    },
  ],
  2716: [
    {
      index: 1768,
      name: 'Virginia Beach-Norfolk, VA-NC CSA',
      population: 1898944,
      area_type: 'CSA',
    },
    {
      index: 1769,
      name: 'Virginia Beach-Norfolk-Newport News, VA-NC MSA',
      population: 1806840,
      area_type: 'MSA',
    },
    {
      index: 2716,
      name: 'York County, Virginia',
      population: 71341,
      area_type: 'County',
    },
  ],
  2717: [
    {
      index: 326,
      name: 'Washington-Baltimore-Arlington, DC-MD-VA-WV-PA CSA',
      population: 9968104,
      area_type: 'CSA',
    },
    {
      index: 327,
      name: 'Washington-Arlington-Alexandria, DC-VA-MD-WV MSA',
      population: 6373756,
      area_type: 'MSA',
    },
    {
      index: 2717,
      name: 'Alexandria city, Virginia',
      population: 155525,
      area_type: 'County',
    },
  ],
  2718: [
    {
      index: 2318,
      name: 'Johnson City-Kingsport-Bristol, TN-VA CSA',
      population: 521528,
      area_type: 'CSA',
    },
    {
      index: 2345,
      name: 'Kingsport-Bristol, TN-VA MSA',
      population: 311272,
      area_type: 'MSA',
    },
    {
      index: 2718,
      name: 'Bristol city, Virginia',
      population: 16975,
      area_type: 'County',
    },
  ],
  2719: [
    {
      index: 2648,
      name: 'Charlottesville, VA MSA',
      population: 223825,
      area_type: 'MSA',
    },
    {
      index: 2719,
      name: 'Charlottesville city, Virginia',
      population: 45373,
      area_type: 'County',
    },
  ],
  2720: [
    {
      index: 1768,
      name: 'Virginia Beach-Norfolk, VA-NC CSA',
      population: 1898944,
      area_type: 'CSA',
    },
    {
      index: 1769,
      name: 'Virginia Beach-Norfolk-Newport News, VA-NC MSA',
      population: 1806840,
      area_type: 'MSA',
    },
    {
      index: 2720,
      name: 'Chesapeake city, Virginia',
      population: 252488,
      area_type: 'County',
    },
  ],
  2721: [
    {
      index: 2650,
      name: 'Richmond, VA MSA',
      population: 1339182,
      area_type: 'MSA',
    },
    {
      index: 2721,
      name: 'Colonial Heights city, Virginia',
      population: 18294,
      area_type: 'County',
    },
  ],
  2722: [
    {
      index: 2696,
      name: 'Danville, VA μSA',
      population: 102181,
      area_type: 'μSA',
    },
    {
      index: 2722,
      name: 'Danville city, Virginia',
      population: 42229,
      area_type: 'County',
    },
  ],
  2723: [
    {
      index: 326,
      name: 'Washington-Baltimore-Arlington, DC-MD-VA-WV-PA CSA',
      population: 9968104,
      area_type: 'CSA',
    },
    {
      index: 327,
      name: 'Washington-Arlington-Alexandria, DC-VA-MD-WV MSA',
      population: 6373756,
      area_type: 'MSA',
    },
    {
      index: 2723,
      name: 'Fairfax city, Virginia',
      population: 24835,
      area_type: 'County',
    },
  ],
  2724: [
    {
      index: 326,
      name: 'Washington-Baltimore-Arlington, DC-MD-VA-WV-PA CSA',
      population: 9968104,
      area_type: 'CSA',
    },
    {
      index: 327,
      name: 'Washington-Arlington-Alexandria, DC-VA-MD-WV MSA',
      population: 6373756,
      area_type: 'MSA',
    },
    {
      index: 2724,
      name: 'Falls Church city, Virginia',
      population: 14586,
      area_type: 'County',
    },
  ],
  2725: [
    {
      index: 1768,
      name: 'Virginia Beach-Norfolk, VA-NC CSA',
      population: 1898944,
      area_type: 'CSA',
    },
    {
      index: 1769,
      name: 'Virginia Beach-Norfolk-Newport News, VA-NC MSA',
      population: 1806840,
      area_type: 'MSA',
    },
    {
      index: 2725,
      name: 'Franklin city, Virginia',
      population: 8247,
      area_type: 'County',
    },
  ],
  2726: [
    {
      index: 326,
      name: 'Washington-Baltimore-Arlington, DC-MD-VA-WV-PA CSA',
      population: 9968104,
      area_type: 'CSA',
    },
    {
      index: 327,
      name: 'Washington-Arlington-Alexandria, DC-VA-MD-WV MSA',
      population: 6373756,
      area_type: 'MSA',
    },
    {
      index: 2726,
      name: 'Fredericksburg city, Virginia',
      population: 28757,
      area_type: 'County',
    },
  ],
  2727: [
    {
      index: 1768,
      name: 'Virginia Beach-Norfolk, VA-NC CSA',
      population: 1898944,
      area_type: 'CSA',
    },
    {
      index: 1769,
      name: 'Virginia Beach-Norfolk-Newport News, VA-NC MSA',
      population: 1806840,
      area_type: 'MSA',
    },
    {
      index: 2727,
      name: 'Hampton city, Virginia',
      population: 138037,
      area_type: 'County',
    },
  ],
  2728: [
    {
      index: 2656,
      name: 'Harrisonburg-Staunton, VA CSA',
      population: 263331,
      area_type: 'CSA',
    },
    {
      index: 2704,
      name: 'Harrisonburg, VA MSA',
      population: 136555,
      area_type: 'MSA',
    },
    {
      index: 2728,
      name: 'Harrisonburg city, Virginia',
      population: 51158,
      area_type: 'County',
    },
  ],
  2729: [
    {
      index: 2650,
      name: 'Richmond, VA MSA',
      population: 1339182,
      area_type: 'MSA',
    },
    {
      index: 2729,
      name: 'Hopewell city, Virginia',
      population: 22962,
      area_type: 'County',
    },
  ],
  2730: [
    {
      index: 2652,
      name: 'Lynchburg, VA MSA',
      population: 263613,
      area_type: 'MSA',
    },
    {
      index: 2730,
      name: 'Lynchburg city, Virginia',
      population: 79287,
      area_type: 'County',
    },
  ],
  2731: [
    {
      index: 326,
      name: 'Washington-Baltimore-Arlington, DC-MD-VA-WV-PA CSA',
      population: 9968104,
      area_type: 'CSA',
    },
    {
      index: 327,
      name: 'Washington-Arlington-Alexandria, DC-VA-MD-WV MSA',
      population: 6373756,
      area_type: 'MSA',
    },
    {
      index: 2731,
      name: 'Manassas city, Virginia',
      population: 42642,
      area_type: 'County',
    },
  ],
  2732: [
    {
      index: 326,
      name: 'Washington-Baltimore-Arlington, DC-MD-VA-WV-PA CSA',
      population: 9968104,
      area_type: 'CSA',
    },
    {
      index: 327,
      name: 'Washington-Arlington-Alexandria, DC-VA-MD-WV MSA',
      population: 6373756,
      area_type: 'MSA',
    },
    {
      index: 2732,
      name: 'Manassas Park city, Virginia',
      population: 16703,
      area_type: 'County',
    },
  ],
  2733: [
    {
      index: 2684,
      name: 'Martinsville, VA μSA',
      population: 63631,
      area_type: 'μSA',
    },
    {
      index: 2733,
      name: 'Martinsville city, Virginia',
      population: 13725,
      area_type: 'County',
    },
  ],
  2734: [
    {
      index: 1768,
      name: 'Virginia Beach-Norfolk, VA-NC CSA',
      population: 1898944,
      area_type: 'CSA',
    },
    {
      index: 1769,
      name: 'Virginia Beach-Norfolk-Newport News, VA-NC MSA',
      population: 1806840,
      area_type: 'MSA',
    },
    {
      index: 2734,
      name: 'Newport News city, Virginia',
      population: 184306,
      area_type: 'County',
    },
  ],
  2735: [
    {
      index: 1768,
      name: 'Virginia Beach-Norfolk, VA-NC CSA',
      population: 1898944,
      area_type: 'CSA',
    },
    {
      index: 1769,
      name: 'Virginia Beach-Norfolk-Newport News, VA-NC MSA',
      population: 1806840,
      area_type: 'MSA',
    },
    {
      index: 2735,
      name: 'Norfolk city, Virginia',
      population: 232995,
      area_type: 'County',
    },
  ],
  2736: [
    {
      index: 2714,
      name: 'Big Stone Gap, VA μSA',
      population: 39030,
      area_type: 'μSA',
    },
    {
      index: 2736,
      name: 'Norton city, Virginia',
      population: 3609,
      area_type: 'County',
    },
  ],
  2737: [
    {
      index: 2650,
      name: 'Richmond, VA MSA',
      population: 1339182,
      area_type: 'MSA',
    },
    {
      index: 2737,
      name: 'Petersburg city, Virginia',
      population: 33394,
      area_type: 'County',
    },
  ],
  2738: [
    {
      index: 1768,
      name: 'Virginia Beach-Norfolk, VA-NC CSA',
      population: 1898944,
      area_type: 'CSA',
    },
    {
      index: 1769,
      name: 'Virginia Beach-Norfolk-Newport News, VA-NC MSA',
      population: 1806840,
      area_type: 'MSA',
    },
    {
      index: 2738,
      name: 'Poquoson city, Virginia',
      population: 12582,
      area_type: 'County',
    },
  ],
  2739: [
    {
      index: 1768,
      name: 'Virginia Beach-Norfolk, VA-NC CSA',
      population: 1898944,
      area_type: 'CSA',
    },
    {
      index: 1769,
      name: 'Virginia Beach-Norfolk-Newport News, VA-NC MSA',
      population: 1806840,
      area_type: 'MSA',
    },
    {
      index: 2739,
      name: 'Portsmouth city, Virginia',
      population: 97029,
      area_type: 'County',
    },
  ],
  2740: [
    {
      index: 2677,
      name: 'Blacksburg-Christiansburg, VA MSA',
      population: 165812,
      area_type: 'MSA',
    },
    {
      index: 2740,
      name: 'Radford city, Virginia',
      population: 16738,
      area_type: 'County',
    },
  ],
  2741: [
    {
      index: 2650,
      name: 'Richmond, VA MSA',
      population: 1339182,
      area_type: 'MSA',
    },
    {
      index: 2741,
      name: 'Richmond city, Virginia',
      population: 229395,
      area_type: 'County',
    },
  ],
  2742: [
    {
      index: 2662,
      name: 'Roanoke, VA MSA',
      population: 314340,
      area_type: 'MSA',
    },
    {
      index: 2742,
      name: 'Roanoke city, Virginia',
      population: 97847,
      area_type: 'County',
    },
  ],
  2743: [
    {
      index: 2662,
      name: 'Roanoke, VA MSA',
      population: 314340,
      area_type: 'MSA',
    },
    {
      index: 2743,
      name: 'Salem city, Virginia',
      population: 25523,
      area_type: 'County',
    },
  ],
  2744: [
    {
      index: 2656,
      name: 'Harrisonburg-Staunton, VA CSA',
      population: 263331,
      area_type: 'CSA',
    },
    {
      index: 2657,
      name: 'Staunton, VA MSA',
      population: 126776,
      area_type: 'MSA',
    },
    {
      index: 2744,
      name: 'Staunton city, Virginia',
      population: 25904,
      area_type: 'County',
    },
  ],
  2745: [
    {
      index: 1768,
      name: 'Virginia Beach-Norfolk, VA-NC CSA',
      population: 1898944,
      area_type: 'CSA',
    },
    {
      index: 1769,
      name: 'Virginia Beach-Norfolk-Newport News, VA-NC MSA',
      population: 1806840,
      area_type: 'MSA',
    },
    {
      index: 2745,
      name: 'Suffolk city, Virginia',
      population: 98537,
      area_type: 'County',
    },
  ],
  2746: [
    {
      index: 1768,
      name: 'Virginia Beach-Norfolk, VA-NC CSA',
      population: 1898944,
      area_type: 'CSA',
    },
    {
      index: 1769,
      name: 'Virginia Beach-Norfolk-Newport News, VA-NC MSA',
      population: 1806840,
      area_type: 'MSA',
    },
    {
      index: 2746,
      name: 'Virginia Beach city, Virginia',
      population: 455618,
      area_type: 'County',
    },
  ],
  2747: [
    {
      index: 2656,
      name: 'Harrisonburg-Staunton, VA CSA',
      population: 263331,
      area_type: 'CSA',
    },
    {
      index: 2657,
      name: 'Staunton, VA MSA',
      population: 126776,
      area_type: 'MSA',
    },
    {
      index: 2747,
      name: 'Waynesboro city, Virginia',
      population: 22808,
      area_type: 'County',
    },
  ],
  2748: [
    {
      index: 1768,
      name: 'Virginia Beach-Norfolk, VA-NC CSA',
      population: 1898944,
      area_type: 'CSA',
    },
    {
      index: 1769,
      name: 'Virginia Beach-Norfolk-Newport News, VA-NC MSA',
      population: 1806840,
      area_type: 'MSA',
    },
    {
      index: 2748,
      name: 'Williamsburg city, Virginia',
      population: 15909,
      area_type: 'County',
    },
  ],
  2749: [
    {
      index: 326,
      name: 'Washington-Baltimore-Arlington, DC-MD-VA-WV-PA CSA',
      population: 9968104,
      area_type: 'CSA',
    },
    {
      index: 2675,
      name: 'Winchester, VA-WV MSA',
      population: 146455,
      area_type: 'MSA',
    },
    {
      index: 2749,
      name: 'Winchester city, Virginia',
      population: 27936,
      area_type: 'County',
    },
  ],
  2751: [
    {
      index: 2750,
      name: 'Moses Lake-Othello, WA CSA',
      population: 122272,
      area_type: 'CSA',
    },
    {
      index: 2751,
      name: 'Adams County, Washington (Othello, WA μSA)',
      population: 20961,
      area_type: 'μSA',
    },
  ],
  2753: [
    {
      index: 605,
      name: 'Lewiston, ID-WA MSA',
      population: 65512,
      area_type: 'MSA',
    },
    {
      index: 2753,
      name: 'Asotin County, Washington',
      population: 22508,
      area_type: 'County',
    },
  ],
  2756: [
    {
      index: 2754,
      name: 'Kennewick-Richland-Walla Walla, WA CSA',
      population: 373359,
      area_type: 'CSA',
    },
    {
      index: 2755,
      name: 'Kennewick-Richland, WA MSA',
      population: 311469,
      area_type: 'MSA',
    },
    {
      index: 2756,
      name: 'Benton County, Washington',
      population: 212791,
      area_type: 'County',
    },
  ],
  2758: [
    {
      index: 2757,
      name: 'Wenatchee, WA MSA',
      population: 124118,
      area_type: 'MSA',
    },
    {
      index: 2758,
      name: 'Chelan County, Washington',
      population: 79926,
      area_type: 'County',
    },
  ],
  2759: [
    {
      index: 2759,
      name: 'Clallam County, Washington (Port Angeles, WA μSA)',
      population: 77805,
      area_type: 'μSA',
    },
  ],
  2761: [
    {
      index: 2075,
      name: 'Portland-Vancouver-Salem, OR-WA CSA',
      population: 3285859,
      area_type: 'CSA',
    },
    {
      index: 2078,
      name: 'Portland-Vancouver-Hillsboro, OR-WA MSA',
      population: 2509489,
      area_type: 'MSA',
    },
    {
      index: 2761,
      name: 'Clark County, Washington',
      population: 516779,
      area_type: 'County',
    },
  ],
  2762: [
    {
      index: 2075,
      name: 'Portland-Vancouver-Salem, OR-WA CSA',
      population: 3285859,
      area_type: 'CSA',
    },
    {
      index: 2762,
      name: 'Cowlitz County, Washington (Longview, WA MSA)',
      population: 111956,
      area_type: 'MSA',
    },
  ],
  2764: [
    {
      index: 2757,
      name: 'Wenatchee, WA MSA',
      population: 124118,
      area_type: 'MSA',
    },
    {
      index: 2764,
      name: 'Douglas County, Washington',
      population: 44192,
      area_type: 'County',
    },
  ],
  2765: [
    {
      index: 2754,
      name: 'Kennewick-Richland-Walla Walla, WA CSA',
      population: 373359,
      area_type: 'CSA',
    },
    {
      index: 2755,
      name: 'Kennewick-Richland, WA MSA',
      population: 311469,
      area_type: 'MSA',
    },
    {
      index: 2765,
      name: 'Franklin County, Washington',
      population: 98678,
      area_type: 'County',
    },
  ],
  2766: [
    {
      index: 2750,
      name: 'Moses Lake-Othello, WA CSA',
      population: 122272,
      area_type: 'CSA',
    },
    {
      index: 2766,
      name: 'Grant County, Washington (Moses Lake, WA μSA)',
      population: 101311,
      area_type: 'μSA',
    },
  ],
  2768: [
    {
      index: 2768,
      name: 'Grays Harbor County, Washington (Aberdeen, WA μSA)',
      population: 77038,
      area_type: 'μSA',
    },
  ],
  2771: [
    {
      index: 2770,
      name: 'Seattle-Tacoma, WA CSA',
      population: 4982019,
      area_type: 'CSA',
    },
    {
      index: 2771,
      name: 'Island County, Washington (Oak Harbor, WA μSA)',
      population: 86625,
      area_type: 'μSA',
    },
  ],
  2774: [
    {
      index: 2770,
      name: 'Seattle-Tacoma, WA CSA',
      population: 4982019,
      area_type: 'CSA',
    },
    {
      index: 2773,
      name: 'Seattle-Tacoma-Bellevue, WA MSA',
      population: 4034248,
      area_type: 'MSA',
    },
    {
      index: 2774,
      name: 'King County, Washington',
      population: 2266789,
      area_type: 'County',
    },
  ],
  2775: [
    {
      index: 2770,
      name: 'Seattle-Tacoma, WA CSA',
      population: 4982019,
      area_type: 'CSA',
    },
    {
      index: 2775,
      name: 'Kitsap County, Washington (Bremerton-Silverdale-Port Orchard, WA MSA)',
      population: 277673,
      area_type: 'MSA',
    },
  ],
  2777: [
    {
      index: 2777,
      name: 'Kittitas County, Washington (Ellensburg, WA μSA)',
      population: 45189,
      area_type: 'μSA',
    },
  ],
  2779: [
    {
      index: 2770,
      name: 'Seattle-Tacoma, WA CSA',
      population: 4982019,
      area_type: 'CSA',
    },
    {
      index: 2779,
      name: 'Lewis County, Washington (Centralia, WA μSA)',
      population: 85370,
      area_type: 'μSA',
    },
  ],
  2781: [
    {
      index: 2770,
      name: 'Seattle-Tacoma, WA CSA',
      population: 4982019,
      area_type: 'CSA',
    },
    {
      index: 2781,
      name: 'Mason County, Washington (Shelton, WA μSA)',
      population: 68166,
      area_type: 'μSA',
    },
  ],
  2783: [
    {
      index: 2770,
      name: 'Seattle-Tacoma, WA CSA',
      population: 4982019,
      area_type: 'CSA',
    },
    {
      index: 2773,
      name: 'Seattle-Tacoma-Bellevue, WA MSA',
      population: 4034248,
      area_type: 'MSA',
    },
    {
      index: 2783,
      name: 'Pierce County, Washington',
      population: 927380,
      area_type: 'County',
    },
  ],
  2784: [
    {
      index: 2770,
      name: 'Seattle-Tacoma, WA CSA',
      population: 4982019,
      area_type: 'CSA',
    },
    {
      index: 2784,
      name: 'Skagit County, Washington (Mount Vernon-Anacortes, WA MSA)',
      population: 131179,
      area_type: 'MSA',
    },
  ],
  2786: [
    {
      index: 2075,
      name: 'Portland-Vancouver-Salem, OR-WA CSA',
      population: 3285859,
      area_type: 'CSA',
    },
    {
      index: 2078,
      name: 'Portland-Vancouver-Hillsboro, OR-WA MSA',
      population: 2509489,
      area_type: 'MSA',
    },
    {
      index: 2786,
      name: 'Skamania County, Washington',
      population: 12460,
      area_type: 'County',
    },
  ],
  2787: [
    {
      index: 2770,
      name: 'Seattle-Tacoma, WA CSA',
      population: 4982019,
      area_type: 'CSA',
    },
    {
      index: 2773,
      name: 'Seattle-Tacoma-Bellevue, WA MSA',
      population: 4034248,
      area_type: 'MSA',
    },
    {
      index: 2787,
      name: 'Snohomish County, Washington',
      population: 840079,
      area_type: 'County',
    },
  ],
  2789: [
    {
      index: 597,
      name: "Spokane-Spokane Valley-Coeur d'Alene, WA-ID CSA",
      population: 781497,
      area_type: 'CSA',
    },
    {
      index: 2788,
      name: 'Spokane-Spokane Valley, WA MSA',
      population: 597919,
      area_type: 'MSA',
    },
    {
      index: 2789,
      name: 'Spokane County, Washington',
      population: 549690,
      area_type: 'County',
    },
  ],
  2790: [
    {
      index: 597,
      name: "Spokane-Spokane Valley-Coeur d'Alene, WA-ID CSA",
      population: 781497,
      area_type: 'CSA',
    },
    {
      index: 2788,
      name: 'Spokane-Spokane Valley, WA MSA',
      population: 597919,
      area_type: 'MSA',
    },
    {
      index: 2790,
      name: 'Stevens County, Washington',
      population: 48229,
      area_type: 'County',
    },
  ],
  2791: [
    {
      index: 2770,
      name: 'Seattle-Tacoma, WA CSA',
      population: 4982019,
      area_type: 'CSA',
    },
    {
      index: 2791,
      name: 'Thurston County, Washington (Olympia-Lacey-Tumwater, WA MSA)',
      population: 298758,
      area_type: 'MSA',
    },
  ],
  2793: [
    {
      index: 2754,
      name: 'Kennewick-Richland-Walla Walla, WA CSA',
      population: 373359,
      area_type: 'CSA',
    },
    {
      index: 2793,
      name: 'Walla Walla County, Washington (Walla Walla, WA MSA)',
      population: 61890,
      area_type: 'MSA',
    },
  ],
  2795: [
    {
      index: 2795,
      name: 'Whatcom County, Washington (Bellingham, WA MSA)',
      population: 230677,
      area_type: 'MSA',
    },
  ],
  2797: [
    {
      index: 600,
      name: 'Pullman-Moscow, WA-ID CSA',
      population: 88597,
      area_type: 'CSA',
    },
    {
      index: 2797,
      name: 'Whitman County, Washington (Pullman, WA μSA)',
      population: 47619,
      area_type: 'μSA',
    },
  ],
  2799: [
    {
      index: 2799,
      name: 'Yakima County, Washington (Yakima, WA MSA)',
      population: 257001,
      area_type: 'MSA',
    },
  ],
  2801: [
    {
      index: 326,
      name: 'Washington-Baltimore-Arlington, DC-MD-VA-WV-PA CSA',
      population: 9968104,
      area_type: 'CSA',
    },
    {
      index: 1155,
      name: 'Hagerstown-Martinsburg, MD-WV MSA',
      population: 302510,
      area_type: 'MSA',
    },
    {
      index: 2801,
      name: 'Berkeley County, West Virginia',
      population: 129490,
      area_type: 'County',
    },
  ],
  2803: [
    {
      index: 980,
      name: 'Charleston-Huntington-Ashland, WV-OH-KY CSA',
      population: 763796,
      area_type: 'CSA',
    },
    {
      index: 2802,
      name: 'Charleston, WV MSA',
      population: 251914,
      area_type: 'MSA',
    },
    {
      index: 2803,
      name: 'Boone County, West Virginia',
      population: 20968,
      area_type: 'County',
    },
  ],
  2804: [
    {
      index: 1957,
      name: 'Pittsburgh-New Castle-Weirton, PA-OH-WV CSA',
      population: 2631213,
      area_type: 'CSA',
    },
    {
      index: 1958,
      name: 'Weirton-Steubenville, WV-OH MSA',
      population: 114235,
      area_type: 'MSA',
    },
    {
      index: 2804,
      name: 'Brooke County, West Virginia',
      population: 21733,
      area_type: 'County',
    },
  ],
  2805: [
    {
      index: 980,
      name: 'Charleston-Huntington-Ashland, WV-OH-KY CSA',
      population: 763796,
      area_type: 'CSA',
    },
    {
      index: 981,
      name: 'Huntington-Ashland, WV-KY-OH MSA',
      population: 354304,
      area_type: 'MSA',
    },
    {
      index: 2805,
      name: 'Cabell County, West Virginia',
      population: 92730,
      area_type: 'County',
    },
  ],
  2806: [
    {
      index: 980,
      name: 'Charleston-Huntington-Ashland, WV-OH-KY CSA',
      population: 763796,
      area_type: 'CSA',
    },
    {
      index: 2802,
      name: 'Charleston, WV MSA',
      population: 251914,
      area_type: 'MSA',
    },
    {
      index: 2806,
      name: 'Clay County, West Virginia',
      population: 7814,
      area_type: 'County',
    },
  ],
  2808: [
    {
      index: 2807,
      name: 'Clarksburg, WV μSA',
      population: 88955,
      area_type: 'μSA',
    },
    {
      index: 2808,
      name: 'Doddridge County, West Virginia',
      population: 7698,
      area_type: 'County',
    },
  ],
  2810: [
    {
      index: 2809,
      name: 'Beckley, WV MSA',
      population: 112369,
      area_type: 'MSA',
    },
    {
      index: 2810,
      name: 'Fayette County, West Virginia',
      population: 39487,
      area_type: 'County',
    },
  ],
  2811: [
    {
      index: 326,
      name: 'Washington-Baltimore-Arlington, DC-MD-VA-WV-PA CSA',
      population: 9968104,
      area_type: 'CSA',
    },
    {
      index: 2675,
      name: 'Winchester, VA-WV MSA',
      population: 146455,
      area_type: 'MSA',
    },
    {
      index: 2811,
      name: 'Hampshire County, West Virginia',
      population: 23468,
      area_type: 'County',
    },
  ],
  2812: [
    {
      index: 1957,
      name: 'Pittsburgh-New Castle-Weirton, PA-OH-WV CSA',
      population: 2631213,
      area_type: 'CSA',
    },
    {
      index: 1958,
      name: 'Weirton-Steubenville, WV-OH MSA',
      population: 114235,
      area_type: 'MSA',
    },
    {
      index: 2812,
      name: 'Hancock County, West Virginia',
      population: 28172,
      area_type: 'County',
    },
  ],
  2813: [
    {
      index: 2807,
      name: 'Clarksburg, WV μSA',
      population: 88955,
      area_type: 'μSA',
    },
    {
      index: 2813,
      name: 'Harrison County, West Virginia',
      population: 64915,
      area_type: 'County',
    },
  ],
  2814: [
    {
      index: 980,
      name: 'Charleston-Huntington-Ashland, WV-OH-KY CSA',
      population: 763796,
      area_type: 'CSA',
    },
    {
      index: 2802,
      name: 'Charleston, WV MSA',
      population: 251914,
      area_type: 'MSA',
    },
    {
      index: 2814,
      name: 'Jackson County, West Virginia',
      population: 27716,
      area_type: 'County',
    },
  ],
  2815: [
    {
      index: 326,
      name: 'Washington-Baltimore-Arlington, DC-MD-VA-WV-PA CSA',
      population: 9968104,
      area_type: 'CSA',
    },
    {
      index: 327,
      name: 'Washington-Arlington-Alexandria, DC-VA-MD-WV MSA',
      population: 6373756,
      area_type: 'MSA',
    },
    {
      index: 2815,
      name: 'Jefferson County, West Virginia',
      population: 58979,
      area_type: 'County',
    },
  ],
  2816: [
    {
      index: 980,
      name: 'Charleston-Huntington-Ashland, WV-OH-KY CSA',
      population: 763796,
      area_type: 'CSA',
    },
    {
      index: 2802,
      name: 'Charleston, WV MSA',
      population: 251914,
      area_type: 'MSA',
    },
    {
      index: 2816,
      name: 'Kanawha County, West Virginia',
      population: 175515,
      area_type: 'County',
    },
  ],
  2817: [
    {
      index: 980,
      name: 'Charleston-Huntington-Ashland, WV-OH-KY CSA',
      population: 763796,
      area_type: 'CSA',
    },
    {
      index: 2802,
      name: 'Charleston, WV MSA',
      population: 251914,
      area_type: 'MSA',
    },
    {
      index: 2817,
      name: 'Lincoln County, West Virginia',
      population: 19901,
      area_type: 'County',
    },
  ],
  2818: [
    {
      index: 980,
      name: 'Charleston-Huntington-Ashland, WV-OH-KY CSA',
      population: 763796,
      area_type: 'CSA',
    },
    {
      index: 2818,
      name: 'Logan County, West Virginia (Mount Gay-Shamrock, WV μSA)',
      population: 31316,
      area_type: 'μSA',
    },
  ],
  2821: [
    {
      index: 2820,
      name: 'Morgantown-Fairmont, WV CSA',
      population: 196993,
      area_type: 'CSA',
    },
    {
      index: 2821,
      name: 'Marion County, West Virginia (Fairmont, WV μSA)',
      population: 55952,
      area_type: 'μSA',
    },
  ],
  2823: [
    {
      index: 1903,
      name: 'Wheeling, WV-OH MSA',
      population: 136708,
      area_type: 'MSA',
    },
    {
      index: 2823,
      name: 'Marshall County, West Virginia',
      population: 29752,
      area_type: 'County',
    },
  ],
  2824: [
    {
      index: 980,
      name: 'Charleston-Huntington-Ashland, WV-OH-KY CSA',
      population: 763796,
      area_type: 'CSA',
    },
    {
      index: 1942,
      name: 'Point Pleasant, WV-OH μSA',
      population: 54068,
      area_type: 'μSA',
    },
    {
      index: 2824,
      name: 'Mason County, West Virginia',
      population: 25000,
      area_type: 'County',
    },
  ],
  2825: [
    {
      index: 2660,
      name: 'Bluefield, WV-VA μSA',
      population: 104669,
      area_type: 'μSA',
    },
    {
      index: 2825,
      name: 'Mercer County, West Virginia',
      population: 58700,
      area_type: 'County',
    },
  ],
  2826: [
    {
      index: 1133,
      name: 'Cumberland, MD-WV MSA',
      population: 94122,
      area_type: 'MSA',
    },
    {
      index: 2826,
      name: 'Mineral County, West Virginia',
      population: 26855,
      area_type: 'County',
    },
  ],
  2828: [
    {
      index: 2820,
      name: 'Morgantown-Fairmont, WV CSA',
      population: 196993,
      area_type: 'CSA',
    },
    {
      index: 2827,
      name: 'Morgantown, WV MSA',
      population: 141041,
      area_type: 'MSA',
    },
    {
      index: 2828,
      name: 'Monongalia County, West Virginia',
      population: 106869,
      area_type: 'County',
    },
  ],
  2829: [
    {
      index: 326,
      name: 'Washington-Baltimore-Arlington, DC-MD-VA-WV-PA CSA',
      population: 9968104,
      area_type: 'CSA',
    },
    {
      index: 1155,
      name: 'Hagerstown-Martinsburg, MD-WV MSA',
      population: 302510,
      area_type: 'MSA',
    },
    {
      index: 2829,
      name: 'Morgan County, West Virginia',
      population: 17430,
      area_type: 'County',
    },
  ],
  2830: [
    {
      index: 1903,
      name: 'Wheeling, WV-OH MSA',
      population: 136708,
      area_type: 'MSA',
    },
    {
      index: 2830,
      name: 'Ohio County, West Virginia',
      population: 41447,
      area_type: 'County',
    },
  ],
  2831: [
    {
      index: 2820,
      name: 'Morgantown-Fairmont, WV CSA',
      population: 196993,
      area_type: 'CSA',
    },
    {
      index: 2827,
      name: 'Morgantown, WV MSA',
      population: 141041,
      area_type: 'MSA',
    },
    {
      index: 2831,
      name: 'Preston County, West Virginia',
      population: 34172,
      area_type: 'County',
    },
  ],
  2832: [
    {
      index: 980,
      name: 'Charleston-Huntington-Ashland, WV-OH-KY CSA',
      population: 763796,
      area_type: 'CSA',
    },
    {
      index: 981,
      name: 'Huntington-Ashland, WV-KY-OH MSA',
      population: 354304,
      area_type: 'MSA',
    },
    {
      index: 2832,
      name: 'Putnam County, West Virginia',
      population: 57015,
      area_type: 'County',
    },
  ],
  2833: [
    {
      index: 2809,
      name: 'Beckley, WV MSA',
      population: 112369,
      area_type: 'MSA',
    },
    {
      index: 2833,
      name: 'Raleigh County, West Virginia',
      population: 72882,
      area_type: 'County',
    },
  ],
  2834: [
    {
      index: 2834,
      name: 'Randolph County, West Virginia (Elkins, WV μSA)',
      population: 27600,
      area_type: 'μSA',
    },
  ],
  2836: [
    {
      index: 2807,
      name: 'Clarksburg, WV μSA',
      population: 88955,
      area_type: 'μSA',
    },
    {
      index: 2836,
      name: 'Taylor County, West Virginia',
      population: 16342,
      area_type: 'County',
    },
  ],
  2837: [
    {
      index: 980,
      name: 'Charleston-Huntington-Ashland, WV-OH-KY CSA',
      population: 763796,
      area_type: 'CSA',
    },
    {
      index: 981,
      name: 'Huntington-Ashland, WV-KY-OH MSA',
      population: 354304,
      area_type: 'MSA',
    },
    {
      index: 2837,
      name: 'Wayne County, West Virginia',
      population: 37998,
      area_type: 'County',
    },
  ],
  2839: [
    {
      index: 2008,
      name: 'Parkersburg-Marietta-Vienna, WV-OH CSA',
      population: 147332,
      area_type: 'CSA',
    },
    {
      index: 2838,
      name: 'Parkersburg-Vienna, WV MSA',
      population: 88431,
      area_type: 'MSA',
    },
    {
      index: 2839,
      name: 'Wirt County, West Virginia',
      population: 5091,
      area_type: 'County',
    },
  ],
  2840: [
    {
      index: 2008,
      name: 'Parkersburg-Marietta-Vienna, WV-OH CSA',
      population: 147332,
      area_type: 'CSA',
    },
    {
      index: 2838,
      name: 'Parkersburg-Vienna, WV MSA',
      population: 88431,
      area_type: 'MSA',
    },
    {
      index: 2840,
      name: 'Wood County, West Virginia',
      population: 83340,
      area_type: 'County',
    },
  ],
  2843: [
    {
      index: 2841,
      name: 'Green Bay-Shawano, WI CSA',
      population: 375375,
      area_type: 'CSA',
    },
    {
      index: 2842,
      name: 'Green Bay, WI MSA',
      population: 330292,
      area_type: 'MSA',
    },
    {
      index: 2843,
      name: 'Brown County, Wisconsin',
      population: 270036,
      area_type: 'County',
    },
  ],
  2846: [
    {
      index: 2844,
      name: 'Appleton-Oshkosh-Neenah, WI CSA',
      population: 415563,
      area_type: 'CSA',
    },
    {
      index: 2845,
      name: 'Appleton, WI MSA',
      population: 244845,
      area_type: 'MSA',
    },
    {
      index: 2846,
      name: 'Calumet County, Wisconsin',
      population: 52718,
      area_type: 'County',
    },
  ],
  2849: [
    {
      index: 2847,
      name: 'Eau Claire-Menomonie, WI CSA',
      population: 219295,
      area_type: 'CSA',
    },
    {
      index: 2848,
      name: 'Eau Claire, WI MSA',
      population: 173644,
      area_type: 'MSA',
    },
    {
      index: 2849,
      name: 'Chippewa County, Wisconsin',
      population: 66807,
      area_type: 'County',
    },
  ],
  2852: [
    {
      index: 2850,
      name: 'Madison-Janesville-Beloit, WI CSA',
      population: 916914,
      area_type: 'CSA',
    },
    {
      index: 2851,
      name: 'Madison, WI MSA',
      population: 687077,
      area_type: 'MSA',
    },
    {
      index: 2852,
      name: 'Columbia County, Wisconsin',
      population: 58193,
      area_type: 'County',
    },
  ],
  2853: [
    {
      index: 2850,
      name: 'Madison-Janesville-Beloit, WI CSA',
      population: 916914,
      area_type: 'CSA',
    },
    {
      index: 2851,
      name: 'Madison, WI MSA',
      population: 687077,
      area_type: 'MSA',
    },
    {
      index: 2853,
      name: 'Dane County, Wisconsin',
      population: 568203,
      area_type: 'County',
    },
  ],
  2855: [
    {
      index: 2854,
      name: 'Milwaukee-Racine-Waukesha, WI CSA',
      population: 2035084,
      area_type: 'CSA',
    },
    {
      index: 2855,
      name: 'Dodge County, Wisconsin (Beaver Dam, WI μSA)',
      population: 88282,
      area_type: 'μSA',
    },
  ],
  2857: [
    {
      index: 1280,
      name: 'Duluth, MN-WI MSA',
      population: 291323,
      area_type: 'MSA',
    },
    {
      index: 2857,
      name: 'Douglas County, Wisconsin',
      population: 44144,
      area_type: 'County',
    },
  ],
  2858: [
    {
      index: 2847,
      name: 'Eau Claire-Menomonie, WI CSA',
      population: 219295,
      area_type: 'CSA',
    },
    {
      index: 2858,
      name: 'Dunn County, Wisconsin (Menomonie, WI μSA)',
      population: 45651,
      area_type: 'μSA',
    },
  ],
  2860: [
    {
      index: 2847,
      name: 'Eau Claire-Menomonie, WI CSA',
      population: 219295,
      area_type: 'CSA',
    },
    {
      index: 2848,
      name: 'Eau Claire, WI MSA',
      population: 173644,
      area_type: 'MSA',
    },
    {
      index: 2860,
      name: 'Eau Claire County, Wisconsin',
      population: 106837,
      area_type: 'County',
    },
  ],
  2861: [
    {
      index: 1205,
      name: 'Marinette-Iron Mountain, WI-MI CSA',
      population: 95816,
      area_type: 'CSA',
    },
    {
      index: 1206,
      name: 'Iron Mountain, MI-WI μSA',
      population: 30562,
      area_type: 'μSA',
    },
    {
      index: 2861,
      name: 'Florence County, Wisconsin',
      population: 4688,
      area_type: 'County',
    },
  ],
  2862: [
    {
      index: 2862,
      name: 'Fond du Lac County, Wisconsin (Fond du Lac, WI MSA)',
      population: 103836,
      area_type: 'MSA',
    },
  ],
  2864: [
    {
      index: 2864,
      name: 'Grant County, Wisconsin (Platteville, WI μSA)',
      population: 51276,
      area_type: 'μSA',
    },
  ],
  2866: [
    {
      index: 2850,
      name: 'Madison-Janesville-Beloit, WI CSA',
      population: 916914,
      area_type: 'CSA',
    },
    {
      index: 2851,
      name: 'Madison, WI MSA',
      population: 687077,
      area_type: 'MSA',
    },
    {
      index: 2866,
      name: 'Green County, Wisconsin',
      population: 36816,
      area_type: 'County',
    },
  ],
  2867: [
    {
      index: 2850,
      name: 'Madison-Janesville-Beloit, WI CSA',
      population: 916914,
      area_type: 'CSA',
    },
    {
      index: 2851,
      name: 'Madison, WI MSA',
      population: 687077,
      area_type: 'MSA',
    },
    {
      index: 2867,
      name: 'Iowa County, Wisconsin',
      population: 23865,
      area_type: 'County',
    },
  ],
  2868: [
    {
      index: 2854,
      name: 'Milwaukee-Racine-Waukesha, WI CSA',
      population: 2035084,
      area_type: 'CSA',
    },
    {
      index: 2868,
      name: 'Jefferson County, Wisconsin (Watertown-Fort Atkinson, WI μSA)',
      population: 85784,
      area_type: 'μSA',
    },
  ],
  2870: [
    {
      index: 626,
      name: 'Chicago-Naperville, IL-IN-WI CSA',
      population: 9806184,
      area_type: 'CSA',
    },
    {
      index: 638,
      name: 'Chicago-Naperville-Elgin, IL-IN-WI MSA',
      population: 9441957,
      area_type: 'MSA',
    },
    {
      index: 2870,
      name: 'Kenosha County, Wisconsin',
      population: 167817,
      area_type: 'County',
    },
  ],
  2871: [
    {
      index: 2841,
      name: 'Green Bay-Shawano, WI CSA',
      population: 375375,
      area_type: 'CSA',
    },
    {
      index: 2842,
      name: 'Green Bay, WI MSA',
      population: 330292,
      area_type: 'MSA',
    },
    {
      index: 2871,
      name: 'Kewaunee County, Wisconsin',
      population: 20623,
      area_type: 'County',
    },
  ],
  2872: [
    {
      index: 1302,
      name: 'La Crosse-Onalaska, WI-MN MSA',
      population: 139094,
      area_type: 'MSA',
    },
    {
      index: 2872,
      name: 'La Crosse County, Wisconsin',
      population: 120294,
      area_type: 'County',
    },
  ],
  2875: [
    {
      index: 2873,
      name: 'Wausau-Stevens Point-Wisconsin Rapids, WI CSA',
      population: 311045,
      area_type: 'CSA',
    },
    {
      index: 2874,
      name: 'Wausau-Weston, WI MSA',
      population: 166334,
      area_type: 'MSA',
    },
    {
      index: 2875,
      name: 'Lincoln County, Wisconsin',
      population: 28376,
      area_type: 'County',
    },
  ],
  2876: [
    {
      index: 2876,
      name: 'Manitowoc County, Wisconsin (Manitowoc, WI μSA)',
      population: 81172,
      area_type: 'μSA',
    },
  ],
  2878: [
    {
      index: 2873,
      name: 'Wausau-Stevens Point-Wisconsin Rapids, WI CSA',
      population: 311045,
      area_type: 'CSA',
    },
    {
      index: 2874,
      name: 'Wausau-Weston, WI MSA',
      population: 166334,
      area_type: 'MSA',
    },
    {
      index: 2878,
      name: 'Marathon County, Wisconsin',
      population: 137958,
      area_type: 'County',
    },
  ],
  2879: [
    {
      index: 1205,
      name: 'Marinette-Iron Mountain, WI-MI CSA',
      population: 95816,
      area_type: 'CSA',
    },
    {
      index: 1245,
      name: 'Marinette, WI-MI μSA',
      population: 65254,
      area_type: 'μSA',
    },
    {
      index: 2879,
      name: 'Marinette County, Wisconsin',
      population: 41988,
      area_type: 'County',
    },
  ],
  2881: [
    {
      index: 2841,
      name: 'Green Bay-Shawano, WI CSA',
      population: 375375,
      area_type: 'CSA',
    },
    {
      index: 2880,
      name: 'Shawano, WI μSA',
      population: 45083,
      area_type: 'μSA',
    },
    {
      index: 2881,
      name: 'Menominee County, Wisconsin',
      population: 4197,
      area_type: 'County',
    },
  ],
  2883: [
    {
      index: 2854,
      name: 'Milwaukee-Racine-Waukesha, WI CSA',
      population: 2035084,
      area_type: 'CSA',
    },
    {
      index: 2882,
      name: 'Milwaukee-Waukesha, WI MSA',
      population: 1559792,
      area_type: 'MSA',
    },
    {
      index: 2883,
      name: 'Milwaukee County, Wisconsin',
      population: 918661,
      area_type: 'County',
    },
  ],
  2884: [
    {
      index: 2841,
      name: 'Green Bay-Shawano, WI CSA',
      population: 375375,
      area_type: 'CSA',
    },
    {
      index: 2842,
      name: 'Green Bay, WI MSA',
      population: 330292,
      area_type: 'MSA',
    },
    {
      index: 2884,
      name: 'Oconto County, Wisconsin',
      population: 39633,
      area_type: 'County',
    },
  ],
  2885: [
    {
      index: 2844,
      name: 'Appleton-Oshkosh-Neenah, WI CSA',
      population: 415563,
      area_type: 'CSA',
    },
    {
      index: 2845,
      name: 'Appleton, WI MSA',
      population: 244845,
      area_type: 'MSA',
    },
    {
      index: 2885,
      name: 'Outagamie County, Wisconsin',
      population: 192127,
      area_type: 'County',
    },
  ],
  2886: [
    {
      index: 2854,
      name: 'Milwaukee-Racine-Waukesha, WI CSA',
      population: 2035084,
      area_type: 'CSA',
    },
    {
      index: 2882,
      name: 'Milwaukee-Waukesha, WI MSA',
      population: 1559792,
      area_type: 'MSA',
    },
    {
      index: 2886,
      name: 'Ozaukee County, Wisconsin',
      population: 93009,
      area_type: 'County',
    },
  ],
  2887: [
    {
      index: 1268,
      name: 'Minneapolis-St. Paul, MN-WI CSA',
      population: 4085415,
      area_type: 'CSA',
    },
    {
      index: 1269,
      name: 'Minneapolis-St. Paul-Bloomington, MN-WI MSA',
      population: 3693729,
      area_type: 'MSA',
    },
    {
      index: 2887,
      name: 'Pierce County, Wisconsin',
      population: 42532,
      area_type: 'County',
    },
  ],
  2888: [
    {
      index: 2873,
      name: 'Wausau-Stevens Point-Wisconsin Rapids, WI CSA',
      population: 311045,
      area_type: 'CSA',
    },
    {
      index: 2888,
      name: 'Portage County, Wisconsin (Stevens Point, WI μSA)',
      population: 70718,
      area_type: 'μSA',
    },
  ],
  2890: [
    {
      index: 2854,
      name: 'Milwaukee-Racine-Waukesha, WI CSA',
      population: 2035084,
      area_type: 'CSA',
    },
    {
      index: 2890,
      name: 'Racine County, Wisconsin (Racine, WI MSA)',
      population: 195846,
      area_type: 'MSA',
    },
  ],
  2892: [
    {
      index: 2850,
      name: 'Madison-Janesville-Beloit, WI CSA',
      population: 916914,
      area_type: 'CSA',
    },
    {
      index: 2892,
      name: 'Rock County, Wisconsin (Janesville-Beloit, WI MSA)',
      population: 164060,
      area_type: 'MSA',
    },
  ],
  2894: [
    {
      index: 1268,
      name: 'Minneapolis-St. Paul, MN-WI CSA',
      population: 4085415,
      area_type: 'CSA',
    },
    {
      index: 1269,
      name: 'Minneapolis-St. Paul-Bloomington, MN-WI MSA',
      population: 3693729,
      area_type: 'MSA',
    },
    {
      index: 2894,
      name: 'St. Croix County, Wisconsin',
      population: 96017,
      area_type: 'County',
    },
  ],
  2895: [
    {
      index: 2850,
      name: 'Madison-Janesville-Beloit, WI CSA',
      population: 916914,
      area_type: 'CSA',
    },
    {
      index: 2895,
      name: 'Sauk County, Wisconsin (Baraboo, WI μSA)',
      population: 65777,
      area_type: 'μSA',
    },
  ],
  2897: [
    {
      index: 2841,
      name: 'Green Bay-Shawano, WI CSA',
      population: 375375,
      area_type: 'CSA',
    },
    {
      index: 2880,
      name: 'Shawano, WI μSA',
      population: 45083,
      area_type: 'μSA',
    },
    {
      index: 2897,
      name: 'Shawano County, Wisconsin',
      population: 40886,
      area_type: 'County',
    },
  ],
  2898: [
    {
      index: 2898,
      name: 'Sheboygan County, Wisconsin (Sheboygan, WI MSA)',
      population: 117841,
      area_type: 'MSA',
    },
  ],
  2900: [
    {
      index: 2854,
      name: 'Milwaukee-Racine-Waukesha, WI CSA',
      population: 2035084,
      area_type: 'CSA',
    },
    {
      index: 2900,
      name: 'Walworth County, Wisconsin (Whitewater, WI μSA)',
      population: 105380,
      area_type: 'μSA',
    },
  ],
  2902: [
    {
      index: 2854,
      name: 'Milwaukee-Racine-Waukesha, WI CSA',
      population: 2035084,
      area_type: 'CSA',
    },
    {
      index: 2882,
      name: 'Milwaukee-Waukesha, WI MSA',
      population: 1559792,
      area_type: 'MSA',
    },
    {
      index: 2902,
      name: 'Washington County, Wisconsin',
      population: 137688,
      area_type: 'County',
    },
  ],
  2903: [
    {
      index: 2854,
      name: 'Milwaukee-Racine-Waukesha, WI CSA',
      population: 2035084,
      area_type: 'CSA',
    },
    {
      index: 2882,
      name: 'Milwaukee-Waukesha, WI MSA',
      population: 1559792,
      area_type: 'MSA',
    },
    {
      index: 2903,
      name: 'Waukesha County, Wisconsin',
      population: 410434,
      area_type: 'County',
    },
  ],
  2904: [
    {
      index: 2844,
      name: 'Appleton-Oshkosh-Neenah, WI CSA',
      population: 415563,
      area_type: 'CSA',
    },
    {
      index: 2904,
      name: 'Winnebago County, Wisconsin (Oshkosh-Neenah, WI MSA)',
      population: 170718,
      area_type: 'MSA',
    },
  ],
  2906: [
    {
      index: 2873,
      name: 'Wausau-Stevens Point-Wisconsin Rapids, WI CSA',
      population: 311045,
      area_type: 'CSA',
    },
    {
      index: 2906,
      name: 'Wood County, Wisconsin (Wisconsin Rapids-Marshfield, WI μSA)',
      population: 73993,
      area_type: 'μSA',
    },
  ],
  2908: [
    {
      index: 2908,
      name: 'Albany County, Wyoming (Laramie, WY μSA)',
      population: 38031,
      area_type: 'μSA',
    },
  ],
  2911: [
    {
      index: 2910,
      name: 'Gillette, WY μSA',
      population: 61366,
      area_type: 'μSA',
    },
    {
      index: 2911,
      name: 'Campbell County, Wyoming',
      population: 47058,
      area_type: 'County',
    },
  ],
  2912: [
    {
      index: 2910,
      name: 'Gillette, WY μSA',
      population: 61366,
      area_type: 'μSA',
    },
    {
      index: 2912,
      name: 'Crook County, Wyoming',
      population: 7448,
      area_type: 'County',
    },
  ],
  2913: [
    {
      index: 2913,
      name: 'Fremont County, Wyoming (Riverton, WY μSA)',
      population: 39472,
      area_type: 'μSA',
    },
  ],
  2915: [
    {
      index: 2915,
      name: 'Laramie County, Wyoming (Cheyenne, WY MSA)',
      population: 100723,
      area_type: 'MSA',
    },
  ],
  2917: [
    {
      index: 2917,
      name: 'Natrona County, Wyoming (Casper, WY MSA)',
      population: 79601,
      area_type: 'MSA',
    },
  ],
  2919: [
    {
      index: 2919,
      name: 'Sheridan County, Wyoming (Sheridan, WY μSA)',
      population: 32096,
      area_type: 'μSA',
    },
  ],
  2921: [
    {
      index: 2921,
      name: 'Sweetwater County, Wyoming (Rock Springs, WY μSA)',
      population: 41345,
      area_type: 'μSA',
    },
  ],
  2923: [
    {
      index: 611,
      name: 'Jackson, WY-ID μSA',
      population: 35831,
      area_type: 'μSA',
    },
    {
      index: 2923,
      name: 'Teton County, Wyoming',
      population: 23287,
      area_type: 'County',
    },
  ],
  2924: [
    {
      index: 2924,
      name: 'Uinta County, Wyoming (Evanston, WY μSA)',
      population: 20712,
      area_type: 'μSA',
    },
  ],
  2926: [
    {
      index: 2910,
      name: 'Gillette, WY μSA',
      population: 61366,
      area_type: 'μSA',
    },
    {
      index: 2926,
      name: 'Weston County, Wyoming',
      population: 6860,
      area_type: 'County',
    },
  ],
  2927: [
    {
      index: 1593,
      name: 'New York-Newark, NY-NJ-CT-PA CSA',
      population: 23070149,
      area_type: 'CSA',
    },
    {
      index: 2927,
      name: 'Fairfield County, Connecticut (Bridgeport-Stamford-Norwalk, CT MSA)',
      population: 962946,
      area_type: 'MSA',
    },
  ],
  2931: [
    {
      index: 2929,
      name: 'Hartford-East Hartford, CT CSA',
      population: 1490406,
      area_type: 'CSA',
    },
    {
      index: 2930,
      name: 'Hartford-East Hartford-Middletown, CT MSA',
      population: 1221725,
      area_type: 'MSA',
    },
    {
      index: 2931,
      name: 'Hartford County, Connecticut',
      population: 898783,
      area_type: 'County',
    },
  ],
  2932: [
    {
      index: 2929,
      name: 'Hartford-East Hartford, CT CSA',
      population: 1490406,
      area_type: 'CSA',
    },
    {
      index: 2930,
      name: 'Hartford-East Hartford-Middletown, CT MSA',
      population: 1221725,
      area_type: 'MSA',
    },
    {
      index: 2932,
      name: 'Middlesex County, Connecticut',
      population: 166537,
      area_type: 'County',
    },
  ],
  2933: [
    {
      index: 2929,
      name: 'Hartford-East Hartford, CT CSA',
      population: 1490406,
      area_type: 'CSA',
    },
    {
      index: 2930,
      name: 'Hartford-East Hartford-Middletown, CT MSA',
      population: 1221725,
      area_type: 'MSA',
    },
    {
      index: 2933,
      name: 'Tolland County, Connecticut',
      population: 156405,
      area_type: 'County',
    },
  ],
  2934: [
    {
      index: 1593,
      name: 'New York-Newark, NY-NJ-CT-PA CSA',
      population: 23070149,
      area_type: 'CSA',
    },
    {
      index: 2934,
      name: 'New Haven County, Connecticut (New Haven-Milford, CT MSA)',
      population: 869527,
      area_type: 'MSA',
    },
  ],
  2936: [
    {
      index: 2929,
      name: 'Hartford-East Hartford, CT CSA',
      population: 1490406,
      area_type: 'CSA',
    },
    {
      index: 2936,
      name: 'New London County, Connecticut (Norwich-New London, CT MSA)',
      population: 268681,
      area_type: 'MSA',
    },
  ],
  2938: [
    {
      index: 1160,
      name: 'Boston-Worcester-Providence, MA-RI-NH-CT CSA',
      population: 8434341,
      area_type: 'CSA',
    },
    {
      index: 1179,
      name: 'Worcester, MA-CT MSA',
      population: 980137,
      area_type: 'MSA',
    },
    {
      index: 2938,
      name: 'Windham County, Connecticut',
      population: 117210,
      area_type: 'County',
    },
  ],
  2939: [
    {
      index: 1593,
      name: 'New York-Newark, NY-NJ-CT-PA CSA',
      population: 23070149,
      area_type: 'CSA',
    },
    {
      index: 2939,
      name: 'Litchfield County, Connecticut (Torrington, CT μSA)',
      population: 186116,
      area_type: 'μSA',
    },
  ],
  2941: [
    {
      index: 2941,
      name: 'Bullock County, Alabama',
      population: 10202,
      area_type: 'County',
    },
  ],
  2942: [
    {
      index: 2942,
      name: 'Butler County, Alabama',
      population: 18650,
      area_type: 'County',
    },
  ],
  2943: [
    {
      index: 2943,
      name: 'Cherokee County, Alabama',
      population: 25302,
      area_type: 'County',
    },
  ],
  2944: [
    {
      index: 2944,
      name: 'Choctaw County, Alabama',
      population: 12439,
      area_type: 'County',
    },
  ],
  2945: [
    {
      index: 2945,
      name: 'Clarke County, Alabama',
      population: 22515,
      area_type: 'County',
    },
  ],
  2946: [
    {
      index: 2946,
      name: 'Clay County, Alabama',
      population: 14198,
      area_type: 'County',
    },
  ],
  2947: [
    {
      index: 2947,
      name: 'Cleburne County, Alabama',
      population: 15346,
      area_type: 'County',
    },
  ],
  2948: [
    {
      index: 2948,
      name: 'Conecuh County, Alabama',
      population: 11206,
      area_type: 'County',
    },
  ],
  2949: [
    {
      index: 2949,
      name: 'Covington County, Alabama',
      population: 37602,
      area_type: 'County',
    },
  ],
  2950: [
    {
      index: 2950,
      name: 'Crenshaw County, Alabama',
      population: 13025,
      area_type: 'County',
    },
  ],
  2951: [
    {
      index: 2951,
      name: 'Fayette County, Alabama',
      population: 16118,
      area_type: 'County',
    },
  ],
  2952: [
    {
      index: 2952,
      name: 'Franklin County, Alabama',
      population: 31932,
      area_type: 'County',
    },
  ],
  2953: [
    {
      index: 2953,
      name: 'Lamar County, Alabama',
      population: 13705,
      area_type: 'County',
    },
  ],
  2954: [
    {
      index: 2954,
      name: 'Macon County, Alabama',
      population: 18516,
      area_type: 'County',
    },
  ],
  2955: [
    {
      index: 2955,
      name: 'Marengo County, Alabama',
      population: 18745,
      area_type: 'County',
    },
  ],
  2956: [
    {
      index: 2956,
      name: 'Marion County, Alabama',
      population: 29156,
      area_type: 'County',
    },
  ],
  2957: [
    {
      index: 2957,
      name: 'Monroe County, Alabama',
      population: 19404,
      area_type: 'County',
    },
  ],
  2958: [
    {
      index: 2958,
      name: 'Perry County, Alabama',
      population: 8035,
      area_type: 'County',
    },
  ],
  2959: [
    {
      index: 2959,
      name: 'Randolph County, Alabama',
      population: 22479,
      area_type: 'County',
    },
  ],
  2960: [
    {
      index: 2960,
      name: 'Sumter County, Alabama',
      population: 11853,
      area_type: 'County',
    },
  ],
  2961: [
    {
      index: 2961,
      name: 'Wilcox County, Alabama',
      population: 10059,
      area_type: 'County',
    },
  ],
  2962: [
    {
      index: 2962,
      name: 'Winston County, Alabama',
      population: 23755,
      area_type: 'County',
    },
  ],
  2963: [
    {
      index: 2963,
      name: 'Aleutians East Borough, Alaska',
      population: 3398,
      area_type: 'County',
    },
  ],
  2964: [
    {
      index: 2964,
      name: 'Aleutians West Census Area, Alaska',
      population: 5122,
      area_type: 'County',
    },
  ],
  2965: [
    {
      index: 2965,
      name: 'Bethel Census Area, Alaska',
      population: 18257,
      area_type: 'County',
    },
  ],
  2966: [
    {
      index: 2966,
      name: 'Bristol Bay Borough, Alaska',
      population: 865,
      area_type: 'County',
    },
  ],
  2967: [
    {
      index: 2967,
      name: 'Chugach Census Area, Alaska',
      population: 6874,
      area_type: 'County',
    },
  ],
  2968: [
    {
      index: 2968,
      name: 'Copper River Census Area, Alaska',
      population: 2589,
      area_type: 'County',
    },
  ],
  2969: [
    {
      index: 2969,
      name: 'Denali Borough, Alaska',
      population: 1585,
      area_type: 'County',
    },
  ],
  2970: [
    {
      index: 2970,
      name: 'Dillingham Census Area, Alaska',
      population: 4723,
      area_type: 'County',
    },
  ],
  2971: [
    {
      index: 2971,
      name: 'Haines Borough, Alaska',
      population: 2056,
      area_type: 'County',
    },
  ],
  2972: [
    {
      index: 2972,
      name: 'Hoonah-Angoon Census Area, Alaska',
      population: 2287,
      area_type: 'County',
    },
  ],
  2973: [
    {
      index: 2973,
      name: 'Kenai Peninsula Borough, Alaska',
      population: 60690,
      area_type: 'County',
    },
  ],
  2974: [
    {
      index: 2974,
      name: 'Kodiak Island Borough, Alaska',
      population: 12720,
      area_type: 'County',
    },
  ],
  2975: [
    {
      index: 2975,
      name: 'Kusilvak Census Area, Alaska',
      population: 8278,
      area_type: 'County',
    },
  ],
  2976: [
    {
      index: 2976,
      name: 'Lake and Peninsula Borough, Alaska',
      population: 1381,
      area_type: 'County',
    },
  ],
  2977: [
    {
      index: 2977,
      name: 'Nome Census Area, Alaska',
      population: 9835,
      area_type: 'County',
    },
  ],
  2978: [
    {
      index: 2978,
      name: 'North Slope Borough, Alaska',
      population: 10805,
      area_type: 'County',
    },
  ],
  2979: [
    {
      index: 2979,
      name: 'Northwest Arctic Borough, Alaska',
      population: 7423,
      area_type: 'County',
    },
  ],
  2980: [
    {
      index: 2980,
      name: 'Petersburg Borough, Alaska',
      population: 3360,
      area_type: 'County',
    },
  ],
  2981: [
    {
      index: 2981,
      name: 'Prince of Wales-Hyder Census Area, Alaska',
      population: 5650,
      area_type: 'County',
    },
  ],
  2982: [
    {
      index: 2982,
      name: 'Sitka City and Borough, Alaska',
      population: 8382,
      area_type: 'County',
    },
  ],
  2983: [
    {
      index: 2983,
      name: 'Skagway Municipality, Alaska',
      population: 1081,
      area_type: 'County',
    },
  ],
  2984: [
    {
      index: 2984,
      name: 'Southeast Fairbanks Census Area, Alaska',
      population: 7021,
      area_type: 'County',
    },
  ],
  2985: [
    {
      index: 2985,
      name: 'Wrangell City and Borough, Alaska',
      population: 2070,
      area_type: 'County',
    },
  ],
  2986: [
    {
      index: 2986,
      name: 'Yakutat City and Borough, Alaska',
      population: 700,
      area_type: 'County',
    },
  ],
  2987: [
    {
      index: 2987,
      name: 'Yukon-Koyukuk Census Area, Alaska',
      population: 5179,
      area_type: 'County',
    },
  ],
  2988: [
    {
      index: 2988,
      name: 'Apache County, Arizona',
      population: 65432,
      area_type: 'County',
    },
  ],
  2989: [
    {
      index: 2989,
      name: 'Greenlee County, Arizona',
      population: 9302,
      area_type: 'County',
    },
  ],
  2990: [
    {
      index: 2990,
      name: 'La Paz County, Arizona',
      population: 16506,
      area_type: 'County',
    },
  ],
  2991: [
    {
      index: 2991,
      name: 'Arkansas County, Arkansas',
      population: 16512,
      area_type: 'County',
    },
  ],
  2992: [
    {
      index: 2992,
      name: 'Ashley County, Arkansas',
      population: 18354,
      area_type: 'County',
    },
  ],
  2993: [
    {
      index: 2993,
      name: 'Bradley County, Arkansas',
      population: 10135,
      area_type: 'County',
    },
  ],
  2994: [
    {
      index: 2994,
      name: 'Carroll County, Arkansas',
      population: 28742,
      area_type: 'County',
    },
  ],
  2995: [
    {
      index: 2995,
      name: 'Chicot County, Arkansas',
      population: 9873,
      area_type: 'County',
    },
  ],
  2996: [
    {
      index: 2996,
      name: 'Clay County, Arkansas',
      population: 14265,
      area_type: 'County',
    },
  ],
  2997: [
    {
      index: 2997,
      name: 'Cleburne County, Arkansas',
      population: 25284,
      area_type: 'County',
    },
  ],
  2998: [
    {
      index: 2998,
      name: 'Conway County, Arkansas',
      population: 21046,
      area_type: 'County',
    },
  ],
  2999: [
    {
      index: 2999,
      name: 'Cross County, Arkansas',
      population: 16601,
      area_type: 'County',
    },
  ],
  3000: [
    {
      index: 3000,
      name: 'Dallas County, Arkansas',
      population: 6191,
      area_type: 'County',
    },
  ],
  3001: [
    {
      index: 3001,
      name: 'Desha County, Arkansas',
      population: 10771,
      area_type: 'County',
    },
  ],
  3002: [
    {
      index: 3002,
      name: 'Drew County, Arkansas',
      population: 16911,
      area_type: 'County',
    },
  ],
  3003: [
    {
      index: 3003,
      name: 'Fulton County, Arkansas',
      population: 12382,
      area_type: 'County',
    },
  ],
  3004: [
    {
      index: 3004,
      name: 'Howard County, Arkansas',
      population: 12557,
      area_type: 'County',
    },
  ],
  3005: [
    {
      index: 3005,
      name: 'Izard County, Arkansas',
      population: 14048,
      area_type: 'County',
    },
  ],
  3006: [
    {
      index: 3006,
      name: 'Jackson County, Arkansas',
      population: 16624,
      area_type: 'County',
    },
  ],
  3007: [
    {
      index: 3007,
      name: 'Johnson County, Arkansas',
      population: 26001,
      area_type: 'County',
    },
  ],
  3008: [
    {
      index: 3008,
      name: 'Lafayette County, Arkansas',
      population: 6101,
      area_type: 'County',
    },
  ],
  3009: [
    {
      index: 3009,
      name: 'Lawrence County, Arkansas',
      population: 16205,
      area_type: 'County',
    },
  ],
  3010: [
    {
      index: 3010,
      name: 'Lee County, Arkansas',
      population: 8364,
      area_type: 'County',
    },
  ],
  3011: [
    {
      index: 3011,
      name: 'Logan County, Arkansas',
      population: 21253,
      area_type: 'County',
    },
  ],
  3012: [
    {
      index: 3012,
      name: 'Marion County, Arkansas',
      population: 17254,
      area_type: 'County',
    },
  ],
  3013: [
    {
      index: 3013,
      name: 'Monroe County, Arkansas',
      population: 6564,
      area_type: 'County',
    },
  ],
  3014: [
    {
      index: 3014,
      name: 'Montgomery County, Arkansas',
      population: 8556,
      area_type: 'County',
    },
  ],
  3015: [
    {
      index: 3015,
      name: 'Pike County, Arkansas',
      population: 10179,
      area_type: 'County',
    },
  ],
  3016: [
    {
      index: 3016,
      name: 'Polk County, Arkansas',
      population: 19337,
      area_type: 'County',
    },
  ],
  3017: [
    {
      index: 3017,
      name: 'Prairie County, Arkansas',
      population: 8069,
      area_type: 'County',
    },
  ],
  3018: [
    {
      index: 3018,
      name: 'Randolph County, Arkansas',
      population: 18837,
      area_type: 'County',
    },
  ],
  3019: [
    {
      index: 3019,
      name: 'Scott County, Arkansas',
      population: 9805,
      area_type: 'County',
    },
  ],
  3020: [
    {
      index: 3020,
      name: 'Searcy County, Arkansas',
      population: 7918,
      area_type: 'County',
    },
  ],
  3021: [
    {
      index: 3021,
      name: 'Sevier County, Arkansas',
      population: 15686,
      area_type: 'County',
    },
  ],
  3022: [
    {
      index: 3022,
      name: 'Stone County, Arkansas',
      population: 12575,
      area_type: 'County',
    },
  ],
  3023: [
    {
      index: 3023,
      name: 'Van Buren County, Arkansas',
      population: 16102,
      area_type: 'County',
    },
  ],
  3024: [
    {
      index: 3024,
      name: 'Woodruff County, Arkansas',
      population: 6049,
      area_type: 'County',
    },
  ],
  3025: [
    {
      index: 3025,
      name: 'Alpine County, California',
      population: 1190,
      area_type: 'County',
    },
  ],
  3026: [
    {
      index: 3026,
      name: 'Amador County, California',
      population: 41412,
      area_type: 'County',
    },
  ],
  3027: [
    {
      index: 3027,
      name: 'Calaveras County, California',
      population: 46563,
      area_type: 'County',
    },
  ],
  3028: [
    {
      index: 3028,
      name: 'Colusa County, California',
      population: 21914,
      area_type: 'County',
    },
  ],
  3029: [
    {
      index: 3029,
      name: 'Glenn County, California',
      population: 28339,
      area_type: 'County',
    },
  ],
  3030: [
    {
      index: 3030,
      name: 'Inyo County, California',
      population: 18718,
      area_type: 'County',
    },
  ],
  3031: [
    {
      index: 3031,
      name: 'Mariposa County, California',
      population: 17020,
      area_type: 'County',
    },
  ],
  3032: [
    {
      index: 3032,
      name: 'Modoc County, California',
      population: 8511,
      area_type: 'County',
    },
  ],
  3033: [
    {
      index: 3033,
      name: 'Mono County, California',
      population: 12978,
      area_type: 'County',
    },
  ],
  3034: [
    {
      index: 3034,
      name: 'Plumas County, California',
      population: 19351,
      area_type: 'County',
    },
  ],
  3035: [
    {
      index: 3035,
      name: 'Sierra County, California',
      population: 3217,
      area_type: 'County',
    },
  ],
  3036: [
    {
      index: 3036,
      name: 'Siskiyou County, California',
      population: 43660,
      area_type: 'County',
    },
  ],
  3037: [
    {
      index: 3037,
      name: 'Trinity County, California',
      population: 15781,
      area_type: 'County',
    },
  ],
  3038: [
    {
      index: 3038,
      name: 'Alamosa County, Colorado',
      population: 16592,
      area_type: 'County',
    },
  ],
  3039: [
    {
      index: 3039,
      name: 'Archuleta County, Colorado',
      population: 14003,
      area_type: 'County',
    },
  ],
  3040: [
    {
      index: 3040,
      name: 'Baca County, Colorado',
      population: 3432,
      area_type: 'County',
    },
  ],
  3041: [
    {
      index: 3041,
      name: 'Bent County, Colorado',
      population: 5399,
      area_type: 'County',
    },
  ],
  3042: [
    {
      index: 3042,
      name: 'Chaffee County, Colorado',
      population: 20223,
      area_type: 'County',
    },
  ],
  3043: [
    {
      index: 3043,
      name: 'Cheyenne County, Colorado',
      population: 1732,
      area_type: 'County',
    },
  ],
  3044: [
    {
      index: 3044,
      name: 'Conejos County, Colorado',
      population: 7579,
      area_type: 'County',
    },
  ],
  3045: [
    {
      index: 3045,
      name: 'Costilla County, Colorado',
      population: 3603,
      area_type: 'County',
    },
  ],
  3046: [
    {
      index: 3046,
      name: 'Crowley County, Colorado',
      population: 5614,
      area_type: 'County',
    },
  ],
  3047: [
    {
      index: 3047,
      name: 'Custer County, Colorado',
      population: 5335,
      area_type: 'County',
    },
  ],
  3048: [
    {
      index: 3048,
      name: 'Delta County, Colorado',
      population: 31602,
      area_type: 'County',
    },
  ],
  3049: [
    {
      index: 3049,
      name: 'Dolores County, Colorado',
      population: 2455,
      area_type: 'County',
    },
  ],
  3050: [
    {
      index: 3050,
      name: 'Grand County, Colorado',
      population: 15769,
      area_type: 'County',
    },
  ],
  3051: [
    {
      index: 3051,
      name: 'Gunnison County, Colorado',
      population: 17267,
      area_type: 'County',
    },
  ],
  3052: [
    {
      index: 3052,
      name: 'Hinsdale County, Colorado',
      population: 775,
      area_type: 'County',
    },
  ],
  3053: [
    {
      index: 3053,
      name: 'Huerfano County, Colorado',
      population: 7082,
      area_type: 'County',
    },
  ],
  3054: [
    {
      index: 3054,
      name: 'Jackson County, Colorado',
      population: 1302,
      area_type: 'County',
    },
  ],
  3055: [
    {
      index: 3055,
      name: 'Kiowa County, Colorado',
      population: 1424,
      area_type: 'County',
    },
  ],
  3056: [
    {
      index: 3056,
      name: 'Kit Carson County, Colorado',
      population: 6961,
      area_type: 'County',
    },
  ],
  3057: [
    {
      index: 3057,
      name: 'Lake County, Colorado',
      population: 7327,
      area_type: 'County',
    },
  ],
  3058: [
    {
      index: 3058,
      name: 'Las Animas County, Colorado',
      population: 14327,
      area_type: 'County',
    },
  ],
  3059: [
    {
      index: 3059,
      name: 'Lincoln County, Colorado',
      population: 5510,
      area_type: 'County',
    },
  ],
  3060: [
    {
      index: 3060,
      name: 'Mineral County, Colorado',
      population: 931,
      area_type: 'County',
    },
  ],
  3061: [
    {
      index: 3061,
      name: 'Montezuma County, Colorado',
      population: 26468,
      area_type: 'County',
    },
  ],
  3062: [
    {
      index: 3062,
      name: 'Otero County, Colorado',
      population: 18303,
      area_type: 'County',
    },
  ],
  3063: [
    {
      index: 3063,
      name: 'Phillips County, Colorado',
      population: 4449,
      area_type: 'County',
    },
  ],
  3064: [
    {
      index: 3064,
      name: 'Prowers County, Colorado',
      population: 11854,
      area_type: 'County',
    },
  ],
  3065: [
    {
      index: 3065,
      name: 'Rio Blanco County, Colorado',
      population: 6569,
      area_type: 'County',
    },
  ],
  3066: [
    {
      index: 3066,
      name: 'Rio Grande County, Colorado',
      population: 11325,
      area_type: 'County',
    },
  ],
  3067: [
    {
      index: 3067,
      name: 'Saguache County, Colorado',
      population: 6623,
      area_type: 'County',
    },
  ],
  3068: [
    {
      index: 3068,
      name: 'San Juan County, Colorado',
      population: 803,
      area_type: 'County',
    },
  ],
  3069: [
    {
      index: 3069,
      name: 'San Miguel County, Colorado',
      population: 8003,
      area_type: 'County',
    },
  ],
  3070: [
    {
      index: 3070,
      name: 'Sedgwick County, Colorado',
      population: 2295,
      area_type: 'County',
    },
  ],
  3071: [
    {
      index: 3071,
      name: 'Washington County, Colorado',
      population: 4812,
      area_type: 'County',
    },
  ],
  3072: [
    {
      index: 3072,
      name: 'Yuma County, Colorado',
      population: 9899,
      area_type: 'County',
    },
  ],
  3073: [
    {
      index: 3073,
      name: 'Bradford County, Florida',
      population: 27313,
      area_type: 'County',
    },
  ],
  3074: [
    {
      index: 3074,
      name: 'Calhoun County, Florida',
      population: 13464,
      area_type: 'County',
    },
  ],
  3075: [
    {
      index: 3075,
      name: 'Dixie County, Florida',
      population: 17124,
      area_type: 'County',
    },
  ],
  3076: [
    {
      index: 3076,
      name: 'Franklin County, Florida',
      population: 12498,
      area_type: 'County',
    },
  ],
  3077: [
    {
      index: 3077,
      name: 'Glades County, Florida',
      population: 12454,
      area_type: 'County',
    },
  ],
  3078: [
    {
      index: 3078,
      name: 'Gulf County, Florida',
      population: 15314,
      area_type: 'County',
    },
  ],
  3079: [
    {
      index: 3079,
      name: 'Hamilton County, Florida',
      population: 13217,
      area_type: 'County',
    },
  ],
  3080: [
    {
      index: 3080,
      name: 'Holmes County, Florida',
      population: 19651,
      area_type: 'County',
    },
  ],
  3081: [
    {
      index: 3081,
      name: 'Jackson County, Florida',
      population: 48211,
      area_type: 'County',
    },
  ],
  3082: [
    {
      index: 3082,
      name: 'Lafayette County, Florida',
      population: 7786,
      area_type: 'County',
    },
  ],
  3083: [
    {
      index: 3083,
      name: 'Liberty County, Florida',
      population: 7603,
      area_type: 'County',
    },
  ],
  3084: [
    {
      index: 3084,
      name: 'Madison County, Florida',
      population: 18198,
      area_type: 'County',
    },
  ],
  3085: [
    {
      index: 3085,
      name: 'Suwannee County, Florida',
      population: 45411,
      area_type: 'County',
    },
  ],
  3086: [
    {
      index: 3086,
      name: 'Taylor County, Florida',
      population: 21283,
      area_type: 'County',
    },
  ],
  3087: [
    {
      index: 3087,
      name: 'Union County, Florida',
      population: 15460,
      area_type: 'County',
    },
  ],
  3088: [
    {
      index: 3088,
      name: 'Washington County, Florida',
      population: 25414,
      area_type: 'County',
    },
  ],
  3089: [
    {
      index: 3089,
      name: 'Appling County, Georgia',
      population: 18428,
      area_type: 'County',
    },
  ],
  3090: [
    {
      index: 3090,
      name: 'Bacon County, Georgia',
      population: 11191,
      area_type: 'County',
    },
  ],
  3091: [
    {
      index: 3091,
      name: 'Baker County, Georgia',
      population: 2788,
      area_type: 'County',
    },
  ],
  3092: [
    {
      index: 3092,
      name: 'Banks County, Georgia',
      population: 19328,
      area_type: 'County',
    },
  ],
  3093: [
    {
      index: 3093,
      name: 'Berrien County, Georgia',
      population: 18214,
      area_type: 'County',
    },
  ],
  3094: [
    {
      index: 3094,
      name: 'Bleckley County, Georgia',
      population: 12257,
      area_type: 'County',
    },
  ],
  3095: [
    {
      index: 3095,
      name: 'Calhoun County, Georgia',
      population: 5469,
      area_type: 'County',
    },
  ],
  3096: [
    {
      index: 3096,
      name: 'Candler County, Georgia',
      population: 11000,
      area_type: 'County',
    },
  ],
  3097: [
    {
      index: 3097,
      name: 'Charlton County, Georgia',
      population: 12781,
      area_type: 'County',
    },
  ],
  3098: [
    {
      index: 3098,
      name: 'Clay County, Georgia',
      population: 2845,
      area_type: 'County',
    },
  ],
  3099: [
    {
      index: 3099,
      name: 'Clinch County, Georgia',
      population: 6662,
      area_type: 'County',
    },
  ],
  3100: [
    {
      index: 3100,
      name: 'Cook County, Georgia',
      population: 17404,
      area_type: 'County',
    },
  ],
  3101: [
    {
      index: 3101,
      name: 'Dodge County, Georgia',
      population: 19802,
      area_type: 'County',
    },
  ],
  3102: [
    {
      index: 3102,
      name: 'Dooly County, Georgia',
      population: 10572,
      area_type: 'County',
    },
  ],
  3103: [
    {
      index: 3103,
      name: 'Early County, Georgia',
      population: 10574,
      area_type: 'County',
    },
  ],
  3104: [
    {
      index: 3104,
      name: 'Elbert County, Georgia',
      population: 19814,
      area_type: 'County',
    },
  ],
  3105: [
    {
      index: 3105,
      name: 'Emanuel County, Georgia',
      population: 22929,
      area_type: 'County',
    },
  ],
  3106: [
    {
      index: 3106,
      name: 'Evans County, Georgia',
      population: 10695,
      area_type: 'County',
    },
  ],
  3107: [
    {
      index: 3107,
      name: 'Fannin County, Georgia',
      population: 25737,
      area_type: 'County',
    },
  ],
  3108: [
    {
      index: 3108,
      name: 'Franklin County, Georgia',
      population: 24128,
      area_type: 'County',
    },
  ],
  3109: [
    {
      index: 3109,
      name: 'Gilmer County, Georgia',
      population: 32407,
      area_type: 'County',
    },
  ],
  3110: [
    {
      index: 3110,
      name: 'Glascock County, Georgia',
      population: 2939,
      area_type: 'County',
    },
  ],
  3111: [
    {
      index: 3111,
      name: 'Grady County, Georgia',
      population: 26008,
      area_type: 'County',
    },
  ],
  3112: [
    {
      index: 3112,
      name: 'Greene County, Georgia',
      population: 20139,
      area_type: 'County',
    },
  ],
  3113: [
    {
      index: 3113,
      name: 'Hart County, Georgia',
      population: 26909,
      area_type: 'County',
    },
  ],
  3114: [
    {
      index: 3114,
      name: 'Irwin County, Georgia',
      population: 9126,
      area_type: 'County',
    },
  ],
  3115: [
    {
      index: 3115,
      name: 'Jeff Davis County, Georgia',
      population: 14889,
      area_type: 'County',
    },
  ],
  3116: [
    {
      index: 3116,
      name: 'Jefferson County, Georgia',
      population: 15314,
      area_type: 'County',
    },
  ],
  3117: [
    {
      index: 3117,
      name: 'Jenkins County, Georgia',
      population: 8689,
      area_type: 'County',
    },
  ],
  3118: [
    {
      index: 3118,
      name: 'Lumpkin County, Georgia',
      population: 34796,
      area_type: 'County',
    },
  ],
  3119: [
    {
      index: 3119,
      name: 'Macon County, Georgia',
      population: 11765,
      area_type: 'County',
    },
  ],
  3120: [
    {
      index: 3120,
      name: 'Miller County, Georgia',
      population: 5807,
      area_type: 'County',
    },
  ],
  3121: [
    {
      index: 3121,
      name: 'Mitchell County, Georgia',
      population: 21116,
      area_type: 'County',
    },
  ],
  3122: [
    {
      index: 3122,
      name: 'Pulaski County, Georgia',
      population: 9984,
      area_type: 'County',
    },
  ],
  3123: [
    {
      index: 3123,
      name: 'Putnam County, Georgia',
      population: 22984,
      area_type: 'County',
    },
  ],
  3124: [
    {
      index: 3124,
      name: 'Rabun County, Georgia',
      population: 17206,
      area_type: 'County',
    },
  ],
  3125: [
    {
      index: 3125,
      name: 'Randolph County, Georgia',
      population: 6116,
      area_type: 'County',
    },
  ],
  3126: [
    {
      index: 3126,
      name: 'Screven County, Georgia',
      population: 13977,
      area_type: 'County',
    },
  ],
  3127: [
    {
      index: 3127,
      name: 'Seminole County, Georgia',
      population: 9127,
      area_type: 'County',
    },
  ],
  3128: [
    {
      index: 3128,
      name: 'Taliaferro County, Georgia',
      population: 1600,
      area_type: 'County',
    },
  ],
  3129: [
    {
      index: 3129,
      name: 'Tattnall County, Georgia',
      population: 24064,
      area_type: 'County',
    },
  ],
  3130: [
    {
      index: 3130,
      name: 'Taylor County, Georgia',
      population: 7737,
      area_type: 'County',
    },
  ],
  3131: [
    {
      index: 3131,
      name: 'Telfair County, Georgia',
      population: 12354,
      area_type: 'County',
    },
  ],
  3132: [
    {
      index: 3132,
      name: 'Towns County, Georgia',
      population: 12972,
      area_type: 'County',
    },
  ],
  3133: [
    {
      index: 3133,
      name: 'Turner County, Georgia',
      population: 8842,
      area_type: 'County',
    },
  ],
  3134: [
    {
      index: 3134,
      name: 'Union County, Georgia',
      population: 26388,
      area_type: 'County',
    },
  ],
  3135: [
    {
      index: 3135,
      name: 'Warren County, Georgia',
      population: 5155,
      area_type: 'County',
    },
  ],
  3136: [
    {
      index: 3136,
      name: 'Washington County, Georgia',
      population: 19738,
      area_type: 'County',
    },
  ],
  3137: [
    {
      index: 3137,
      name: 'Webster County, Georgia',
      population: 2328,
      area_type: 'County',
    },
  ],
  3138: [
    {
      index: 3138,
      name: 'Wheeler County, Georgia',
      population: 7314,
      area_type: 'County',
    },
  ],
  3139: [
    {
      index: 3139,
      name: 'White County, Georgia',
      population: 28806,
      area_type: 'County',
    },
  ],
  3140: [
    {
      index: 3140,
      name: 'Wilcox County, Georgia',
      population: 8761,
      area_type: 'County',
    },
  ],
  3141: [
    {
      index: 3141,
      name: 'Wilkes County, Georgia',
      population: 9599,
      area_type: 'County',
    },
  ],
  3142: [
    {
      index: 3142,
      name: 'Wilkinson County, Georgia',
      population: 8681,
      area_type: 'County',
    },
  ],
  3143: [
    {
      index: 3143,
      name: 'Kalawao County, Hawaii',
      population: 82,
      area_type: 'County',
    },
  ],
  3144: [
    {
      index: 3144,
      name: 'Adams County, Idaho',
      population: 4817,
      area_type: 'County',
    },
  ],
  3145: [
    {
      index: 3145,
      name: 'Bear Lake County, Idaho',
      population: 6722,
      area_type: 'County',
    },
  ],
  3146: [
    {
      index: 3146,
      name: 'Benewah County, Idaho',
      population: 10370,
      area_type: 'County',
    },
  ],
  3147: [
    {
      index: 3147,
      name: 'Boundary County, Idaho',
      population: 13345,
      area_type: 'County',
    },
  ],
  3148: [
    {
      index: 3148,
      name: 'Caribou County, Idaho',
      population: 7190,
      area_type: 'County',
    },
  ],
  3149: [
    {
      index: 3149,
      name: 'Clark County, Idaho',
      population: 806,
      area_type: 'County',
    },
  ],
  3150: [
    {
      index: 3150,
      name: 'Clearwater County, Idaho',
      population: 9015,
      area_type: 'County',
    },
  ],
  3151: [
    {
      index: 3151,
      name: 'Custer County, Idaho',
      population: 4506,
      area_type: 'County',
    },
  ],
  3152: [
    {
      index: 3152,
      name: 'Gooding County, Idaho',
      population: 15715,
      area_type: 'County',
    },
  ],
  3153: [
    {
      index: 3153,
      name: 'Idaho County, Idaho',
      population: 17593,
      area_type: 'County',
    },
  ],
  3154: [
    {
      index: 3154,
      name: 'Lemhi County, Idaho',
      population: 8240,
      area_type: 'County',
    },
  ],
  3155: [
    {
      index: 3155,
      name: 'Lewis County, Idaho',
      population: 3763,
      area_type: 'County',
    },
  ],
  3156: [
    {
      index: 3156,
      name: 'Lincoln County, Idaho',
      population: 5329,
      area_type: 'County',
    },
  ],
  3157: [
    {
      index: 3157,
      name: 'Oneida County, Idaho',
      population: 4712,
      area_type: 'County',
    },
  ],
  3158: [
    {
      index: 3158,
      name: 'Shoshone County, Idaho',
      population: 14012,
      area_type: 'County',
    },
  ],
  3159: [
    {
      index: 3159,
      name: 'Valley County, Idaho',
      population: 12464,
      area_type: 'County',
    },
  ],
  3160: [
    {
      index: 3160,
      name: 'Washington County, Idaho',
      population: 11087,
      area_type: 'County',
    },
  ],
  3161: [
    {
      index: 3161,
      name: 'Brown County, Illinois',
      population: 6330,
      area_type: 'County',
    },
  ],
  3162: [
    {
      index: 3162,
      name: 'Carroll County, Illinois',
      population: 15529,
      area_type: 'County',
    },
  ],
  3163: [
    {
      index: 3163,
      name: 'Cass County, Illinois',
      population: 12657,
      area_type: 'County',
    },
  ],
  3164: [
    {
      index: 3164,
      name: 'Clark County, Illinois',
      population: 15229,
      area_type: 'County',
    },
  ],
  3165: [
    {
      index: 3165,
      name: 'Clay County, Illinois',
      population: 13047,
      area_type: 'County',
    },
  ],
  3166: [
    {
      index: 3166,
      name: 'Crawford County, Illinois',
      population: 18536,
      area_type: 'County',
    },
  ],
  3167: [
    {
      index: 3167,
      name: 'De Witt County, Illinois',
      population: 15310,
      area_type: 'County',
    },
  ],
  3168: [
    {
      index: 3168,
      name: 'Douglas County, Illinois',
      population: 19755,
      area_type: 'County',
    },
  ],
  3169: [
    {
      index: 3169,
      name: 'Edgar County, Illinois',
      population: 16433,
      area_type: 'County',
    },
  ],
  3170: [
    {
      index: 3170,
      name: 'Edwards County, Illinois',
      population: 6071,
      area_type: 'County',
    },
  ],
  3171: [
    {
      index: 3171,
      name: 'Fayette County, Illinois',
      population: 21305,
      area_type: 'County',
    },
  ],
  3172: [
    {
      index: 3172,
      name: 'Ford County, Illinois',
      population: 13249,
      area_type: 'County',
    },
  ],
  3173: [
    {
      index: 3173,
      name: 'Franklin County, Illinois',
      population: 37242,
      area_type: 'County',
    },
  ],
  3174: [
    {
      index: 3174,
      name: 'Gallatin County, Illinois',
      population: 4855,
      area_type: 'County',
    },
  ],
  3175: [
    {
      index: 3175,
      name: 'Greene County, Illinois',
      population: 11651,
      area_type: 'County',
    },
  ],
  3176: [
    {
      index: 3176,
      name: 'Hamilton County, Illinois',
      population: 7984,
      area_type: 'County',
    },
  ],
  3177: [
    {
      index: 3177,
      name: 'Hardin County, Illinois',
      population: 3597,
      area_type: 'County',
    },
  ],
  3178: [
    {
      index: 3178,
      name: 'Iroquois County, Illinois',
      population: 26473,
      area_type: 'County',
    },
  ],
  3179: [
    {
      index: 3179,
      name: 'Jasper County, Illinois',
      population: 9212,
      area_type: 'County',
    },
  ],
  3180: [
    {
      index: 3180,
      name: 'Jo Daviess County, Illinois',
      population: 21758,
      area_type: 'County',
    },
  ],
  3181: [
    {
      index: 3181,
      name: 'Lawrence County, Illinois',
      population: 14914,
      area_type: 'County',
    },
  ],
  3182: [
    {
      index: 3182,
      name: 'Mason County, Illinois',
      population: 12748,
      area_type: 'County',
    },
  ],
  3183: [
    {
      index: 3183,
      name: 'Montgomery County, Illinois',
      population: 28020,
      area_type: 'County',
    },
  ],
  3184: [
    {
      index: 3184,
      name: 'Moultrie County, Illinois',
      population: 14323,
      area_type: 'County',
    },
  ],
  3185: [
    {
      index: 3185,
      name: 'Perry County, Illinois',
      population: 20588,
      area_type: 'County',
    },
  ],
  3186: [
    {
      index: 3186,
      name: 'Pike County, Illinois',
      population: 14484,
      area_type: 'County',
    },
  ],
  3187: [
    {
      index: 3187,
      name: 'Pope County, Illinois',
      population: 3770,
      area_type: 'County',
    },
  ],
  3188: [
    {
      index: 3188,
      name: 'Pulaski County, Illinois',
      population: 4991,
      area_type: 'County',
    },
  ],
  3189: [
    {
      index: 3189,
      name: 'Randolph County, Illinois',
      population: 30068,
      area_type: 'County',
    },
  ],
  3190: [
    {
      index: 3190,
      name: 'Richland County, Illinois',
      population: 15435,
      area_type: 'County',
    },
  ],
  3191: [
    {
      index: 3191,
      name: 'Saline County, Illinois',
      population: 23087,
      area_type: 'County',
    },
  ],
  3192: [
    {
      index: 3192,
      name: 'Schuyler County, Illinois',
      population: 6746,
      area_type: 'County',
    },
  ],
  3193: [
    {
      index: 3193,
      name: 'Shelby County, Illinois',
      population: 20761,
      area_type: 'County',
    },
  ],
  3194: [
    {
      index: 3194,
      name: 'Union County, Illinois',
      population: 16767,
      area_type: 'County',
    },
  ],
  3195: [
    {
      index: 3195,
      name: 'Wabash County, Illinois',
      population: 11087,
      area_type: 'County',
    },
  ],
  3196: [
    {
      index: 3196,
      name: 'Warren County, Illinois',
      population: 16354,
      area_type: 'County',
    },
  ],
  3197: [
    {
      index: 3197,
      name: 'Washington County, Illinois',
      population: 13643,
      area_type: 'County',
    },
  ],
  3198: [
    {
      index: 3198,
      name: 'Wayne County, Illinois',
      population: 15872,
      area_type: 'County',
    },
  ],
  3199: [
    {
      index: 3199,
      name: 'White County, Illinois',
      population: 13614,
      area_type: 'County',
    },
  ],
  3200: [
    {
      index: 3200,
      name: 'Blackford County, Indiana',
      population: 11919,
      area_type: 'County',
    },
  ],
  3201: [
    {
      index: 3201,
      name: 'Crawford County, Indiana',
      population: 10536,
      area_type: 'County',
    },
  ],
  3202: [
    {
      index: 3202,
      name: 'Fountain County, Indiana',
      population: 16574,
      area_type: 'County',
    },
  ],
  3203: [
    {
      index: 3203,
      name: 'Fulton County, Indiana',
      population: 20327,
      area_type: 'County',
    },
  ],
  3204: [
    {
      index: 3204,
      name: 'Gibson County, Indiana',
      population: 32993,
      area_type: 'County',
    },
  ],
  3205: [
    {
      index: 3205,
      name: 'Greene County, Indiana',
      population: 31006,
      area_type: 'County',
    },
  ],
  3206: [
    {
      index: 3206,
      name: 'Jay County, Indiana',
      population: 20198,
      area_type: 'County',
    },
  ],
  3207: [
    {
      index: 3207,
      name: 'LaGrange County, Indiana',
      population: 40866,
      area_type: 'County',
    },
  ],
  3208: [
    {
      index: 3208,
      name: 'Martin County, Indiana',
      population: 9803,
      area_type: 'County',
    },
  ],
  3209: [
    {
      index: 3209,
      name: 'Orange County, Indiana',
      population: 19623,
      area_type: 'County',
    },
  ],
  3210: [
    {
      index: 3210,
      name: 'Perry County, Indiana',
      population: 19183,
      area_type: 'County',
    },
  ],
  3211: [
    {
      index: 3211,
      name: 'Pulaski County, Indiana',
      population: 12485,
      area_type: 'County',
    },
  ],
  3212: [
    {
      index: 3212,
      name: 'Randolph County, Indiana',
      population: 24437,
      area_type: 'County',
    },
  ],
  3213: [
    {
      index: 3213,
      name: 'Ripley County, Indiana',
      population: 29087,
      area_type: 'County',
    },
  ],
  3214: [
    {
      index: 3214,
      name: 'Rush County, Indiana',
      population: 16673,
      area_type: 'County',
    },
  ],
  3215: [
    {
      index: 3215,
      name: 'Spencer County, Indiana',
      population: 19967,
      area_type: 'County',
    },
  ],
  3216: [
    {
      index: 3216,
      name: 'Starke County, Indiana',
      population: 23258,
      area_type: 'County',
    },
  ],
  3217: [
    {
      index: 3217,
      name: 'Switzerland County, Indiana',
      population: 10006,
      area_type: 'County',
    },
  ],
  3218: [
    {
      index: 3218,
      name: 'Tipton County, Indiana',
      population: 15361,
      area_type: 'County',
    },
  ],
  3219: [
    {
      index: 3219,
      name: 'White County, Indiana',
      population: 24598,
      area_type: 'County',
    },
  ],
  3220: [
    {
      index: 3220,
      name: 'Adair County, Iowa',
      population: 7494,
      area_type: 'County',
    },
  ],
  3221: [
    {
      index: 3221,
      name: 'Adams County, Iowa',
      population: 3611,
      area_type: 'County',
    },
  ],
  3222: [
    {
      index: 3222,
      name: 'Allamakee County, Iowa',
      population: 13960,
      area_type: 'County',
    },
  ],
  3223: [
    {
      index: 3223,
      name: 'Appanoose County, Iowa',
      population: 12094,
      area_type: 'County',
    },
  ],
  3224: [
    {
      index: 3224,
      name: 'Audubon County, Iowa',
      population: 5598,
      area_type: 'County',
    },
  ],
  3225: [
    {
      index: 3225,
      name: 'Buchanan County, Iowa',
      population: 20714,
      area_type: 'County',
    },
  ],
  3226: [
    {
      index: 3226,
      name: 'Butler County, Iowa',
      population: 14269,
      area_type: 'County',
    },
  ],
  3227: [
    {
      index: 3227,
      name: 'Calhoun County, Iowa',
      population: 9725,
      area_type: 'County',
    },
  ],
  3228: [
    {
      index: 3228,
      name: 'Cass County, Iowa',
      population: 13104,
      area_type: 'County',
    },
  ],
  3229: [
    {
      index: 3229,
      name: 'Cedar County, Iowa',
      population: 18399,
      area_type: 'County',
    },
  ],
  3230: [
    {
      index: 3230,
      name: 'Cherokee County, Iowa',
      population: 11491,
      area_type: 'County',
    },
  ],
  3231: [
    {
      index: 3231,
      name: 'Chickasaw County, Iowa',
      population: 11716,
      area_type: 'County',
    },
  ],
  3232: [
    {
      index: 3232,
      name: 'Clarke County, Iowa',
      population: 9692,
      area_type: 'County',
    },
  ],
  3233: [
    {
      index: 3233,
      name: 'Clayton County, Iowa',
      population: 17027,
      area_type: 'County',
    },
  ],
  3234: [
    {
      index: 3234,
      name: 'Crawford County, Iowa',
      population: 16123,
      area_type: 'County',
    },
  ],
  3235: [
    {
      index: 3235,
      name: 'Davis County, Iowa',
      population: 9130,
      area_type: 'County',
    },
  ],
  3236: [
    {
      index: 3236,
      name: 'Decatur County, Iowa',
      population: 7683,
      area_type: 'County',
    },
  ],
  3237: [
    {
      index: 3237,
      name: 'Delaware County, Iowa',
      population: 17568,
      area_type: 'County',
    },
  ],
  3238: [
    {
      index: 3238,
      name: 'Emmet County, Iowa',
      population: 9176,
      area_type: 'County',
    },
  ],
  3239: [
    {
      index: 3239,
      name: 'Fayette County, Iowa',
      population: 19294,
      area_type: 'County',
    },
  ],
  3240: [
    {
      index: 3240,
      name: 'Floyd County, Iowa',
      population: 15337,
      area_type: 'County',
    },
  ],
  3241: [
    {
      index: 3241,
      name: 'Franklin County, Iowa',
      population: 9916,
      area_type: 'County',
    },
  ],
  3242: [
    {
      index: 3242,
      name: 'Fremont County, Iowa',
      population: 6464,
      area_type: 'County',
    },
  ],
  3243: [
    {
      index: 3243,
      name: 'Greene County, Iowa',
      population: 8741,
      area_type: 'County',
    },
  ],
  3244: [
    {
      index: 3244,
      name: 'Hamilton County, Iowa',
      population: 14820,
      area_type: 'County',
    },
  ],
  3245: [
    {
      index: 3245,
      name: 'Hancock County, Iowa',
      population: 10685,
      area_type: 'County',
    },
  ],
  3246: [
    {
      index: 3246,
      name: 'Hardin County, Iowa',
      population: 16567,
      area_type: 'County',
    },
  ],
  3247: [
    {
      index: 3247,
      name: 'Henry County, Iowa',
      population: 20196,
      area_type: 'County',
    },
  ],
  3248: [
    {
      index: 3248,
      name: 'Howard County, Iowa',
      population: 9533,
      area_type: 'County',
    },
  ],
  3249: [
    {
      index: 3249,
      name: 'Humboldt County, Iowa',
      population: 9572,
      area_type: 'County',
    },
  ],
  3250: [
    {
      index: 3250,
      name: 'Ida County, Iowa',
      population: 6888,
      area_type: 'County',
    },
  ],
  3251: [
    {
      index: 3251,
      name: 'Iowa County, Iowa',
      population: 16475,
      area_type: 'County',
    },
  ],
  3252: [
    {
      index: 3252,
      name: 'Jackson County, Iowa',
      population: 19324,
      area_type: 'County',
    },
  ],
  3253: [
    {
      index: 3253,
      name: 'Keokuk County, Iowa',
      population: 9904,
      area_type: 'County',
    },
  ],
  3254: [
    {
      index: 3254,
      name: 'Kossuth County, Iowa',
      population: 14475,
      area_type: 'County',
    },
  ],
  3255: [
    {
      index: 3255,
      name: 'Louisa County, Iowa',
      population: 10677,
      area_type: 'County',
    },
  ],
  3256: [
    {
      index: 3256,
      name: 'Lucas County, Iowa',
      population: 8689,
      area_type: 'County',
    },
  ],
  3257: [
    {
      index: 3257,
      name: 'Lyon County, Iowa',
      population: 12179,
      area_type: 'County',
    },
  ],
  3258: [
    {
      index: 3258,
      name: 'Mitchell County, Iowa',
      population: 10532,
      area_type: 'County',
    },
  ],
  3259: [
    {
      index: 3259,
      name: 'Monona County, Iowa',
      population: 8486,
      area_type: 'County',
    },
  ],
  3260: [
    {
      index: 3260,
      name: 'Monroe County, Iowa',
      population: 7550,
      area_type: 'County',
    },
  ],
  3261: [
    {
      index: 3261,
      name: 'Montgomery County, Iowa',
      population: 10205,
      area_type: 'County',
    },
  ],
  3262: [
    {
      index: 3262,
      name: "O'Brien County, Iowa",
      population: 14060,
      area_type: 'County',
    },
  ],
  3263: [
    {
      index: 3263,
      name: 'Osceola County, Iowa',
      population: 6036,
      area_type: 'County',
    },
  ],
  3264: [
    {
      index: 3264,
      name: 'Page County, Iowa',
      population: 15143,
      area_type: 'County',
    },
  ],
  3265: [
    {
      index: 3265,
      name: 'Palo Alto County, Iowa',
      population: 8764,
      area_type: 'County',
    },
  ],
  3266: [
    {
      index: 3266,
      name: 'Plymouth County, Iowa',
      population: 25681,
      area_type: 'County',
    },
  ],
  3267: [
    {
      index: 3267,
      name: 'Pocahontas County, Iowa',
      population: 7053,
      area_type: 'County',
    },
  ],
  3268: [
    {
      index: 3268,
      name: 'Poweshiek County, Iowa',
      population: 18467,
      area_type: 'County',
    },
  ],
  3269: [
    {
      index: 3269,
      name: 'Ringgold County, Iowa',
      population: 4670,
      area_type: 'County',
    },
  ],
  3270: [
    {
      index: 3270,
      name: 'Sac County, Iowa',
      population: 9673,
      area_type: 'County',
    },
  ],
  3271: [
    {
      index: 3271,
      name: 'Shelby County, Iowa',
      population: 11645,
      area_type: 'County',
    },
  ],
  3272: [
    {
      index: 3272,
      name: 'Sioux County, Iowa',
      population: 36050,
      area_type: 'County',
    },
  ],
  3273: [
    {
      index: 3273,
      name: 'Tama County, Iowa',
      population: 16903,
      area_type: 'County',
    },
  ],
  3274: [
    {
      index: 3274,
      name: 'Taylor County, Iowa',
      population: 5858,
      area_type: 'County',
    },
  ],
  3275: [
    {
      index: 3275,
      name: 'Union County, Iowa',
      population: 11887,
      area_type: 'County',
    },
  ],
  3276: [
    {
      index: 3276,
      name: 'Van Buren County, Iowa',
      population: 7256,
      area_type: 'County',
    },
  ],
  3277: [
    {
      index: 3277,
      name: 'Wayne County, Iowa',
      population: 6467,
      area_type: 'County',
    },
  ],
  3278: [
    {
      index: 3278,
      name: 'Winnebago County, Iowa',
      population: 10617,
      area_type: 'County',
    },
  ],
  3279: [
    {
      index: 3279,
      name: 'Winneshiek County, Iowa',
      population: 19974,
      area_type: 'County',
    },
  ],
  3280: [
    {
      index: 3280,
      name: 'Wright County, Iowa',
      population: 12681,
      area_type: 'County',
    },
  ],
  3281: [
    {
      index: 3281,
      name: 'Allen County, Kansas',
      population: 12579,
      area_type: 'County',
    },
  ],
  3282: [
    {
      index: 3282,
      name: 'Anderson County, Kansas',
      population: 7776,
      area_type: 'County',
    },
  ],
  3283: [
    {
      index: 3283,
      name: 'Barber County, Kansas',
      population: 4122,
      area_type: 'County',
    },
  ],
  3284: [
    {
      index: 3284,
      name: 'Bourbon County, Kansas',
      population: 14493,
      area_type: 'County',
    },
  ],
  3285: [
    {
      index: 3285,
      name: 'Brown County, Kansas',
      population: 9364,
      area_type: 'County',
    },
  ],
  3286: [
    {
      index: 3286,
      name: 'Chautauqua County, Kansas',
      population: 3415,
      area_type: 'County',
    },
  ],
  3287: [
    {
      index: 3287,
      name: 'Cherokee County, Kansas',
      population: 19088,
      area_type: 'County',
    },
  ],
  3288: [
    {
      index: 3288,
      name: 'Cheyenne County, Kansas',
      population: 2583,
      area_type: 'County',
    },
  ],
  3289: [
    {
      index: 3289,
      name: 'Clark County, Kansas',
      population: 1933,
      area_type: 'County',
    },
  ],
  3290: [
    {
      index: 3290,
      name: 'Clay County, Kansas',
      population: 8043,
      area_type: 'County',
    },
  ],
  3291: [
    {
      index: 3291,
      name: 'Cloud County, Kansas',
      population: 8946,
      area_type: 'County',
    },
  ],
  3292: [
    {
      index: 3292,
      name: 'Coffey County, Kansas',
      population: 8280,
      area_type: 'County',
    },
  ],
  3293: [
    {
      index: 3293,
      name: 'Comanche County, Kansas',
      population: 1681,
      area_type: 'County',
    },
  ],
  3294: [
    {
      index: 3294,
      name: 'Decatur County, Kansas',
      population: 2689,
      area_type: 'County',
    },
  ],
  3295: [
    {
      index: 3295,
      name: 'Dickinson County, Kansas',
      population: 18430,
      area_type: 'County',
    },
  ],
  3296: [
    {
      index: 3296,
      name: 'Edwards County, Kansas',
      population: 2739,
      area_type: 'County',
    },
  ],
  3297: [
    {
      index: 3297,
      name: 'Elk County, Kansas',
      population: 2441,
      area_type: 'County',
    },
  ],
  3298: [
    {
      index: 3298,
      name: 'Ellsworth County, Kansas',
      population: 6355,
      area_type: 'County',
    },
  ],
  3299: [
    {
      index: 3299,
      name: 'Gove County, Kansas',
      population: 2717,
      area_type: 'County',
    },
  ],
  3300: [
    {
      index: 3300,
      name: 'Graham County, Kansas',
      population: 2411,
      area_type: 'County',
    },
  ],
  3301: [
    {
      index: 3301,
      name: 'Grant County, Kansas',
      population: 7197,
      area_type: 'County',
    },
  ],
  3302: [
    {
      index: 3302,
      name: 'Gray County, Kansas',
      population: 5729,
      area_type: 'County',
    },
  ],
  3303: [
    {
      index: 3303,
      name: 'Greeley County, Kansas',
      population: 1223,
      area_type: 'County',
    },
  ],
  3304: [
    {
      index: 3304,
      name: 'Greenwood County, Kansas',
      population: 5939,
      area_type: 'County',
    },
  ],
  3305: [
    {
      index: 3305,
      name: 'Hamilton County, Kansas',
      population: 2430,
      area_type: 'County',
    },
  ],
  3306: [
    {
      index: 3306,
      name: 'Harper County, Kansas',
      population: 5323,
      area_type: 'County',
    },
  ],
  3307: [
    {
      index: 3307,
      name: 'Haskell County, Kansas',
      population: 3576,
      area_type: 'County',
    },
  ],
  3308: [
    {
      index: 3308,
      name: 'Hodgeman County, Kansas',
      population: 1755,
      area_type: 'County',
    },
  ],
  3309: [
    {
      index: 3309,
      name: 'Jewell County, Kansas',
      population: 2898,
      area_type: 'County',
    },
  ],
  3310: [
    {
      index: 3310,
      name: 'Kingman County, Kansas',
      population: 7193,
      area_type: 'County',
    },
  ],
  3311: [
    {
      index: 3311,
      name: 'Kiowa County, Kansas',
      population: 2404,
      area_type: 'County',
    },
  ],
  3312: [
    {
      index: 3312,
      name: 'Lane County, Kansas',
      population: 1556,
      area_type: 'County',
    },
  ],
  3313: [
    {
      index: 3313,
      name: 'Lincoln County, Kansas',
      population: 2899,
      area_type: 'County',
    },
  ],
  3314: [
    {
      index: 3314,
      name: 'Logan County, Kansas',
      population: 2705,
      area_type: 'County',
    },
  ],
  3315: [
    {
      index: 3315,
      name: 'Marion County, Kansas',
      population: 11868,
      area_type: 'County',
    },
  ],
  3316: [
    {
      index: 3316,
      name: 'Marshall County, Kansas',
      population: 9982,
      area_type: 'County',
    },
  ],
  3317: [
    {
      index: 3317,
      name: 'Meade County, Kansas',
      population: 3897,
      area_type: 'County',
    },
  ],
  3318: [
    {
      index: 3318,
      name: 'Mitchell County, Kansas',
      population: 5738,
      area_type: 'County',
    },
  ],
  3319: [
    {
      index: 3319,
      name: 'Morris County, Kansas',
      population: 5349,
      area_type: 'County',
    },
  ],
  3320: [
    {
      index: 3320,
      name: 'Morton County, Kansas',
      population: 2599,
      area_type: 'County',
    },
  ],
  3321: [
    {
      index: 3321,
      name: 'Nemaha County, Kansas',
      population: 10115,
      area_type: 'County',
    },
  ],
  3322: [
    {
      index: 3322,
      name: 'Neosho County, Kansas',
      population: 15606,
      area_type: 'County',
    },
  ],
  3323: [
    {
      index: 3323,
      name: 'Ness County, Kansas',
      population: 2645,
      area_type: 'County',
    },
  ],
  3324: [
    {
      index: 3324,
      name: 'Norton County, Kansas',
      population: 5301,
      area_type: 'County',
    },
  ],
  3325: [
    {
      index: 3325,
      name: 'Osborne County, Kansas',
      population: 3490,
      area_type: 'County',
    },
  ],
  3326: [
    {
      index: 3326,
      name: 'Pawnee County, Kansas',
      population: 6179,
      area_type: 'County',
    },
  ],
  3327: [
    {
      index: 3327,
      name: 'Phillips County, Kansas',
      population: 4809,
      area_type: 'County',
    },
  ],
  3328: [
    {
      index: 3328,
      name: 'Pratt County, Kansas',
      population: 9067,
      area_type: 'County',
    },
  ],
  3329: [
    {
      index: 3329,
      name: 'Rawlins County, Kansas',
      population: 2528,
      area_type: 'County',
    },
  ],
  3330: [
    {
      index: 3330,
      name: 'Republic County, Kansas',
      population: 4642,
      area_type: 'County',
    },
  ],
  3331: [
    {
      index: 3331,
      name: 'Rice County, Kansas',
      population: 9407,
      area_type: 'County',
    },
  ],
  3332: [
    {
      index: 3332,
      name: 'Rooks County, Kansas',
      population: 4813,
      area_type: 'County',
    },
  ],
  3333: [
    {
      index: 3333,
      name: 'Rush County, Kansas',
      population: 2927,
      area_type: 'County',
    },
  ],
  3334: [
    {
      index: 3334,
      name: 'Russell County, Kansas',
      population: 6639,
      area_type: 'County',
    },
  ],
  3335: [
    {
      index: 3335,
      name: 'Scott County, Kansas',
      population: 5014,
      area_type: 'County',
    },
  ],
  3336: [
    {
      index: 3336,
      name: 'Sheridan County, Kansas',
      population: 2425,
      area_type: 'County',
    },
  ],
  3337: [
    {
      index: 3337,
      name: 'Sherman County, Kansas',
      population: 5830,
      area_type: 'County',
    },
  ],
  3338: [
    {
      index: 3338,
      name: 'Smith County, Kansas',
      population: 3533,
      area_type: 'County',
    },
  ],
  3339: [
    {
      index: 3339,
      name: 'Stafford County, Kansas',
      population: 3993,
      area_type: 'County',
    },
  ],
  3340: [
    {
      index: 3340,
      name: 'Stanton County, Kansas',
      population: 1963,
      area_type: 'County',
    },
  ],
  3341: [
    {
      index: 3341,
      name: 'Stevens County, Kansas',
      population: 5175,
      area_type: 'County',
    },
  ],
  3342: [
    {
      index: 3342,
      name: 'Thomas County, Kansas',
      population: 7893,
      area_type: 'County',
    },
  ],
  3343: [
    {
      index: 3343,
      name: 'Trego County, Kansas',
      population: 2752,
      area_type: 'County',
    },
  ],
  3344: [
    {
      index: 3344,
      name: 'Wallace County, Kansas',
      population: 1488,
      area_type: 'County',
    },
  ],
  3345: [
    {
      index: 3345,
      name: 'Washington County, Kansas',
      population: 5501,
      area_type: 'County',
    },
  ],
  3346: [
    {
      index: 3346,
      name: 'Wichita County, Kansas',
      population: 2064,
      area_type: 'County',
    },
  ],
  3347: [
    {
      index: 3347,
      name: 'Wilson County, Kansas',
      population: 8622,
      area_type: 'County',
    },
  ],
  3348: [
    {
      index: 3348,
      name: 'Woodson County, Kansas',
      population: 3109,
      area_type: 'County',
    },
  ],
  3349: [
    {
      index: 3349,
      name: 'Adair County, Kentucky',
      population: 19067,
      area_type: 'County',
    },
  ],
  3350: [
    {
      index: 3350,
      name: 'Breathitt County, Kentucky',
      population: 13351,
      area_type: 'County',
    },
  ],
  3351: [
    {
      index: 3351,
      name: 'Breckinridge County, Kentucky',
      population: 20943,
      area_type: 'County',
    },
  ],
  3352: [
    {
      index: 3352,
      name: 'Caldwell County, Kentucky',
      population: 12570,
      area_type: 'County',
    },
  ],
  3353: [
    {
      index: 3353,
      name: 'Carlisle County, Kentucky',
      population: 4720,
      area_type: 'County',
    },
  ],
  3354: [
    {
      index: 3354,
      name: 'Carroll County, Kentucky',
      population: 10938,
      area_type: 'County',
    },
  ],
  3355: [
    {
      index: 3355,
      name: 'Casey County, Kentucky',
      population: 15920,
      area_type: 'County',
    },
  ],
  3356: [
    {
      index: 3356,
      name: 'Clinton County, Kentucky',
      population: 9123,
      area_type: 'County',
    },
  ],
  3357: [
    {
      index: 3357,
      name: 'Crittenden County, Kentucky',
      population: 8981,
      area_type: 'County',
    },
  ],
  3358: [
    {
      index: 3358,
      name: 'Cumberland County, Kentucky',
      population: 5946,
      area_type: 'County',
    },
  ],
  3359: [
    {
      index: 3359,
      name: 'Elliott County, Kentucky',
      population: 7293,
      area_type: 'County',
    },
  ],
  3360: [
    {
      index: 3360,
      name: 'Fleming County, Kentucky',
      population: 15288,
      area_type: 'County',
    },
  ],
  3361: [
    {
      index: 3361,
      name: 'Floyd County, Kentucky',
      population: 34978,
      area_type: 'County',
    },
  ],
  3362: [
    {
      index: 3362,
      name: 'Fulton County, Kentucky',
      population: 6382,
      area_type: 'County',
    },
  ],
  3363: [
    {
      index: 3363,
      name: 'Garrard County, Kentucky',
      population: 17589,
      area_type: 'County',
    },
  ],
  3364: [
    {
      index: 3364,
      name: 'Grayson County, Kentucky',
      population: 26631,
      area_type: 'County',
    },
  ],
  3365: [
    {
      index: 3365,
      name: 'Harlan County, Kentucky',
      population: 25662,
      area_type: 'County',
    },
  ],
  3366: [
    {
      index: 3366,
      name: 'Harrison County, Kentucky',
      population: 19103,
      area_type: 'County',
    },
  ],
  3367: [
    {
      index: 3367,
      name: 'Hart County, Kentucky',
      population: 19600,
      area_type: 'County',
    },
  ],
  3368: [
    {
      index: 3368,
      name: 'Hickman County, Kentucky',
      population: 4422,
      area_type: 'County',
    },
  ],
  3369: [
    {
      index: 3369,
      name: 'Jackson County, Kentucky',
      population: 12973,
      area_type: 'County',
    },
  ],
  3370: [
    {
      index: 3370,
      name: 'Johnson County, Kentucky',
      population: 22244,
      area_type: 'County',
    },
  ],
  3371: [
    {
      index: 3371,
      name: 'Knott County, Kentucky',
      population: 13874,
      area_type: 'County',
    },
  ],
  3372: [
    {
      index: 3372,
      name: 'Lawrence County, Kentucky',
      population: 16109,
      area_type: 'County',
    },
  ],
  3373: [
    {
      index: 3373,
      name: 'Lee County, Kentucky',
      population: 7261,
      area_type: 'County',
    },
  ],
  3374: [
    {
      index: 3374,
      name: 'Leslie County, Kentucky',
      population: 10093,
      area_type: 'County',
    },
  ],
  3375: [
    {
      index: 3375,
      name: 'Letcher County, Kentucky',
      population: 20893,
      area_type: 'County',
    },
  ],
  3376: [
    {
      index: 3376,
      name: 'Lewis County, Kentucky',
      population: 12954,
      area_type: 'County',
    },
  ],
  3377: [
    {
      index: 3377,
      name: 'Logan County, Kentucky',
      population: 27877,
      area_type: 'County',
    },
  ],
  3378: [
    {
      index: 3378,
      name: 'Lyon County, Kentucky',
      population: 9101,
      area_type: 'County',
    },
  ],
  3379: [
    {
      index: 3379,
      name: 'McCreary County, Kentucky',
      population: 16701,
      area_type: 'County',
    },
  ],
  3380: [
    {
      index: 3380,
      name: 'Magoffin County, Kentucky',
      population: 11357,
      area_type: 'County',
    },
  ],
  3381: [
    {
      index: 3381,
      name: 'Marion County, Kentucky',
      population: 19775,
      area_type: 'County',
    },
  ],
  3382: [
    {
      index: 3382,
      name: 'Marshall County, Kentucky',
      population: 31777,
      area_type: 'County',
    },
  ],
  3383: [
    {
      index: 3383,
      name: 'Martin County, Kentucky',
      population: 11095,
      area_type: 'County',
    },
  ],
  3384: [
    {
      index: 3384,
      name: 'Mercer County, Kentucky',
      population: 22902,
      area_type: 'County',
    },
  ],
  3385: [
    {
      index: 3385,
      name: 'Monroe County, Kentucky',
      population: 11355,
      area_type: 'County',
    },
  ],
  3386: [
    {
      index: 3386,
      name: 'Morgan County, Kentucky',
      population: 14120,
      area_type: 'County',
    },
  ],
  3387: [
    {
      index: 3387,
      name: 'Nicholas County, Kentucky',
      population: 7805,
      area_type: 'County',
    },
  ],
  3388: [
    {
      index: 3388,
      name: 'Ohio County, Kentucky',
      population: 23527,
      area_type: 'County',
    },
  ],
  3389: [
    {
      index: 3389,
      name: 'Owen County, Kentucky',
      population: 11290,
      area_type: 'County',
    },
  ],
  3390: [
    {
      index: 3390,
      name: 'Owsley County, Kentucky',
      population: 3929,
      area_type: 'County',
    },
  ],
  3391: [
    {
      index: 3391,
      name: 'Perry County, Kentucky',
      population: 27361,
      area_type: 'County',
    },
  ],
  3392: [
    {
      index: 3392,
      name: 'Pike County, Kentucky',
      population: 56286,
      area_type: 'County',
    },
  ],
  3393: [
    {
      index: 3393,
      name: 'Powell County, Kentucky',
      population: 13083,
      area_type: 'County',
    },
  ],
  3394: [
    {
      index: 3394,
      name: 'Robertson County, Kentucky',
      population: 2229,
      area_type: 'County',
    },
  ],
  3395: [
    {
      index: 3395,
      name: 'Rockcastle County, Kentucky',
      population: 16242,
      area_type: 'County',
    },
  ],
  3396: [
    {
      index: 3396,
      name: 'Rowan County, Kentucky',
      population: 24388,
      area_type: 'County',
    },
  ],
  3397: [
    {
      index: 3397,
      name: 'Russell County, Kentucky',
      population: 18178,
      area_type: 'County',
    },
  ],
  3398: [
    {
      index: 3398,
      name: 'Simpson County, Kentucky',
      population: 19949,
      area_type: 'County',
    },
  ],
  3399: [
    {
      index: 3399,
      name: 'Todd County, Kentucky',
      population: 12404,
      area_type: 'County',
    },
  ],
  3400: [
    {
      index: 3400,
      name: 'Trimble County, Kentucky',
      population: 8539,
      area_type: 'County',
    },
  ],
  3401: [
    {
      index: 3401,
      name: 'Union County, Kentucky',
      population: 12961,
      area_type: 'County',
    },
  ],
  3402: [
    {
      index: 3402,
      name: 'Washington County, Kentucky',
      population: 12061,
      area_type: 'County',
    },
  ],
  3403: [
    {
      index: 3403,
      name: 'Wayne County, Kentucky',
      population: 19681,
      area_type: 'County',
    },
  ],
  3404: [
    {
      index: 3404,
      name: 'Webster County, Kentucky',
      population: 12726,
      area_type: 'County',
    },
  ],
  3405: [
    {
      index: 3405,
      name: 'Wolfe County, Kentucky',
      population: 6400,
      area_type: 'County',
    },
  ],
  3406: [
    {
      index: 3406,
      name: 'Allen Parish, Louisiana',
      population: 22320,
      area_type: 'County',
    },
  ],
  3407: [
    {
      index: 3407,
      name: 'Avoyelles Parish, Louisiana',
      population: 38751,
      area_type: 'County',
    },
  ],
  3408: [
    {
      index: 3408,
      name: 'Bienville Parish, Louisiana',
      population: 12641,
      area_type: 'County',
    },
  ],
  3409: [
    {
      index: 3409,
      name: 'Caldwell Parish, Louisiana',
      population: 9554,
      area_type: 'County',
    },
  ],
  3410: [
    {
      index: 3410,
      name: 'Catahoula Parish, Louisiana',
      population: 8566,
      area_type: 'County',
    },
  ],
  3411: [
    {
      index: 3411,
      name: 'Claiborne Parish, Louisiana',
      population: 13744,
      area_type: 'County',
    },
  ],
  3412: [
    {
      index: 3412,
      name: 'East Carroll Parish, Louisiana',
      population: 6990,
      area_type: 'County',
    },
  ],
  3413: [
    {
      index: 3413,
      name: 'Evangeline Parish, Louisiana',
      population: 31986,
      area_type: 'County',
    },
  ],
  3414: [
    {
      index: 3414,
      name: 'Franklin Parish, Louisiana',
      population: 19308,
      area_type: 'County',
    },
  ],
  3415: [
    {
      index: 3415,
      name: 'Jackson Parish, Louisiana',
      population: 14839,
      area_type: 'County',
    },
  ],
  3416: [
    {
      index: 3416,
      name: 'LaSalle Parish, Louisiana',
      population: 14729,
      area_type: 'County',
    },
  ],
  3417: [
    {
      index: 3417,
      name: 'Madison Parish, Louisiana',
      population: 9478,
      area_type: 'County',
    },
  ],
  3418: [
    {
      index: 3418,
      name: 'Red River Parish, Louisiana',
      population: 7420,
      area_type: 'County',
    },
  ],
  3419: [
    {
      index: 3419,
      name: 'Richland Parish, Louisiana',
      population: 19826,
      area_type: 'County',
    },
  ],
  3420: [
    {
      index: 3420,
      name: 'Sabine Parish, Louisiana',
      population: 21985,
      area_type: 'County',
    },
  ],
  3421: [
    {
      index: 3421,
      name: 'Tensas Parish, Louisiana',
      population: 3846,
      area_type: 'County',
    },
  ],
  3422: [
    {
      index: 3422,
      name: 'West Carroll Parish, Louisiana',
      population: 9475,
      area_type: 'County',
    },
  ],
  3423: [
    {
      index: 3423,
      name: 'Winn Parish, Louisiana',
      population: 13205,
      area_type: 'County',
    },
  ],
  3424: [
    {
      index: 3424,
      name: 'Aroostook County, Maine',
      population: 67255,
      area_type: 'County',
    },
  ],
  3425: [
    {
      index: 3425,
      name: 'Franklin County, Maine',
      population: 30474,
      area_type: 'County',
    },
  ],
  3426: [
    {
      index: 3426,
      name: 'Hancock County, Maine',
      population: 56701,
      area_type: 'County',
    },
  ],
  3427: [
    {
      index: 3427,
      name: 'Knox County, Maine',
      population: 41164,
      area_type: 'County',
    },
  ],
  3428: [
    {
      index: 3428,
      name: 'Lincoln County, Maine',
      population: 36215,
      area_type: 'County',
    },
  ],
  3429: [
    {
      index: 3429,
      name: 'Oxford County, Maine',
      population: 59495,
      area_type: 'County',
    },
  ],
  3430: [
    {
      index: 3430,
      name: 'Piscataquis County, Maine',
      population: 17417,
      area_type: 'County',
    },
  ],
  3431: [
    {
      index: 3431,
      name: 'Somerset County, Maine',
      population: 51098,
      area_type: 'County',
    },
  ],
  3432: [
    {
      index: 3432,
      name: 'Waldo County, Maine',
      population: 40241,
      area_type: 'County',
    },
  ],
  3433: [
    {
      index: 3433,
      name: 'Washington County, Maine',
      population: 31437,
      area_type: 'County',
    },
  ],
  3434: [
    {
      index: 3434,
      name: 'Caroline County, Maryland',
      population: 33433,
      area_type: 'County',
    },
  ],
  3435: [
    {
      index: 3435,
      name: 'Garrett County, Maryland',
      population: 28579,
      area_type: 'County',
    },
  ],
  3436: [
    {
      index: 3436,
      name: 'Kent County, Maryland',
      population: 19320,
      area_type: 'County',
    },
  ],
  3437: [
    {
      index: 3437,
      name: 'Nantucket County, Massachusetts',
      population: 14421,
      area_type: 'County',
    },
  ],
  3438: [
    {
      index: 3438,
      name: 'Alcona County, Michigan',
      population: 10417,
      area_type: 'County',
    },
  ],
  3439: [
    {
      index: 3439,
      name: 'Alger County, Michigan',
      population: 8807,
      area_type: 'County',
    },
  ],
  3440: [
    {
      index: 3440,
      name: 'Antrim County, Michigan',
      population: 24249,
      area_type: 'County',
    },
  ],
  3441: [
    {
      index: 3441,
      name: 'Arenac County, Michigan',
      population: 15089,
      area_type: 'County',
    },
  ],
  3442: [
    {
      index: 3442,
      name: 'Baraga County, Michigan',
      population: 8277,
      area_type: 'County',
    },
  ],
  3443: [
    {
      index: 3443,
      name: 'Barry County, Michigan',
      population: 63554,
      area_type: 'County',
    },
  ],
  3444: [
    {
      index: 3444,
      name: 'Charlevoix County, Michigan',
      population: 26293,
      area_type: 'County',
    },
  ],
  3445: [
    {
      index: 3445,
      name: 'Cheboygan County, Michigan',
      population: 25940,
      area_type: 'County',
    },
  ],
  3446: [
    {
      index: 3446,
      name: 'Clare County, Michigan',
      population: 31352,
      area_type: 'County',
    },
  ],
  3447: [
    {
      index: 3447,
      name: 'Crawford County, Michigan',
      population: 13491,
      area_type: 'County',
    },
  ],
  3448: [
    {
      index: 3448,
      name: 'Emmet County, Michigan',
      population: 34163,
      area_type: 'County',
    },
  ],
  3449: [
    {
      index: 3449,
      name: 'Gladwin County, Michigan',
      population: 25728,
      area_type: 'County',
    },
  ],
  3450: [
    {
      index: 3450,
      name: 'Gogebic County, Michigan',
      population: 14319,
      area_type: 'County',
    },
  ],
  3451: [
    {
      index: 3451,
      name: 'Huron County, Michigan',
      population: 31248,
      area_type: 'County',
    },
  ],
  3452: [
    {
      index: 3452,
      name: 'Iosco County, Michigan',
      population: 25521,
      area_type: 'County',
    },
  ],
  3453: [
    {
      index: 3453,
      name: 'Iron County, Michigan',
      population: 11622,
      area_type: 'County',
    },
  ],
  3454: [
    {
      index: 3454,
      name: 'Lake County, Michigan',
      population: 12594,
      area_type: 'County',
    },
  ],
  3455: [
    {
      index: 3455,
      name: 'Luce County, Michigan',
      population: 5330,
      area_type: 'County',
    },
  ],
  3456: [
    {
      index: 3456,
      name: 'Mackinac County, Michigan',
      population: 10941,
      area_type: 'County',
    },
  ],
  3457: [
    {
      index: 3457,
      name: 'Manistee County, Michigan',
      population: 25287,
      area_type: 'County',
    },
  ],
  3458: [
    {
      index: 3458,
      name: 'Montmorency County, Michigan',
      population: 9569,
      area_type: 'County',
    },
  ],
  3459: [
    {
      index: 3459,
      name: 'Newaygo County, Michigan',
      population: 50886,
      area_type: 'County',
    },
  ],
  3460: [
    {
      index: 3460,
      name: 'Oceana County, Michigan',
      population: 26973,
      area_type: 'County',
    },
  ],
  3461: [
    {
      index: 3461,
      name: 'Ogemaw County, Michigan',
      population: 20970,
      area_type: 'County',
    },
  ],
  3462: [
    {
      index: 3462,
      name: 'Ontonagon County, Michigan',
      population: 5863,
      area_type: 'County',
    },
  ],
  3463: [
    {
      index: 3463,
      name: 'Osceola County, Michigan',
      population: 23274,
      area_type: 'County',
    },
  ],
  3464: [
    {
      index: 3464,
      name: 'Oscoda County, Michigan',
      population: 8404,
      area_type: 'County',
    },
  ],
  3465: [
    {
      index: 3465,
      name: 'Otsego County, Michigan',
      population: 25644,
      area_type: 'County',
    },
  ],
  3466: [
    {
      index: 3466,
      name: 'Presque Isle County, Michigan',
      population: 13361,
      area_type: 'County',
    },
  ],
  3467: [
    {
      index: 3467,
      name: 'Roscommon County, Michigan',
      population: 23708,
      area_type: 'County',
    },
  ],
  3468: [
    {
      index: 3468,
      name: 'Sanilac County, Michigan',
      population: 40657,
      area_type: 'County',
    },
  ],
  3469: [
    {
      index: 3469,
      name: 'Schoolcraft County, Michigan',
      population: 8188,
      area_type: 'County',
    },
  ],
  3470: [
    {
      index: 3470,
      name: 'Tuscola County, Michigan',
      population: 52945,
      area_type: 'County',
    },
  ],
  3471: [
    {
      index: 3471,
      name: 'Van Buren County, Michigan',
      population: 75692,
      area_type: 'County',
    },
  ],
  3472: [
    {
      index: 3472,
      name: 'Aitkin County, Minnesota',
      population: 16126,
      area_type: 'County',
    },
  ],
  3473: [
    {
      index: 3473,
      name: 'Becker County, Minnesota',
      population: 35371,
      area_type: 'County',
    },
  ],
  3474: [
    {
      index: 3474,
      name: 'Big Stone County, Minnesota',
      population: 5144,
      area_type: 'County',
    },
  ],
  3475: [
    {
      index: 3475,
      name: 'Chippewa County, Minnesota',
      population: 12284,
      area_type: 'County',
    },
  ],
  3476: [
    {
      index: 3476,
      name: 'Clearwater County, Minnesota',
      population: 8649,
      area_type: 'County',
    },
  ],
  3477: [
    {
      index: 3477,
      name: 'Cook County, Minnesota',
      population: 5708,
      area_type: 'County',
    },
  ],
  3478: [
    {
      index: 3478,
      name: 'Cottonwood County, Minnesota',
      population: 11356,
      area_type: 'County',
    },
  ],
  3479: [
    {
      index: 3479,
      name: 'Faribault County, Minnesota',
      population: 13926,
      area_type: 'County',
    },
  ],
  3480: [
    {
      index: 3480,
      name: 'Grant County, Minnesota',
      population: 6136,
      area_type: 'County',
    },
  ],
  3481: [
    {
      index: 3481,
      name: 'Hubbard County, Minnesota',
      population: 21960,
      area_type: 'County',
    },
  ],
  3482: [
    {
      index: 3482,
      name: 'Jackson County, Minnesota',
      population: 9893,
      area_type: 'County',
    },
  ],
  3483: [
    {
      index: 3483,
      name: 'Kanabec County, Minnesota',
      population: 16463,
      area_type: 'County',
    },
  ],
  3484: [
    {
      index: 3484,
      name: 'Kittson County, Minnesota',
      population: 4059,
      area_type: 'County',
    },
  ],
  3485: [
    {
      index: 3485,
      name: 'Koochiching County, Minnesota',
      population: 11844,
      area_type: 'County',
    },
  ],
  3486: [
    {
      index: 3486,
      name: 'Lac qui Parle County, Minnesota',
      population: 6689,
      area_type: 'County',
    },
  ],
  3487: [
    {
      index: 3487,
      name: 'Lake of the Woods County, Minnesota',
      population: 3871,
      area_type: 'County',
    },
  ],
  3488: [
    {
      index: 3488,
      name: 'Lincoln County, Minnesota',
      population: 5580,
      area_type: 'County',
    },
  ],
  3489: [
    {
      index: 3489,
      name: 'Mahnomen County, Minnesota',
      population: 5328,
      area_type: 'County',
    },
  ],
  3490: [
    {
      index: 3490,
      name: 'Marshall County, Minnesota',
      population: 8861,
      area_type: 'County',
    },
  ],
  3491: [
    {
      index: 3491,
      name: 'Meeker County, Minnesota',
      population: 23496,
      area_type: 'County',
    },
  ],
  3492: [
    {
      index: 3492,
      name: 'Morrison County, Minnesota',
      population: 34246,
      area_type: 'County',
    },
  ],
  3493: [
    {
      index: 3493,
      name: 'Murray County, Minnesota',
      population: 8060,
      area_type: 'County',
    },
  ],
  3494: [
    {
      index: 3494,
      name: 'Norman County, Minnesota',
      population: 6377,
      area_type: 'County',
    },
  ],
  3495: [
    {
      index: 3495,
      name: 'Pennington County, Minnesota',
      population: 13845,
      area_type: 'County',
    },
  ],
  3496: [
    {
      index: 3496,
      name: 'Pine County, Minnesota',
      population: 29446,
      area_type: 'County',
    },
  ],
  3497: [
    {
      index: 3497,
      name: 'Pipestone County, Minnesota',
      population: 9355,
      area_type: 'County',
    },
  ],
  3498: [
    {
      index: 3498,
      name: 'Pope County, Minnesota',
      population: 11431,
      area_type: 'County',
    },
  ],
  3499: [
    {
      index: 3499,
      name: 'Red Lake County, Minnesota',
      population: 3874,
      area_type: 'County',
    },
  ],
  3500: [
    {
      index: 3500,
      name: 'Redwood County, Minnesota',
      population: 15361,
      area_type: 'County',
    },
  ],
  3501: [
    {
      index: 3501,
      name: 'Renville County, Minnesota',
      population: 14525,
      area_type: 'County',
    },
  ],
  3502: [
    {
      index: 3502,
      name: 'Rock County, Minnesota',
      population: 9537,
      area_type: 'County',
    },
  ],
  3503: [
    {
      index: 3503,
      name: 'Roseau County, Minnesota',
      population: 15292,
      area_type: 'County',
    },
  ],
  3504: [
    {
      index: 3504,
      name: 'Sibley County, Minnesota',
      population: 14955,
      area_type: 'County',
    },
  ],
  3505: [
    {
      index: 3505,
      name: 'Stevens County, Minnesota',
      population: 9637,
      area_type: 'County',
    },
  ],
  3506: [
    {
      index: 3506,
      name: 'Swift County, Minnesota',
      population: 9755,
      area_type: 'County',
    },
  ],
  3507: [
    {
      index: 3507,
      name: 'Todd County, Minnesota',
      population: 25538,
      area_type: 'County',
    },
  ],
  3508: [
    {
      index: 3508,
      name: 'Traverse County, Minnesota',
      population: 3275,
      area_type: 'County',
    },
  ],
  3509: [
    {
      index: 3509,
      name: 'Wadena County, Minnesota',
      population: 14307,
      area_type: 'County',
    },
  ],
  3510: [
    {
      index: 3510,
      name: 'Waseca County, Minnesota',
      population: 18893,
      area_type: 'County',
    },
  ],
  3511: [
    {
      index: 3511,
      name: 'Watonwan County, Minnesota',
      population: 11075,
      area_type: 'County',
    },
  ],
  3512: [
    {
      index: 3512,
      name: 'Yellow Medicine County, Minnesota',
      population: 9486,
      area_type: 'County',
    },
  ],
  3513: [
    {
      index: 3513,
      name: 'Amite County, Mississippi',
      population: 12619,
      area_type: 'County',
    },
  ],
  3514: [
    {
      index: 3514,
      name: 'Attala County, Mississippi',
      population: 17509,
      area_type: 'County',
    },
  ],
  3515: [
    {
      index: 3515,
      name: 'Benton County, Mississippi',
      population: 7550,
      area_type: 'County',
    },
  ],
  3516: [
    {
      index: 3516,
      name: 'Calhoun County, Mississippi',
      population: 12781,
      area_type: 'County',
    },
  ],
  3517: [
    {
      index: 3517,
      name: 'Chickasaw County, Mississippi',
      population: 16812,
      area_type: 'County',
    },
  ],
  3518: [
    {
      index: 3518,
      name: 'Choctaw County, Mississippi',
      population: 8037,
      area_type: 'County',
    },
  ],
  3519: [
    {
      index: 3519,
      name: 'Claiborne County, Mississippi',
      population: 8805,
      area_type: 'County',
    },
  ],
  3520: [
    {
      index: 3520,
      name: 'Franklin County, Mississippi',
      population: 7642,
      area_type: 'County',
    },
  ],
  3521: [
    {
      index: 3521,
      name: 'George County, Mississippi',
      population: 25206,
      area_type: 'County',
    },
  ],
  3522: [
    {
      index: 3522,
      name: 'Greene County, Mississippi',
      population: 13552,
      area_type: 'County',
    },
  ],
  3523: [
    {
      index: 3523,
      name: 'Humphreys County, Mississippi',
      population: 7333,
      area_type: 'County',
    },
  ],
  3524: [
    {
      index: 3524,
      name: 'Issaquena County, Mississippi',
      population: 1273,
      area_type: 'County',
    },
  ],
  3525: [
    {
      index: 3525,
      name: 'Jefferson County, Mississippi',
      population: 7087,
      area_type: 'County',
    },
  ],
  3526: [
    {
      index: 3526,
      name: 'Jefferson Davis County, Mississippi',
      population: 11088,
      area_type: 'County',
    },
  ],
  3527: [
    {
      index: 3527,
      name: 'Lawrence County, Mississippi',
      population: 11713,
      area_type: 'County',
    },
  ],
  3528: [
    {
      index: 3528,
      name: 'Leake County, Mississippi',
      population: 21135,
      area_type: 'County',
    },
  ],
  3529: [
    {
      index: 3529,
      name: 'Marion County, Mississippi',
      population: 24050,
      area_type: 'County',
    },
  ],
  3530: [
    {
      index: 3530,
      name: 'Monroe County, Mississippi',
      population: 33577,
      area_type: 'County',
    },
  ],
  3531: [
    {
      index: 3531,
      name: 'Montgomery County, Mississippi',
      population: 9530,
      area_type: 'County',
    },
  ],
  3532: [
    {
      index: 3532,
      name: 'Neshoba County, Mississippi',
      population: 28673,
      area_type: 'County',
    },
  ],
  3533: [
    {
      index: 3533,
      name: 'Newton County, Mississippi',
      population: 21029,
      area_type: 'County',
    },
  ],
  3534: [
    {
      index: 3534,
      name: 'Noxubee County, Mississippi',
      population: 9990,
      area_type: 'County',
    },
  ],
  3535: [
    {
      index: 3535,
      name: 'Panola County, Mississippi',
      population: 32661,
      area_type: 'County',
    },
  ],
  3536: [
    {
      index: 3536,
      name: 'Quitman County, Mississippi',
      population: 5701,
      area_type: 'County',
    },
  ],
  3537: [
    {
      index: 3537,
      name: 'Scott County, Mississippi',
      population: 27707,
      area_type: 'County',
    },
  ],
  3538: [
    {
      index: 3538,
      name: 'Sharkey County, Mississippi',
      population: 3488,
      area_type: 'County',
    },
  ],
  3539: [
    {
      index: 3539,
      name: 'Smith County, Mississippi',
      population: 14092,
      area_type: 'County',
    },
  ],
  3540: [
    {
      index: 3540,
      name: 'Tallahatchie County, Mississippi',
      population: 12035,
      area_type: 'County',
    },
  ],
  3541: [
    {
      index: 3541,
      name: 'Tippah County, Mississippi',
      population: 21431,
      area_type: 'County',
    },
  ],
  3542: [
    {
      index: 3542,
      name: 'Tishomingo County, Mississippi',
      population: 18619,
      area_type: 'County',
    },
  ],
  3543: [
    {
      index: 3543,
      name: 'Union County, Mississippi',
      population: 28125,
      area_type: 'County',
    },
  ],
  3544: [
    {
      index: 3544,
      name: 'Walthall County, Mississippi',
      population: 13761,
      area_type: 'County',
    },
  ],
  3545: [
    {
      index: 3545,
      name: 'Wayne County, Mississippi',
      population: 19681,
      area_type: 'County',
    },
  ],
  3546: [
    {
      index: 3546,
      name: 'Wilkinson County, Mississippi',
      population: 8143,
      area_type: 'County',
    },
  ],
  3547: [
    {
      index: 3547,
      name: 'Winston County, Mississippi',
      population: 17543,
      area_type: 'County',
    },
  ],
  3548: [
    {
      index: 3548,
      name: 'Yalobusha County, Mississippi',
      population: 12364,
      area_type: 'County',
    },
  ],
  3549: [
    {
      index: 3549,
      name: 'Atchison County, Missouri',
      population: 5182,
      area_type: 'County',
    },
  ],
  3550: [
    {
      index: 3550,
      name: 'Barry County, Missouri',
      population: 34926,
      area_type: 'County',
    },
  ],
  3551: [
    {
      index: 3551,
      name: 'Barton County, Missouri',
      population: 11694,
      area_type: 'County',
    },
  ],
  3552: [
    {
      index: 3552,
      name: 'Benton County, Missouri',
      population: 20224,
      area_type: 'County',
    },
  ],
  3553: [
    {
      index: 3553,
      name: 'Camden County, Missouri',
      population: 43768,
      area_type: 'County',
    },
  ],
  3554: [
    {
      index: 3554,
      name: 'Carroll County, Missouri',
      population: 8423,
      area_type: 'County',
    },
  ],
  3555: [
    {
      index: 3555,
      name: 'Carter County, Missouri',
      population: 5268,
      area_type: 'County',
    },
  ],
  3556: [
    {
      index: 3556,
      name: 'Cedar County, Missouri',
      population: 14601,
      area_type: 'County',
    },
  ],
  3557: [
    {
      index: 3557,
      name: 'Chariton County, Missouri',
      population: 7386,
      area_type: 'County',
    },
  ],
  3558: [
    {
      index: 3558,
      name: 'Crawford County, Missouri',
      population: 22659,
      area_type: 'County',
    },
  ],
  3559: [
    {
      index: 3559,
      name: 'Dade County, Missouri',
      population: 7660,
      area_type: 'County',
    },
  ],
  3560: [
    {
      index: 3560,
      name: 'Daviess County, Missouri',
      population: 8435,
      area_type: 'County',
    },
  ],
  3561: [
    {
      index: 3561,
      name: 'Dent County, Missouri',
      population: 14467,
      area_type: 'County',
    },
  ],
  3562: [
    {
      index: 3562,
      name: 'Douglas County, Missouri',
      population: 11975,
      area_type: 'County',
    },
  ],
  3563: [
    {
      index: 3563,
      name: 'Gasconade County, Missouri',
      population: 14768,
      area_type: 'County',
    },
  ],
  3564: [
    {
      index: 3564,
      name: 'Gentry County, Missouri',
      population: 6253,
      area_type: 'County',
    },
  ],
  3565: [
    {
      index: 3565,
      name: 'Grundy County, Missouri',
      population: 9838,
      area_type: 'County',
    },
  ],
  3566: [
    {
      index: 3566,
      name: 'Harrison County, Missouri',
      population: 8199,
      area_type: 'County',
    },
  ],
  3567: [
    {
      index: 3567,
      name: 'Henry County, Missouri',
      population: 22438,
      area_type: 'County',
    },
  ],
  3568: [
    {
      index: 3568,
      name: 'Hickory County, Missouri',
      population: 8630,
      area_type: 'County',
    },
  ],
  3569: [
    {
      index: 3569,
      name: 'Holt County, Missouri',
      population: 4262,
      area_type: 'County',
    },
  ],
  3570: [
    {
      index: 3570,
      name: 'Iron County, Missouri',
      population: 9414,
      area_type: 'County',
    },
  ],
  3571: [
    {
      index: 3571,
      name: 'Knox County, Missouri',
      population: 3776,
      area_type: 'County',
    },
  ],
  3572: [
    {
      index: 3572,
      name: 'Lawrence County, Missouri',
      population: 38683,
      area_type: 'County',
    },
  ],
  3573: [
    {
      index: 3573,
      name: 'Linn County, Missouri',
      population: 11820,
      area_type: 'County',
    },
  ],
  3574: [
    {
      index: 3574,
      name: 'Livingston County, Missouri',
      population: 14402,
      area_type: 'County',
    },
  ],
  3575: [
    {
      index: 3575,
      name: 'McDonald County, Missouri',
      population: 23588,
      area_type: 'County',
    },
  ],
  3576: [
    {
      index: 3576,
      name: 'Macon County, Missouri',
      population: 15049,
      area_type: 'County',
    },
  ],
  3577: [
    {
      index: 3577,
      name: 'Madison County, Missouri',
      population: 12753,
      area_type: 'County',
    },
  ],
  3578: [
    {
      index: 3578,
      name: 'Maries County, Missouri',
      population: 8431,
      area_type: 'County',
    },
  ],
  3579: [
    {
      index: 3579,
      name: 'Mercer County, Missouri',
      population: 3437,
      area_type: 'County',
    },
  ],
  3580: [
    {
      index: 3580,
      name: 'Miller County, Missouri',
      population: 25403,
      area_type: 'County',
    },
  ],
  3581: [
    {
      index: 3581,
      name: 'Mississippi County, Missouri',
      population: 11688,
      area_type: 'County',
    },
  ],
  3582: [
    {
      index: 3582,
      name: 'Monroe County, Missouri',
      population: 8652,
      area_type: 'County',
    },
  ],
  3583: [
    {
      index: 3583,
      name: 'Montgomery County, Missouri',
      population: 11470,
      area_type: 'County',
    },
  ],
  3584: [
    {
      index: 3584,
      name: 'Morgan County, Missouri',
      population: 21785,
      area_type: 'County',
    },
  ],
  3585: [
    {
      index: 3585,
      name: 'New Madrid County, Missouri',
      population: 15695,
      area_type: 'County',
    },
  ],
  3586: [
    {
      index: 3586,
      name: 'Oregon County, Missouri',
      population: 8732,
      area_type: 'County',
    },
  ],
  3587: [
    {
      index: 3587,
      name: 'Ozark County, Missouri',
      population: 8940,
      area_type: 'County',
    },
  ],
  3588: [
    {
      index: 3588,
      name: 'Pemiscot County, Missouri',
      population: 14841,
      area_type: 'County',
    },
  ],
  3589: [
    {
      index: 3589,
      name: 'Perry County, Missouri',
      population: 18858,
      area_type: 'County',
    },
  ],
  3590: [
    {
      index: 3590,
      name: 'Pike County, Missouri',
      population: 17664,
      area_type: 'County',
    },
  ],
  3591: [
    {
      index: 3591,
      name: 'Putnam County, Missouri',
      population: 4666,
      area_type: 'County',
    },
  ],
  3592: [
    {
      index: 3592,
      name: 'Reynolds County, Missouri',
      population: 6006,
      area_type: 'County',
    },
  ],
  3593: [
    {
      index: 3593,
      name: 'St. Clair County, Missouri',
      population: 9576,
      area_type: 'County',
    },
  ],
  3594: [
    {
      index: 3594,
      name: 'Ste. Genevieve County, Missouri',
      population: 18644,
      area_type: 'County',
    },
  ],
  3595: [
    {
      index: 3595,
      name: 'Scotland County, Missouri',
      population: 4643,
      area_type: 'County',
    },
  ],
  3596: [
    {
      index: 3596,
      name: 'Shannon County, Missouri',
      population: 7193,
      area_type: 'County',
    },
  ],
  3597: [
    {
      index: 3597,
      name: 'Shelby County, Missouri',
      population: 5982,
      area_type: 'County',
    },
  ],
  3598: [
    {
      index: 3598,
      name: 'Stoddard County, Missouri',
      population: 28377,
      area_type: 'County',
    },
  ],
  3599: [
    {
      index: 3599,
      name: 'Stone County, Missouri',
      population: 32136,
      area_type: 'County',
    },
  ],
  3600: [
    {
      index: 3600,
      name: 'Sullivan County, Missouri',
      population: 5840,
      area_type: 'County',
    },
  ],
  3601: [
    {
      index: 3601,
      name: 'Texas County, Missouri',
      population: 25336,
      area_type: 'County',
    },
  ],
  3602: [
    {
      index: 3602,
      name: 'Vernon County, Missouri',
      population: 19651,
      area_type: 'County',
    },
  ],
  3603: [
    {
      index: 3603,
      name: 'Washington County, Missouri',
      population: 23441,
      area_type: 'County',
    },
  ],
  3604: [
    {
      index: 3604,
      name: 'Wayne County, Missouri',
      population: 10792,
      area_type: 'County',
    },
  ],
  3605: [
    {
      index: 3605,
      name: 'Worth County, Missouri',
      population: 1955,
      area_type: 'County',
    },
  ],
  3606: [
    {
      index: 3606,
      name: 'Wright County, Missouri',
      population: 19156,
      area_type: 'County',
    },
  ],
  3607: [
    {
      index: 3607,
      name: 'Beaverhead County, Montana',
      population: 9719,
      area_type: 'County',
    },
  ],
  3608: [
    {
      index: 3608,
      name: 'Big Horn County, Montana',
      population: 12851,
      area_type: 'County',
    },
  ],
  3609: [
    {
      index: 3609,
      name: 'Blaine County, Montana',
      population: 6936,
      area_type: 'County',
    },
  ],
  3610: [
    {
      index: 3610,
      name: 'Broadwater County, Montana',
      population: 7793,
      area_type: 'County',
    },
  ],
  3611: [
    {
      index: 3611,
      name: 'Carter County, Montana',
      population: 1382,
      area_type: 'County',
    },
  ],
  3612: [
    {
      index: 3612,
      name: 'Chouteau County, Montana',
      population: 5898,
      area_type: 'County',
    },
  ],
  3613: [
    {
      index: 3613,
      name: 'Custer County, Montana',
      population: 12032,
      area_type: 'County',
    },
  ],
  3614: [
    {
      index: 3614,
      name: 'Daniels County, Montana',
      population: 1628,
      area_type: 'County',
    },
  ],
  3615: [
    {
      index: 3615,
      name: 'Dawson County, Montana',
      population: 8830,
      area_type: 'County',
    },
  ],
  3616: [
    {
      index: 3616,
      name: 'Deer Lodge County, Montana',
      population: 9510,
      area_type: 'County',
    },
  ],
  3617: [
    {
      index: 3617,
      name: 'Fallon County, Montana',
      population: 3011,
      area_type: 'County',
    },
  ],
  3618: [
    {
      index: 3618,
      name: 'Fergus County, Montana',
      population: 11663,
      area_type: 'County',
    },
  ],
  3619: [
    {
      index: 3619,
      name: 'Garfield County, Montana',
      population: 1218,
      area_type: 'County',
    },
  ],
  3620: [
    {
      index: 3620,
      name: 'Glacier County, Montana',
      population: 13681,
      area_type: 'County',
    },
  ],
  3621: [
    {
      index: 3621,
      name: 'Golden Valley County, Montana',
      population: 835,
      area_type: 'County',
    },
  ],
  3622: [
    {
      index: 3622,
      name: 'Granite County, Montana',
      population: 3502,
      area_type: 'County',
    },
  ],
  3623: [
    {
      index: 3623,
      name: 'Hill County, Montana',
      population: 16068,
      area_type: 'County',
    },
  ],
  3624: [
    {
      index: 3624,
      name: 'Judith Basin County, Montana',
      population: 2105,
      area_type: 'County',
    },
  ],
  3625: [
    {
      index: 3625,
      name: 'Lake County, Montana',
      population: 32853,
      area_type: 'County',
    },
  ],
  3626: [
    {
      index: 3626,
      name: 'Liberty County, Montana',
      population: 1972,
      area_type: 'County',
    },
  ],
  3627: [
    {
      index: 3627,
      name: 'Lincoln County, Montana',
      population: 21525,
      area_type: 'County',
    },
  ],
  3628: [
    {
      index: 3628,
      name: 'McCone County, Montana',
      population: 1709,
      area_type: 'County',
    },
  ],
  3629: [
    {
      index: 3629,
      name: 'Madison County, Montana',
      population: 9265,
      area_type: 'County',
    },
  ],
  3630: [
    {
      index: 3630,
      name: 'Meagher County, Montana',
      population: 2013,
      area_type: 'County',
    },
  ],
  3631: [
    {
      index: 3631,
      name: 'Mineral County, Montana',
      population: 5058,
      area_type: 'County',
    },
  ],
  3632: [
    {
      index: 3632,
      name: 'Musselshell County, Montana',
      population: 5197,
      area_type: 'County',
    },
  ],
  3633: [
    {
      index: 3633,
      name: 'Park County, Montana',
      population: 17790,
      area_type: 'County',
    },
  ],
  3634: [
    {
      index: 3634,
      name: 'Petroleum County, Montana',
      population: 524,
      area_type: 'County',
    },
  ],
  3635: [
    {
      index: 3635,
      name: 'Phillips County, Montana',
      population: 4240,
      area_type: 'County',
    },
  ],
  3636: [
    {
      index: 3636,
      name: 'Pondera County, Montana',
      population: 6078,
      area_type: 'County',
    },
  ],
  3637: [
    {
      index: 3637,
      name: 'Powder River County, Montana',
      population: 1725,
      area_type: 'County',
    },
  ],
  3638: [
    {
      index: 3638,
      name: 'Powell County, Montana',
      population: 7051,
      area_type: 'County',
    },
  ],
  3639: [
    {
      index: 3639,
      name: 'Prairie County, Montana',
      population: 1107,
      area_type: 'County',
    },
  ],
  3640: [
    {
      index: 3640,
      name: 'Ravalli County, Montana',
      population: 47298,
      area_type: 'County',
    },
  ],
  3641: [
    {
      index: 3641,
      name: 'Richland County, Montana',
      population: 11237,
      area_type: 'County',
    },
  ],
  3642: [
    {
      index: 3642,
      name: 'Roosevelt County, Montana',
      population: 10572,
      area_type: 'County',
    },
  ],
  3643: [
    {
      index: 3643,
      name: 'Rosebud County, Montana',
      population: 8088,
      area_type: 'County',
    },
  ],
  3644: [
    {
      index: 3644,
      name: 'Sanders County, Montana',
      population: 13442,
      area_type: 'County',
    },
  ],
  3645: [
    {
      index: 3645,
      name: 'Sheridan County, Montana',
      population: 3564,
      area_type: 'County',
    },
  ],
  3646: [
    {
      index: 3646,
      name: 'Sweet Grass County, Montana',
      population: 3715,
      area_type: 'County',
    },
  ],
  3647: [
    {
      index: 3647,
      name: 'Teton County, Montana',
      population: 6368,
      area_type: 'County',
    },
  ],
  3648: [
    {
      index: 3648,
      name: 'Toole County, Montana',
      population: 5082,
      area_type: 'County',
    },
  ],
  3649: [
    {
      index: 3649,
      name: 'Treasure County, Montana',
      population: 758,
      area_type: 'County',
    },
  ],
  3650: [
    {
      index: 3650,
      name: 'Valley County, Montana',
      population: 7513,
      area_type: 'County',
    },
  ],
  3651: [
    {
      index: 3651,
      name: 'Wheatland County, Montana',
      population: 2032,
      area_type: 'County',
    },
  ],
  3652: [
    {
      index: 3652,
      name: 'Wibaux County, Montana',
      population: 919,
      area_type: 'County',
    },
  ],
  3653: [
    {
      index: 3653,
      name: 'Antelope County, Nebraska',
      population: 6293,
      area_type: 'County',
    },
  ],
  3654: [
    {
      index: 3654,
      name: 'Arthur County, Nebraska',
      population: 433,
      area_type: 'County',
    },
  ],
  3655: [
    {
      index: 3655,
      name: 'Blaine County, Nebraska',
      population: 453,
      area_type: 'County',
    },
  ],
  3656: [
    {
      index: 3656,
      name: 'Boone County, Nebraska',
      population: 5385,
      area_type: 'County',
    },
  ],
  3657: [
    {
      index: 3657,
      name: 'Box Butte County, Nebraska',
      population: 10672,
      area_type: 'County',
    },
  ],
  3658: [
    {
      index: 3658,
      name: 'Boyd County, Nebraska',
      population: 1741,
      area_type: 'County',
    },
  ],
  3659: [
    {
      index: 3659,
      name: 'Brown County, Nebraska',
      population: 2872,
      area_type: 'County',
    },
  ],
  3660: [
    {
      index: 3660,
      name: 'Burt County, Nebraska',
      population: 6755,
      area_type: 'County',
    },
  ],
  3661: [
    {
      index: 3661,
      name: 'Butler County, Nebraska',
      population: 8427,
      area_type: 'County',
    },
  ],
  3662: [
    {
      index: 3662,
      name: 'Cedar County, Nebraska',
      population: 8371,
      area_type: 'County',
    },
  ],
  3663: [
    {
      index: 3663,
      name: 'Chase County, Nebraska',
      population: 3772,
      area_type: 'County',
    },
  ],
  3664: [
    {
      index: 3664,
      name: 'Cherry County, Nebraska',
      population: 5464,
      area_type: 'County',
    },
  ],
  3665: [
    {
      index: 3665,
      name: 'Cheyenne County, Nebraska',
      population: 9511,
      area_type: 'County',
    },
  ],
  3666: [
    {
      index: 3666,
      name: 'Clay County, Nebraska',
      population: 6049,
      area_type: 'County',
    },
  ],
  3667: [
    {
      index: 3667,
      name: 'Colfax County, Nebraska',
      population: 10444,
      area_type: 'County',
    },
  ],
  3668: [
    {
      index: 3668,
      name: 'Cuming County, Nebraska',
      population: 8929,
      area_type: 'County',
    },
  ],
  3669: [
    {
      index: 3669,
      name: 'Custer County, Nebraska',
      population: 10476,
      area_type: 'County',
    },
  ],
  3670: [
    {
      index: 3670,
      name: 'Dawes County, Nebraska',
      population: 8241,
      area_type: 'County',
    },
  ],
  3671: [
    {
      index: 3671,
      name: 'Deuel County, Nebraska',
      population: 1902,
      area_type: 'County',
    },
  ],
  3672: [
    {
      index: 3672,
      name: 'Dundy County, Nebraska',
      population: 1590,
      area_type: 'County',
    },
  ],
  3673: [
    {
      index: 3673,
      name: 'Fillmore County, Nebraska',
      population: 5553,
      area_type: 'County',
    },
  ],
  3674: [
    {
      index: 3674,
      name: 'Franklin County, Nebraska',
      population: 2873,
      area_type: 'County',
    },
  ],
  3675: [
    {
      index: 3675,
      name: 'Frontier County, Nebraska',
      population: 2633,
      area_type: 'County',
    },
  ],
  3676: [
    {
      index: 3676,
      name: 'Furnas County, Nebraska',
      population: 4575,
      area_type: 'County',
    },
  ],
  3677: [
    {
      index: 3677,
      name: 'Garden County, Nebraska',
      population: 1837,
      area_type: 'County',
    },
  ],
  3678: [
    {
      index: 3678,
      name: 'Garfield County, Nebraska',
      population: 1801,
      area_type: 'County',
    },
  ],
  3679: [
    {
      index: 3679,
      name: 'Grant County, Nebraska',
      population: 576,
      area_type: 'County',
    },
  ],
  3680: [
    {
      index: 3680,
      name: 'Greeley County, Nebraska',
      population: 2227,
      area_type: 'County',
    },
  ],
  3681: [
    {
      index: 3681,
      name: 'Hamilton County, Nebraska',
      population: 9429,
      area_type: 'County',
    },
  ],
  3682: [
    {
      index: 3682,
      name: 'Harlan County, Nebraska',
      population: 3054,
      area_type: 'County',
    },
  ],
  3683: [
    {
      index: 3683,
      name: 'Hayes County, Nebraska',
      population: 849,
      area_type: 'County',
    },
  ],
  3684: [
    {
      index: 3684,
      name: 'Hitchcock County, Nebraska',
      population: 2598,
      area_type: 'County',
    },
  ],
  3685: [
    {
      index: 3685,
      name: 'Holt County, Nebraska',
      population: 10043,
      area_type: 'County',
    },
  ],
  3686: [
    {
      index: 3686,
      name: 'Hooker County, Nebraska',
      population: 686,
      area_type: 'County',
    },
  ],
  3687: [
    {
      index: 3687,
      name: 'Jefferson County, Nebraska',
      population: 7154,
      area_type: 'County',
    },
  ],
  3688: [
    {
      index: 3688,
      name: 'Johnson County, Nebraska',
      population: 5287,
      area_type: 'County',
    },
  ],
  3689: [
    {
      index: 3689,
      name: 'Keith County, Nebraska',
      population: 8269,
      area_type: 'County',
    },
  ],
  3690: [
    {
      index: 3690,
      name: 'Keya Paha County, Nebraska',
      population: 787,
      area_type: 'County',
    },
  ],
  3691: [
    {
      index: 3691,
      name: 'Kimball County, Nebraska',
      population: 3315,
      area_type: 'County',
    },
  ],
  3692: [
    {
      index: 3692,
      name: 'Knox County, Nebraska',
      population: 8336,
      area_type: 'County',
    },
  ],
  3693: [
    {
      index: 3693,
      name: 'Loup County, Nebraska',
      population: 599,
      area_type: 'County',
    },
  ],
  3694: [
    {
      index: 3694,
      name: 'Morrill County, Nebraska',
      population: 4527,
      area_type: 'County',
    },
  ],
  3695: [
    {
      index: 3695,
      name: 'Nance County, Nebraska',
      population: 3326,
      area_type: 'County',
    },
  ],
  3696: [
    {
      index: 3696,
      name: 'Nemaha County, Nebraska',
      population: 7035,
      area_type: 'County',
    },
  ],
  3697: [
    {
      index: 3697,
      name: 'Nuckolls County, Nebraska',
      population: 4041,
      area_type: 'County',
    },
  ],
  3698: [
    {
      index: 3698,
      name: 'Otoe County, Nebraska',
      population: 16198,
      area_type: 'County',
    },
  ],
  3699: [
    {
      index: 3699,
      name: 'Pawnee County, Nebraska',
      population: 2528,
      area_type: 'County',
    },
  ],
  3700: [
    {
      index: 3700,
      name: 'Perkins County, Nebraska',
      population: 2829,
      area_type: 'County',
    },
  ],
  3701: [
    {
      index: 3701,
      name: 'Phelps County, Nebraska',
      population: 8988,
      area_type: 'County',
    },
  ],
  3702: [
    {
      index: 3702,
      name: 'Polk County, Nebraska',
      population: 5166,
      area_type: 'County',
    },
  ],
  3703: [
    {
      index: 3703,
      name: 'Red Willow County, Nebraska',
      population: 10573,
      area_type: 'County',
    },
  ],
  3704: [
    {
      index: 3704,
      name: 'Richardson County, Nebraska',
      population: 7705,
      area_type: 'County',
    },
  ],
  3705: [
    {
      index: 3705,
      name: 'Rock County, Nebraska',
      population: 1245,
      area_type: 'County',
    },
  ],
  3706: [
    {
      index: 3706,
      name: 'Saline County, Nebraska',
      population: 14116,
      area_type: 'County',
    },
  ],
  3707: [
    {
      index: 3707,
      name: 'Sheridan County, Nebraska',
      population: 4996,
      area_type: 'County',
    },
  ],
  3708: [
    {
      index: 3708,
      name: 'Sherman County, Nebraska',
      population: 2980,
      area_type: 'County',
    },
  ],
  3709: [
    {
      index: 3709,
      name: 'Thayer County, Nebraska',
      population: 4885,
      area_type: 'County',
    },
  ],
  3710: [
    {
      index: 3710,
      name: 'Thomas County, Nebraska',
      population: 671,
      area_type: 'County',
    },
  ],
  3711: [
    {
      index: 3711,
      name: 'Thurston County, Nebraska',
      population: 6507,
      area_type: 'County',
    },
  ],
  3712: [
    {
      index: 3712,
      name: 'Valley County, Nebraska',
      population: 4073,
      area_type: 'County',
    },
  ],
  3713: [
    {
      index: 3713,
      name: 'Wayne County, Nebraska',
      population: 9871,
      area_type: 'County',
    },
  ],
  3714: [
    {
      index: 3714,
      name: 'Webster County, Nebraska',
      population: 3336,
      area_type: 'County',
    },
  ],
  3715: [
    {
      index: 3715,
      name: 'Wheeler County, Nebraska',
      population: 785,
      area_type: 'County',
    },
  ],
  3716: [
    {
      index: 3716,
      name: 'York County, Nebraska',
      population: 14354,
      area_type: 'County',
    },
  ],
  3717: [
    {
      index: 3717,
      name: 'Esmeralda County, Nevada',
      population: 744,
      area_type: 'County',
    },
  ],
  3718: [
    {
      index: 3718,
      name: 'Lander County, Nevada',
      population: 5766,
      area_type: 'County',
    },
  ],
  3719: [
    {
      index: 3719,
      name: 'Lincoln County, Nevada',
      population: 4482,
      area_type: 'County',
    },
  ],
  3720: [
    {
      index: 3720,
      name: 'Mineral County, Nevada',
      population: 4525,
      area_type: 'County',
    },
  ],
  3721: [
    {
      index: 3721,
      name: 'Pershing County, Nevada',
      population: 6462,
      area_type: 'County',
    },
  ],
  3722: [
    {
      index: 3722,
      name: 'White Pine County, Nevada',
      population: 8788,
      area_type: 'County',
    },
  ],
  3723: [
    {
      index: 3723,
      name: 'Carroll County, New Hampshire',
      population: 52199,
      area_type: 'County',
    },
  ],
  3724: [
    {
      index: 3724,
      name: 'Catron County, New Mexico',
      population: 3827,
      area_type: 'County',
    },
  ],
  3725: [
    {
      index: 3725,
      name: 'Colfax County, New Mexico',
      population: 12246,
      area_type: 'County',
    },
  ],
  3726: [
    {
      index: 3726,
      name: 'De Baca County, New Mexico',
      population: 1693,
      area_type: 'County',
    },
  ],
  3727: [
    {
      index: 3727,
      name: 'Guadalupe County, New Mexico',
      population: 4310,
      area_type: 'County',
    },
  ],
  3728: [
    {
      index: 3728,
      name: 'Harding County, New Mexico',
      population: 628,
      area_type: 'County',
    },
  ],
  3729: [
    {
      index: 3729,
      name: 'Hidalgo County, New Mexico',
      population: 4003,
      area_type: 'County',
    },
  ],
  3730: [
    {
      index: 3730,
      name: 'Quay County, New Mexico',
      population: 8546,
      area_type: 'County',
    },
  ],
  3731: [
    {
      index: 3731,
      name: 'Sierra County, New Mexico',
      population: 11436,
      area_type: 'County',
    },
  ],
  3732: [
    {
      index: 3732,
      name: 'Socorro County, New Mexico',
      population: 16115,
      area_type: 'County',
    },
  ],
  3733: [
    {
      index: 3733,
      name: 'Union County, New Mexico',
      population: 3980,
      area_type: 'County',
    },
  ],
  3734: [
    {
      index: 3734,
      name: 'Allegany County, New York',
      population: 46694,
      area_type: 'County',
    },
  ],
  3735: [
    {
      index: 3735,
      name: 'Chenango County, New York',
      population: 46458,
      area_type: 'County',
    },
  ],
  3736: [
    {
      index: 3736,
      name: 'Delaware County, New York',
      population: 44740,
      area_type: 'County',
    },
  ],
  3737: [
    {
      index: 3737,
      name: 'Essex County, New York',
      population: 36910,
      area_type: 'County',
    },
  ],
  3738: [
    {
      index: 3738,
      name: 'Greene County, New York',
      population: 48061,
      area_type: 'County',
    },
  ],
  3739: [
    {
      index: 3739,
      name: 'Hamilton County, New York',
      population: 5118,
      area_type: 'County',
    },
  ],
  3740: [
    {
      index: 3740,
      name: 'Lewis County, New York',
      population: 26699,
      area_type: 'County',
    },
  ],
  3741: [
    {
      index: 3741,
      name: 'Schuyler County, New York',
      population: 17650,
      area_type: 'County',
    },
  ],
  3742: [
    {
      index: 3742,
      name: 'Sullivan County, New York',
      population: 79658,
      area_type: 'County',
    },
  ],
  3743: [
    {
      index: 3743,
      name: 'Wyoming County, New York',
      population: 39666,
      area_type: 'County',
    },
  ],
  3744: [
    {
      index: 3744,
      name: 'Alleghany County, North Carolina',
      population: 11185,
      area_type: 'County',
    },
  ],
  3745: [
    {
      index: 3745,
      name: 'Ashe County, North Carolina',
      population: 27110,
      area_type: 'County',
    },
  ],
  3746: [
    {
      index: 3746,
      name: 'Avery County, North Carolina',
      population: 17571,
      area_type: 'County',
    },
  ],
  3747: [
    {
      index: 3747,
      name: 'Bertie County, North Carolina',
      population: 17240,
      area_type: 'County',
    },
  ],
  3748: [
    {
      index: 3748,
      name: 'Bladen County, North Carolina',
      population: 29446,
      area_type: 'County',
    },
  ],
  3749: [
    {
      index: 3749,
      name: 'Caswell County, North Carolina',
      population: 22614,
      area_type: 'County',
    },
  ],
  3750: [
    {
      index: 3750,
      name: 'Cherokee County, North Carolina',
      population: 29512,
      area_type: 'County',
    },
  ],
  3751: [
    {
      index: 3751,
      name: 'Chowan County, North Carolina',
      population: 13940,
      area_type: 'County',
    },
  ],
  3752: [
    {
      index: 3752,
      name: 'Clay County, North Carolina',
      population: 11614,
      area_type: 'County',
    },
  ],
  3753: [
    {
      index: 3753,
      name: 'Columbus County, North Carolina',
      population: 49885,
      area_type: 'County',
    },
  ],
  3754: [
    {
      index: 3754,
      name: 'Duplin County, North Carolina',
      population: 48990,
      area_type: 'County',
    },
  ],
  3755: [
    {
      index: 3755,
      name: 'Graham County, North Carolina',
      population: 7980,
      area_type: 'County',
    },
  ],
  3756: [
    {
      index: 3756,
      name: 'Greene County, North Carolina',
      population: 20211,
      area_type: 'County',
    },
  ],
  3757: [
    {
      index: 3757,
      name: 'Hertford County, North Carolina',
      population: 20875,
      area_type: 'County',
    },
  ],
  3758: [
    {
      index: 3758,
      name: 'Hyde County, North Carolina',
      population: 4576,
      area_type: 'County',
    },
  ],
  3759: [
    {
      index: 3759,
      name: 'Macon County, North Carolina',
      population: 38065,
      area_type: 'County',
    },
  ],
  3760: [
    {
      index: 3760,
      name: 'Martin County, North Carolina',
      population: 21508,
      area_type: 'County',
    },
  ],
  3761: [
    {
      index: 3761,
      name: 'Mitchell County, North Carolina',
      population: 15094,
      area_type: 'County',
    },
  ],
  3762: [
    {
      index: 3762,
      name: 'Montgomery County, North Carolina',
      population: 25894,
      area_type: 'County',
    },
  ],
  3763: [
    {
      index: 3763,
      name: 'Polk County, North Carolina',
      population: 19986,
      area_type: 'County',
    },
  ],
  3764: [
    {
      index: 3764,
      name: 'Sampson County, North Carolina',
      population: 59120,
      area_type: 'County',
    },
  ],
  3765: [
    {
      index: 3765,
      name: 'Tyrrell County, North Carolina',
      population: 3365,
      area_type: 'County',
    },
  ],
  3766: [
    {
      index: 3766,
      name: 'Warren County, North Carolina',
      population: 18713,
      area_type: 'County',
    },
  ],
  3767: [
    {
      index: 3767,
      name: 'Washington County, North Carolina',
      population: 10828,
      area_type: 'County',
    },
  ],
  3768: [
    {
      index: 3768,
      name: 'Yancey County, North Carolina',
      population: 18811,
      area_type: 'County',
    },
  ],
  3769: [
    {
      index: 3769,
      name: 'Adams County, North Dakota',
      population: 2115,
      area_type: 'County',
    },
  ],
  3770: [
    {
      index: 3770,
      name: 'Barnes County, North Dakota',
      population: 10758,
      area_type: 'County',
    },
  ],
  3771: [
    {
      index: 3771,
      name: 'Benson County, North Dakota',
      population: 5770,
      area_type: 'County',
    },
  ],
  3772: [
    {
      index: 3772,
      name: 'Bottineau County, North Dakota',
      population: 6376,
      area_type: 'County',
    },
  ],
  3773: [
    {
      index: 3773,
      name: 'Bowman County, North Dakota',
      population: 2894,
      area_type: 'County',
    },
  ],
  3774: [
    {
      index: 3774,
      name: 'Burke County, North Dakota',
      population: 2155,
      area_type: 'County',
    },
  ],
  3775: [
    {
      index: 3775,
      name: 'Cavalier County, North Dakota',
      population: 3597,
      area_type: 'County',
    },
  ],
  3776: [
    {
      index: 3776,
      name: 'Dickey County, North Dakota',
      population: 4923,
      area_type: 'County',
    },
  ],
  3777: [
    {
      index: 3777,
      name: 'Divide County, North Dakota',
      population: 2187,
      area_type: 'County',
    },
  ],
  3778: [
    {
      index: 3778,
      name: 'Dunn County, North Dakota',
      population: 4015,
      area_type: 'County',
    },
  ],
  3779: [
    {
      index: 3779,
      name: 'Eddy County, North Dakota',
      population: 2314,
      area_type: 'County',
    },
  ],
  3780: [
    {
      index: 3780,
      name: 'Emmons County, North Dakota',
      population: 3250,
      area_type: 'County',
    },
  ],
  3781: [
    {
      index: 3781,
      name: 'Foster County, North Dakota',
      population: 3378,
      area_type: 'County',
    },
  ],
  3782: [
    {
      index: 3782,
      name: 'Golden Valley County, North Dakota',
      population: 1744,
      area_type: 'County',
    },
  ],
  3783: [
    {
      index: 3783,
      name: 'Grant County, North Dakota',
      population: 2243,
      area_type: 'County',
    },
  ],
  3784: [
    {
      index: 3784,
      name: 'Griggs County, North Dakota',
      population: 2252,
      area_type: 'County',
    },
  ],
  3785: [
    {
      index: 3785,
      name: 'Hettinger County, North Dakota',
      population: 2406,
      area_type: 'County',
    },
  ],
  3786: [
    {
      index: 3786,
      name: 'Kidder County, North Dakota',
      population: 2393,
      area_type: 'County',
    },
  ],
  3787: [
    {
      index: 3787,
      name: 'LaMoure County, North Dakota',
      population: 4098,
      area_type: 'County',
    },
  ],
  3788: [
    {
      index: 3788,
      name: 'Logan County, North Dakota',
      population: 1855,
      area_type: 'County',
    },
  ],
  3789: [
    {
      index: 3789,
      name: 'McIntosh County, North Dakota',
      population: 2475,
      area_type: 'County',
    },
  ],
  3790: [
    {
      index: 3790,
      name: 'McKenzie County, North Dakota',
      population: 13908,
      area_type: 'County',
    },
  ],
  3791: [
    {
      index: 3791,
      name: 'McLean County, North Dakota',
      population: 9824,
      area_type: 'County',
    },
  ],
  3792: [
    {
      index: 3792,
      name: 'Mercer County, North Dakota',
      population: 8333,
      area_type: 'County',
    },
  ],
  3793: [
    {
      index: 3793,
      name: 'Mountrail County, North Dakota',
      population: 9290,
      area_type: 'County',
    },
  ],
  3794: [
    {
      index: 3794,
      name: 'Nelson County, North Dakota',
      population: 2995,
      area_type: 'County',
    },
  ],
  3795: [
    {
      index: 3795,
      name: 'Pembina County, North Dakota',
      population: 6763,
      area_type: 'County',
    },
  ],
  3796: [
    {
      index: 3796,
      name: 'Pierce County, North Dakota',
      population: 3942,
      area_type: 'County',
    },
  ],
  3797: [
    {
      index: 3797,
      name: 'Ramsey County, North Dakota',
      population: 11515,
      area_type: 'County',
    },
  ],
  3798: [
    {
      index: 3798,
      name: 'Ransom County, North Dakota',
      population: 5640,
      area_type: 'County',
    },
  ],
  3799: [
    {
      index: 3799,
      name: 'Rolette County, North Dakota',
      population: 11933,
      area_type: 'County',
    },
  ],
  3800: [
    {
      index: 3800,
      name: 'Sargent County, North Dakota',
      population: 3795,
      area_type: 'County',
    },
  ],
  3801: [
    {
      index: 3801,
      name: 'Sheridan County, North Dakota',
      population: 1295,
      area_type: 'County',
    },
  ],
  3802: [
    {
      index: 3802,
      name: 'Sioux County, North Dakota',
      population: 3711,
      area_type: 'County',
    },
  ],
  3803: [
    {
      index: 3803,
      name: 'Slope County, North Dakota',
      population: 672,
      area_type: 'County',
    },
  ],
  3804: [
    {
      index: 3804,
      name: 'Steele County, North Dakota',
      population: 1788,
      area_type: 'County',
    },
  ],
  3805: [
    {
      index: 3805,
      name: 'Towner County, North Dakota',
      population: 2064,
      area_type: 'County',
    },
  ],
  3806: [
    {
      index: 3806,
      name: 'Traill County, North Dakota',
      population: 7958,
      area_type: 'County',
    },
  ],
  3807: [
    {
      index: 3807,
      name: 'Walsh County, North Dakota',
      population: 10438,
      area_type: 'County',
    },
  ],
  3808: [
    {
      index: 3808,
      name: 'Wells County, North Dakota',
      population: 3930,
      area_type: 'County',
    },
  ],
  3809: [
    {
      index: 3809,
      name: 'Adams County, Ohio',
      population: 27420,
      area_type: 'County',
    },
  ],
  3810: [
    {
      index: 3810,
      name: 'Hardin County, Ohio',
      population: 30416,
      area_type: 'County',
    },
  ],
  3811: [
    {
      index: 3811,
      name: 'Harrison County, Ohio',
      population: 14378,
      area_type: 'County',
    },
  ],
  3812: [
    {
      index: 3812,
      name: 'Henry County, Ohio',
      population: 27512,
      area_type: 'County',
    },
  ],
  3813: [
    {
      index: 3813,
      name: 'Highland County, Ohio',
      population: 43391,
      area_type: 'County',
    },
  ],
  3814: [
    {
      index: 3814,
      name: 'Holmes County, Ohio',
      population: 44390,
      area_type: 'County',
    },
  ],
  3815: [
    {
      index: 3815,
      name: 'Meigs County, Ohio',
      population: 21969,
      area_type: 'County',
    },
  ],
  3816: [
    {
      index: 3816,
      name: 'Monroe County, Ohio',
      population: 13234,
      area_type: 'County',
    },
  ],
  3817: [
    {
      index: 3817,
      name: 'Morgan County, Ohio',
      population: 13668,
      area_type: 'County',
    },
  ],
  3818: [
    {
      index: 3818,
      name: 'Noble County, Ohio',
      population: 14335,
      area_type: 'County',
    },
  ],
  3819: [
    {
      index: 3819,
      name: 'Paulding County, Ohio',
      population: 18757,
      area_type: 'County',
    },
  ],
  3820: [
    {
      index: 3820,
      name: 'Pike County, Ohio',
      population: 27005,
      area_type: 'County',
    },
  ],
  3821: [
    {
      index: 3821,
      name: 'Preble County, Ohio',
      population: 40596,
      area_type: 'County',
    },
  ],
  3822: [
    {
      index: 3822,
      name: 'Putnam County, Ohio',
      population: 34334,
      area_type: 'County',
    },
  ],
  3823: [
    {
      index: 3823,
      name: 'Vinton County, Ohio',
      population: 12565,
      area_type: 'County',
    },
  ],
  3824: [
    {
      index: 3824,
      name: 'Williams County, Ohio',
      population: 36652,
      area_type: 'County',
    },
  ],
  3825: [
    {
      index: 3825,
      name: 'Wyandot County, Ohio',
      population: 21567,
      area_type: 'County',
    },
  ],
  3826: [
    {
      index: 3826,
      name: 'Adair County, Oklahoma',
      population: 19576,
      area_type: 'County',
    },
  ],
  3827: [
    {
      index: 3827,
      name: 'Alfalfa County, Oklahoma',
      population: 5637,
      area_type: 'County',
    },
  ],
  3828: [
    {
      index: 3828,
      name: 'Atoka County, Oklahoma',
      population: 14262,
      area_type: 'County',
    },
  ],
  3829: [
    {
      index: 3829,
      name: 'Beaver County, Oklahoma',
      population: 5016,
      area_type: 'County',
    },
  ],
  3830: [
    {
      index: 3830,
      name: 'Blaine County, Oklahoma',
      population: 8409,
      area_type: 'County',
    },
  ],
  3831: [
    {
      index: 3831,
      name: 'Caddo County, Oklahoma',
      population: 26198,
      area_type: 'County',
    },
  ],
  3832: [
    {
      index: 3832,
      name: 'Choctaw County, Oklahoma',
      population: 14358,
      area_type: 'County',
    },
  ],
  3833: [
    {
      index: 3833,
      name: 'Cimarron County, Oklahoma',
      population: 2252,
      area_type: 'County',
    },
  ],
  3834: [
    {
      index: 3834,
      name: 'Coal County, Oklahoma',
      population: 5313,
      area_type: 'County',
    },
  ],
  3835: [
    {
      index: 3835,
      name: 'Craig County, Oklahoma',
      population: 14123,
      area_type: 'County',
    },
  ],
  3836: [
    {
      index: 3836,
      name: 'Delaware County, Oklahoma',
      population: 41413,
      area_type: 'County',
    },
  ],
  3837: [
    {
      index: 3837,
      name: 'Dewey County, Oklahoma',
      population: 4401,
      area_type: 'County',
    },
  ],
  3838: [
    {
      index: 3838,
      name: 'Garvin County, Oklahoma',
      population: 25713,
      area_type: 'County',
    },
  ],
  3839: [
    {
      index: 3839,
      name: 'Grant County, Oklahoma',
      population: 4124,
      area_type: 'County',
    },
  ],
  3840: [
    {
      index: 3840,
      name: 'Greer County, Oklahoma',
      population: 5547,
      area_type: 'County',
    },
  ],
  3841: [
    {
      index: 3841,
      name: 'Harmon County, Oklahoma',
      population: 2428,
      area_type: 'County',
    },
  ],
  3842: [
    {
      index: 3842,
      name: 'Harper County, Oklahoma',
      population: 3129,
      area_type: 'County',
    },
  ],
  3843: [
    {
      index: 3843,
      name: 'Haskell County, Oklahoma',
      population: 11641,
      area_type: 'County',
    },
  ],
  3844: [
    {
      index: 3844,
      name: 'Hughes County, Oklahoma',
      population: 13407,
      area_type: 'County',
    },
  ],
  3845: [
    {
      index: 3845,
      name: 'Jefferson County, Oklahoma',
      population: 5389,
      area_type: 'County',
    },
  ],
  3846: [
    {
      index: 3846,
      name: 'Johnston County, Oklahoma',
      population: 10406,
      area_type: 'County',
    },
  ],
  3847: [
    {
      index: 3847,
      name: 'Kingfisher County, Oklahoma',
      population: 15293,
      area_type: 'County',
    },
  ],
  3848: [
    {
      index: 3848,
      name: 'Kiowa County, Oklahoma',
      population: 8345,
      area_type: 'County',
    },
  ],
  3849: [
    {
      index: 3849,
      name: 'Latimer County, Oklahoma',
      population: 9630,
      area_type: 'County',
    },
  ],
  3850: [
    {
      index: 3850,
      name: 'Le Flore County, Oklahoma',
      population: 48907,
      area_type: 'County',
    },
  ],
  3851: [
    {
      index: 3851,
      name: 'McCurtain County, Oklahoma',
      population: 30931,
      area_type: 'County',
    },
  ],
  3852: [
    {
      index: 3852,
      name: 'McIntosh County, Oklahoma',
      population: 19451,
      area_type: 'County',
    },
  ],
  3853: [
    {
      index: 3853,
      name: 'Major County, Oklahoma',
      population: 7502,
      area_type: 'County',
    },
  ],
  3854: [
    {
      index: 3854,
      name: 'Marshall County, Oklahoma',
      population: 15882,
      area_type: 'County',
    },
  ],
  3855: [
    {
      index: 3855,
      name: 'Mayes County, Oklahoma',
      population: 39589,
      area_type: 'County',
    },
  ],
  3856: [
    {
      index: 3856,
      name: 'Murray County, Oklahoma',
      population: 13672,
      area_type: 'County',
    },
  ],
  3857: [
    {
      index: 3857,
      name: 'Noble County, Oklahoma',
      population: 10896,
      area_type: 'County',
    },
  ],
  3858: [
    {
      index: 3858,
      name: 'Nowata County, Oklahoma',
      population: 9483,
      area_type: 'County',
    },
  ],
  3859: [
    {
      index: 3859,
      name: 'Okfuskee County, Oklahoma',
      population: 11134,
      area_type: 'County',
    },
  ],
  3860: [
    {
      index: 3860,
      name: 'Pushmataha County, Oklahoma',
      population: 10769,
      area_type: 'County',
    },
  ],
  3861: [
    {
      index: 3861,
      name: 'Roger Mills County, Oklahoma',
      population: 3320,
      area_type: 'County',
    },
  ],
  3862: [
    {
      index: 3862,
      name: 'Seminole County, Oklahoma',
      population: 23351,
      area_type: 'County',
    },
  ],
  3863: [
    {
      index: 3863,
      name: 'Tillman County, Oklahoma',
      population: 6977,
      area_type: 'County',
    },
  ],
  3864: [
    {
      index: 3864,
      name: 'Washita County, Oklahoma',
      population: 10732,
      area_type: 'County',
    },
  ],
  3865: [
    {
      index: 3865,
      name: 'Woods County, Oklahoma',
      population: 8587,
      area_type: 'County',
    },
  ],
  3866: [
    {
      index: 3866,
      name: 'Baker County, Oregon',
      population: 16938,
      area_type: 'County',
    },
  ],
  3867: [
    {
      index: 3867,
      name: 'Gilliam County, Oregon',
      population: 2018,
      area_type: 'County',
    },
  ],
  3868: [
    {
      index: 3868,
      name: 'Grant County, Oregon',
      population: 7218,
      area_type: 'County',
    },
  ],
  3869: [
    {
      index: 3869,
      name: 'Harney County, Oregon',
      population: 7515,
      area_type: 'County',
    },
  ],
  3870: [
    {
      index: 3870,
      name: 'Jefferson County, Oregon',
      population: 25330,
      area_type: 'County',
    },
  ],
  3871: [
    {
      index: 3871,
      name: 'Lake County, Oregon',
      population: 8385,
      area_type: 'County',
    },
  ],
  3872: [
    {
      index: 3872,
      name: 'Sherman County, Oregon',
      population: 1955,
      area_type: 'County',
    },
  ],
  3873: [
    {
      index: 3873,
      name: 'Tillamook County, Oregon',
      population: 27574,
      area_type: 'County',
    },
  ],
  3874: [
    {
      index: 3874,
      name: 'Wallowa County, Oregon',
      population: 7659,
      area_type: 'County',
    },
  ],
  3875: [
    {
      index: 3875,
      name: 'Wheeler County, Oregon',
      population: 1445,
      area_type: 'County',
    },
  ],
  3876: [
    {
      index: 3876,
      name: 'Bedford County, Pennsylvania',
      population: 47418,
      area_type: 'County',
    },
  ],
  3877: [
    {
      index: 3877,
      name: 'Cameron County, Pennsylvania',
      population: 4418,
      area_type: 'County',
    },
  ],
  3878: [
    {
      index: 3878,
      name: 'Clarion County, Pennsylvania',
      population: 37346,
      area_type: 'County',
    },
  ],
  3879: [
    {
      index: 3879,
      name: 'Forest County, Pennsylvania',
      population: 6626,
      area_type: 'County',
    },
  ],
  3880: [
    {
      index: 3880,
      name: 'Fulton County, Pennsylvania',
      population: 14533,
      area_type: 'County',
    },
  ],
  3881: [
    {
      index: 3881,
      name: 'Greene County, Pennsylvania',
      population: 34663,
      area_type: 'County',
    },
  ],
  3882: [
    {
      index: 3882,
      name: 'Jefferson County, Pennsylvania',
      population: 43794,
      area_type: 'County',
    },
  ],
  3883: [
    {
      index: 3883,
      name: 'Juniata County, Pennsylvania',
      population: 23339,
      area_type: 'County',
    },
  ],
  3884: [
    {
      index: 3884,
      name: 'Potter County, Pennsylvania',
      population: 16220,
      area_type: 'County',
    },
  ],
  3885: [
    {
      index: 3885,
      name: 'Sullivan County, Pennsylvania',
      population: 5855,
      area_type: 'County',
    },
  ],
  3886: [
    {
      index: 3886,
      name: 'Susquehanna County, Pennsylvania',
      population: 38074,
      area_type: 'County',
    },
  ],
  3887: [
    {
      index: 3887,
      name: 'Tioga County, Pennsylvania',
      population: 41106,
      area_type: 'County',
    },
  ],
  3888: [
    {
      index: 3888,
      name: 'Wayne County, Pennsylvania',
      population: 51173,
      area_type: 'County',
    },
  ],
  3889: [
    {
      index: 3889,
      name: 'Abbeville County, South Carolina',
      population: 24356,
      area_type: 'County',
    },
  ],
  3890: [
    {
      index: 3890,
      name: 'Allendale County, South Carolina',
      population: 7579,
      area_type: 'County',
    },
  ],
  3891: [
    {
      index: 3891,
      name: 'Bamberg County, South Carolina',
      population: 12908,
      area_type: 'County',
    },
  ],
  3892: [
    {
      index: 3892,
      name: 'Barnwell County, South Carolina',
      population: 20414,
      area_type: 'County',
    },
  ],
  3893: [
    {
      index: 3893,
      name: 'Chesterfield County, South Carolina',
      population: 43683,
      area_type: 'County',
    },
  ],
  3894: [
    {
      index: 3894,
      name: 'Colleton County, South Carolina',
      population: 38599,
      area_type: 'County',
    },
  ],
  3895: [
    {
      index: 3895,
      name: 'Dillon County, South Carolina',
      population: 27738,
      area_type: 'County',
    },
  ],
  3896: [
    {
      index: 3896,
      name: 'Hampton County, South Carolina',
      population: 18113,
      area_type: 'County',
    },
  ],
  3897: [
    {
      index: 3897,
      name: 'Lee County, South Carolina',
      population: 16153,
      area_type: 'County',
    },
  ],
  3898: [
    {
      index: 3898,
      name: 'McCormick County, South Carolina',
      population: 9764,
      area_type: 'County',
    },
  ],
  3899: [
    {
      index: 3899,
      name: 'Marion County, South Carolina',
      population: 28450,
      area_type: 'County',
    },
  ],
  3900: [
    {
      index: 3900,
      name: 'Williamsburg County, South Carolina',
      population: 30058,
      area_type: 'County',
    },
  ],
  3901: [
    {
      index: 3901,
      name: 'Aurora County, South Dakota',
      population: 2755,
      area_type: 'County',
    },
  ],
  3902: [
    {
      index: 3902,
      name: 'Bennett County, South Dakota',
      population: 3336,
      area_type: 'County',
    },
  ],
  3903: [
    {
      index: 3903,
      name: 'Bon Homme County, South Dakota',
      population: 7062,
      area_type: 'County',
    },
  ],
  3904: [
    {
      index: 3904,
      name: 'Brule County, South Dakota',
      population: 5321,
      area_type: 'County',
    },
  ],
  3905: [
    {
      index: 3905,
      name: 'Buffalo County, South Dakota',
      population: 1861,
      area_type: 'County',
    },
  ],
  3906: [
    {
      index: 3906,
      name: 'Butte County, South Dakota',
      population: 10774,
      area_type: 'County',
    },
  ],
  3907: [
    {
      index: 3907,
      name: 'Campbell County, South Dakota',
      population: 1349,
      area_type: 'County',
    },
  ],
  3908: [
    {
      index: 3908,
      name: 'Charles Mix County, South Dakota',
      population: 9213,
      area_type: 'County',
    },
  ],
  3909: [
    {
      index: 3909,
      name: 'Clark County, South Dakota',
      population: 3912,
      area_type: 'County',
    },
  ],
  3910: [
    {
      index: 3910,
      name: 'Corson County, South Dakota',
      population: 3826,
      area_type: 'County',
    },
  ],
  3911: [
    {
      index: 3911,
      name: 'Custer County, South Dakota',
      population: 9006,
      area_type: 'County',
    },
  ],
  3912: [
    {
      index: 3912,
      name: 'Day County, South Dakota',
      population: 5479,
      area_type: 'County',
    },
  ],
  3913: [
    {
      index: 3913,
      name: 'Deuel County, South Dakota',
      population: 4352,
      area_type: 'County',
    },
  ],
  3914: [
    {
      index: 3914,
      name: 'Dewey County, South Dakota',
      population: 5140,
      area_type: 'County',
    },
  ],
  3915: [
    {
      index: 3915,
      name: 'Douglas County, South Dakota',
      population: 2776,
      area_type: 'County',
    },
  ],
  3916: [
    {
      index: 3916,
      name: 'Fall River County, South Dakota',
      population: 7370,
      area_type: 'County',
    },
  ],
  3917: [
    {
      index: 3917,
      name: 'Faulk County, South Dakota',
      population: 2126,
      area_type: 'County',
    },
  ],
  3918: [
    {
      index: 3918,
      name: 'Grant County, South Dakota',
      population: 7463,
      area_type: 'County',
    },
  ],
  3919: [
    {
      index: 3919,
      name: 'Gregory County, South Dakota',
      population: 3962,
      area_type: 'County',
    },
  ],
  3920: [
    {
      index: 3920,
      name: 'Haakon County, South Dakota',
      population: 1826,
      area_type: 'County',
    },
  ],
  3921: [
    {
      index: 3921,
      name: 'Hand County, South Dakota',
      population: 3140,
      area_type: 'County',
    },
  ],
  3922: [
    {
      index: 3922,
      name: 'Harding County, South Dakota',
      population: 1330,
      area_type: 'County',
    },
  ],
  3923: [
    {
      index: 3923,
      name: 'Hutchinson County, South Dakota',
      population: 7368,
      area_type: 'County',
    },
  ],
  3924: [
    {
      index: 3924,
      name: 'Hyde County, South Dakota',
      population: 1184,
      area_type: 'County',
    },
  ],
  3925: [
    {
      index: 3925,
      name: 'Jackson County, South Dakota',
      population: 2821,
      area_type: 'County',
    },
  ],
  3926: [
    {
      index: 3926,
      name: 'Jones County, South Dakota',
      population: 884,
      area_type: 'County',
    },
  ],
  3927: [
    {
      index: 3927,
      name: 'Kingsbury County, South Dakota',
      population: 5294,
      area_type: 'County',
    },
  ],
  3928: [
    {
      index: 3928,
      name: 'Lake County, South Dakota',
      population: 10972,
      area_type: 'County',
    },
  ],
  3929: [
    {
      index: 3929,
      name: 'Lyman County, South Dakota',
      population: 3692,
      area_type: 'County',
    },
  ],
  3930: [
    {
      index: 3930,
      name: 'McPherson County, South Dakota',
      population: 2395,
      area_type: 'County',
    },
  ],
  3931: [
    {
      index: 3931,
      name: 'Marshall County, South Dakota',
      population: 4374,
      area_type: 'County',
    },
  ],
  3932: [
    {
      index: 3932,
      name: 'Mellette County, South Dakota',
      population: 1892,
      area_type: 'County',
    },
  ],
  3933: [
    {
      index: 3933,
      name: 'Miner County, South Dakota',
      population: 2304,
      area_type: 'County',
    },
  ],
  3934: [
    {
      index: 3934,
      name: 'Moody County, South Dakota',
      population: 6349,
      area_type: 'County',
    },
  ],
  3935: [
    {
      index: 3935,
      name: 'Oglala Lakota County, South Dakota',
      population: 13519,
      area_type: 'County',
    },
  ],
  3936: [
    {
      index: 3936,
      name: 'Perkins County, South Dakota',
      population: 2804,
      area_type: 'County',
    },
  ],
  3937: [
    {
      index: 3937,
      name: 'Potter County, South Dakota',
      population: 2438,
      area_type: 'County',
    },
  ],
  3938: [
    {
      index: 3938,
      name: 'Roberts County, South Dakota',
      population: 10163,
      area_type: 'County',
    },
  ],
  3939: [
    {
      index: 3939,
      name: 'Sanborn County, South Dakota',
      population: 2415,
      area_type: 'County',
    },
  ],
  3940: [
    {
      index: 3940,
      name: 'Spink County, South Dakota',
      population: 6235,
      area_type: 'County',
    },
  ],
  3941: [
    {
      index: 3941,
      name: 'Sully County, South Dakota',
      population: 1471,
      area_type: 'County',
    },
  ],
  3942: [
    {
      index: 3942,
      name: 'Todd County, South Dakota',
      population: 9220,
      area_type: 'County',
    },
  ],
  3943: [
    {
      index: 3943,
      name: 'Tripp County, South Dakota',
      population: 5565,
      area_type: 'County',
    },
  ],
  3944: [
    {
      index: 3944,
      name: 'Walworth County, South Dakota',
      population: 5265,
      area_type: 'County',
    },
  ],
  3945: [
    {
      index: 3945,
      name: 'Ziebach County, South Dakota',
      population: 2395,
      area_type: 'County',
    },
  ],
  3946: [
    {
      index: 3946,
      name: 'Benton County, Tennessee',
      population: 16002,
      area_type: 'County',
    },
  ],
  3947: [
    {
      index: 3947,
      name: 'Bledsoe County, Tennessee',
      population: 14798,
      area_type: 'County',
    },
  ],
  3948: [
    {
      index: 3948,
      name: 'Carroll County, Tennessee',
      population: 28458,
      area_type: 'County',
    },
  ],
  3949: [
    {
      index: 3949,
      name: 'Claiborne County, Tennessee',
      population: 32431,
      area_type: 'County',
    },
  ],
  3950: [
    {
      index: 3950,
      name: 'Clay County, Tennessee',
      population: 7620,
      area_type: 'County',
    },
  ],
  3951: [
    {
      index: 3951,
      name: 'Decatur County, Tennessee',
      population: 11564,
      area_type: 'County',
    },
  ],
  3952: [
    {
      index: 3952,
      name: 'DeKalb County, Tennessee',
      population: 21003,
      area_type: 'County',
    },
  ],
  3953: [
    {
      index: 3953,
      name: 'Fentress County, Tennessee',
      population: 19332,
      area_type: 'County',
    },
  ],
  3954: [
    {
      index: 3954,
      name: 'Giles County, Tennessee',
      population: 30554,
      area_type: 'County',
    },
  ],
  3955: [
    {
      index: 3955,
      name: 'Grundy County, Tennessee',
      population: 13783,
      area_type: 'County',
    },
  ],
  3956: [
    {
      index: 3956,
      name: 'Hancock County, Tennessee',
      population: 6845,
      area_type: 'County',
    },
  ],
  3957: [
    {
      index: 3957,
      name: 'Hardeman County, Tennessee',
      population: 25529,
      area_type: 'County',
    },
  ],
  3958: [
    {
      index: 3958,
      name: 'Hardin County, Tennessee',
      population: 27077,
      area_type: 'County',
    },
  ],
  3959: [
    {
      index: 3959,
      name: 'Henderson County, Tennessee',
      population: 27929,
      area_type: 'County',
    },
  ],
  3960: [
    {
      index: 3960,
      name: 'Hickman County, Tennessee',
      population: 25455,
      area_type: 'County',
    },
  ],
  3961: [
    {
      index: 3961,
      name: 'Houston County, Tennessee',
      population: 8219,
      area_type: 'County',
    },
  ],
  3962: [
    {
      index: 3962,
      name: 'Humphreys County, Tennessee',
      population: 19106,
      area_type: 'County',
    },
  ],
  3963: [
    {
      index: 3963,
      name: 'Johnson County, Tennessee',
      population: 18086,
      area_type: 'County',
    },
  ],
  3964: [
    {
      index: 3964,
      name: 'Lake County, Tennessee',
      population: 6507,
      area_type: 'County',
    },
  ],
  3965: [
    {
      index: 3965,
      name: 'Lauderdale County, Tennessee',
      population: 24793,
      area_type: 'County',
    },
  ],
  3966: [
    {
      index: 3966,
      name: 'Lewis County, Tennessee',
      population: 12957,
      area_type: 'County',
    },
  ],
  3967: [
    {
      index: 3967,
      name: 'Lincoln County, Tennessee',
      population: 36004,
      area_type: 'County',
    },
  ],
  3968: [
    {
      index: 3968,
      name: 'McNairy County, Tennessee',
      population: 25988,
      area_type: 'County',
    },
  ],
  3969: [
    {
      index: 3969,
      name: 'Meigs County, Tennessee',
      population: 13272,
      area_type: 'County',
    },
  ],
  3970: [
    {
      index: 3970,
      name: 'Monroe County, Tennessee',
      population: 47740,
      area_type: 'County',
    },
  ],
  3971: [
    {
      index: 3971,
      name: 'Perry County, Tennessee',
      population: 8685,
      area_type: 'County',
    },
  ],
  3972: [
    {
      index: 3972,
      name: 'Pickett County, Tennessee',
      population: 5107,
      area_type: 'County',
    },
  ],
  3973: [
    {
      index: 3973,
      name: 'Scott County, Tennessee',
      population: 22035,
      area_type: 'County',
    },
  ],
  3974: [
    {
      index: 3974,
      name: 'Van Buren County, Tennessee',
      population: 6429,
      area_type: 'County',
    },
  ],
  3975: [
    {
      index: 3975,
      name: 'Wayne County, Tennessee',
      population: 16308,
      area_type: 'County',
    },
  ],
  3976: [
    {
      index: 3976,
      name: 'White County, Tennessee',
      population: 28064,
      area_type: 'County',
    },
  ],
  3977: [
    {
      index: 3977,
      name: 'Bailey County, Texas',
      population: 6779,
      area_type: 'County',
    },
  ],
  3978: [
    {
      index: 3978,
      name: 'Baylor County, Texas',
      population: 3466,
      area_type: 'County',
    },
  ],
  3979: [
    {
      index: 3979,
      name: 'Blanco County, Texas',
      population: 12418,
      area_type: 'County',
    },
  ],
  3980: [
    {
      index: 3980,
      name: 'Borden County, Texas',
      population: 585,
      area_type: 'County',
    },
  ],
  3981: [
    {
      index: 3981,
      name: 'Bosque County, Texas',
      population: 18697,
      area_type: 'County',
    },
  ],
  3982: [
    {
      index: 3982,
      name: 'Brewster County, Texas',
      population: 9343,
      area_type: 'County',
    },
  ],
  3983: [
    {
      index: 3983,
      name: 'Briscoe County, Texas',
      population: 1431,
      area_type: 'County',
    },
  ],
  3984: [
    {
      index: 3984,
      name: 'Brooks County, Texas',
      population: 6906,
      area_type: 'County',
    },
  ],
  3985: [
    {
      index: 3985,
      name: 'Burnet County, Texas',
      population: 52502,
      area_type: 'County',
    },
  ],
  3986: [
    {
      index: 3986,
      name: 'Cass County, Texas',
      population: 28539,
      area_type: 'County',
    },
  ],
  3987: [
    {
      index: 3987,
      name: 'Castro County, Texas',
      population: 7298,
      area_type: 'County',
    },
  ],
  3988: [
    {
      index: 3988,
      name: 'Childress County, Texas',
      population: 6809,
      area_type: 'County',
    },
  ],
  3989: [
    {
      index: 3989,
      name: 'Cochran County, Texas',
      population: 2526,
      area_type: 'County',
    },
  ],
  3990: [
    {
      index: 3990,
      name: 'Coke County, Texas',
      population: 3333,
      area_type: 'County',
    },
  ],
  3991: [
    {
      index: 3991,
      name: 'Coleman County, Texas',
      population: 7850,
      area_type: 'County',
    },
  ],
  3992: [
    {
      index: 3992,
      name: 'Collingsworth County, Texas',
      population: 2568,
      area_type: 'County',
    },
  ],
  3993: [
    {
      index: 3993,
      name: 'Colorado County, Texas',
      population: 20754,
      area_type: 'County',
    },
  ],
  3994: [
    {
      index: 3994,
      name: 'Comanche County, Texas',
      population: 13878,
      area_type: 'County',
    },
  ],
  3995: [
    {
      index: 3995,
      name: 'Concho County, Texas',
      population: 3340,
      area_type: 'County',
    },
  ],
  3996: [
    {
      index: 3996,
      name: 'Cottle County, Texas',
      population: 1307,
      area_type: 'County',
    },
  ],
  3997: [
    {
      index: 3997,
      name: 'Crane County, Texas',
      population: 4546,
      area_type: 'County',
    },
  ],
  3998: [
    {
      index: 3998,
      name: 'Crockett County, Texas',
      population: 2943,
      area_type: 'County',
    },
  ],
  3999: [
    {
      index: 3999,
      name: 'Culberson County, Texas',
      population: 2155,
      area_type: 'County',
    },
  ],
  4000: [
    {
      index: 4000,
      name: 'Dallam County, Texas',
      population: 7241,
      area_type: 'County',
    },
  ],
  4001: [
    {
      index: 4001,
      name: 'Delta County, Texas',
      population: 5406,
      area_type: 'County',
    },
  ],
  4002: [
    {
      index: 4002,
      name: 'DeWitt County, Texas',
      population: 19772,
      area_type: 'County',
    },
  ],
  4003: [
    {
      index: 4003,
      name: 'Dickens County, Texas',
      population: 1726,
      area_type: 'County',
    },
  ],
  4004: [
    {
      index: 4004,
      name: 'Dimmit County, Texas',
      population: 8387,
      area_type: 'County',
    },
  ],
  4005: [
    {
      index: 4005,
      name: 'Donley County, Texas',
      population: 3339,
      area_type: 'County',
    },
  ],
  4006: [
    {
      index: 4006,
      name: 'Eastland County, Texas',
      population: 17944,
      area_type: 'County',
    },
  ],
  4007: [
    {
      index: 4007,
      name: 'Edwards County, Texas',
      population: 1422,
      area_type: 'County',
    },
  ],
  4008: [
    {
      index: 4008,
      name: 'Fayette County, Texas',
      population: 24913,
      area_type: 'County',
    },
  ],
  4009: [
    {
      index: 4009,
      name: 'Fisher County, Texas',
      population: 3622,
      area_type: 'County',
    },
  ],
  4010: [
    {
      index: 4010,
      name: 'Floyd County, Texas',
      population: 5235,
      area_type: 'County',
    },
  ],
  4011: [
    {
      index: 4011,
      name: 'Foard County, Texas',
      population: 1057,
      area_type: 'County',
    },
  ],
  4012: [
    {
      index: 4012,
      name: 'Franklin County, Texas',
      population: 10618,
      area_type: 'County',
    },
  ],
  4013: [
    {
      index: 4013,
      name: 'Freestone County, Texas',
      population: 19950,
      area_type: 'County',
    },
  ],
  4014: [
    {
      index: 4014,
      name: 'Gaines County, Texas',
      population: 22181,
      area_type: 'County',
    },
  ],
  4015: [
    {
      index: 4015,
      name: 'Garza County, Texas',
      population: 6262,
      area_type: 'County',
    },
  ],
  4016: [
    {
      index: 4016,
      name: 'Glasscock County, Texas',
      population: 1164,
      area_type: 'County',
    },
  ],
  4017: [
    {
      index: 4017,
      name: 'Gonzales County, Texas',
      population: 19832,
      area_type: 'County',
    },
  ],
  4018: [
    {
      index: 4018,
      name: 'Grimes County, Texas',
      population: 30754,
      area_type: 'County',
    },
  ],
  4019: [
    {
      index: 4019,
      name: 'Hall County, Texas',
      population: 2810,
      area_type: 'County',
    },
  ],
  4020: [
    {
      index: 4020,
      name: 'Hamilton County, Texas',
      population: 8298,
      area_type: 'County',
    },
  ],
  4021: [
    {
      index: 4021,
      name: 'Hansford County, Texas',
      population: 5151,
      area_type: 'County',
    },
  ],
  4022: [
    {
      index: 4022,
      name: 'Hardeman County, Texas',
      population: 3516,
      area_type: 'County',
    },
  ],
  4023: [
    {
      index: 4023,
      name: 'Hartley County, Texas',
      population: 5208,
      area_type: 'County',
    },
  ],
  4024: [
    {
      index: 4024,
      name: 'Haskell County, Texas',
      population: 5403,
      area_type: 'County',
    },
  ],
  4025: [
    {
      index: 4025,
      name: 'Hemphill County, Texas',
      population: 3217,
      area_type: 'County',
    },
  ],
  4026: [
    {
      index: 4026,
      name: 'Hill County, Texas',
      population: 37329,
      area_type: 'County',
    },
  ],
  4027: [
    {
      index: 4027,
      name: 'Houston County, Texas',
      population: 21950,
      area_type: 'County',
    },
  ],
  4028: [
    {
      index: 4028,
      name: 'Jack County, Texas',
      population: 8922,
      area_type: 'County',
    },
  ],
  4029: [
    {
      index: 4029,
      name: 'Jackson County, Texas',
      population: 15142,
      area_type: 'County',
    },
  ],
  4030: [
    {
      index: 4030,
      name: 'Jasper County, Texas',
      population: 32484,
      area_type: 'County',
    },
  ],
  4031: [
    {
      index: 4031,
      name: 'Jeff Davis County, Texas',
      population: 1903,
      area_type: 'County',
    },
  ],
  4032: [
    {
      index: 4032,
      name: 'Jim Hogg County, Texas',
      population: 4763,
      area_type: 'County',
    },
  ],
  4033: [
    {
      index: 4033,
      name: 'Karnes County, Texas',
      population: 14836,
      area_type: 'County',
    },
  ],
  4034: [
    {
      index: 4034,
      name: 'Kent County, Texas',
      population: 740,
      area_type: 'County',
    },
  ],
  4035: [
    {
      index: 4035,
      name: 'Kimble County, Texas',
      population: 4422,
      area_type: 'County',
    },
  ],
  4036: [
    {
      index: 4036,
      name: 'King County, Texas',
      population: 233,
      area_type: 'County',
    },
  ],
  4037: [
    {
      index: 4037,
      name: 'Kinney County, Texas',
      population: 3128,
      area_type: 'County',
    },
  ],
  4038: [
    {
      index: 4038,
      name: 'Knox County, Texas',
      population: 3273,
      area_type: 'County',
    },
  ],
  4039: [
    {
      index: 4039,
      name: 'Lamb County, Texas',
      population: 12724,
      area_type: 'County',
    },
  ],
  4040: [
    {
      index: 4040,
      name: 'La Salle County, Texas',
      population: 6604,
      area_type: 'County',
    },
  ],
  4041: [
    {
      index: 4041,
      name: 'Lavaca County, Texas',
      population: 20589,
      area_type: 'County',
    },
  ],
  4042: [
    {
      index: 4042,
      name: 'Lee County, Texas',
      population: 17954,
      area_type: 'County',
    },
  ],
  4043: [
    {
      index: 4043,
      name: 'Leon County, Texas',
      population: 16209,
      area_type: 'County',
    },
  ],
  4044: [
    {
      index: 4044,
      name: 'Limestone County, Texas',
      population: 22253,
      area_type: 'County',
    },
  ],
  4045: [
    {
      index: 4045,
      name: 'Lipscomb County, Texas',
      population: 2854,
      area_type: 'County',
    },
  ],
  4046: [
    {
      index: 4046,
      name: 'Live Oak County, Texas',
      population: 11428,
      area_type: 'County',
    },
  ],
  4047: [
    {
      index: 4047,
      name: 'Llano County, Texas',
      population: 22540,
      area_type: 'County',
    },
  ],
  4048: [
    {
      index: 4048,
      name: 'McCulloch County, Texas',
      population: 7497,
      area_type: 'County',
    },
  ],
  4049: [
    {
      index: 4049,
      name: 'McMullen County, Texas',
      population: 576,
      area_type: 'County',
    },
  ],
  4050: [
    {
      index: 4050,
      name: 'Madison County, Texas',
      population: 13661,
      area_type: 'County',
    },
  ],
  4051: [
    {
      index: 4051,
      name: 'Marion County, Texas',
      population: 9560,
      area_type: 'County',
    },
  ],
  4052: [
    {
      index: 4052,
      name: 'Mason County, Texas',
      population: 3982,
      area_type: 'County',
    },
  ],
  4053: [
    {
      index: 4053,
      name: 'Menard County, Texas',
      population: 1968,
      area_type: 'County',
    },
  ],
  4054: [
    {
      index: 4054,
      name: 'Milam County, Texas',
      population: 25628,
      area_type: 'County',
    },
  ],
  4055: [
    {
      index: 4055,
      name: 'Mills County, Texas',
      population: 4500,
      area_type: 'County',
    },
  ],
  4056: [
    {
      index: 4056,
      name: 'Mitchell County, Texas',
      population: 8943,
      area_type: 'County',
    },
  ],
  4057: [
    {
      index: 4057,
      name: 'Montague County, Texas',
      population: 21063,
      area_type: 'County',
    },
  ],
  4058: [
    {
      index: 4058,
      name: 'Morris County, Texas',
      population: 12083,
      area_type: 'County',
    },
  ],
  4059: [
    {
      index: 4059,
      name: 'Motley County, Texas',
      population: 1032,
      area_type: 'County',
    },
  ],
  4060: [
    {
      index: 4060,
      name: 'Newton County, Texas',
      population: 12052,
      area_type: 'County',
    },
  ],
  4061: [
    {
      index: 4061,
      name: 'Ochiltree County, Texas',
      population: 9606,
      area_type: 'County',
    },
  ],
  4062: [
    {
      index: 4062,
      name: 'Panola County, Texas',
      population: 22677,
      area_type: 'County',
    },
  ],
  4063: [
    {
      index: 4063,
      name: 'Parmer County, Texas',
      population: 9620,
      area_type: 'County',
    },
  ],
  4064: [
    {
      index: 4064,
      name: 'Pecos County, Texas',
      population: 14735,
      area_type: 'County',
    },
  ],
  4065: [
    {
      index: 4065,
      name: 'Polk County, Texas',
      population: 53255,
      area_type: 'County',
    },
  ],
  4066: [
    {
      index: 4066,
      name: 'Presidio County, Texas',
      population: 5939,
      area_type: 'County',
    },
  ],
  4067: [
    {
      index: 4067,
      name: 'Rains County, Texas',
      population: 12823,
      area_type: 'County',
    },
  ],
  4068: [
    {
      index: 4068,
      name: 'Reagan County, Texas',
      population: 3135,
      area_type: 'County',
    },
  ],
  4069: [
    {
      index: 4069,
      name: 'Real County, Texas',
      population: 2840,
      area_type: 'County',
    },
  ],
  4070: [
    {
      index: 4070,
      name: 'Red River County, Texas',
      population: 11542,
      area_type: 'County',
    },
  ],
  4071: [
    {
      index: 4071,
      name: 'Refugio County, Texas',
      population: 6632,
      area_type: 'County',
    },
  ],
  4072: [
    {
      index: 4072,
      name: 'Runnels County, Texas',
      population: 9859,
      area_type: 'County',
    },
  ],
  4073: [
    {
      index: 4073,
      name: 'Sabine County, Texas',
      population: 10048,
      area_type: 'County',
    },
  ],
  4074: [
    {
      index: 4074,
      name: 'San Augustine County, Texas',
      population: 7857,
      area_type: 'County',
    },
  ],
  4075: [
    {
      index: 4075,
      name: 'San Jacinto County, Texas',
      population: 28348,
      area_type: 'County',
    },
  ],
  4076: [
    {
      index: 4076,
      name: 'San Saba County, Texas',
      population: 5824,
      area_type: 'County',
    },
  ],
  4077: [
    {
      index: 4077,
      name: 'Schleicher County, Texas',
      population: 2357,
      area_type: 'County',
    },
  ],
  4078: [
    {
      index: 4078,
      name: 'Shackelford County, Texas',
      population: 3186,
      area_type: 'County',
    },
  ],
  4079: [
    {
      index: 4079,
      name: 'Shelby County, Texas',
      population: 24008,
      area_type: 'County',
    },
  ],
  4080: [
    {
      index: 4080,
      name: 'Sherman County, Texas',
      population: 2799,
      area_type: 'County',
    },
  ],
  4081: [
    {
      index: 4081,
      name: 'Somervell County, Texas',
      population: 9757,
      area_type: 'County',
    },
  ],
  4082: [
    {
      index: 4082,
      name: 'Stephens County, Texas',
      population: 9390,
      area_type: 'County',
    },
  ],
  4083: [
    {
      index: 4083,
      name: 'Stonewall County, Texas',
      population: 1182,
      area_type: 'County',
    },
  ],
  4084: [
    {
      index: 4084,
      name: 'Sutton County, Texas',
      population: 3217,
      area_type: 'County',
    },
  ],
  4085: [
    {
      index: 4085,
      name: 'Swisher County, Texas',
      population: 6881,
      area_type: 'County',
    },
  ],
  4086: [
    {
      index: 4086,
      name: 'Terrell County, Texas',
      population: 693,
      area_type: 'County',
    },
  ],
  4087: [
    {
      index: 4087,
      name: 'Terry County, Texas',
      population: 11567,
      area_type: 'County',
    },
  ],
  4088: [
    {
      index: 4088,
      name: 'Throckmorton County, Texas',
      population: 1550,
      area_type: 'County',
    },
  ],
  4089: [
    {
      index: 4089,
      name: 'Trinity County, Texas',
      population: 13996,
      area_type: 'County',
    },
  ],
  4090: [
    {
      index: 4090,
      name: 'Tyler County, Texas',
      population: 20030,
      area_type: 'County',
    },
  ],
  4091: [
    {
      index: 4091,
      name: 'Upton County, Texas',
      population: 3152,
      area_type: 'County',
    },
  ],
  4092: [
    {
      index: 4092,
      name: 'Van Zandt County, Texas',
      population: 62859,
      area_type: 'County',
    },
  ],
  4093: [
    {
      index: 4093,
      name: 'Ward County, Texas',
      population: 10964,
      area_type: 'County',
    },
  ],
  4094: [
    {
      index: 4094,
      name: 'Wheeler County, Texas',
      population: 4807,
      area_type: 'County',
    },
  ],
  4095: [
    {
      index: 4095,
      name: 'Winkler County, Texas',
      population: 7306,
      area_type: 'County',
    },
  ],
  4096: [
    {
      index: 4096,
      name: 'Wood County, Texas',
      population: 46857,
      area_type: 'County',
    },
  ],
  4097: [
    {
      index: 4097,
      name: 'Yoakum County, Texas',
      population: 7451,
      area_type: 'County',
    },
  ],
  4098: [
    {
      index: 4098,
      name: 'Young County, Texas',
      population: 17962,
      area_type: 'County',
    },
  ],
  4099: [
    {
      index: 4099,
      name: 'Zavala County, Texas',
      population: 9377,
      area_type: 'County',
    },
  ],
  4100: [
    {
      index: 4100,
      name: 'Beaver County, Utah',
      population: 7327,
      area_type: 'County',
    },
  ],
  4101: [
    {
      index: 4101,
      name: 'Daggett County, Utah',
      population: 1014,
      area_type: 'County',
    },
  ],
  4102: [
    {
      index: 4102,
      name: 'Duchesne County, Utah',
      population: 20161,
      area_type: 'County',
    },
  ],
  4103: [
    {
      index: 4103,
      name: 'Emery County, Utah',
      population: 10099,
      area_type: 'County',
    },
  ],
  4104: [
    {
      index: 4104,
      name: 'Garfield County, Utah',
      population: 5281,
      area_type: 'County',
    },
  ],
  4105: [
    {
      index: 4105,
      name: 'Grand County, Utah',
      population: 9769,
      area_type: 'County',
    },
  ],
  4106: [
    {
      index: 4106,
      name: 'Kane County, Utah',
      population: 8227,
      area_type: 'County',
    },
  ],
  4107: [
    {
      index: 4107,
      name: 'Millard County, Utah',
      population: 13330,
      area_type: 'County',
    },
  ],
  4108: [
    {
      index: 4108,
      name: 'Piute County, Utah',
      population: 1487,
      area_type: 'County',
    },
  ],
  4109: [
    {
      index: 4109,
      name: 'Rich County, Utah',
      population: 2628,
      area_type: 'County',
    },
  ],
  4110: [
    {
      index: 4110,
      name: 'San Juan County, Utah',
      population: 14359,
      area_type: 'County',
    },
  ],
  4111: [
    {
      index: 4111,
      name: 'Sanpete County, Utah',
      population: 29724,
      area_type: 'County',
    },
  ],
  4112: [
    {
      index: 4112,
      name: 'Sevier County, Utah',
      population: 22069,
      area_type: 'County',
    },
  ],
  4113: [
    {
      index: 4113,
      name: 'Wayne County, Utah',
      population: 2645,
      area_type: 'County',
    },
  ],
  4114: [
    {
      index: 4114,
      name: 'Addison County, Vermont',
      population: 37578,
      area_type: 'County',
    },
  ],
  4115: [
    {
      index: 4115,
      name: 'Caledonia County, Vermont',
      population: 30579,
      area_type: 'County',
    },
  ],
  4116: [
    {
      index: 4116,
      name: 'Essex County, Vermont',
      population: 5994,
      area_type: 'County',
    },
  ],
  4117: [
    {
      index: 4117,
      name: 'Lamoille County, Vermont',
      population: 26090,
      area_type: 'County',
    },
  ],
  4118: [
    {
      index: 4118,
      name: 'Orleans County, Vermont',
      population: 27666,
      area_type: 'County',
    },
  ],
  4119: [
    {
      index: 4119,
      name: 'Windham County, Vermont',
      population: 45842,
      area_type: 'County',
    },
  ],
  4120: [
    {
      index: 4120,
      name: 'Accomack County, Virginia',
      population: 33191,
      area_type: 'County',
    },
  ],
  4121: [
    {
      index: 4121,
      name: 'Alleghany County, Virginia',
      population: 14835,
      area_type: 'County',
    },
  ],
  4122: [
    {
      index: 4122,
      name: 'Bath County, Virginia',
      population: 4049,
      area_type: 'County',
    },
  ],
  4123: [
    {
      index: 4123,
      name: 'Brunswick County, Virginia',
      population: 15921,
      area_type: 'County',
    },
  ],
  4124: [
    {
      index: 4124,
      name: 'Buchanan County, Virginia',
      population: 19352,
      area_type: 'County',
    },
  ],
  4125: [
    {
      index: 4125,
      name: 'Buckingham County, Virginia',
      population: 16982,
      area_type: 'County',
    },
  ],
  4126: [
    {
      index: 4126,
      name: 'Caroline County, Virginia',
      population: 31957,
      area_type: 'County',
    },
  ],
  4127: [
    {
      index: 4127,
      name: 'Carroll County, Virginia',
      population: 29147,
      area_type: 'County',
    },
  ],
  4128: [
    {
      index: 4128,
      name: 'Charlotte County, Virginia',
      population: 11475,
      area_type: 'County',
    },
  ],
  4129: [
    {
      index: 4129,
      name: 'Cumberland County, Virginia',
      population: 9746,
      area_type: 'County',
    },
  ],
  4130: [
    {
      index: 4130,
      name: 'Dickenson County, Virginia',
      population: 13725,
      area_type: 'County',
    },
  ],
  4131: [
    {
      index: 4131,
      name: 'Essex County, Virginia',
      population: 10630,
      area_type: 'County',
    },
  ],
  4132: [
    {
      index: 4132,
      name: 'Floyd County, Virginia',
      population: 15619,
      area_type: 'County',
    },
  ],
  4133: [
    {
      index: 4133,
      name: 'Grayson County, Virginia',
      population: 15343,
      area_type: 'County',
    },
  ],
  4134: [
    {
      index: 4134,
      name: 'Greensville County, Virginia',
      population: 11226,
      area_type: 'County',
    },
  ],
  4135: [
    {
      index: 4135,
      name: 'Halifax County, Virginia',
      population: 33644,
      area_type: 'County',
    },
  ],
  4136: [
    {
      index: 4136,
      name: 'Highland County, Virginia',
      population: 2301,
      area_type: 'County',
    },
  ],
  4137: [
    {
      index: 4137,
      name: 'King George County, Virginia',
      population: 27856,
      area_type: 'County',
    },
  ],
  4138: [
    {
      index: 4138,
      name: 'Lancaster County, Virginia',
      population: 10750,
      area_type: 'County',
    },
  ],
  4139: [
    {
      index: 4139,
      name: 'Lee County, Virginia',
      population: 21982,
      area_type: 'County',
    },
  ],
  4140: [
    {
      index: 4140,
      name: 'Louisa County, Virginia',
      population: 40116,
      area_type: 'County',
    },
  ],
  4141: [
    {
      index: 4141,
      name: 'Lunenburg County, Virginia',
      population: 12031,
      area_type: 'County',
    },
  ],
  4142: [
    {
      index: 4142,
      name: 'Mecklenburg County, Virginia',
      population: 30508,
      area_type: 'County',
    },
  ],
  4143: [
    {
      index: 4143,
      name: 'Middlesex County, Virginia',
      population: 10943,
      area_type: 'County',
    },
  ],
  4144: [
    {
      index: 4144,
      name: 'Northampton County, Virginia',
      population: 11900,
      area_type: 'County',
    },
  ],
  4145: [
    {
      index: 4145,
      name: 'Northumberland County, Virginia',
      population: 12302,
      area_type: 'County',
    },
  ],
  4146: [
    {
      index: 4146,
      name: 'Nottoway County, Virginia',
      population: 15559,
      area_type: 'County',
    },
  ],
  4147: [
    {
      index: 4147,
      name: 'Orange County, Virginia',
      population: 37991,
      area_type: 'County',
    },
  ],
  4148: [
    {
      index: 4148,
      name: 'Page County, Virginia',
      population: 23750,
      area_type: 'County',
    },
  ],
  4149: [
    {
      index: 4149,
      name: 'Patrick County, Virginia',
      population: 17643,
      area_type: 'County',
    },
  ],
  4150: [
    {
      index: 4150,
      name: 'Prince Edward County, Virginia',
      population: 21927,
      area_type: 'County',
    },
  ],
  4151: [
    {
      index: 4151,
      name: 'Richmond County, Virginia',
      population: 9080,
      area_type: 'County',
    },
  ],
  4152: [
    {
      index: 4152,
      name: 'Rockbridge County, Virginia',
      population: 22593,
      area_type: 'County',
    },
  ],
  4153: [
    {
      index: 4153,
      name: 'Russell County, Virginia',
      population: 25448,
      area_type: 'County',
    },
  ],
  4154: [
    {
      index: 4154,
      name: 'Shenandoah County, Virginia',
      population: 44968,
      area_type: 'County',
    },
  ],
  4155: [
    {
      index: 4155,
      name: 'Smyth County, Virginia',
      population: 29449,
      area_type: 'County',
    },
  ],
  4156: [
    {
      index: 4156,
      name: 'Surry County, Virginia',
      population: 6527,
      area_type: 'County',
    },
  ],
  4157: [
    {
      index: 4157,
      name: 'Westmoreland County, Virginia',
      population: 18712,
      area_type: 'County',
    },
  ],
  4158: [
    {
      index: 4158,
      name: 'Wythe County, Virginia',
      population: 28111,
      area_type: 'County',
    },
  ],
  4159: [
    {
      index: 4159,
      name: 'Buena Vista city, Virginia',
      population: 6591,
      area_type: 'County',
    },
  ],
  4160: [
    {
      index: 4160,
      name: 'Covington city, Virginia',
      population: 5679,
      area_type: 'County',
    },
  ],
  4161: [
    {
      index: 4161,
      name: 'Emporia city, Virginia',
      population: 5481,
      area_type: 'County',
    },
  ],
  4162: [
    {
      index: 4162,
      name: 'Galax city, Virginia',
      population: 6730,
      area_type: 'County',
    },
  ],
  4163: [
    {
      index: 4163,
      name: 'Lexington city, Virginia',
      population: 7457,
      area_type: 'County',
    },
  ],
  4164: [
    {
      index: 4164,
      name: 'Columbia County, Washington',
      population: 4026,
      area_type: 'County',
    },
  ],
  4165: [
    {
      index: 4165,
      name: 'Ferry County, Washington',
      population: 7448,
      area_type: 'County',
    },
  ],
  4166: [
    {
      index: 4166,
      name: 'Garfield County, Washington',
      population: 2363,
      area_type: 'County',
    },
  ],
  4167: [
    {
      index: 4167,
      name: 'Jefferson County, Washington',
      population: 33589,
      area_type: 'County',
    },
  ],
  4168: [
    {
      index: 4168,
      name: 'Klickitat County, Washington',
      population: 23271,
      area_type: 'County',
    },
  ],
  4169: [
    {
      index: 4169,
      name: 'Lincoln County, Washington',
      population: 11601,
      area_type: 'County',
    },
  ],
  4170: [
    {
      index: 4170,
      name: 'Okanogan County, Washington',
      population: 43127,
      area_type: 'County',
    },
  ],
  4171: [
    {
      index: 4171,
      name: 'Pacific County, Washington',
      population: 24113,
      area_type: 'County',
    },
  ],
  4172: [
    {
      index: 4172,
      name: 'Pend Oreille County, Washington',
      population: 14179,
      area_type: 'County',
    },
  ],
  4173: [
    {
      index: 4173,
      name: 'San Juan County, Washington',
      population: 18662,
      area_type: 'County',
    },
  ],
  4174: [
    {
      index: 4174,
      name: 'Wahkiakum County, Washington',
      population: 4688,
      area_type: 'County',
    },
  ],
  4175: [
    {
      index: 4175,
      name: 'Barbour County, West Virginia',
      population: 15414,
      area_type: 'County',
    },
  ],
  4176: [
    {
      index: 4176,
      name: 'Braxton County, West Virginia',
      population: 12185,
      area_type: 'County',
    },
  ],
  4177: [
    {
      index: 4177,
      name: 'Calhoun County, West Virginia',
      population: 6068,
      area_type: 'County',
    },
  ],
  4178: [
    {
      index: 4178,
      name: 'Gilmer County, West Virginia',
      population: 7325,
      area_type: 'County',
    },
  ],
  4179: [
    {
      index: 4179,
      name: 'Grant County, West Virginia',
      population: 10968,
      area_type: 'County',
    },
  ],
  4180: [
    {
      index: 4180,
      name: 'Greenbrier County, West Virginia',
      population: 32435,
      area_type: 'County',
    },
  ],
  4181: [
    {
      index: 4181,
      name: 'Hardy County, West Virginia',
      population: 14192,
      area_type: 'County',
    },
  ],
  4182: [
    {
      index: 4182,
      name: 'Lewis County, West Virginia',
      population: 16767,
      area_type: 'County',
    },
  ],
  4183: [
    {
      index: 4183,
      name: 'McDowell County, West Virginia',
      population: 17850,
      area_type: 'County',
    },
  ],
  4184: [
    {
      index: 4184,
      name: 'Mingo County, West Virginia',
      population: 22573,
      area_type: 'County',
    },
  ],
  4185: [
    {
      index: 4185,
      name: 'Monroe County, West Virginia',
      population: 12296,
      area_type: 'County',
    },
  ],
  4186: [
    {
      index: 4186,
      name: 'Nicholas County, West Virginia',
      population: 24335,
      area_type: 'County',
    },
  ],
  4187: [
    {
      index: 4187,
      name: 'Pendleton County, West Virginia',
      population: 6011,
      area_type: 'County',
    },
  ],
  4188: [
    {
      index: 4188,
      name: 'Pleasants County, West Virginia',
      population: 7586,
      area_type: 'County',
    },
  ],
  4189: [
    {
      index: 4189,
      name: 'Pocahontas County, West Virginia',
      population: 7819,
      area_type: 'County',
    },
  ],
  4190: [
    {
      index: 4190,
      name: 'Ritchie County, West Virginia',
      population: 8207,
      area_type: 'County',
    },
  ],
  4191: [
    {
      index: 4191,
      name: 'Roane County, West Virginia',
      population: 13834,
      area_type: 'County',
    },
  ],
  4192: [
    {
      index: 4192,
      name: 'Summers County, West Virginia',
      population: 11762,
      area_type: 'County',
    },
  ],
  4193: [
    {
      index: 4193,
      name: 'Tucker County, West Virginia',
      population: 6568,
      area_type: 'County',
    },
  ],
  4194: [
    {
      index: 4194,
      name: 'Tyler County, West Virginia',
      population: 8183,
      area_type: 'County',
    },
  ],
  4195: [
    {
      index: 4195,
      name: 'Upshur County, West Virginia',
      population: 23712,
      area_type: 'County',
    },
  ],
  4196: [
    {
      index: 4196,
      name: 'Webster County, West Virginia',
      population: 8167,
      area_type: 'County',
    },
  ],
  4197: [
    {
      index: 4197,
      name: 'Wetzel County, West Virginia',
      population: 14025,
      area_type: 'County',
    },
  ],
  4198: [
    {
      index: 4198,
      name: 'Wyoming County, West Virginia',
      population: 20527,
      area_type: 'County',
    },
  ],
  4199: [
    {
      index: 4199,
      name: 'Adams County, Wisconsin',
      population: 21226,
      area_type: 'County',
    },
  ],
  4200: [
    {
      index: 4200,
      name: 'Ashland County, Wisconsin',
      population: 16039,
      area_type: 'County',
    },
  ],
  4201: [
    {
      index: 4201,
      name: 'Barron County, Wisconsin',
      population: 46843,
      area_type: 'County',
    },
  ],
  4202: [
    {
      index: 4202,
      name: 'Bayfield County, Wisconsin',
      population: 16608,
      area_type: 'County',
    },
  ],
  4203: [
    {
      index: 4203,
      name: 'Buffalo County, Wisconsin',
      population: 13391,
      area_type: 'County',
    },
  ],
  4204: [
    {
      index: 4204,
      name: 'Burnett County, Wisconsin',
      population: 17036,
      area_type: 'County',
    },
  ],
  4205: [
    {
      index: 4205,
      name: 'Clark County, Wisconsin',
      population: 34691,
      area_type: 'County',
    },
  ],
  4206: [
    {
      index: 4206,
      name: 'Crawford County, Wisconsin',
      population: 16007,
      area_type: 'County',
    },
  ],
  4207: [
    {
      index: 4207,
      name: 'Door County, Wisconsin',
      population: 30526,
      area_type: 'County',
    },
  ],
  4208: [
    {
      index: 4208,
      name: 'Forest County, Wisconsin',
      population: 9381,
      area_type: 'County',
    },
  ],
  4209: [
    {
      index: 4209,
      name: 'Green Lake County, Wisconsin',
      population: 19220,
      area_type: 'County',
    },
  ],
  4210: [
    {
      index: 4210,
      name: 'Iron County, Wisconsin',
      population: 6224,
      area_type: 'County',
    },
  ],
  4211: [
    {
      index: 4211,
      name: 'Jackson County, Wisconsin',
      population: 20836,
      area_type: 'County',
    },
  ],
  4212: [
    {
      index: 4212,
      name: 'Juneau County, Wisconsin',
      population: 26866,
      area_type: 'County',
    },
  ],
  4213: [
    {
      index: 4213,
      name: 'Lafayette County, Wisconsin',
      population: 16877,
      area_type: 'County',
    },
  ],
  4214: [
    {
      index: 4214,
      name: 'Langlade County, Wisconsin',
      population: 19559,
      area_type: 'County',
    },
  ],
  4215: [
    {
      index: 4215,
      name: 'Marquette County, Wisconsin',
      population: 15779,
      area_type: 'County',
    },
  ],
  4216: [
    {
      index: 4216,
      name: 'Monroe County, Wisconsin',
      population: 46109,
      area_type: 'County',
    },
  ],
  4217: [
    {
      index: 4217,
      name: 'Oneida County, Wisconsin',
      population: 38212,
      area_type: 'County',
    },
  ],
  4218: [
    {
      index: 4218,
      name: 'Pepin County, Wisconsin',
      population: 7410,
      area_type: 'County',
    },
  ],
  4219: [
    {
      index: 4219,
      name: 'Polk County, Wisconsin',
      population: 45709,
      area_type: 'County',
    },
  ],
  4220: [
    {
      index: 4220,
      name: 'Price County, Wisconsin',
      population: 14179,
      area_type: 'County',
    },
  ],
  4221: [
    {
      index: 4221,
      name: 'Richland County, Wisconsin',
      population: 17090,
      area_type: 'County',
    },
  ],
  4222: [
    {
      index: 4222,
      name: 'Rusk County, Wisconsin',
      population: 14186,
      area_type: 'County',
    },
  ],
  4223: [
    {
      index: 4223,
      name: 'Sawyer County, Wisconsin',
      population: 18559,
      area_type: 'County',
    },
  ],
  4224: [
    {
      index: 4224,
      name: 'Taylor County, Wisconsin',
      population: 19975,
      area_type: 'County',
    },
  ],
  4225: [
    {
      index: 4225,
      name: 'Trempealeau County, Wisconsin',
      population: 30899,
      area_type: 'County',
    },
  ],
  4226: [
    {
      index: 4226,
      name: 'Vernon County, Wisconsin',
      population: 31060,
      area_type: 'County',
    },
  ],
  4227: [
    {
      index: 4227,
      name: 'Vilas County, Wisconsin',
      population: 23763,
      area_type: 'County',
    },
  ],
  4228: [
    {
      index: 4228,
      name: 'Washburn County, Wisconsin',
      population: 16911,
      area_type: 'County',
    },
  ],
  4229: [
    {
      index: 4229,
      name: 'Waupaca County, Wisconsin',
      population: 51488,
      area_type: 'County',
    },
  ],
  4230: [
    {
      index: 4230,
      name: 'Waushara County, Wisconsin',
      population: 24999,
      area_type: 'County',
    },
  ],
  4231: [
    {
      index: 4231,
      name: 'Big Horn County, Wyoming',
      population: 11855,
      area_type: 'County',
    },
  ],
  4232: [
    {
      index: 4232,
      name: 'Carbon County, Wyoming',
      population: 14542,
      area_type: 'County',
    },
  ],
  4233: [
    {
      index: 4233,
      name: 'Converse County, Wyoming',
      population: 13786,
      area_type: 'County',
    },
  ],
  4234: [
    {
      index: 4234,
      name: 'Goshen County, Wyoming',
      population: 12562,
      area_type: 'County',
    },
  ],
  4235: [
    {
      index: 4235,
      name: 'Hot Springs County, Wyoming',
      population: 4588,
      area_type: 'County',
    },
  ],
  4236: [
    {
      index: 4236,
      name: 'Johnson County, Wyoming',
      population: 8730,
      area_type: 'County',
    },
  ],
  4237: [
    {
      index: 4237,
      name: 'Lincoln County, Wyoming',
      population: 20660,
      area_type: 'County',
    },
  ],
  4238: [
    {
      index: 4238,
      name: 'Niobrara County, Wyoming',
      population: 2380,
      area_type: 'County',
    },
  ],
  4239: [
    {
      index: 4239,
      name: 'Park County, Wyoming',
      population: 30518,
      area_type: 'County',
    },
  ],
  4240: [
    {
      index: 4240,
      name: 'Platte County, Wyoming',
      population: 8645,
      area_type: 'County',
    },
  ],
  4241: [
    {
      index: 4241,
      name: 'Sublette County, Wyoming',
      population: 8763,
      area_type: 'County',
    },
  ],
  4242: [
    {
      index: 4242,
      name: 'Washakie County, Wyoming',
      population: 7719,
      area_type: 'County',
    },
  ],
};