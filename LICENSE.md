<!-- SPDX-License-Identifier: Apache-2.0 -->

# Copyright info for US population distribution treemap

## `data.js`

To the extent possible under law, Christopher Phan has waived all copyright
and related or neighboring rights to `data.js`. This work is published from:
United States.

Full license: https://creativecommons.org/publicdomain/zero/1.0/

## `index.html`

`index.html` is licensed under a [Creative Commons Attribution 4.0
International License](https://creativecommons.org/licenses/by/4.0/).

## `reset.css`

`reset.css` was written by Eric Meyer, [who placed in the public
domain.](https://meyerweb.com/eric/tools/css/reset/)

## `treemap.svg`

`treemap.svg` is licensed under a [Creative Commons Attribution 4.0
International License](https://creativecommons.org/licenses/by/4.0/).

## All other files

Copyright 2023 Christopher Phan

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
