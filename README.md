# US population distribution

An interactive treemap showing the population of each population center in the United States.

- Hosted online here:
  [`https://chrisphan.com/US_population_distribution/`](https://chrisphan.com/US_population_distribution/)

- `treemap.svg` is a standalone (non-interactive) version.

- Available under open licenses. See `LICENSE.md` for more information.

- The code to produce the treemaps and other data is here:
  [`https://codeberg.org/christopherphan/US_pop_dist_data`](https://codeberg.org/christopherphan/US_pop_dist_data)
